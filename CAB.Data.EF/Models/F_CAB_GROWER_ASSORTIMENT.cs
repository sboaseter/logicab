﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAB.Data.EF.Models
{
    public partial class F_CAB_GROWER_ASSORTIMENT
    {
        public F_CAB_GROWER_ASSORTIMENT()
        {
            this.GRAS_CREATED_ON = DateTime.Now;
            this.GRAS_CREATED_BY = "LogiCAB";

            this.GRAS_MODIFIED_ON = DateTime.Now;
            this.GRAS_MODIFIED_BY = "LogiCAB";

            this.GRAS_START_DATE = DateTime.Now;
        }
    }
}
