﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAB.Data.EF.Models {
    public partial class F_CAB_ADDRESS : EntityObject {
        public override string ToString() {
            return String.Format("{0} {1}, {2} {3}**", this.ADDR_STREET, this.ADDR_NO, this.ADDR_ZIPCODE, this.ADDR_CITY);
        }
    }
}

