﻿using CAB.Domain.Repositories;
using System;
using System.ComponentModel.DataAnnotations;
namespace CAB.Domain.Models {
    [Serializable]
    public class Login {
        
        public int Sequence { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(50, ErrorMessage = "Must be under 50 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Password is required", AllowEmptyStrings = true)]
        [StringLength(50, ErrorMessage = "Must be under 50 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public int? Language { get; set; }
        public short Blocked { get; set; }
        public int LoginGroup { get; set; }
        public int Organisation { get; set; }
        public string OrganisationName { get; set; }
        public int Person { get; set; }

        public override string ToString() {
            return String.Format("Sequence: {0}, Name: {1}, Password: {2}, Language: {3}, Blocked: {4}", Sequence, Name, Password, Language, Blocked);
        }
        public string error { get; set; }


    }
}
