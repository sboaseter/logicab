﻿using System;

namespace CAB.Domain.Models {
    [Serializable]
    public class CabSupplier {
        public String Name { get; set; }
        public String Growername { get; set; }
        public int GrelSeq { get; set; }
    }
}
