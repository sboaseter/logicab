﻿using System;
using System.Collections.Generic;
namespace CAB.Domain.Models {
    [Serializable]
    public class CabCode {
        public int Seq { get; set; }
        public string Code { get; set; }
        public int? Picture { get; set; }

        public List<CabProperties> Properties { get; set; }
    }
}
