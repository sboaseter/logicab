﻿using System;
namespace CAB.Domain.Models {
    [Serializable]
    public class Setting {
        public int Sequence { get; set; }
        public string Id { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }

    }
}
