﻿using System;
namespace CAB.Domain.Models {
    [Serializable]
    public class Address {
        public int Sequence { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public string Street { get; set; }
        public string StreetNr { get; set; }
    }
}
