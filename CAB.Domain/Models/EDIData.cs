﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAB.Domain.Models {
    public class EDIData {

        /*
         *  F_CAB_CD_CAB_CODE, CABC_SEQUENCE
         */
        public decimal ArtCode { get; set; }

        /*
         * F_CAB_GROWER_CASK_MATRIX
         */
        public decimal APE { get; set; }
        public decimal TotalUnits { get; set; }
        public decimal UnitsPerCar { get; set; }

        public string strSortcode1 { get; set; }
        public string strSortcode2 { get; set; }
        public string strSortcode3 { get; set; }
        public string strSortcode4 { get; set; }
        public string strSortcode5 { get; set; }
        public string strSortcode6 { get; set; }
        public string strSortcode7 { get; set; }
        public string strSortcode8 { get; set; }
        public string strSortcode9 { get; set; }
        public string strSortcode10 { get; set; }

        public string strSortCodeZN1 { get; set; }
        public string strSortCodeZN2 { get; set; }
        public string strSortCodeZN3 { get; set; }
        public string strSortCodeZN4 { get; set; }
        public string strSortCodeZN5 { get; set; }
        public string strSortCodeZN6 { get; set; }
        public string strSortCodeZN7 { get; set; }
        public string strSortCodeZN8 { get; set; }
        public string strSortCodeZN9 { get; set; }
        public string strSortCodeZN10 { get; set; }


        /*
         * F_CAB_CD_CAB_CODE -> F_CAB_CAB_DETAIL
         */
        public string strQuality { get; set; }
        public string strMaturity { get; set; }
        public decimal EmbCode { get; set; }

        /*
         * F_CAB_OFFER_DETAILS/F_CAB_OFFER_HEADERS?
         */
        public string strPartySize { get; set; }

        public string strVet1Code { get; set; }
        public string strVet2Code { get; set; }

        public string strSellPrice { get; set; }

        public decimal lngPTYCount { get; set; }


        public decimal AuinSeq { get; set; }
        public decimal AUSESeq { get; set; }

        public string ALNumber { get; set; }
        public string ALNumberFirst { get; set; }


        public decimal lngMaxParty { get; set; }
        public DateTime dtTransport { get; set; }
        public DateTime dtAuction { get; set; }
        public string AuctionGroupCode { get; set; }
        public string CarNumber { get; set; }
        public bool IsSelfInspect { get; set; }


        public string CDPPCode { get; set; }
        public decimal NumOfCars { get; set; }

        public decimal lngLineSequence { get; set; }

        public string IRNNumber { get; set; }
    }
}
