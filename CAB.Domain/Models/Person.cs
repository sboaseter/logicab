﻿using System;

namespace CAB.Domain.Models {
    [Serializable]
    public class Person {
        public int Sequence { get; set; }
        public int Organisation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public decimal? ExtRef { get; set; }
        public DateTime CreatedOn { get; set; }

        public string FullName {
            get {
                return String.Format("{0} {1} {2}", this.FirstName, this.MiddleName, this.LastName);
            }
        }
    }
}
