﻿using System;
namespace CAB.Domain.Models {
    [Serializable]
    public class Orad {
        public int Sequence { get; set; }
        public int Organisation { get; set; }
        public int Status { get; set; }
        public int Type { get; set; }
    }
}
