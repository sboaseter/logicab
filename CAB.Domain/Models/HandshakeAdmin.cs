﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAB.Domain.Models {

    [Serializable]
    public class HandshakeAdmin {
        public int Sequence { get; set; }
        public string Mail { get; set; }
        public string Remark { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime? AcceptDate { get; set; }

        public bool Blocked { get; set; }

        public string Grower { get; set; }
        public int GrowerSequence { get; set; }

        public string Buyer { get; set; }
        public int BuyerSequence { get; set; }

        public HandshakeOption Options { get; set; }

    }
}
