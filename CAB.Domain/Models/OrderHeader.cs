﻿using CAB.Domain.Enums;
using System;
using System.Linq;
namespace CAB.Domain.Models {
    [Serializable]
    public class OrderHeader {
        public int id { get; set; }
        public int Status { get; set; }
        public IQueryable<OrderDetail> Details { get; set; }
    }
    [Serializable]
    public class OrderDetail {
        public int id { get; set; }
        public int OrderHeader { get; set; }
    }
}
