﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAB.Domain.Models {
    public class GMRecord {
        public int Seq { get; set; }
        public string Desc { get; set; }
        public int Num { get; set; }
        public int Rest { get; set; }
        public int Pic { get; set; }
        public string Quality { get; set; }
        public string APE { get; set; }
        public string Cask { get; set; }
        public int InUse { get; set; }
    }
}
