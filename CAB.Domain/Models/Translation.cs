﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAB.Domain.Models {
    public class Translation {
        public int Sequence { get; set; }
        public int LanguageSeq { get; set; }
        public string Language { get; set; }
        public string Name { get; set; }
        public string TranslationText { get; set; }
    }
}
