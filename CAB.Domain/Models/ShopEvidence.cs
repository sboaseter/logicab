﻿using System;
namespace CAB.Domain.Models {
    [Serializable]
    public class ShopEvidence {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
