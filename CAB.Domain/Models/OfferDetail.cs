﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAB.Domain.Helpers;

namespace CAB.Domain.Models {
    [Serializable]
    public class OfferDetail {

        public string Display {
            get {
                var layout = Utils.GetSetting("Shop", "Display_Article");
                if (layout == "GroupDesc") {
                    return String.Format("{0} {1}", Properties.Group, this.Properties.Desc);
                } else if (layout == "GroupTypeDesc") {
                    return String.Format("{0} {1}", Properties.Grouptype, this.Properties.Desc);
                }
                return this.Properties.Desc;
            }
        }
        public int? Buyer { get; set; }
        public int Sequence { get; set; }
        public int OFHDSequence { get; set; }
        public string Offertype { get; set; }
        public int CabCode { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public DateTime LatestOrderDateTime { get; set; }
        public DateTime DeliverTimeEnd { get; set; }

        public CabSupplier Supplier { get; set; }
        public bool IsSpecial { get; set; }
        public DateTime ArrivalDate { get; set; }
        public bool InTransit {
            get {
                return ArrivalDate > DateTime.Now;

            }
        }

        public string Caption { get; set; }

        public int? _PictureSequence { get; set; }
        public string _PictureString { get; set; }
        public string Picture {
            get {
                return !String.IsNullOrEmpty(_PictureString) 
                    ? _PictureString :
                        (_PictureSequence != null 
                        ? String.Format("~/Picture/ShowImage/{0}", _PictureSequence) : 
                            String.Format("~/Picture/ShowImageCAB/{0}", CabCode));
            }
        }

        public decimal Price { get; set; }
//        public IEnumerable<Staffel> PriceStaffel { get; set; }
        public int Available { get; set; }

        public CabProperties Properties { get; set; }

        public decimal UnitsPrLayer { get; set; }
        public decimal NumOfUnits { get; set; }
        public decimal LayersPrTrolley { get; set; }
        public string CaskCode { get; set; }

        public string Remark { get; set; }
        public string OfferNr { get; set; }

        public string SearchString {
            get {
                return this.Properties.ToString();
            }
        }
    }
}
