﻿using System;
namespace CAB.Domain.Models {
    [Serializable]
    public class Handshake {
        public int Sequence { get; set; }
        public int OrganisationSequenceGrower { get; set; }
        public int? OrganisationSequenceBuyer { get; set; }
        public int Type { get; set; }
        public DateTime? Request { get; set; }
        public DateTime? Accept { get; set; }
        public DateTime? Approve { get; set; }
        public bool Blocked { get; set; }
        public string Email { get; set; }
        public string Remark { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
