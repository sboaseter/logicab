﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAB.Domain.Models {
    public class EDIHeader {
        public decimal UNH_ID { get; set; }
        public decimal UNCode { get; set; }
        public string AuctionCode { get; set; }
        public decimal SenderId { get; set; } // Debt.nummer LF: CINF_AUCTION_DEBTOR_NO, CAB: ORGA_VBA_NUMMER (Needs to be changed to general debt.nummer pr auction)
        public decimal lngUNHCount { get; set; }

        public string strVersion { get; set; }
        public string strRelease { get; set; }
        public bool IsTest { get; set; }
        public bool RequestEABResponse { get; set; }
        public bool IsGP { get; set; }
        public decimal lngNumOfPlates { get; set; }
        public decimal lngSegmentCount { get; set; }
    }
}
