﻿using System;
using System.Collections.Generic;

namespace CAB.Domain.Models {
    [Serializable]
    public class CabProperties {

        private string _Maingroup { get; set; }
        public string Maingroup {
            get {
                return _Maingroup;
            }
            set {
                _Maingroup = value;
                Setprop("Maingroup", value);
            }
        }
        public int? MaingroupSeq { get; set; }

        private string _Group { get; set; }
        public string Group {
            get {
                return _Group;
            }
            set {
                _Group = value;
                Setprop("Group", value);
            }
        }

        private string _Grouptype { get; set; }
        public string Grouptype {
            get {
                return _Grouptype;
            }
            set {
                _Grouptype = value;
                Setprop("Grouptype", value);
            }
        }
        public string CABCode { get; set; }

        private Dictionary<string, string> DProps = new Dictionary<string, string>();

        private string _Value1;
        public string Value1 {
            get {
                return _Value1;
            }
            set {
                if (value.IndexOf("cm") > 0) {
                    _Value1 = value.Replace(" cm", "");
                    Setprop("Value1", _Value1);
                } else {
                    _Value1 = value;
                    Setprop("Value1", _Value1);
                }
            }
        }

        private string _Value2;
        public string Value2 {
            get {
                return _Value2;
            }
            set {
                if (value.IndexOf("cm") > 0) {
                    _Value2 = value.Replace(" cm", "");
                    Setprop("Value2", _Value2);
                } else {
                    _Value2 = value;
                    Setprop("Value2", _Value2);
                }
            
            }
        }
        private string _Value3;
        public string Value3 {
            get {
                return _Value3;
            }
            set {
                _Value3 = value;
                Setprop("Value3", _Value3);
            }
        }
        private string _Value4;
        public string Value4 {
            get {
                return _Value4;
            }
            set {
                _Value4 = value;
                Setprop("Value4", _Value4);
            }
        }
        private string _Value5;
        public string Value5 {
            get {
                return _Value5;
            }
            set {
                _Value5 = value;
                Setprop("Value5", _Value5);
            }
        }

        private string _Value6;
        public string Value6 {
            get {
                return _Value6;
            }
            set {
                if (value.IndexOf("gr") > 0) {
                    _Value6 = value.Replace(" gr", "");
                } else {
                    _Value6 = value;
                    Setprop("Value6", _Value6);
                }
            }
        }
        // Pottype
        private string _Value7;
        public string Value7 {
            get {
                return _Value7;
            }
            set {
                _Value7 = value;
                Setprop("Value7", _Value7);
            }
        }
        // Potsize
        private string _Value8;
        public string Value8 {
            get {
                return _Value8;
            }
            set {
                _Value8 = value;
                Setprop("Value8", _Value8);
            }
        }

        public string Desc { get; set; }

        public override string ToString() {
            string result = "";
            if (!String.IsNullOrEmpty(Value1))
                result += Value1 + " cm";
            if (!String.IsNullOrEmpty(Value2))
                result += ", " + Value2;
            if (!String.IsNullOrEmpty(Value3))
                result += ", " + Value3;
            if (!String.IsNullOrEmpty(Value4))
                result += ", " + Value4;
            if (!String.IsNullOrEmpty(Value5))
                result += ", " + Value5;
            if (!String.IsNullOrEmpty(Value6))
                result += ", " + Value6 + " gr";
            if (!String.IsNullOrEmpty(Value7))
                result += ", " + Value7;
            if (!String.IsNullOrEmpty(Value8))
                result += ", " + Value8;
            return result;
        }
        public void Setprop(string key, string value) {
            if (DProps.ContainsKey(key)) {
                DProps[key] = value;
            } else {
                DProps.Add(key, value);
            }
        }

        public string Getprop(string key) {
            string result = null;

            if (DProps.ContainsKey(key)) {
                result = DProps[key];
            }

            return result;
        }
    }
}
