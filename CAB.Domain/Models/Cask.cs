﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAB.Domain.Models {
    public class Cask {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Desc { get; set; }
        public int Transport { get; set; }
    }
}
