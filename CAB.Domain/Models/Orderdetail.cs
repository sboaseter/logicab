﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAB.Domain.Models {
    public class Orderdetail {
        public string CABCode { get; set; }
        public int CABSeq { get; set; }
        public string GroupDesc { get; set; }
        public string GrouptypeDesc { get; set; }
        public decimal NumOfItems { get; set; }
        public decimal NumOfItemsPrUnit { get; set; }
        public int PIC_SEQ { get; set; }
        public string PIC_URL { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public bool Sticker { get; set; }
        public string CustomerCode { get; set; }
        public decimal NumOfUnits { get; set; }
        public decimal NumOfItemsOrdered { get; set; }
        public int Status { get; set; }
        public int Seq { get; set; }
        public int OFDTSeq { get; set; }

        // CABProperties
        public string Desc { get; set; }
        public string Val1 { get; set; }
        public string Val2 { get; set; }
        public string Val3 { get; set; }
        public string Val4 { get; set; }
        public string Val5 { get; set; }
        public string Val6 { get; set; }
        //// CABProperties End

        // Cask info
        public string CaskCode { get; set; }
        public decimal CaskUnitsPrLayer { get; set; }
        public decimal CaskNumOfUnits { get; set; }
        public decimal CaskLayersPrTrolley { get; set; }
        //// Cask info end
    }
}
