﻿using CAB.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Web;
namespace CAB.Domain.Models {
    [Serializable]
    public class Organisation {
        public static string MapPath(string path) {
            if (HttpContext.Current != null)
                return HttpContext.Current.Server.MapPath(path);

            return HttpRuntime.AppDomainAppPath + path.Replace("~", string.Empty).Replace('/', '\\');
        }
        //        public Organisation(string orgaType) {
        //            this.OrganisationType = orgaType;
        //        }

        /* Core information */
        public int Sequence { get; set; }
        public string Name { get; set; }
        public string Type {
            get {
                switch (OrganisationType) {
                    case "GROW":
                        return OrgaType.Grower;
                    case "BUYR":
                        return OrgaType.Buyer;
                    case "COMM":
                        return OrgaType.Comm;
                    case "LOGP":
                        return OrgaType.Logp;
                    default:
                        return OrgaType.Grower;
                }
            }

        }
        public string OrganisationType { get; set; }

        public int Status { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string VAT { get; set; }
        public string Mailbox { get; set; }
        public string VBA { get; set; }
        public string BVH { get; set; }
        public string EAN { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Code { get; set; }
        public int? Picture { get; set; }
        public string PictureStr {
            get {
                if (this.Picture == null) return "~/Content/nopic.jpg";
                return String.Format("~/Image/ShowImage/{0}", this.Picture);
            }
        }
        public string Referer { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public Orad Orad { get; set; }
        public string OradStatus {
            get {
                switch (Orad.Status) {
                    case SubscriptionStatus.ACTIEF:
                        return "Actief";
                    case SubscriptionStatus.AFGEMELD:
                        return "Afgemeld";
                    case SubscriptionStatus.PROSPECT:
                        return "Prospect";
                    default:
                        return "Onbekend";
                }
            }
        }
        public string OradMember {
            get {
                switch (Orad.Type) {
                    case SubscriptionType.Bronze:
                        return "Bronze";
                    case SubscriptionType.Silver:
                        return "Zilver";
                    case SubscriptionType.Gold:
                        return "Goud";
                    default:
                        return "Geen";
                }
            }
        }

        public bool IsBuyer {
            get {
                return Type == OrgaType.Buyer;
            }
        }
        public bool IsGrower {
            get {
                return Type == OrgaType.Grower;
            }
        }
        public bool IsComm {
            get {
                return Type == OrgaType.Comm;
            }
        }
        public int GrowerSequence { get; set; }
        public int BuyerSequence { get; set; }
        public int TypeSequence {
            get {
                switch (this.Type) {
                    case OrgaType.Grower:
                        return this.GrowerSequence;
                    case OrgaType.Buyer:
                        return this.BuyerSequence;
                    case OrgaType.Comm:
                        return this.BuyerSequence;
                    case OrgaType.Logp:
                        return this.BuyerSequence;
                    default:
                        return -1;
                }
            }
        }

        /* Main Group Matrix */

        public IEnumerable<Maingroup> Maingroups { get; set; }
        public IEnumerable<Address> Addresses { get; set; }

    }
}
