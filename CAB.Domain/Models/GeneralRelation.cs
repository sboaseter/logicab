﻿using CAB.Domain.Repositories;
using System;
namespace CAB.Domain.Models {
    [Serializable]
    public class GeneralRelation {
        public int Sequence { get; set; }
        public string Name { get; set; }
        public int? Logo { get; set; }
        public string Web { get; set; }

        public override string ToString() {
            return Name;
        }
        public string ToFullString() {
            return String.Format("Sequence: {0}, Name: {1}, Logo: {2}, Web: {3}", Sequence, Name, Logo, Web);
        }
    }
}
