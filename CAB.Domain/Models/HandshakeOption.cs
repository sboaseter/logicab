﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAB.Domain.Models {
    [Serializable]
    public class HandshakeOption {
        public int Sequence { get; set; }
        public int Handshake { get; set; }
        public int? Address { get; set; }
        public int? Transporter { get; set; }
        public bool AutoConfirm { get; set; }
        public bool AutoEKT { get; set; }
        public bool AutoKWB { get; set; }

        public bool SendConfirmMail { get; set; }
        public bool ReceiveOfferMail { get; set; }
        public bool AutoPublishStock { get; set; }
        public bool AutoPublishOffer { get; set; }
        public DateTime LatestDeliveryTime { get; set; }
        public bool DeliveryStockAllWorkdays { get; set; }
        public bool DeliveryOfferAllWorkdays { get; set; }

    }
}
