﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CAB.Domain.Models {
    public class OrganisationFunction {
        public int Sequence { get; set; }
        public int FunctionSequence { get; set; }
        public string Code { get; set; }
        public string  Description { get; set; }
    }
}
