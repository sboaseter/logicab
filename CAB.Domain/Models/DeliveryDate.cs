﻿using System;
namespace CAB.Domain.Models {
    [Serializable]
    public class DeliveryDate {
        public int Organisation { get; set; }
        public int? OfferHeader { get; set; }
        public int Maingroup { get; set; }

        public string MaingroupStr {
            get {
                switch (this.Maingroup) {
                    case 1:
                        return "plants";
                    case 2:
                        return "flowers";
                    case 3:
                        return "fishes";
                    case 4:
                        return "misc";
                    case 5:
                        return "dryflowers";
                    default:
                        return "N/A";
                }
            }
        }
        public bool? Internal { get; set; }
        public string Grower { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public string OfferType { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DelDate { get; set; }
        public int NumDetails { get; set; }
    }
}
