﻿
namespace CAB.Domain.Enums {
    public static class OfferType {
        public const string Stock = "S";
        public const string Offer = "O";
    }
}
