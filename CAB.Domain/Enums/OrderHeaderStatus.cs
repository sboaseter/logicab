﻿
namespace CAB.Domain.Enums {
    public static class OrderHeaderStatus {
        public const int REQ = 1;
        public const int CONFIRMED = 2;
        public const int REJECTED = 3;
        public const int KWBSENT = 4;
        public const int EKTKWBSENT = 5;
        public const int NEW = 6;
        public const int CANCEL = 7;
        public const int CHANGEDORDER = 8;
        public const int CHANGEDBYGROWER = 9;
        public const int ORDERED = 10;
        public const int CHANGEDBYBUYER = 11;
        public const int SAVEDBYBUYER = 12;
        public const int SAVEDBYGROWER = 13;
        public const int EKTSENT = 14;
        public const int BUYERRECALL = 15;
    }
}
