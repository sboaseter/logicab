﻿namespace CAB.Domain.Enums {
    public static class SubscriptionType {
        public const int Bronze = 3;
        public const int Silver = 2;
        public const int Gold = 1;
        public const int None = 0;
    }
}
