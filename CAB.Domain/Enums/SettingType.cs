﻿
namespace CAB.Domain.Enums {
    public static class SettingType {
        public const int Shop = 0;
        public const int Organisation = 1;
        public const int Person = 2;
        public const int All = 3;
    }
}
