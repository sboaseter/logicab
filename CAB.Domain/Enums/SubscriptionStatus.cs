﻿namespace CAB.Domain.Enums {
    public static class SubscriptionStatus {
        public const int PROSPECT = 1;
        public const int ACTIEF = 3;
        public const int AFGEMELD = 4;
    }
}
