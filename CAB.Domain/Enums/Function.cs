﻿using System.ComponentModel;
namespace CAB.Domain.Enums {
    public enum Function {
		[Description("Vers van de Kweker Shop")]
		VVDK_SHOP = 4,

		[Description("Vers van de Kweker Beheerder")]
		VVDK_BEHEER = 5,

		[Description("Basis Functie voor de LogiOfferte Applicatie.")]
		LOGIOFFERTE = 6,

		[Description("Beheren van orders")]
		MANAGE_ORDERS = 7,

		[Description("Kopen van voorraad")]
		BUY_STOCK = 9,

		[Description("Kopen van aanbieding")]
		BUY_OFFER = 10,

		[Description("Wijzigen offerte")]
		CHANGE_OFFER = 13,

		[Description("Wijzigen prijzen")]
		CHANGE_PRICES = 15,

		[Description("Onderhoud internetshop KVK")]
		MANAGE_INTERNETSHOP = 17,

		[Description("Mag gebruik maken van de LogiCAB Services.")]
		LOGISERVICE = 18,

		[Description("Globaal beheer organisaties")]
		GLOBAAL_BEHEER = 19,

		[Description("Gebruiker van de klant van de klant.")]
		KVK_USER = 20,

		[Description("Basis functie voor stand alone shops.")]
		STAND_ALONE_SHOP = 21,

		[Description("Basis functie voor shop in combinatie met LF.")]
		SHOP_LF = 22,

		[Description("Basis functie voor onderhoud en gebruik subklanten")]
		MANAGE_SUB_CUSTOMERS = 23,

		[Description("Optie om de bepaalde processen automatisch uit te voeren")]
		AUTO_PROCESS = 24,

		[Description("Beheren van offertes")]
		MANAGE_OFFERS = 25,

		[Description("test")]
		LOGIOFFERTST = 26,

	}
}