﻿
namespace CAB.Domain.Enums {
    public static class OrderDetailStatus {
        public const int REQ = 1;
        public const int REJECTED = 2;
        public const int CANCEL = 3;
        public const int CHANGEDORDER = 4;
        public const int CHANGEDBYGROWER = 5;
        public const int ORDERED = 6;
        public const int CHANGEDBYBUYER = 7;
        public const int CONFIRMED = 8;
    }
}
