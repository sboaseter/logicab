﻿namespace CAB.Domain.Enums {
    public static class OrgaType {
        public const string Grower = "GROW";
        public const string Buyer = "BUYR";
        public const string Comm = "COMM";
        public const string Logp = "LOGP";
    }
}
