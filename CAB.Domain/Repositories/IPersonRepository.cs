﻿using CAB.Domain.Models;
using System.Linq;

namespace CAB.Domain.Repositories {
    public interface IPersonRepository {
        IQueryable<Person> List();
        Person Get(int Sequence);
    }
}
