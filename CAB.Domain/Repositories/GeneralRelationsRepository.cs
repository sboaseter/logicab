﻿using CAB.Data.EF.Models;
using CAB.Domain.Models;
using CAB.Domain.Providers;
using System.Collections.Generic;
using System.Linq;

namespace CAB.Domain.Repositories {
    public class GeneralRelationsRepository : IGeneralRelationsRepository {
        private static List<GeneralRelation> _generalRelationsCache;
        private IQueryable<GeneralRelation> _generalRelationsQuery;
        private Entities ctx;
        public GeneralRelationsRepository(
            IDataProvider dataProvider) {
            ctx = dataProvider.GetContext() as Entities;
            _generalRelationsQuery =
                from g in ctx.F_CAB_GENERAL_RELATIONS
                select new GeneralRelation {
                    Sequence = g.GREL_SEQUENCE,
                    Name = g.GREL_NAME,
                    Logo = g.GREL_FK_PICT_SEQ,
                    Web = g.GREL_WEBSITE_URL
                };
        }
        public GeneralRelationsRepository() {
            ctx = new DataProvider().GetContext() as Entities;
            _generalRelationsQuery =
                from g in ctx.F_CAB_GENERAL_RELATIONS
                select new GeneralRelation {
                    Sequence = g.GREL_SEQUENCE,
                    Name = g.GREL_NAME,
                    Logo = g.GREL_FK_PICT_SEQ,
                    Web = g.GREL_WEBSITE_URL
                };
        }

        public GeneralRelation Get(string name) {
            if (_generalRelationsCache == null) _generalRelationsCache = new List<GeneralRelation>();
            if (_generalRelationsCache.Any(x => x.Name == name)) {
                return _generalRelationsCache.FirstOrDefault(x => x.Name == name);
            }
            GeneralRelation gr = _generalRelationsQuery.FirstOrDefault(x => x.Name == name);
            return CheckGr(gr);
        }

        public GeneralRelation Get(int seq) {
            if (_generalRelationsCache == null) _generalRelationsCache = new List<GeneralRelation>();
            if (_generalRelationsCache.Any(x => x.Sequence == seq)) {
                return _generalRelationsCache.FirstOrDefault(x => x.Sequence == seq);
            }
            GeneralRelation gr = _generalRelationsQuery.FirstOrDefault(x => x.Sequence == seq);
            return CheckGr(gr);
        }

        private GeneralRelation CheckGr(GeneralRelation gr) {
            if (gr == null) {
                return new GeneralRelation {
                    Sequence = -1,
                    Name = "Unknown",
                    Logo = null,
                    Web = ""
                };
            } else {
                _generalRelationsCache.Add(gr);
                return gr;
            }
        }       
        public List<GeneralRelation> GetAll() {
            return _generalRelationsCache.OrderBy(x => x.Name).ToList();
        }
        public int Count() {
            return _generalRelationsCache.Count;
        }
        public void FillCache() {
            _generalRelationsCache =
                (from g in ctx.F_CAB_GENERAL_RELATIONS
                 select new GeneralRelation {
                     Sequence = g.GREL_SEQUENCE,
                     Name = g.GREL_NAME,
                     Logo = g.GREL_FK_PICT_SEQ,
                     Web = g.GREL_WEBSITE_URL
                 }).ToList();
        }
    }
}
