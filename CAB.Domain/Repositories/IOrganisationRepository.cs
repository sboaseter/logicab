﻿using CAB.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace CAB.Domain.Repositories {
    public interface IOrganisationRepository {
        int Count();
        Organisation Get(string organisation);
        Organisation Get(int sequence);
        void DeleteFromCache(int sequence);
        void Refresh(int id);
        List<Organisation> List(bool all = false);
        int Add(Organisation organisation);
        bool UpdateOrad(int orad, string type, string value);
        void FillCache();
    }
}
