﻿
namespace CAB.Domain.Repositories {
    public interface ITranslationRepository {
        string Get(int language, string text);
        void Clear();
        void Initialize();
    }
}
