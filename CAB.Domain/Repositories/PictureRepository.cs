﻿using CAB.Data.EF.Models;
using CAB.Domain.Providers;
using log4net;
using System;
using System.Linq;
using System.Runtime.Caching;

namespace CAB.Domain.Repositories {
    public class PictureRepository : IPictureRepository {

        private Entities ctx;
        private ILog Log;
        public PictureRepository(
            IDataProvider dataProvider,
            ILog log) {
            ctx = dataProvider.GetContext() as Entities;
            Log = log;
        }
        public byte[] Picture(int id) {
            ObjectCache cache = MemoryCache.Default;
            if (cache.Contains("CachedImage" + id)) {
                return (byte[])cache.Get("CachedImage" + id);
            } else {
                CacheItemPolicy cip = new CacheItemPolicy();
                cip.SlidingExpiration = new TimeSpan(0, 0, 10);
                byte[] img = ctx.F_CAB_PICTURE.Single(x => x.PICT_SEQUENCE == id).PICT_PICTURE;
                cache.Add("CachedImage" + id, img , cip);
                return img;
            } 
        }

        public byte[] CABPicture(int id) {
            return ctx.F_CAB_CD_CAB_CODE.Single(x => x.CABC_SEQUENCE == id).F_CAB_PICTURE.PICT_PICTURE;
        }

        public byte[] AssortimentPicture(int id) {
            return ctx.F_CAB_GROWER_ASSORTIMENT.Single(x => x.GRAS_SEQUENCE == id).F_CAB_PICTURE.PICT_PICTURE;
        }

        public byte[] OfferPicture(int id) {
            return ctx.F_CAB_OFFER_DETAILS.Single(x => x.OFDT_SEQUENCE == id).F_CAB_PICTURE.PICT_PICTURE;
        }

        public byte[] Thumbnail(byte[] data) {
            throw new NotImplementedException();
        }
    }
}
