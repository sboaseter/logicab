﻿using CAB.Domain.Models;
using System.Collections.Generic;

namespace CAB.Domain.Repositories {
    public interface ICabcodeRepository {
        CabCode Get(string code);
        CabCode Get(int seq);
        List<CabCode> GetAll();
        int Count();
        void FillCache();
    }
}
