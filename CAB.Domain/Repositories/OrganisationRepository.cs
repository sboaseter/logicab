﻿using CAB.Data.EF.Models;
using CAB.Domain.Enums;
using CAB.Domain.Models;
using CAB.Domain.Providers;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CAB.Domain.Repositories {
    public class OrganisationRepository : IOrganisationRepository {
        public static List<Organisation> _organisationCache;
        public IQueryable<Organisation> _organisationQuery;

        private Entities ctx;
        public ILog Log { get; private set; }
        public OrganisationRepository(
            IDataProvider dataProvider,
            ILog log) {
            ctx = dataProvider.GetContext() as Entities;
            Log = log;
            _organisationQuery = ctx.F_CAB_ORGANISATION.Select(x => new Organisation {
                Sequence = x.ORGA_SEQUENCE,
                Name = x.ORGA_NAME,
                OrganisationType = x.ORGA_TYPE,
                Phone = x.ORGA_PHONE_NO,
                Email = x.ORGA_EMAIL,
                VAT = x.ORGA_VAT_NUMBER,
                Mailbox = x.ORGA_MAILBOX,
                VBA = x.ORGA_VBA_NUMBER,
                BVH = x.ORGA_BVH_NUMBER,
                EAN = x.ORGA_EAN_CODE,
                StartDate = x.ORGA_START_DATE,
                EndDate = x.ORGA_END_DATE,
                Code = x.ORGA_CODE,
                Picture = x.ORGA_FK_PICTURE,
                Referer = x.ORGA_REFER,
                CreatedOn = x.ORGA_CREATED_ON,
                ModifiedOn = x.ORGA_MODIFIED_ON,
                Orad = x.F_CAB_ORGANISATION_ADMIN.Select(o => new Orad {
                    Sequence = o.ORAD_SEQUENCE,
                    Organisation = o.ORAD_FK_ORGANISATION,
                    Status = o.ORAD_STATUS,
                    Type = o.ORAD_MEMBERTYPE
                }).FirstOrDefault(),

                Maingroups = x.F_CAB_ORGA_MAIN_GROUP_MATRIX.Select(m => new Maingroup {
                    Sequence = m.F_CAB_CD_MAIN_GROUP_TYPE.CDMG_SEQUENCE,
                    Description = m.F_CAB_CD_MAIN_GROUP_TYPE.CDMG_DESC,
                    DaysOffer = m.F_CAB_CD_MAIN_GROUP_TYPE.CDMG_NUM_OF_DAYS_OFFER,
                    Order = m.ORMA_ORDER
                }),
                Addresses = x.F_CAB_ADDRESS.Select(a => new Address {
                    Sequence = a.ADDR_SEQUENCE,
                    City = a.ADDR_CITY,
                    Zipcode = a.ADDR_ZIPCODE,
                    Street = a.ADDR_STREET,
                    StreetNr = a.ADDR_NO
                })
            });
        }

        public void FillCache() {
            Log.Info("OrganisationRepository _organisationCache filling.");
            _organisationCache = _organisationQuery.ToList();
            Log.InfoFormat("OrganisationRepository _organisationCache filled: {0} entries.", _organisationCache.Count);
        }

        public int Count() {
            return _organisationCache.Count();
        }

        private Organisation CheckOrganisation(Organisation o) {
            if (o == null) {
                return new Organisation {
                    Sequence = -1
                };
            } else {
                _organisationCache.Add(o);
                return o;
            }
        }

        public Organisation Get(string organisation) {
            if (_organisationCache == null) _organisationCache = new List<Organisation>();
            if (_organisationCache.Any(x => x.Name == organisation)) {
                return _organisationCache.FirstOrDefault(x => x.Name == organisation);
            }
            Organisation o = _organisationQuery.FirstOrDefault(x => x.Name == organisation);
            return CheckOrganisation(o);
        }
        public Organisation Get(int organisation) {
            if (_organisationCache == null) _organisationCache = new List<Organisation>();
            if (_organisationCache.Any(x => x.Sequence == organisation)) {
                return _organisationCache.FirstOrDefault(x => x.Sequence == organisation);
            }
            Organisation o = _organisationQuery.FirstOrDefault(x => x.Sequence == organisation);
            return CheckOrganisation(o);
        }

        public List<Organisation> List(bool all = false) {
            if (_organisationCache == null) FillCache();
            if (all) return _organisationCache.OrderBy(x => x.Name).ToList();
            return _organisationCache.OrderBy(x => x.Name).ToList();
        }

        public int Add(Organisation organisation) {
            throw new NotImplementedException();
        }

        public void Refresh(int id) {
            this.DeleteFromCache(id);
            this.Get(id);
        }

        public void Update(Organisation organisation) {
            var dborga = ctx.F_CAB_ORGANISATION.Single(x => x.ORGA_SEQUENCE == organisation.Sequence);
            ctx.F_CAB_ORGANISATION.Attach(dborga);
            ctx.SaveChanges();
            _organisationCache.Remove(this.Get(organisation.Sequence));
            this.Get(organisation.Sequence);

        }

        public bool UpdateOrad(int orad, string type, string value) {
            var oa = ctx.F_CAB_ORGANISATION_ADMIN.FirstOrDefault(x => x.ORAD_SEQUENCE == orad);
            ctx.F_CAB_ORGANISATION_ADMIN.Attach(oa);
            switch (type) {
                case "STATUS":
                    oa.ORAD_STATUS = Int32.Parse(value);
                    break;
                default:
                    oa.ORAD_MEMBERTYPE = Int32.Parse(value);
                    break;
            }
            ctx.SaveChanges();
            this.Update(this.Get(oa.ORAD_FK_ORGANISATION));
            return true;
        }

        public void DeleteFromCache(int sequence) {
            _organisationCache.Remove(this.Get(sequence));
        }
    }
}
