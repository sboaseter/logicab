﻿using CAB.Domain.Models;
using System.Linq;

namespace CAB.Domain.Repositories {
    public interface ILoginRepository {
        int Count();
        /*int Sequence(string username);*/
        Login Get(string username);
        bool Validate(string username, string password);
        IQueryable<Login> GetLogins();
        int Add(Login login);
    }
}
