﻿using CAB.Data.EF.Models;
using CAB.Domain.Models;
using CAB.Domain.Providers;
using System.Linq;

namespace CAB.Domain.Repositories {
    public class PersonRepository : IPersonRepository {
        private IQueryable<Person> _persons;
        private Entities ctx;
        public PersonRepository(
            IDataProvider dataProvider
            ) {
            ctx = dataProvider.GetContext() as Entities;
            _persons = ctx.F_CAB_PERSON.Select(x => new Person {
                Sequence = x.PERS_SEQUENCE,
                Organisation = x.PERS_FK_ORGANISATION,
                FirstName = x.PERS_FIRST_NAME,
                MiddleName = x.PERS_MIDDLE_NAME,
                LastName = x.PERS_LAST_NAME,
                Phone = x.PERS_PHONE_NO,
                Email = x.PERS_EMAIL,
                ExtRef = x.PERS_EXT_REF,
                CreatedOn = x.PERS_CREATED_ON
            });
        }
        public IQueryable<Person> List() {
            return _persons;
        }


        public Person Get(int Sequence) {
            return _persons.Single(x => x.Sequence == Sequence);
        }
    }
}
