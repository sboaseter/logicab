﻿using CAB.Data.EF.Models;
using CAB.Domain.Models;
using CAB.Domain.Providers;
using log4net;
using System.Linq;

namespace CAB.Domain.Repositories {
    public class LoginRepository : ILoginRepository {
        public IQueryable<Login> Logins;
        private Entities ctx;
        public ILog Log { get; private set; }
        private IOrganisationRepository _organisationRepository;
        public LoginRepository(
            IDataProvider dataProvider, 
            IOrganisationRepository organisationRepository,
            ILog log) {
            ctx = dataProvider.GetContext() as Entities;
            _organisationRepository = organisationRepository;
            Log = log;
            Logins = ctx.F_CAB_LOGIN.Select(x => new Login {
                Sequence = x.LOGI_SEQUENCE,
                Name = x.LOGI_USER_NAME,
                Password = x.LOGI_PASS,
                Language = x.LOGI_FK_CDLA_SEQ,
                Blocked = x.LOGI_BLOCKED_YN,
                LoginGroup = x.LOGI_FK_LOGIN_GROUP,
                Person = x.LOGI_FK_PERSON,
                Organisation = x.F_CAB_PERSON.PERS_FK_ORGANISATION
            });
        }
        public LoginRepository() {
            ctx = new DataProvider().GetContext() as Entities;
            Logins = ctx.F_CAB_LOGIN.Select(x => new Login {
                Sequence = x.LOGI_SEQUENCE,
                Name = x.LOGI_USER_NAME,
                Password = x.LOGI_PASS,
                Language = x.LOGI_FK_CDLA_SEQ,
                Blocked = x.LOGI_BLOCKED_YN,
                LoginGroup = x.LOGI_FK_LOGIN_GROUP,
                Person = x.LOGI_FK_PERSON,
                Organisation = x.F_CAB_PERSON.PERS_FK_ORGANISATION
            });
        }
        public int Count() {
            return Logins.Count();
        }
/*
        public int Sequence(string username) {
            var query = Logins.FirstOrDefault(x => x.Name == username);
            if (query != null)
                return query.Sequence;
            return -1;
        }
 */ 
        public Login Get(string username) {
            return Logins.Single(x => x.Name == username);
        }

        public bool Validate(string username, string password) {
            return Logins.Any(x => x.Name == username && x.Password == password && x.Blocked == 0);
        }
        public IQueryable<Login> GetLogins() {
            return Logins.OrderBy(x => x.Name);
        }
        public int Add(Login login) {
            F_CAB_LOGIN newLogin = new F_CAB_LOGIN {
                LOGI_USER_NAME = login.Name,
                LOGI_PASS = login.Password,
                LOGI_BLOCKED_YN = 0,
                LOGI_FK_CDLA_SEQ = 1,
                LOGI_FK_LOGIN_GROUP = 1,
                LOGI_PASSWORD = login.Password.ToUpper(),
                LOGI_FK_PERSON = 1                
            };
            //ctx.F_CAB_LOGIN.AddObject(newLogin);
            ctx.F_CAB_LOGIN.AddObject(newLogin);
            ctx.SaveChanges();
            return newLogin.LOGI_SEQUENCE;
        }
        public bool Save(Login login) {
            var dbLogin = Logins.Single(x => x.Sequence == login.Sequence);
            if (login == dbLogin) return true;
            dbLogin.Language = login.Language;
//            dbLogin
            return true;
        }
    }
}
