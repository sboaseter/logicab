﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAB.Domain.Models;
using CAB.Data.EF.Models;
using CAB.Domain.Providers;

namespace CAB.Domain.Repositories {
    public class CabcodeRepository : ICabcodeRepository {
        private static List<CabCode> _cabcodeCache;
        private IQueryable<CabCode> _cabcodeQuery;
        private Entities ctx;
        public CabcodeRepository(
            IDataProvider dataProvider) {
            ctx = dataProvider.GetContext() as Entities;
            _cabcodeQuery =
                from c in ctx.F_CAB_CD_CAB_CODE
                select new CabCode {
                    Seq = c.CABC_SEQUENCE,
                    Code = c.CABC_CAB_CODE,
                    Picture = c.CABC_FK_PICTURE
                };
        }
        public CabcodeRepository() {
            ctx = new DataProvider().GetContext() as Entities;
            _cabcodeQuery =
                from c in ctx.F_CAB_CD_CAB_CODE
                select new CabCode {
                    Seq = c.CABC_SEQUENCE,
                    Code = c.CABC_CAB_CODE,
                    Picture = c.CABC_FK_PICTURE
                };
        }

        private CabCode CheckCabCode(CabCode cc) {
            if (cc == null) {
                return new CabCode {
                    Seq = -1,
                    Code = null,
                    Picture = null
                };
            } else {
                _cabcodeCache.Add(cc);
                return cc;
            }
        }

        public CabCode Get(string code) {
            if (_cabcodeCache == null) _cabcodeCache = new List<CabCode>();
            if (_cabcodeCache.Any(x => x.Code == code)) {
                return _cabcodeCache.FirstOrDefault(x => x.Code == code);
            }
            CabCode cc = _cabcodeQuery.FirstOrDefault(x => x.Code == code);
            return CheckCabCode(cc);
        }

        public CabCode Get(int seq) {
            if (_cabcodeCache == null) _cabcodeCache = new List<CabCode>();
            if (_cabcodeCache.Any(x => x.Seq == seq)) {
                return _cabcodeCache.FirstOrDefault(x => x.Seq == seq);
            }
            CabCode cc = _cabcodeQuery.FirstOrDefault(x => x.Seq == seq);
            return CheckCabCode(cc);            
        }

        public List<CabCode> GetAll() {
            return _cabcodeCache;
        }

        public int Count() {
            return _cabcodeCache.Count;
        }

        public void FillCache() {
            _cabcodeCache = _cabcodeQuery.ToList();
        }
    }
}
