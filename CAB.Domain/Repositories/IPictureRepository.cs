﻿using CAB.Domain.Models;
using System.Linq;

namespace CAB.Domain.Repositories {
    public interface IPictureRepository {
        byte[] Picture(int id);
        byte[] CABPicture(int id);
        byte[] AssortimentPicture(int id);
        byte[] OfferPicture(int id);
        byte[] Thumbnail(byte[] data);
    }
}
