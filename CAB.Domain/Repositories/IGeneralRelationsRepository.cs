﻿using CAB.Domain.Models;
using System.Collections.Generic;

namespace CAB.Domain.Repositories {
    public interface IGeneralRelationsRepository {
        GeneralRelation Get(string name);
        GeneralRelation Get(int seq);
        List<GeneralRelation> GetAll();
        int Count();
        void FillCache();
    }
}
