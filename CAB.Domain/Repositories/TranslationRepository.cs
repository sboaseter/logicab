﻿using CAB.Data.EF.Models;
using CAB.Domain.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;  
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using System.Data.Entity.Core.EntityClient;
using log4net;
using System.Net;
using System.IO;
using System.Collections;
using System.Text;
using Newtonsoft.Json;
using System.Configuration;

namespace CAB.Domain.Repositories {
    public class TranslationRepository : ITranslationRepository {
        private Entities ctx;
//        private const string apiUrl = "http://api.logicab.nl/api/v1/";
        private static Dictionary<int, Dictionary<string, string>> translationCache;
        private static List<Dictionary<int, string>> missingTranslationsCache;
        private string ConnectionString;
        private string apiUrl = ConfigurationManager.AppSettings["apiUrl"] as String;
        public ILog Log { get; private set; }
        public TranslationRepository(
            IDataProvider dataProvider, ILog log) {
            ctx = dataProvider.GetContext() as Entities;
            EntityConnection ec = (EntityConnection)ctx.Connection;
            SqlConnection sc = (SqlConnection)ec.StoreConnection;
            ConnectionString = sc.ConnectionString;
            Log = log;

        }
        public TranslationRepository() {
            ctx = new DataProvider().GetContext() as Entities;
        }
        public void Clear() {
            translationCache = null;
        }

        public void Initialize() {
            if (translationCache == null) translationCache = new Dictionary<int, Dictionary<string, string>>();
            var languages = ctx.F_CAB_CD_LANGUAGE.Select(x => new { Seq = x.CDLA_SEQUENCE, Desc = x.CDLA_LANGUAGE_DESC }).ToList();
            foreach (var l in languages) {
                if (!translationCache.ContainsKey(l.Seq))
                    translationCache.Add(l.Seq, new Dictionary<string, string>());
                foreach (var entry in ctx.F_CAB_TRANSLATION.Where(x => x.CATR_FK_LANGUAGE == l.Seq).Select(y => new { Label = y.CATR_LABEL_NAME, Result = y.CATR_LABEL_DESCRIPTION })) {
                    if (!translationCache[l.Seq].ContainsKey(entry.Label))
                        translationCache[l.Seq].Add(entry.Label, entry.Result);
                }
                Log.InfoFormat("{0}: {1} Translations", l.Desc, translationCache[l.Seq].Count);
            }
        }

        public string Get(int language, string text) {
            text = text.ToLower();
            if (translationCache == null) translationCache = new Dictionary<int, Dictionary<string, string>>();
            if (translationCache.ContainsKey(language)) {
                if (translationCache[language].ContainsKey(text)) {
                    return translationCache[language][text];
                }
            }
            var result = queryLabel(language, text);
            if (result == null) {


                

                /*
                 * Launch async task to check F_CAB_MISSING_TRANSLATIONS
                 */
                Task.Factory.StartNew(() => RegisterMissingTranslation(language, text));

                /*
                 * Label/Language combination does not exist in this DB, lets ask apitest.logicab.nl!
                 *
                */
                Task.Factory.StartNew(() => QueryLogiCABApiTranslation(language, text));
                result = queryLabel(1, text);
                if (result == null) {
                    return String.Format("{0}*", text);
                } else {
                    if (!translationCache.ContainsKey(language))
                        translationCache.Add(language, new Dictionary<string, string>());
                    translationCache[language].Add(text, String.Format("{0}*", result));
                    return String.Format("{0}*", result);

                }
            } else {
                if (!translationCache.ContainsKey(language))
                    translationCache.Add(language, new Dictionary<string, string>());
                translationCache[language].Add(text, result);
                return result;
            }
        }

        private string queryLabel(int Language, string Label) {
            using (var conn = new SqlConnection(ConnectionString)) {
                conn.Open();
                var rows = conn.Query<string>(@"SELECT CATR_LABEL_DESCRIPTION FROM F_CAB_TRANSLATION WHERE CATR_FK_LANGUAGE = @Language AND CATR_LABEL_NAME = @Label",
                    new { Language = Language, Label = Label });
                return rows.FirstOrDefault();
            } 	
        }

        private void RegisterMissingTranslation(int language, string text) {
            using (var conn = new SqlConnection(ConnectionString)) {
                conn.Open();
                var entryExists = conn.Query("SELECT CMTR_FK_LANGUAGE, CMTR_LABEL_NAME FROM F_CAB_MISSING_TRANSLATION WHERE CMTR_FK_LANGUAGE = @Language AND CMTR_LABEL_NAME = @Text",
                    new { Language = language, Text = text });
                if (entryExists.Count() == 0) { // Lets insert it
                    var res = conn.Query("INSERT INTO F_CAB_MISSING_TRANSLATION (CMTR_FK_LANGUAGE, CMTR_LABEL_NAME) VALUES (@Language, @Text)", new { Language = language, Text = text });
                }
            }
        }

        private void QueryLogiCABApiTranslation(int language, string text) {
            // By now the missing translation is allready added to F_CAB_MISSING_TRANSLATIONS
            var translationUrl = String.Format("{0}/translate", apiUrl);

            Dictionary<string, string> paramters = new Dictionary<string, string>();
            paramters.Add("label", text);

            var populatedEndPoint = CreateFormattedPostRequest(paramters);
            byte[] bytes = Encoding.UTF8.GetBytes(populatedEndPoint);

            HttpWebRequest request = CreateWebRequest(translationUrl, bytes.Length);

            using (var requestStream = request.GetRequestStream()) {
                requestStream.Write(bytes, 0, bytes.Length);
            }

            using (var response = (HttpWebResponse)request.GetResponse()) {
                if (response.StatusCode != HttpStatusCode.OK) {
                    string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                    throw new ApplicationException(message);
                } else {
                    var encoding = ASCIIEncoding.ASCII;
                    using (var reader = new StreamReader(response.GetResponseStream(), encoding)) {
                        string responseText = reader.ReadToEnd();
                        var jsDyn = JsonConvert.DeserializeObject<TranslationRecord>(responseText);
                        if (jsDyn.Count == 0) {
                            Log.InfoFormat("Missing translation on Remote & Base site: {0}", text);
                        } else {
                            // Found translations.
                            this.AddTranslations(language, jsDyn);
                        }                       
                    }
                }
            } 

        }

        private void AddTranslations(int language, TranslationRecord jsDs) {
            // get iso-values of languages 
            List<LanguageISO> Languages = new List<LanguageISO>();
            using (var conn = new SqlConnection(ConnectionString)) {
                conn.Open();
                Languages = conn.Query<LanguageISO>("SELECT CDLA_SEQUENCE AS Sequence, CDLA_ISO as Iso FROM F_CAB_CD_LANGUAGE WHERE CDLA_ISO IS NOT NULL").ToList();
            }
            LanguageISO reqLang = Languages.FirstOrDefault(x => x.Sequence == language);
            // This will only insert the entry with the requested language, could insert all...
            using (var conn = new SqlConnection(ConnectionString)) {
                conn.Open();
                string[] isoAndTrans = jsDs.Translations.FirstOrDefault(x => x[0] == reqLang.Iso); // CDLA_ISO needs to be filled, so guarantee proper matching of languages across databases.
                Log.InfoFormat("Translation for: [{0}] in [{1}] (Seq: {2})", jsDs.Label, reqLang.Iso, reqLang.Sequence);
                var ins = conn.Query(@"
                        insert into F_CAB_TRANSLATION(CATR_FK_LANGUAGE, CATR_WINDOW_NAME, CATR_LABEL_ID, CATR_LABEL_NAME, CATR_LABEL_DESCRIPTION, CATR_CREATED_BY) 
                        values 
                        (@Language, 'Translation', @MaxId, @Text, @TranslationText, @CreatedBy)",
                    new { Language = reqLang.Sequence, MaxId = 0, Text = jsDs.Label, TranslationText = isoAndTrans[1], CreatedBy = "LogiCAB" });
                var res = conn.Query("DELETE FROM F_CAB_MISSING_TRANSLATION WHERE CMTR_FK_LANGUAGE = @Id AND CMTR_LABEL_NAME = @LabelName", new { Id = reqLang.Sequence, LabelName = jsDs.Label});
                if (translationCache[language].ContainsKey(jsDs.Label)) {
                    translationCache[language].Remove(jsDs.Label);
                }


            }

        }        

        private HttpWebRequest CreateWebRequest(string endPoint, Int32 contentLength) {
            var request = (HttpWebRequest)WebRequest.Create(endPoint);

            request.Method = "POST";
            request.ContentLength = contentLength;
            request.ContentType = "application/x-www-form-urlencoded";

            return request;
        } 

        private string CreateFormattedPostRequest( ICollection<KeyValuePair<string, string>> values )  
        {  
            var paramterBuilder = new StringBuilder();  
            var counter = 0;  
            foreach ( var value in values )  
            {  
                paramterBuilder.AppendFormat( "{0}={1}", value.Key, HttpUtility.UrlEncode( value.Value ) );  
  
                if ( counter != values.Count - 1 )  
                {  
                    paramterBuilder.Append( "&" );  
                }  
  
                counter++;  
            }  
  
            return paramterBuilder.ToString();  
        } 

    }
    public class TranslationRecord {
        public string Label { get; set; }
        public int Count { get; set; }
        public List<string[]> Translations { get; set; }
    }

    public class LanguageISO {
        public int Sequence { get; set; }
        public string Iso { get; set; }
    }
}
