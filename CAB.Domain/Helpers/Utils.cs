﻿using CAB.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
namespace CAB.Domain.Helpers {
    public static class Utils {
        public static List<OfferDetail> OfferdetailsCache {
            get { return SessionWrapper.GetFromSession<List<OfferDetail>>("offerdetailscache"); }
            set { SessionWrapper.SetInSession<List<OfferDetail>>("offerdetailscache", value); }
        }
        public static List<Setting> ShopSettings {
            get { return SessionWrapper.GetFromSession<List<Setting>>("shopsettings"); }
            set { SessionWrapper.SetInSession<List<Setting>>("shopsettings", value); }
        }
        public static List<Setting> OrgaSettings {
            get { return SessionWrapper.GetFromSession<List<Setting>>("orgasettings"); }
            set { SessionWrapper.SetInSession<List<Setting>>("orgasettings", value); }
        }
        public static List<Setting> PersSettings {
            get { return SessionWrapper.GetFromSession<List<Setting>>("perssettings"); }
            set { SessionWrapper.SetInSession<List<Setting>>("perssettings", value); }
        }
        public static string GetSetting(string type, string setting) {
            switch (type) {
                case "Shop":
                    if (ShopSettings.Any(x => x.Id == setting)) {
                        return ShopSettings.Single(x => x.Id == setting).Value;
                    }
                    break;
                case "Orga":
                    if (OrgaSettings.Any(x => x.Id == setting)) {
                        return OrgaSettings.Single(x => x.Id == setting).Value;
                    }
                    break;
                case "Pers":
                    if (PersSettings.Any(x => x.Id == setting)) {
                        return PersSettings.Single(x => x.Id == setting).Value;
                    }
                    break;
                default:
                    return String.Empty;
            }
            return String.Empty;
        }
        public static ShopEvidence ShopEvidence {
            get { return SessionWrapper.GetFromSession<ShopEvidence>("shopevidence"); }
            set { SessionWrapper.SetInSession<ShopEvidence>("shopevidence", value); }
        }
    }
}