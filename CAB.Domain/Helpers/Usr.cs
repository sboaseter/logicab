﻿using CAB.Domain.Enums;
using CAB.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CAB.Domain.Helpers {
    public static class Usr {
        public static int LoginSequence {
            get { return SessionWrapper.GetFromSession<int>("login_sequence"); }
            set { SessionWrapper.SetInSession<int>("login_sequence", value); }
        }
        public static string LoginName {
            get { return SessionWrapper.GetFromSession<string>("login_name"); }
            set { SessionWrapper.SetInSession<string>("login_name", value); }
        }
        public static string LoginPass {
            get { return SessionWrapper.GetFromSession<string>("login_pass"); }
            set { SessionWrapper.SetInSession<string>("login_pass", value); }
        }

        public static int LoginGroup {
            get { return SessionWrapper.GetFromSession<int>("login_group"); }
            set { SessionWrapper.SetInSession<int>("login_group", value); }
        }

        public static int Language {
            get {
                var lang = SessionWrapper.GetFromSession<int>("language");
                if (lang != 0) return lang;
                return 1;
            }
            set { SessionWrapper.SetInSession<int>("language", value); }
        }
        public static string OrganisationType {
            get {
                var oType = SessionWrapper.GetFromSession<string>("orga_type");
                switch (oType) {
                    case OrgaType.Buyer:
                        return OrgaType.Buyer;
                    case OrgaType.Grower:
                        return OrgaType.Grower;
                    case OrgaType.Comm:
                        return OrgaType.Comm;
                    case OrgaType.Logp:
                        return OrgaType.Logp;
                    default:
                        return OrgaType.Buyer;
                }
            }
            set { SessionWrapper.SetInSession<string>("orga_type", value); }
        }

        public static bool GrowerModule
        {
            get {
                return Usr.Functions.Any(x => x == "GROWER_MODULE");
//                return SessionWrapper.GetFromSession<bool>("growermodule");  
            }
            set { SessionWrapper.SetInSession<bool>("growermodule", value); }
        }

        public static int OrganisationSequence {
            get { return SessionWrapper.GetFromSession<int>("orga_sequence"); }
            set { SessionWrapper.SetInSession<int>("orga_sequence", value); }
        }
        public static int OrgaTypeSequence {
            get { return SessionWrapper.GetFromSession<int>("orga_type_sequence"); }
            set { SessionWrapper.SetInSession<int>("orga_type_sequence", value); }
        }
        public static int GrowerSequence {
            get { return SessionWrapper.GetFromSession<int>("grower_sequence"); }
            set { SessionWrapper.SetInSession<int>("grower_sequence", value); }
        }
        public static IQueryable<Handshake> Handshakes {
            get { return SessionWrapper.GetFromSession<IQueryable<Handshake>>("handshakes"); }
            set { SessionWrapper.SetInSession<IQueryable<Handshake>>("handshakes", value); }
        }
        public static Person Person {
            get { return SessionWrapper.GetFromSession<Person>("person"); }
            set { SessionWrapper.SetInSession<Person>("person", value); }
        }
        public static Organisation Organisation {
            get { return SessionWrapper.GetFromSession<Organisation>("organisation"); }
            set { SessionWrapper.SetInSession<Organisation>("organisation", value); }
        }
        public static List<string> Functions {
            get { return SessionWrapper.GetFromSession<List<string>>("functions"); }
            set { SessionWrapper.SetInSession<List<string>>("functions", value); }
        }
        public static bool hasFunction(string name) {
            if (Functions.Contains(name)) return true;
            return false;
        }
        public static DateTime SelectedDate {
            get { return SessionWrapper.GetFromSession<DateTime>("selecteddate"); }
            set { SessionWrapper.SetInSession<DateTime>("selecteddate", value); }
        }
        public static int SelectedMaingroup {
            get { return SessionWrapper.GetFromSession<int>("selectedmaingroup"); }
            set { SessionWrapper.SetInSession<int>("selectedmaingroup", value); }
        }
        public static int SelectedHandshake {
            get { return SessionWrapper.GetFromSession<int>("selectedhandshake"); }
            set { SessionWrapper.SetInSession<int>("selectedhandshake", value); }
        }
        public static string SelectedOffertype {
            get { return SessionWrapper.GetFromSession<string>("selectedoffertype") == OfferType.Stock ? OfferType.Stock : OfferType.Offer; }
            set { SessionWrapper.SetInSession<string>("selectedoffertype", value); }
        }
        public static Dictionary<string, List<string>> Filters {
            get { return SessionWrapper.GetFromSession<Dictionary<string, List<string>>>("filters"); }
            set { SessionWrapper.SetInSession<Dictionary<string, List<string>>>("filters", value); }
        }
        public static string LastFilter {
            get { return SessionWrapper.GetFromSession<string>("lastfilter"); }
            set { SessionWrapper.SetInSession<string>("lastfilter", value); }
        }
        public static string DeliveryHeader {
            get { return SessionWrapper.GetFromSession<string>("DeliveryHeader"); }
            set { SessionWrapper.SetInSession<string>("DeliveryHeader", value); }
        }
        public static List<Setting> OrganisationSettings
        {
            get { return SessionWrapper.GetFromSession<List<Setting>>("OrganisationSettings"); }
            set { SessionWrapper.SetInSession<List<Setting>>("OrganisationSettings", value); }
        }
    }
}
