﻿using CAB.Data.EF.Models;
using CAB.Domain.Models;
using System.Linq;

namespace CAB.Domain.Providers {
    public class ShopEvidenceProvider : IShopEvidenceProvider {
        private Entities ctx;
        public ShopEvidenceProvider(IDataProvider dataProvider) {
            ctx = dataProvider.GetContext() as Entities;
        }
        public ShopEvidenceProvider() {
            ctx = new DataProvider().GetContext() as Entities;
        }
        public ShopEvidence Get(string url) {
            var spee = ctx.F_CAB_SHOP_EVIDENCE
                .Where(x => url.Contains(x.SPEE_VALUE) && x.SPEE_SERVICE == "LogiOfferte")
                .Select(x => x).FirstOrDefault();
            if (spee != null) {
                return new ShopEvidence {
                    Id = spee.SPEE_FK_SHOP,
                    Name = spee.F_CAB_SHOP_SKIN.SPSN_CODE
                };
            } else {
                return new ShopEvidence {
                    Id = 1,
                    Name = "LogiOfferte"
                };
            }
        }
    }
}
