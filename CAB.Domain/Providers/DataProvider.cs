﻿using CAB.Data.EF.Models;
using log4net;
using System.Data.Entity.Core.Objects;

namespace CAB.Domain.Providers {
    public class DataProvider : IDataProvider {
        private ObjectContext _context;
        public DataProvider(ILog log) {
            _context = new Entities();
        }
        public DataProvider() {
            _context = new Entities();
        }
        public ObjectContext GetContext() {
            return _context;
        }
    }
}
