﻿namespace CAB.Domain.Providers {
    public interface ISpecialsProvider {
        int[] Get(int number);
        int[] Get(int number, int sequence, string type);
        int[] Get(int number, int buyer);
    }
}
