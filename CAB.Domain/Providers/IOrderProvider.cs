﻿using CAB.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAB.Domain.Providers {
    public interface IOrderProvider {
        OrderHeader Get(int id);
        OrderHeader CreateOrder(int id);
        string CreateEKT(int id);
        string CreateKWB(int id);
        void UpdateDetailsKWBSent(int id);
        void UpdateDetailsEKTSent(int id);
    }
}
