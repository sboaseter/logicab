﻿
using CAB.Domain.Models;
using System.Collections.Generic;
namespace CAB.Domain.Providers {
    public interface IDeliveryDatesProvider {
        List<DeliveryDate> Get(int buyer, int maingroup, string Type = "", int Sequence = -1);
    }
}
