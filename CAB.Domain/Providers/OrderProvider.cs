﻿using CAB.Data.EF.Models;
using CAB.Domain.Enums;
using CAB.Domain.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAB.Domain.Providers {
    public class OrderProvider : IOrderProvider {
        private ISettingsProvider _settingsProvider;
        private Entities ctx;
        private ILog Log;
        public IQueryable<OrderHeader> Orders;
//        public IQueryable<OrderDetail> OrderDetails;
        private string ConnectionString;
        public OrderProvider(
            IDataProvider dataProvider,
            ISettingsProvider settingsProvider,
            ILog log) {
            ctx = dataProvider.GetContext() as Entities;
            EntityConnection ec = (EntityConnection)ctx.Connection;
            SqlConnection sc = (SqlConnection)ec.StoreConnection;
            ConnectionString = sc.ConnectionString;
            _settingsProvider = settingsProvider;
            Log = log;
            Orders =
                from orhe in ctx.F_CAB_ORDER_HEA.Include("F_CAB_ORDER_DET")
                select new OrderHeader {
                    id = orhe.ORHE_SEQUENCE,
                    Status = orhe.ORHE_FK_ORDER_STATUS,
                    Details = ctx.F_CAB_ORDER_DET.Where(x => x.ORDE_FK_ORDER_HEA == orhe.ORHE_SEQUENCE).Select(y => new OrderDetail {
                        id = y.ORDE_SEQUENCE,
                        OrderHeader = y.ORDE_FK_ORDER_HEA
                    })
                };

        }
        public OrderHeader Get(int id) {
            return Orders.FirstOrDefault(x => x.id == id);
        }
        public OrderHeader CreateOrder(int id) {
            OrderHeader ret = new OrderHeader() { id = 123 };
//            ret.Details = OrderDetails.Where(x => x.OrderHeader == ret);
            return ret;
        }
        public string CreateEKT(int id) {
            StringBuilder sb = new StringBuilder();
            int i = 0;
            var _OrderNo = "";

            var CABConnStr = ConfigurationManager.ConnectionStrings["WebshopConnection"].ToString();
            SqlConnection c = new SqlConnection(CABConnStr);
            c.Open();
            SqlCommand sc = new SqlCommand(getOrderHeaderSQL(id), c);
            SqlDataReader sdr = sc.ExecuteReader(CommandBehavior.SingleRow);

            if (sdr.Read()) {
                //_OrderNo = sdr["ORHE_NO"].ToString();
                _OrderNo = sdr["DetailSeq"].ToString();
                sb.AppendLine(String.Format("UNB+UNOA:2+{0}:14+{1}:ZZ+{2}:{3}+{4}++CAB 1.7++++0'", sdr["AuctionEAN"].ToString(), sdr["ReverseAddress"].ToString(), DateTime.Now.ToString("yyMMdd"), DateTime.Now.ToString("HHmm"), _OrderNo));
                sb.AppendLine(String.Format("UNH+{0}+CLOCKT:003:007:EF'", _OrderNo));
                sb.AppendLine(String.Format("BGM+493'"));
                sb.AppendLine(String.Format("DTM+97:{0}:102'", sdr["DeliveryDate"].ToString()));
                sb.AppendLine(String.Format("NAD+BY+{0}'", sdr["BuyerID"].ToString()));
                sb.AppendLine(String.Format("NAD+FLA+{0}'", sdr["AuctionID"].ToString()));
                sb.AppendLine(String.Format("NAD+SE+{0}:160:9'", sdr["SenderEAN"].ToString()));
                i = i + 6;
            } else {
                return "";
            }

            sdr.Close();
            sc.CommandText = getOrderDetailsSQL(id);
            sdr = sc.ExecuteReader();
            int lineNo = 1;
            decimal d = 0.00M;
            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            SqlDataReader sdrProp;
            SqlDataReader sdrPropVBN;
            SqlConnection cProp = new SqlConnection(CABConnStr);
            cProp.Open();

            while (sdr.Read()) {
                sb.AppendLine(String.Format("LIN+++{0}:VBN'", sdr["VBNCode"].ToString()));
                sb.AppendLine(String.Format("DTM+9:{0}:402'", DateTime.Now.ToString("hhmmss")));
                sb.AppendLine(String.Format("NAD+MF+{0}'", sdr["GrowerID"].ToString()));

                if (!string.IsNullOrEmpty(sdr["CUST_REF"].ToString())) {
                    sb.AppendLine(String.Format("RFF+ADZ:{0}'", sdr["CUST_REF"].ToString()));
                    i++;
                }

                var agj = sdr["ORDE_SEQUENCE"].ToString();

                if (agj.Length > 7)
                    agj = agj.Substring(agj.Length - 7);

                sb.AppendLine(String.Format("RFF+AGJ:{0}'", agj));
                sb.AppendLine(String.Format("RFF+FAC:99'"));

                if (!string.IsNullOrEmpty(sdr["ORDER_REF"].ToString())) {
                    sb.AppendLine(String.Format("RFF+ON:{0}'", sdr["ORDER_REF"]));
                    i++;
                }

                sb.AppendLine(String.Format("QTY+52:{0}'", sdr["NumberOfItemsPerUnit"].ToString()));
                sb.AppendLine(String.Format("QTY+66:{0}'", sdr["NumberOfUnits"].ToString()));
                decimal.TryParse(sdr["Price"].ToString(), out d);
                sb.AppendLine(String.Format(nfi, "PRI+INV:{0:0.00}'", d));
                sdrProp = new SqlCommand(getOrderDetailPropertiesSQL(int.Parse(sdr["ORDE_SEQUENCE"].ToString())), cProp).ExecuteReader();


                while (sdrProp.Read()) {
                    if (sdrProp["VBNPropValue"].ToString().Length > 0) {
                        if (sdrProp["VBNPropCode"].ToString() == "S99")
                            sb.AppendLine(String.Format("IMD++{0}+:::{1}'", sdrProp["VBNPropCode"].ToString(), sdrProp["VBNPropValue"].ToString()));
                        else
                            sb.AppendLine(String.Format("IMD++{0}+{1}'", sdrProp["VBNPropCode"].ToString(), sdrProp["VBNPropValue"].ToString()));
                        i++;
                    }
                }

                sdrProp.Close();
                sdrPropVBN = new SqlCommand(getOrderDetailPropertiesVBNSQL(int.Parse(sdr["ORDE_SEQUENCE"].ToString())), cProp).ExecuteReader();
                while (sdrPropVBN.Read()) {
                    if (sdrPropVBN["VBNPropValue"].ToString().Length > 0) {
                        if (sdrPropVBN["VBNPropCode"].ToString() == "S99")
                            sb.AppendLine(String.Format("IMD++{0}+:::{1}'", sdrPropVBN["VBNPropCode"].ToString(), sdrPropVBN["VBNPropValue"].ToString()));
                        else
                            sb.AppendLine(String.Format("IMD++{0}+{1}'", sdrPropVBN["VBNPropCode"].ToString(), sdrPropVBN["VBNPropValue"].ToString()));
                        i++;
                    }
                }
                sdrPropVBN.Close();


                int dumInt = 34;

                if (sdr["CAB_DESC"].ToString().Length < 34)
                    dumInt = sdr["CAB_DESC"].ToString().Length;

                if (dumInt > 0)
                    sb.AppendLine(String.Format("IMD++S99+:::{0}'", sdr["CAB_DESC"].ToString().Substring(0, dumInt)));
                else
                    i--;

                sb.AppendLine(String.Format("PAC+++{0}'", sdr["CASKCode"].ToString()));
                sb.AppendLine(String.Format("EQD+BX++1'"));
                i = i + 11;
                lineNo++;
            }

            cProp.Close();
            sdr.Close();
            c.Close();
            i = i + 1; // UNT
            sb.AppendLine(String.Format("UNT+{0}+{1}'", i, _OrderNo));
            sb.AppendLine(String.Format("UNZ+1+{0}'", _OrderNo));
            return sb.ToString();
        }
        public string CreateKWB(int id) {
            StringBuilder sb = new StringBuilder();
            int i = 0;
            var _OrderNo = "";
            String buyerEAN = "";
            String growerEAN = "";
            String growLoc = "";
            String deliverDate = "";
            String transactionDate = "";
            var CABConnStr = ConfigurationManager.ConnectionStrings["WebshopConnection"].ToString();
            SqlConnection c = new SqlConnection(CABConnStr);
            c.Open();
            SqlCommand sc = new SqlCommand(getOrderHeaderKWBSQL(id), c);
            SqlDataReader sdr = sc.ExecuteReader(CommandBehavior.SingleRow);

            if (sdr.Read()) {
                //_OrderNo = sdr["ORHE_NO"].ToString();
                _OrderNo = sdr["DetailSeq"].ToString();

                sb.AppendLine(String.Format("UNB+UNOA:2+8713783439227:14+{0}:14+{1}:{2}+{3}++CAB V1.7++0'", sdr["GrowerEAN"].ToString(), DateTime.Now.ToString("yyMMdd"), DateTime.Now.ToString("HHmm"), _OrderNo));
                sb.AppendLine(String.Format("UNH+{0}+ORDERS:D:02A:UN:DF0221'", _OrderNo));
                sb.AppendLine(String.Format("BGM+220:42:6+{0}:1'", _OrderNo));
                sb.AppendLine(String.Format("DTM+137:{0}:203'", sdr["TransactionDate"].ToString()));
                //sb.AppendLine(String.Format("DTM+2:{0}:203'", sdr["DeliveryDate"].ToString()));
                buyerEAN = sdr["BuyerEAN"].ToString();
                growerEAN = sdr["GrowerEAN"].ToString();
                deliverDate = sdr["DeliveryDate"].ToString();
                growLoc = sdr["GROWER_LOCATION"].ToString();
                transactionDate = sdr["TransactionDate"].ToString();
                //sb.AppendLine(String.Format("NAD+BY+{0}:160:9'", sdr["BuyerEAN"].ToString()));
                //sb.AppendLine(String.Format("NAD+SE+{0}:160:9'", sdr["GrowerEAN"].ToString()));
                //sb.AppendLine(String.Format("NAD+DP+{0}:160:9'", sdr["BuyerEAN"].ToString()));
                //sb.AppendLine(String.Format("CUX+7:EUR'"));
                i = i + 3;
            } else {
                return "";
            }

            sdr.Close();
            sc.CommandText = getOrderDetailsKWBSQL(id);
            sdr = sc.ExecuteReader();
            int lineNo = 1;
            decimal d = 0.00M;
            System.Globalization.NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            SqlDataReader sdrProp;
            SqlConnection cProp = new SqlConnection(CABConnStr);
            SqlDataReader sdrPropVBN;
            StringBuilder sbRFF = new StringBuilder();
            cProp.Open();

            while (sdr.Read()) {
                sb.AppendLine(String.Format("LIN+{0}++{1}:CC:57:VBN'", lineNo, sdr["VBNCode"].ToString()));

                if (string.IsNullOrEmpty(sdr["GROWER_ID"].ToString()))
                    sb.AppendLine(String.Format("PIA+5+{0}:IN:57:92'", sdr["CAB_CODE"]));
                else
                    sb.AppendLine(String.Format("PIA+5+{0}:SA:57:91'", sdr["GROWER_ID"]));

                //sb.AppendLine(String.Format("IMD+++{0}:161:86:{1}'", sdr["CAB_CODE"].ToString(), sdr["CAB_DESC"].ToString()));
                var imd = String.Format("{0} {1}", sdr["CAB_DESC"], sdr["PROP_SHAPE"]);
                if(imd.Length > 35) imd = imd.Substring(0, 35);


                if (string.IsNullOrEmpty(sdr["GROWER_ID"].ToString()))
                    sb.AppendLine(String.Format("IMD+++{0}:161:86:{1}'", sdr["CAB_CODE"], imd));
                else
                    sb.AppendLine(String.Format("IMD+++{0}:161:86:{1}'", sdr["GROWER_ID"], imd)); 


                sb.AppendLine(String.Format("QTY+21:{0}:1'", sdr["Quantity"]));
                sb.AppendLine(String.Format("DTM+2:{0}:203'", deliverDate));
                sb.AppendLine(String.Format("DTM+137:{0}:203'", transactionDate));

                if (!string.IsNullOrEmpty(sdr["REMARK"].ToString())) {
                    sb.AppendLine(String.Format("FTX+COI+++{0}:'", sdr["REMARK"]));        // Do not include if remark is empty, i-1?
                    i++;
                }

                sdrProp = new SqlCommand(getOrderDetailPropertiesSQL(int.Parse(sdr["ORDE_SEQUENCE"].ToString())), cProp).ExecuteReader();
                sbRFF.Length = 0;

                while (sdrProp.Read()) {
                    if (sdrProp["VBNPropValue"].ToString().Length > 0) {
                        sb.AppendLine(String.Format("CCI+++{0}:161:VBN'", sdrProp["VBNPropCode"].ToString().ToUpper()));
                        sb.AppendLine(String.Format("CAV+{0}:161:VBN'", sdrProp["VBNPropValue"].ToString().ToUpper()));
                        i = i + 2;
                    }
                    //if (sdrProp["CABPropValue"].ToString().Length > 0)
                    //{
                    //	sbRFF.AppendLine(String.Format("RFF+{0}:{1}'", sdrProp["CABPropCode"].ToString().ToUpper(), sdrProp["CABPropValue"].ToString().ToUpper()));
                    //	i = i + 1;
                    //}
                }
                sdrProp.Close();
                // TODO
                sdrPropVBN = new SqlCommand(getOrderDetailPropertiesVBNSQL(int.Parse(sdr["ORDE_SEQUENCE"].ToString())), cProp).ExecuteReader();
                while (sdrPropVBN.Read()) {
                    if (sdrPropVBN["VBNPropValue"].ToString().Length > 0) {
                        if (sdrPropVBN["VBNPropCode"].ToString() == "S99") {
                            //                            sb.AppendLine(String.Format("CCI+++{0}:161:VBN'", sdrProp["VBNPropCode"].ToString().ToUpper()));
                            //                            sb.AppendLine(String.Format("CAV+{0}:161:VBN'", sdrProp["VBNPropValue"].ToString().ToUpper()));
                            //sb.AppendLine(String.Format("IMD++{0}+:::{1}'", sdrPropVBN["VBNPropCode"].ToString(), sdrPropVBN["VBNPropValue"].ToString()));
                        } else {
                            sb.AppendLine(String.Format("CCI+++{0}:161:VBN'", sdrPropVBN["VBNPropCode"].ToString().ToUpper()));
                            sb.AppendLine(String.Format("CAV+{0}:161:VBN'", sdrPropVBN["VBNPropValue"].ToString().ToUpper()));
                            i = i + 2;
                            //   sb.AppendLine(String.Format("IMD++{0}+{1}'", sdrPropVBN["VBNPropCode"].ToString(), sdrPropVBN["VBNPropValue"].ToString()));
                        }

                    }
                }
                sdrPropVBN.Close();
                decimal.TryParse(sdr["Price"].ToString(), out d);
                sb.AppendLine(String.Format(nfi, "PRI+INV:{0:0.000}::32:1:1'", d));
                //sb.Append(sbRFF);
                sb.AppendLine("CUX+7:EUR'");
                sb.AppendLine(String.Format("PAC++2+{0}:67:VBN'", sdr["DetCask"]));
                sb.AppendLine(String.Format("MEA+AAE++1:{0}'", sdr["Noipu"])); //Aantallen in stuks per fust.
                //sb.AppendLine("PAC++3+2:146:EF'");
                sb.AppendLine(String.Format("PAC++3+{0}:146:EF'", sdr["BulkCask"]));
                sb.AppendLine(String.Format("MEA+AAE++1:{0}'", sdr["PALL"].ToString())); // Aantallen in stuks op een kar. Dus CACA_NUM_OF_UNITS * CACA_UNITS_PER_LAYER * CACA_LAYERS_PER_TROLLEY 
                sb.AppendLine(String.Format("MEA+AAE++4:{0}'", sdr["LPT"].ToString())); // Aantal lagen! , dus CACA_LAYERS_PER_TROLLEY
                sb.AppendLine(String.Format("NAD+BY+{0}:160:9'", buyerEAN));
                sb.AppendLine(String.Format("RFF+ON:{0}'", sdr["ORDE_REF"]));
                sb.AppendLine(String.Format("NAD+SE+{0}:160:9'", growerEAN));
                sb.AppendLine(String.Format("RFF+VN:{0}'", sdr["ORDE_SEQUENCE"]));

                //Zou een locatie moeten zijn, maar ja hebben we die ook?
                if (String.IsNullOrEmpty(growLoc))
                    sb.AppendLine(String.Format("NAD+DP+{0}:160:9'", growerEAN));
                else
                    sb.AppendLine(String.Format("NAD+DP+{0}:160:9'", growLoc));

                i = i + 18;
                lineNo++;
            }

            cProp.Close();
            sdr.Close();
            c.Close();
            i = i + 2; // UNS + UNT
            sb.AppendLine("UNS+S'");
            sb.AppendLine(String.Format("UNT+{0}+{1}'", i, _OrderNo));
            sb.AppendLine(String.Format("UNZ+1+{0}'", _OrderNo));
            return sb.ToString();
        }
        private string getOrderHeaderSQL(int orderSequence) {
            // Voorlopig de query aanhouden zoals deze ook in CAB gebruikt wordt. Als er meer tijd is naar de DTE (?)
            string SQLH = @"select TOP 1 ORHE_NO,
                            'CLOCKT' MsgType,
                            case when charindex('@florinet.', B.ORGA_MAILBOX) > 0
                                then upper(substring(B.ORGA_MAILBOX,0,charindex('@florinet.', B.ORGA_MAILBOX)))               
                                else 'MB01559' end as ReverseAddress,  
		        
                            case when len(G.ORGA_VBA_NUMBER) > 0                    
                                then 
			                        case isnull(BEKT_BY_VBA, '') 
				                        when '' 
				                        then B.ORGA_VBA_NUMBER 
				                        else BEKT_BY_VBA 
			                        end                        
                                else 
			                        case when len(G.ORGA_BVH_NUMBER) > 0                            
                                            then 
						                        case isnull(BEKT_BY_BVH, '') 
						                        when '' 
						                        then B.ORGA_BVH_NUMBER 
						                        else BEKT_BY_BVH 
					                        end                              
                                            else 
						                        case isnull(BEKT_BY_VBA, '') 
						                        when '' 
						                        then B.ORGA_VBA_NUMBER 
						                        else BEKT_BY_VBA 
					                        end 
				                        end
			                        end BuyerID,

	                        CASE WHEN LEN(G.ORGA_EAN_CODE) > 0     
                                    THEN G.ORGA_EAN_CODE            
			                        ELSE '8714231140214'
	                        END AS SenderEAN,
                            case when isnull(BEKT_UNB_GROWER_EAN,1) = 1
                                then case when len (G.ORGA_EAN_CODE) > 0     
                                    then G.ORGA_EAN_CODE            
                                    else '0' end         
                                else case when len(G.ORGA_VBA_NUMBER) > 0             
                                            then '8714231140214'            
                                            else case when len(G.ORGA_BVH_NUMBER) > 0               
                                                    then '8714231140184'                  
                                                    else '8714231140214' end end end AuctionEAN,      
                            case when len(G.ORGA_VBA_NUMBER) > 0             
                                then isnull(BEKT_FLA_VBA,'64')      
                                else case when len(G.ORGA_BVH_NUMBER) > 0           
                                        then isnull(BEKT_FLA_BVH,'65')         
                                        else isnull(BEKT_FLA_VBA,'64') end end AuctionID,  
                            dbo.FormatDate(CASE WHEN ORHE_DELIVERY_DATE < GETDATE() THEN DATEADD(HOUR, 3, GETDATE()) ELSE ORHE_DELIVERY_DATE END,'yyyyMMdd') DeliveryDate,
                            dbo.FormatDate(getdate(),'yyyyMMdd') TransactionDate,
                            ORHE_DESC,
                            ORHE_NETT_HEADER_VALUE,
                            ORDE_SEQUENCE AS DetailSeq 
                        from F_CAB_ORDER_HEA   
                        join F_CAB_BUYER on ORHE_FK_BUYER = BUYR_SEQUENCE 
                        join F_CAB_GROWER on ORHE_FK_GROWER = GROW_SEQUENCE  
                        join F_CAB_ORGANISATION B on BUYR_FK_ORGA = B.ORGA_SEQUENCE 
                        join F_CAB_ORGANISATION G on GROW_FK_ORGA = G.ORGA_SEQUENCE  
                        JOIN dbo.F_CAB_ORDER_DET WITH (NOLOCK) ON  ORHE_SEQUENCE = ORDE_FK_ORDER_HEA
                        left outer join F_CAB_BUYER_EKT on BEKT_FK_BUYER = BUYR_SEQUENCE
                        where ORDE_EKT_SEND_YN = 0  AND ORHE_SEQUENCE = " + orderSequence.ToString();
            return SQLH;

        }
        private string getOrderDetailsSQL(int orderSequence) {
            string SQLD = @" SELECT (SELECT TOP 1 VBNC_VBN_CODE 
										FROM dbo.F_CAB_CD_VBN_CODE WITH (NOLOCK) 
										JOIN dbo.F_CAB_CAB_VBN_MATRIX WITH (NOLOCK) ON CVMA_FK_VBN_CODE = VBNC_SEQUENCE  
										WHERE CVMA_FK_CAB_CODE = ORDE_FK_CAB_CODE) AS VBNCode, 
										dbo.FormatDate(GETDATE(),'HHHHnn') + RIGHT('00' + CONVERT(VARCHAR, DATEPART(SS, GETDATE())), 2) AS LINTime, 
										CASE WHEN LEN(G.ORGA_VBA_NUMBER) > 0  
											 THEN G.ORGA_VBA_NUMBER  
											 ELSE CASE WHEN LEN(G.ORGA_BVH_NUMBER) > 0  
													   THEN G.ORGA_BVH_NUMBER  
													   ELSE G.ORGA_VBA_NUMBER 
												  END 
										END AS GrowerID, 
										ORDE_NUM_OF_ITEMS AS Quantity, 
										CABC_CAB_CODE AS CABCode,
										ORDE_NUM_OF_UNITS AS NumberOfUnits, 
										ORDE_NUM_OF_ITEMS_PER_UNIT AS NumberOfItemsPerUnit, 
										CONVERT(NUMERIC(10,2), ORDE_ITEM_PRICE) AS Price, 
										CDCA_CASK_CODE AS CASKCode, 
										ORDE_SEQUENCE,
										CABC_CAB_DESC AS CAB_DESC,
										ORDE_CUSTOMER_CODE AS CUST_REF,
										ISNULL(ORDE_EXT_REF_ID, '') AS ORDER_REF	
								FROM dbo.F_CAB_ORDER_HEA WITH (NOLOCK)
								JOIN dbo.F_CAB_ORDER_DET WITH (NOLOCK) ON ORDE_FK_ORDER_HEA = ORHE_SEQUENCE  
								JOIN dbo.F_CAB_CD_CASK WITH (NOLOCK) ON ORDE_FK_CASK_CODE = CDCA_SEQUENCE 
								JOIN dbo.F_CAB_GROWER WITH (NOLOCK) ON ORHE_FK_GROWER = GROW_SEQUENCE 
								JOIN dbo.F_CAB_ORGANISATION AS G WITH (NOLOCK) ON GROW_FK_ORGA = G.ORGA_SEQUENCE 
								JOIN dbo.F_CAB_CD_CAB_CODE WITH (NOLOCK) ON ORDE_FK_CAB_CODE = CABC_SEQUENCE      
								WHERE ORDE_NUM_OF_ITEMS > 0 
									AND ORDE_FK_ORDER_DET_STATUS IN (6,8) 
									AND ORDE_EKT_SEND_YN = 0  
									AND ORHE_SEQUENCE = " + orderSequence.ToString();
            return SQLD;
        }
        private string getOrderDetailPropertiesVBNSQL(int orderDetailSequence) {
            string SQLP = @"
            select cdvp_vbn_prop_code as VBNPropCode, cagd_value as VBNPropValue from F_CAB_ORDER_DET
			join dbo.F_CAB_ORDER_HEA on orhe_sequence = orde_fk_order_hea
			join dbo.F_CAB_GROWER_ASSORTIMENT on orhe_fk_grower = gras_fk_grower
			join dbo.F_CAB_GRAS_DETAIL on gras_sequence = CAGD_FK_GRAS_SEQ
			join dbo.F_CAB_CD_GRAS_PROP_TYPE on CAGD_FK_CDGP_SEQ = cdgp_sequence
			join dbo.F_cab_cd_vbn_prop_type on cdvp_sequence = CDGP_FK_VBN_PROP_TYPE
			where ORDE_SEQUENCE =  " + orderDetailSequence.ToString() + @" and gras_fk_cab_code = ORDE_FK_CAB_CODE";
            return SQLP;

        }
        private string getOrderDetailPropertiesSQL(int orderDetailSequence) {
            string SQLP = @" 
                SELECT	ISNULL(CDVP_VBN_PROP_CODE,'') AS VBNPropCode, 
		                ISNULL(MAX(CDVL_LOOKUP_VALUE),'') AS VBNPropValue, 
		                MAX(CABD_VALUE) AS CABPropValue, 
		                MAX(CDPT_DESC_PREFIX) AS CABPropCode 
                FROM dbo.F_CAB_ORDER_DET WITH (NOLOCK)
                JOIN dbo.F_CAB_CAB_DETAIL WITH (NOLOCK) ON ORDE_FK_CAB_CODE = CABD_FK_CAB_CODE 
                JOIN dbo.F_CAB_CD_CAB_PROP_TYPE WITH (NOLOCK) ON CABD_FK_TYPE = CDPT_SEQUENCE 
                JOIN dbo.F_CAB_CD_CAB_PROP_LOOKUP WITH (NOLOCK) ON CDPL_FK_PROP_TYPE = CDPT_SEQUENCE and CDPL_LOOKUP_VALUE = CABD_VALUE 
                LEFT OUTER JOIN dbo.F_CAB_CD_VBN_PROP_TYPE WITH (NOLOCK) ON CDPT_FK_VBN_PROP_TYPE = CDVP_SEQUENCE 
                LEFT OUTER JOIN dbo.F_CAB_CD_CAB_VBN_LK_MATRIX WITH (NOLOCK) ON CVLM_FK_CAB_LOOKUP_VALUE = CDPL_SEQUENCE 
                LEFT OUTER JOIN dbo.F_CAB_CD_VBN_PROP_LOOKUP WITH (NOLOCK) ON CVLM_FK_VBN_LOOKUP_VALUE = CDVL_SEQUENCE 
                WHERE ORDE_SEQUENCE =  " + orderDetailSequence.ToString() + @"
	                AND ISNULL(CDVL_LOOKUP_VALUE, '') <> ''
                    AND ISNULL(CDVP_VBN_PROP_CODE, '') <> 'S99'
                GROUP BY CDVP_VBN_PROP_CODE
                ORDER BY VBNPropCode";
            //Omschrijving zit al in het imd segment, dit is dus overbodig.
            /*UNION  
            SELECT	'S99',
                    VBNC_DESC, 
                    VBNC_DESC,
                    'Oms' 
            FROM dbo.F_CAB_ORDER_DET WITH (NOLOCK) 
            JOIN dbo.F_CAB_CAB_VBN_MATRIX WITH (NOLOCK) ON ORDE_FK_CAB_CODE = CVMA_FK_CAB_CODE  
            JOIN dbo.F_CAB_CD_VBN_CODE WITH (NOLOCK) ON CVMA_FK_VBN_CODE = VBNC_SEQUENCE 
            WHERE ORDE_SEQUENCE =  " + orderDetailSequence.ToString() + @"
            ORDER BY VBNPropCode";*/
            return SQLP;
        }
        private string getOrderHeaderKWBSQL(int orderSequence) {
            //dbo.FormatDate(CASE WHEN ORHE_DELIVERY_DATE < GETDATE() THEN DATEADD(HOUR, 3, GETDATE()) ELSE ORHE_DELIVERY_DATE END,'yyyymmddHHHHnn') AS DeliveryDate, 
            string SQLH = @"SELECT TOP 1 B.ORGA_VBA_NUMBER BuyerID, 
                        dbo.FormatDate(GETDATE(),'yyyymmddHHHHnn') AS TransactionDate, 
                        dbo.FormatDate(CASE WHEN ORHE_DELIVERY_DATE < GETDATE() THEN CAST(CONVERT(VARCHAR, GETDATE(), 102) + ' ' + '11:00' AS DATETIME) ELSE CAST(CONVERT(VARCHAR, ORHE_DELIVERY_DATE, 102) + ' ' + '11:00' AS DATETIME) END,'yyyymmddHHHHnn') AS DeliveryDate, 
                        ORHE_NO, 
                        ORHE_DESC, 
                        ORHE_NETT_HEADER_VALUE PriceTotal, 
                        CASE G.ORGA_EAN_CODE WHEN '' THEN '87#' + G.ORGA_NAME_UPPER + '#' ELSE G.ORGA_EAN_CODE END AS GrowerEAN, 
                        CASE B.ORGA_EAN_CODE WHEN '' THEN '87#' + B.ORGA_NAME_UPPER + '#' ELSE B.ORGA_EAN_CODE END AS BuyerEAN, 
                        '8714231140214' AS AuctionEAN,
                        ADDR_GLN_LOCATION_CODE AS GROWER_LOCATION,
						ORDE_SEQUENCE AS DetailSeq
                FROM dbo.F_CAB_ORDER_HEA WITH (NOLOCK)
                JOIN dbo.F_CAB_GROWER WITH (NOLOCK) ON ORHE_FK_GROWER = GROW_SEQUENCE 
                JOIN dbo.F_CAB_BUYER WITH (NOLOCK) ON ORHE_FK_BUYER = BUYR_SEQUENCE 
                JOIN dbo.F_CAB_ORGANISATION AS G WITH (NOLOCK) ON G.ORGA_SEQUENCE = GROW_FK_ORGA 
                JOIN dbo.F_CAB_ORGANISATION AS B WITH (NOLOCK) ON B.ORGA_SEQUENCE = BUYR_FK_ORGA 
				JOIN dbo.F_CAB_ORDER_DET WITH (NOLOCK) ON  ORHE_SEQUENCE = ORDE_FK_ORDER_hEA
                LEFT OUTER JOIN dbo.F_CAB_ADDRESS WITH (NOLOCK) ON G.ORGA_SEQUENCE = ADDR_FK_ORGANISATION
                WHERE  ORDE_KWB_SEND_YN = 0 AND ORHE_SEQUENCE = " + orderSequence.ToString();
            return SQLH;
        }
        private string getOrderDetailsKWBSQL(int orderSequence) {
            string SQLD = @" 
                SELECT  VBNC_VBN_CODE AS VBNCode, 
                        VBNC_DESC AS VBNDesc, 
                        ORDE_NUM_OF_ITEMS_PER_UNIT AS Noipu, 
                        ORDE_NUM_OF_ITEMS AS Quantity, 
                        ORDE_NUM_OF_UNITS AS NumberOfUnits, 
                        CONVERT(NUMERIC(10,3), ORDE_ITEM_PRICE) AS Price,
                        ORDE_REMARK, 
                        Blk.CDCA_ID AS BulkCask, 
                        Det.CDCA_CASK_CODE AS DetCask, 
                        ORDE_SEQUENCE,
                        CASE ISNULL(ORDE_CUSTOMER_CODE, '') WHEN '' THEN CAST(ORDE_SEQUENCE AS VARCHAR(50)) ELSE ORDE_CUSTOMER_CODE END AS ORDE_REF,
                        CACA_NUM_OF_UNITS AS NOU, 
                        CACA_UNITS_PER_LAYER AS UPL, 
                        CACA_LAYERS_PER_TROLLEY AS LPT, 
                        CACA_NUM_OF_UNITS * CACA_UNITS_PER_LAYER * CACA_LAYERS_PER_TROLLEY AS PALL,
						CABC_CAB_CODE AS CAB_CODE,
						CABC_CAB_DESC AS CAB_DESC,
                        ISNULL((SELECT CASE WHEN MAX(PROP_FK_CDMG_SEQ) = 2 THEN MAX(PROP_VAL4) ELSE '' END FROM dbo.F_CAB_CAB_PROP WITH (NOLOCK) WHERE PROP_FK_CABC_SEQ = CABC_SEQUENCE AND PROP_FK_CDLA_SEQ = 1), '') AS PROP_SHAPE,
                        ORDE_REMARK AS REMARK,
                        ISNULL(GRAS_GROWER_ID , '') AS GROWER_ID
                FROM dbo.F_CAB_ORDER_HEA WITH (NOLOCK)
                JOIN dbo.F_CAB_ORDER_DET WITH (NOLOCK) ON ORDE_FK_ORDER_HEA = ORHE_SEQUENCE  
                JOIN dbo.F_CAB_CAB_VBN_MATRIX WITH (NOLOCK) ON CVMA_FK_CAB_CODE = ORDE_FK_CAB_CODE 
                JOIN dbo.F_CAB_CD_VBN_CODE WITH (NOLOCK) ON CVMA_FK_VBN_CODE = VBNC_SEQUENCE 
                JOIN dbo.F_CAB_CD_CASK AS Blk WITH (NOLOCK) ON ORHE_FK_BULK_CASK = Blk.CDCA_SEQUENCE 
                JOIN dbo.F_CAB_CD_CASK AS Det WITH (NOLOCK) ON ORDE_FK_CASK_CODE = Det.CDCA_SEQUENCE 
                JOIN dbo.F_CAB_CD_CAB_CODE AS C WITH (NOLOCK) ON ORDE_FK_CAB_CODE = C.CABC_SEQUENCE
                JOIN dbo.F_CAB_CAB_CASK_MATRIX WITH (NOLOCK) ON C.CABC_SEQUENCE = CACA_FK_CAB_CODE AND ORDE_FK_CACA_SEQ = CACA_SEQUENCE   
                JOIN dbo.F_CAB_GROWER WITH (NOLOCK) ON ORHE_FK_GROWER = GROW_SEQUENCE
                JOIN dbo.F_CAB_GROWER_ASSORTIMENT WITH (NOLOCK) ON GROW_SEQUENCE = GRAS_FK_GROWER AND GRAS_FK_CAB_CODE = C.CABC_SEQUENCE AND GRAS_SELECTED_YN = 1  
                WHERE 
                    ORDE_NUM_OF_ITEMS > 0 
					AND ORDE_FK_ORDER_DET_STATUS IN (6,8) 
					AND ORDE_KWB_SEND_YN = 0  
                    AND ORHE_SEQUENCE = " + orderSequence.ToString();
            return SQLD;
        }

        public void UpdateDetailsKWBSent(int id) {
            try { 
                var details = ctx.F_CAB_ORDER_DET.Where(x => x.ORDE_FK_ORDER_HEA == id);
                foreach (var d in details) {
                    d.ORDE_KWB_SEND_YN = 1;
                }
                ctx.SaveChanges();
                UpdateHeaderStatus(id);
            } catch (Exception ex) {
                Log.ErrorFormat("Error in UpdateDetailsKWBSent: Header: {0}, Ex:  {1}", id, ex.ToString());
            }
        }
        public void UpdateDetailsEKTSent(int id) {
            try {
                var details = ctx.F_CAB_ORDER_DET.Where(x => x.ORDE_FK_ORDER_HEA == id);
                foreach (var d in details) {
                    d.ORDE_EKT_SEND_YN = 1;
                }
                ctx.SaveChanges();
                UpdateHeaderStatus(id);
            } catch (Exception ex) {
                Log.ErrorFormat("Error in UpdateDetailsEKTSent: Header: {0}, Ex:  {1}", id, ex.ToString());
            }
        }
        private void UpdateHeaderStatus(int id) {
            var header = ctx.F_CAB_ORDER_HEA.Single(x => x.ORHE_SEQUENCE == id);
            var anyKWBSentMissing = ctx.F_CAB_ORDER_DET.Any(x => x.ORDE_FK_ORDER_HEA == id && x.ORDE_KWB_SEND_YN == 0);
            var anyEKTSentMissing = ctx.F_CAB_ORDER_DET.Any(x => x.ORDE_FK_ORDER_HEA == id && x.ORDE_EKT_SEND_YN == 0);
            if (!anyKWBSentMissing && !anyEKTSentMissing) header.ORHE_FK_ORDER_STATUS = OrderHeaderStatus.EKTKWBSENT;
            if (!anyKWBSentMissing && anyEKTSentMissing) header.ORHE_FK_ORDER_STATUS = OrderHeaderStatus.KWBSENT;
            if (anyKWBSentMissing && !anyEKTSentMissing) header.ORHE_FK_ORDER_STATUS = OrderHeaderStatus.EKTSENT;
            if (anyKWBSentMissing && anyEKTSentMissing) header.ORHE_FK_ORDER_STATUS = OrderHeaderStatus.CONFIRMED;
            ctx.SaveChanges();
        }
    }
}
