﻿using CAB.Data.EF.Models;
using CAB.Domain.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Configuration;

namespace CAB.Domain.Providers {
    public class DeliveryDatesProvider : IDeliveryDatesProvider { // From interface incase we decide on a different implementation of getting the dates.
        private Entities ctx;
        public ILog Log { get; private set; }
        private string ConnectionString;
        private static string LoggingLevel;
        public DeliveryDatesProvider(IDataProvider dataProvider, ILog log) {
            ctx = dataProvider.GetContext() as Entities;
            EntityConnection ec = (EntityConnection)ctx.Connection;
            SqlConnection sc = (SqlConnection)ec.StoreConnection;
            ConnectionString = sc.ConnectionString;
            LoggingLevel = ConfigurationManager.AppSettings["Loggingdetail"];
            Log = log;
        }

        public List<DeliveryDate> Get(int buyer, int maingroup, string Type = "", int Sequence = -1) {
            if (LoggingLevel == "All")
                Log.InfoFormat("Executing proc_GetPossibleDeliveryDates: \t Buyer:{0}, Type: {1}, Sequence: {2}", buyer, Type, Sequence);
            using (var con = new SqlConnection(ConnectionString)) {
                con.Open();
                using (SqlCommand cmd = ((SqlConnection)con).CreateCommand()) {
                    cmd.CommandText = "Proc_GetPossibleDeliveryDates";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@BUYR_SEQ", SqlDbType.Int);
                    cmd.Parameters[0].Value = buyer;
                    if (!String.IsNullOrEmpty(Type)) {
                        cmd.Parameters.Add("@OFHD_TYPE", SqlDbType.VarChar);
                        cmd.Parameters[1].Value = Type;
                    }
                    if (Sequence > -1) {
                        cmd.Parameters.Add("@OFHD_SEQ", SqlDbType.Int);
                        cmd.Parameters[2].Value = Sequence;
                    }                    
                    try {
                        var reader = cmd.ExecuteReader();
                        List<DeliveryDate> delDates = new List<DeliveryDate>();
                        while (reader.Read()) {
                            DeliveryDate nDate = new DeliveryDate();
                            nDate.Organisation = (int)reader["ORGA_SEQ"];
                            nDate.OfferHeader = reader["OFHD_SEQ"] as int?;
                            nDate.Maingroup = (int)reader["MAIN_GROUP"];
                            nDate.Internal = (bool?)reader["Internal"];
                            nDate.Grower = (string)reader["GROWER"];
                            nDate.ValidFrom = (DateTime)reader["VALID_FROM"];
                            nDate.ValidTo = (DateTime)reader["VALID_TO"];
                            nDate.OfferType = (string)reader["OFHD_TYPE"];
                            nDate.OrderDate = (DateTime)reader["ORDER_DATE"];
                            nDate.DelDate = (DateTime)reader["DEL_DATE"];
                            nDate.NumDetails = (int)reader["Nr_Det"];
                            delDates.Add(nDate);
                        }
                        if (LoggingLevel == "DAL") {
                            Log.Info("Completed executing proc_GetPossibleDeliveryDates. Result:");
                            int counter = 1;
                            foreach (DeliveryDate d in delDates) {
                                Log.InfoFormat("[{11}]:: Orga:{0}, Seq:{1}, Mg:{2}, Int:{3}, Grow:{4}, VF:{5}, VT:{6}, Type:{7}, OD:{8}, DD:{9},Num:{10},", 
                                    d.Organisation, d.OfferHeader, d.Maingroup, d.Internal, d.Grower, d.ValidFrom, d.ValidTo, d.OfferType, d.OrderDate, d.DelDate, d.NumDetails, counter++);
                            }
                        }
                        if (maingroup != -1)
                            return delDates.Where(x => x.Maingroup == maingroup).ToList();
                        return delDates;
                    } catch (Exception ex) {
                        Log.ErrorFormat("Error while executing Proc_GetPossibleDeliveryDates: {0}", ex.ToString());
                    }
                }
                con.Close();
                con.Dispose();
            }
            return null;
        }
    }
}
