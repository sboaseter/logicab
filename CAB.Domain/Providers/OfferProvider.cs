﻿using CAB.Data.EF.Models;
using CAB.Domain.Helpers;
using CAB.Domain.Models;
using CAB.Domain.Repositories;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CAB.Domain.Providers {
    public class OfferProvider : IOfferProvider {
        private ISettingsProvider _settingsProvider;
        private IGeneralRelationsRepository _generalRelationsRepository;
        private ICabcodeRepository _cabcodeRepository;
        public IQueryable<OfferDetail> Offerdetails;
        private Entities ctx;
        private ILog Log;
        private IQueryable<OfferDetail> OfferQuery;
        string[] categories = { "Grouptype", "Group", "Value3", "Value1", "Value2", "Value4", "Value5", "Value6", "Value7", "Value8" };
        public OfferProvider(
            IDataProvider dataProvider, 
            ISettingsProvider settingsProvider,
            IGeneralRelationsRepository generalRelationsRepository,
            ICabcodeRepository cabcodeRepository,
            ILog log) {
            DateTime now = DateTime.Now;
            ctx = dataProvider.GetContext() as Entities;
            _settingsProvider = settingsProvider;
            _generalRelationsRepository = generalRelationsRepository;
            _cabcodeRepository = cabcodeRepository;
            Log = log;

            // TODO: Incoroporate _generalRelationsCache: DI GeneralRelationsRepository?
            Offerdetails = 
                from ofdt in ctx.F_CAB_OFFER_DETAILS
                join ofhd in ctx.F_CAB_OFFER_HEADERS on ofdt.OFDT_FK_OFFER_HEADER equals ofhd.OFHD_SEQUENCE
                join caca in ctx.F_CAB_CAB_CASK_MATRIX on ofdt.OFDT_FK_CACA_SEQ equals caca.CACA_SEQUENCE
                join cask in ctx.F_CAB_CD_CASK on caca.CACA_FK_CASK equals cask.CDCA_SEQUENCE
                join gras in ctx.F_CAB_GROWER_ASSORTIMENT on ofdt.OFDT_FK_GROWER_ASSORTIMENT equals gras.GRAS_SEQUENCE
                join prop in ctx.F_CAB_CAB_PROP on ofdt.F_CAB_CD_CAB_CODE.CABC_SEQUENCE equals prop.PROP_FK_CABC_SEQ
                join grel in ctx.F_CAB_GENERAL_RELATIONS on ofdt.OFDT_FK_GREL_SEQ equals grel.GREL_SEQUENCE into greltable
                //join grel in _generalRelationsRepository.GetAll() on ofdt.OFDT_FK_GREL_SEQ equals grel.Sequence into greltable
                from g_loj in greltable.DefaultIfEmpty()
                where
                    ofhd.OFHD_FK_BUYER == Usr.OrgaTypeSequence &&
                    ofhd.OFHD_DELETED_BY_OWNER_YN == false &&
                    ofhd.OFHD_DELETED_YN == 0 &&
                    prop.PROP_FK_CDLA_SEQ == Usr.Language                              
                select new OfferDetail {
                    Sequence = ofdt.OFDT_SEQUENCE,
                    Buyer = ofhd.OFHD_FK_BUYER,
                    OFHDSequence = ofhd.OFHD_SEQUENCE,
                    Offertype = ofhd.OFHD_OFFER_TYPE,
                    CabCode = ofdt.OFDT_FK_CAB_CODE,
                    Supplier = new CabSupplier {
                        Name = ofdt.OFDT_SUPPLIER,
                        Growername = ofhd.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_NAME,
                        GrelSeq = g_loj == null ? -1 : g_loj.GREL_SEQUENCE
                    },
                    //_PictureSequence = ofdt.OFDT_FK_PICTURE ?? (gras.GRAS_FK_PICTURE ?? ((cabcode.Picture ?? 0))),
                    _PictureSequence = ofdt.OFDT_FK_PICTURE ?? gras.GRAS_FK_PICTURE,
                    _PictureString = ofdt.OFDT_EXT_PICTURE_REF ?? "",
                    Price = ofdt.OFDT_ITEM_PRICE,
                    Properties = new CabProperties {
                        Desc = prop.PROP_CAB_DESC,
                        Group = prop.PROP_GROUP_TRANS_DESC,
                        Grouptype = prop.PROP_GROUP_TYPE_TRANS_DESC,
                        Maingroup = prop.PROP_MAIN_GROUP_TRANS_DESC,
                        MaingroupSeq = prop.PROP_FK_CDMG_SEQ,
                        CABCode = prop.PROP_CABC_CAB_CODE,
                        Value1 = prop.PROP_VAL1,
                        Value2 = prop.PROP_VAL2,
                        Value3 = prop.PROP_VAL3,
                        Value4 = prop.PROP_VAL4,
                        Value5 = prop.PROP_VAL5,
                        Value6 = prop.PROP_VAL6,
                        Value7 = prop.PROP_POTTYPE,
                        Value8 = prop.PROP_POTSIZE
                    },
                    CaskCode = cask.CDCA_CASK_CODE,
                    UnitsPrLayer = caca.CACA_UNITS_PER_LAYER,
                    NumOfUnits = caca.CACA_NUM_OF_UNITS,
                    LayersPrTrolley = caca.CACA_LAYERS_PER_TROLLEY,
                    Available =     (int)ofdt.OFDT_NUM_OF_ITEMS >= (gras.GRAS_NUM_OF_ITEMS / caca.CACA_NUM_OF_UNITS) ?
                                    (int)(gras.GRAS_NUM_OF_ITEMS / caca.CACA_NUM_OF_UNITS) :
                                    (int)ofdt.OFDT_NUM_OF_ITEMS,
                    Remark = ofdt.OFDT_REMARK,
                    OfferNr = ofhd.OFHD_NO,
                    ValidFrom = ofhd.OFHD_VALID_FROM,
                    ValidTo = ofhd.OFHD_VALID_TO,
                    LatestOrderDateTime = ofdt.OFDT_LATEST_ORDER_DATE_TIME ?? ofhd.OFHD_VALID_TO,
                    DeliverTimeEnd = ofdt.OFDT_DELIVERY_DATE_TIME_END ?? ofhd.OFHD_VALID_TO,
                    IsSpecial = ofdt.OFDT_SPECIAL_YN,
                    ArrivalDate = ofdt.OFDT_ARRIVAL_DATE,
                    Caption = ofhd.OFHD_CAPTION
                };
            OfferQuery =
                Offerdetails.Where(x =>
                    x.ValidTo >= now &&
                    x.ValidFrom <= Usr.SelectedDate &&
                    x.Properties.MaingroupSeq == Usr.SelectedMaingroup &&
                    x.Offertype == Usr.SelectedOffertype &&
                    x.Buyer == Usr.OrgaTypeSequence
            );

        }
        public IQueryable<OfferDetail> Get(int mg, string type) {
            if (_settingsProvider.AppSetting("Loggingdetail") == "All") {
                Log.InfoFormat("OfferProvider.Get: BuyerSeq: {0}, SelectedDate: {1}, Language: {2}, OfferType: {3}, Maingroup: {4}",
                    Usr.OrgaTypeSequence,
                    Usr.SelectedDate,
                    Usr.Language,
                    Usr.SelectedOffertype,
                    Usr.SelectedMaingroup
                    );            
            }
            return OfferQuery;
        }

        public Dictionary<string, List<string>> GetCategories(bool filtered = false) {
            Dictionary<string, List<string>> FilterCategories = new Dictionary<string, List<string>>();
            if(Utils.OfferdetailsCache == null || !Utils.OfferdetailsCache.Any(x =>
                                                    x.Properties.MaingroupSeq == Usr.SelectedMaingroup
                                                    && x.Offertype == Usr.SelectedOffertype
                                                    && x.Buyer == Usr.OrgaTypeSequence))
                Utils.OfferdetailsCache = OfferQuery.ToList();

            FilterCategories["Supplier"] = (from c in Utils.OfferdetailsCache select c.Supplier.Growername).Distinct().OrderBy(x => x).ToList();
            foreach (string pName in categories) {
                FilterCategories[pName] = (from c in Utils.OfferdetailsCache select c.Properties.Getprop(pName)).Distinct().OrderBy(x => x).ToList();
            }
            if (Usr.Filters == null) Usr.Filters = new Dictionary<string, List<string>>();
            if (filtered) {
                if (Usr.Filters.ContainsKey("Supplier") && Usr.LastFilter != "Supplier")
                    FilterCategories["Supplier"] = (from c in Utils.OfferdetailsCache.Where(x => Usr.Filters["Supplier"].Contains(x.Supplier.Growername)) select c.Supplier.Growername).Distinct().OrderBy(x => x).ToList();
                foreach (string pName in categories) {
                    if (Usr.LastFilter != pName && Usr.LastFilter != null) {
                        var test1 = (from c in Utils.OfferdetailsCache.Where(x => Usr.Filters[Usr.LastFilter].Contains(x.Properties.Getprop(pName))) select c.Properties.Getprop(pName)).Distinct().OrderBy(x => x).ToList();
                        FilterCategories[pName] = (from c in Utils.OfferdetailsCache.Where(x => Usr.Filters[Usr.LastFilter].Contains(x.Properties.Getprop(pName))) select c.Properties.Getprop(pName)).Distinct().OrderBy(x => x).ToList();
                    }
                }
            }
            return FilterCategories;
        }
    }
}
