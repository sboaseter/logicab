﻿using CAB.Data.EF.Models;
using CAB.Domain.Enums;
using System.Collections.Generic;
using System.Linq;
namespace CAB.Domain.Providers {
    public class FunctionsProvider : IFunctionsProvider {
        private Entities ctx;
        public FunctionsProvider(IDataProvider dataProvider) {
            ctx = dataProvider.GetContext() as Entities;
        }

        public List<string> Get(int organisation) {
            var functions = ctx.F_CAB_ORGANISATION_FUNCTION
                            .Where(x => x.ORFU_FK_ORGA == organisation)
                            .Select(x => x.F_CAB_FUNCTION.FUNC_CODE);
            if (functions != null) return functions.ToList();
            return new List<string>();            
        }


        public List<Function> List() {
            return ctx.F_CAB_FUNCTION.Select(x => (Function)x.FUNC_SEQUENCE).ToList();
        }
    }
}
