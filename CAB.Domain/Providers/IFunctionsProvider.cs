﻿
using CAB.Domain.Enums;
using System.Collections.Generic;
namespace CAB.Domain.Providers {
    public interface IFunctionsProvider {
        List<string> Get(int organisation);
        List<Function> List();
    }
}
