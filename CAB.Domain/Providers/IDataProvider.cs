﻿using System.Data.Entity;
using System.Data.Entity.Core.Objects;

namespace CAB.Domain.Providers {
    public interface IDataProvider {
        ObjectContext GetContext();
    }
}
