﻿using CAB.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace CAB.Domain.Providers {
    public interface IOfferProvider {
        IQueryable<OfferDetail> Get(int mg, string type);
        Dictionary<string, List<string>> GetCategories(bool filtered = false);
    }
}
