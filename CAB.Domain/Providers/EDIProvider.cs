﻿using CAB.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAB.Domain.Providers {
    public class EDIProvider {
        public EDIHeader Header { get; set; }
        public List<EDIData> EDI { get; set; }
        public List<String> Body { get; set; }
        public EDIProvider() {
            Body = new List<String>();
        }
        public string Generate() {
            StringBuilder sb = new StringBuilder();
            foreach (var l in this.Body) {
                sb.AppendLine(l);
            }
            return sb.ToString();
        }
        public bool IsEDIDataValid(EDIData edi) {
            if (Int32.Parse(edi.strPartySize) == 0)
                return false;
            return true;
        }
        public void InsertData(ref String line, bool increaseSgmCount) {
            Body.Add(line);
            Console.WriteLine(String.Format("{0}: {1}", Body.Count, line));
            line = String.Empty;
            this.Header.lngSegmentCount++;
        }
    }
}
