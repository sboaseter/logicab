﻿using CAB.Data.EF.Models;
using System.Collections.Generic;
using System;
using CAB.Domain.Models;
using log4net;
using System.Data.SqlClient;
using System.Data;
using CAB.Domain.Helpers;
using CAB.Domain.Enums;
using System.Data.Entity.Core.EntityClient;
using System.Configuration;
namespace CAB.Domain.Providers {
    public class SettingsProvider : ISettingsProvider {

        private Entities ctx;
        public ILog Log { get; private set; }
        private string ConnectionString;
        private static Dictionary<string, string> settingCache;

        public SettingsProvider(IDataProvider dataProvider, ILog log) {
            ctx = dataProvider.GetContext() as Entities;
            EntityConnection ec = (EntityConnection)ctx.Connection;
            SqlConnection sc = (SqlConnection)ec.StoreConnection;
            ConnectionString = sc.ConnectionString;
            Log = log;
        }

        public SettingsProvider() {
            ctx = new DataProvider().GetContext() as Entities;
            EntityConnection ec = (EntityConnection)ctx.Connection;
            SqlConnection sc = (SqlConnection)ec.StoreConnection;
            ConnectionString = sc.ConnectionString;
        }

        public string AppSetting(string setting) {
            if (settingCache == null) settingCache = new Dictionary<string, string>();
            if (settingCache.ContainsKey(setting)) {
                return settingCache[setting];
            }
            settingCache.Add(setting, ConfigurationManager.AppSettings[setting]);
            return settingCache[setting];
        }

        public List<Setting> Shop(int id) {
            return _get(Utils.ShopEvidence.Id, SettingType.Shop);
        }
        public List<Setting> Organisation(int id) {
            return _get(Usr.OrganisationSequence, SettingType.Organisation);
        }
        public List<Setting> Person(int id) {
            return _get(Usr.Person.Sequence, SettingType.Person);
        }
        public List<Setting> All() {
            return _get(-1, SettingType.All);
        }
        
        private List<Setting> _get(int id, int type) {
            using (var con = new SqlConnection(ConnectionString)) {
                con.Open();
                using (SqlCommand cmd = ((SqlConnection)con).CreateCommand()) {
                    cmd.CommandText = "proc_GetSettings";
                    cmd.CommandType = CommandType.StoredProcedure;
                    switch(type) {
                        case SettingType.Shop:
                            cmd.Parameters.Add("@SHOP_SEQ", SqlDbType.Int);
                            break;
                        case SettingType.Organisation:
                            cmd.Parameters.Add("@ORGA_SEQ", SqlDbType.Int);
                            break;
                        case SettingType.Person:
                            cmd.Parameters.Add("@PERS_SEQ", SqlDbType.Int);
                            break;
                        default:
                            break;
                    }
                    if(id != -1)
                        cmd.Parameters[0].Value = id;
                    try {
                        var reader = cmd.ExecuteReader();
                        List<Setting> settings = new List<Setting>();
                        while (reader.Read()) {
                            Setting s = new Setting();
                            s.Sequence = (int)reader["SEQUENCE"];
                            s.Id = (string)reader["CODE_ID"];
                            s.Description = (string)reader["DESCRIPTION"];
                            s.Value = (string)reader["VALUE"];
                            s.Type = (string)reader["SETTING_TYPE"];
                            settings.Add(s);
                        }
                        return settings;
                    } catch (Exception ex) {
                        Log.ErrorFormat("Error while executing proc_GetSettings: {0}", ex.ToString());
                    }
                }
                con.Close();
                con.Dispose();
            }
            return null;
        }
    }
}
