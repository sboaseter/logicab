﻿using CAB.Domain.Models;

namespace CAB.Domain.Providers {
    public interface IShopEvidenceProvider {
        ShopEvidence Get(string url);
    }
}
