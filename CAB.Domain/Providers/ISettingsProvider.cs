﻿
using CAB.Domain.Models;
using System.Collections.Generic;
namespace CAB.Domain.Providers {
    public interface ISettingsProvider {
        string AppSetting(string setting);
        List<Setting> Shop(int id);
        List<Setting> Organisation(int id);
        List<Setting> Person(int id);
        List<Setting> All();
    }
}
