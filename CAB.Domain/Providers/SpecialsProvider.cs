﻿using CAB.Data.EF.Models;
using CAB.Domain.Enums;
using CAB.Domain.Helpers;
using log4net;
using System;
using System.Linq;

namespace CAB.Domain.Providers {
    public class SpecialsProvider : ISpecialsProvider {
        private Entities ctx;
        private ILog Log;
        private IQueryable<F_CAB_OFFER_DETAILS> _specials;
        public SpecialsProvider(IDataProvider dataProvider, ILog log) {
            ctx = dataProvider.GetContext() as Entities;
            Log = log;
            _specials = ctx.F_CAB_OFFER_DETAILS.Where(
                x => x.OFDT_SPECIAL_YN == true &&
                     x.OFDT_FK_GROWER_ASSORTIMENT != null &&
                     x.F_CAB_OFFER_HEADERS.OFHD_DELETED_YN == 0 &&
                     x.F_CAB_OFFER_HEADERS.OFHD_DELETED_BY_OWNER_YN == false &&
                     x.F_CAB_OFFER_HEADERS.OFHD_VALID_TO > DateTime.Now
                     );
        }

        public int[] Get(int number) {
            return (from s in _specials
                    select
                        s.OFDT_FK_PICTURE ??
                        s.F_CAB_GROWER_ASSORTIMENT.GRAS_FK_PICTURE ??
                        s.F_CAB_CD_CAB_CODE.CABC_FK_PICTURE ??
                        -1).Take(number).ToArray();
        }

        public int[] Get(int number, int buyer) {
            return (from s in _specials.Where(x => x.F_CAB_OFFER_HEADERS.OFHD_FK_BUYER == buyer)
                    select
                        s.OFDT_FK_PICTURE ??
                        s.F_CAB_GROWER_ASSORTIMENT.GRAS_FK_PICTURE ??
                        s.F_CAB_CD_CAB_CODE.CABC_FK_PICTURE ??
                        -1).Take(number).ToArray();
        }

        public int[] Get(int number, int sequence, string type) {

            switch (type.ToString()) {
                case OrgaType.Grower:
                    _specials = _specials.Where(x => x.F_CAB_OFFER_HEADERS.OFHD_FK_GROWER == sequence);
                    break;
                case OrgaType.Buyer:
                    _specials = _specials.Where(x => x.F_CAB_OFFER_HEADERS.OFHD_FK_BUYER == sequence);
                    break;
                case OrgaType.Comm:
                    _specials = _specials.Where(x => x.F_CAB_OFFER_HEADERS.OFHD_FK_BUYER == sequence);
                    break;
                default:
                    break;
            }
            return (from s in _specials
                    select
                        s.OFDT_FK_PICTURE ??
                        s.F_CAB_GROWER_ASSORTIMENT.GRAS_FK_PICTURE ??
                        s.F_CAB_CD_CAB_CODE.CABC_FK_PICTURE ??
                        -1).ToArray();

        }
    }
}
