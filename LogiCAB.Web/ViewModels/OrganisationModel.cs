﻿using CAB.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.ViewModels {
    [Serializable]
    public class OrganisationModel {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Type { get; set; }
        public List<int> Maingroups {get;set;}
        public List<int> Functions { get; set; }
        public int Skin { get; set; }
        public Address Address { get; set; }
        public PersonModel Person { get; set; }
        public LoginModel Login { get; set; }
    }
    [Serializable]
    public class PersonModel {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
    [Serializable]
    public class LoginModel {
        public string Name { get; set; }
        public string Password { get; set; }
        public int Language { get; set; }
    }
}