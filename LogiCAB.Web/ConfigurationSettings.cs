﻿using System.Configuration;
namespace LogiCAB.Web {
    public static class ConfigurationSettings {
        /*
         * TODO: Add getters for configuration from the database; Make sure ConnectionString is valid!
         */
        //test
        private static string _cabConnectionString;
        public static string CabConnectionString {
            get {
                if (!string.IsNullOrEmpty(_cabConnectionString)) return _cabConnectionString;
                _cabConnectionString = ConfigurationManager.ConnectionStrings["WebshopConnection"].ToString();
                return _cabConnectionString;
            }
        }

        private static string _lfConnectionString;
        public static string LfConnectionString {
            get {
                if (!string.IsNullOrEmpty(_lfConnectionString)) return _lfConnectionString;
                _lfConnectionString = ConfigurationManager.ConnectionStrings["WebshopConnection"].ToString();
                return _lfConnectionString;
            }
        }
    }
}