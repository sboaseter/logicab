﻿using CAB.Domain.Helpers;
using CAB.Domain.Enums;
using DevExpress.Utils;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using LogiCAB.Web.DAL;
using LogiCAB.Web.Domain;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.IO;
using log4net;
using System.Reflection;
using System.Configuration;

namespace LogiCAB.Web.Controllers {
    [Authorize]
    [HandleError]
    public class OrderController : Controller {
        public ActionResult Index() {
            return View(getOrders());
        }

        public ActionResult IndexBuyer() {
            return View(getOrdersBuyer());
        }
        public ActionResult IndexGrower() {
            return View(getOrdersGrower());
        }
        public ActionResult ListBuyer() {
            return PartialView("ListBuyer", getOrdersBuyer());
        }
        public ActionResult ListGrower() {
            return PartialView("ListGrower", getOrdersGrower());
        }

        public ActionResult List() {
            return PartialView("List", getOrders());
        }

        public ActionResult ExportTo() {
            var orders = getOrders();
            return GridViewExtension.ExportToXls(CreateExportGridViewSettings(), orders);
        }

        public ActionResult ExportBuyer() {
            var orders = getOrdersBuyer();
            return GridViewExtension.ExportToXls(CreateExportGridViewSettingsBuyer(), orders);
        }

        public ActionResult ExportGrower() {
            return GridViewExtension.ExportToXls(CreateExportGridViewSettingsGrower(), getOrdersGrower());
        }

        public void ExportToStream() {
            GridViewExtension.WriteXls(CreateExportGridViewSettings(), getOrders(), HttpContext.Response.OutputStream);
        }

        static GridViewSettings exportGridViewSettings;
        public static GridViewSettings ExportGridViewSettings {
            get {
                return CreateExportGridViewSettings();
            }
        }

        static GridViewSettings exportGridViewSettingsGrower;
        public static GridViewSettings ExportGridViewSettingsGrower {
            get {
                return CreateExportGridViewSettingsGrower();
            }
        }
        static GridViewSettings exportGridViewSettingsBuyer;
        public static GridViewSettings ExportGridViewSettingsBuyer {
            get {
                return CreateExportGridViewSettingsBuyer();
            }
        }

        static GridViewSettings CreateExportGridViewSettingsBuyer() {
            GridViewSettings settings = new GridViewSettings();

            settings.Name = "gvExport";
            settings.KeyFieldName = "seq";
            settings.Width = Unit.Percentage(100);
            settings.CallbackRouteValues = new { Controller = "Order", Action = "ListBuyer" };

            settings.SettingsPager.Position = PagerPosition.TopAndBottom;

            settings.Settings.ShowFilterRow = true;
            settings.Settings.ShowGroupPanel = true;
            settings.Settings.ShowFooter = true;

            settings.Settings.ShowHeaderFilterButton = true;
            settings.SettingsPopup.HeaderFilter.Height = 200;

            settings.SettingsPager.PageSize = 15;
            settings.SettingsPager.PageSizeItemSettings.Visible = true;

            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "price");
            settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, "price");
            settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, "units").DisplayFormat = "{0} Eenheden";

            settings.SettingsCookies.Enabled = true;

            settings.HtmlRowPrepared = (sender, e) => {
                string status = (string)e.GetValue("status");
                if (status == "Geweigerd") {
                    e.Row.Style.Add("background-color", "rgb(255, 150, 150)");
                }
            };

            settings.Columns.Add(column => {
                column.FieldName = "grower";
                column.Caption = Extensions.dict(Usr.Language, "grower");
                column.GroupIndex = 0;
            });
            settings.Columns.Add(column => {
                column.FieldName = "orheno";
                column.Caption = Extensions.dict(Usr.Language, "ordernr");
            });

            settings.Columns.Add(column => {
                column.FieldName = "date";
                column.Caption = Extensions.dict(Usr.Language, "orderdate");
                column.Width = Unit.Pixel(80);
                
                column.PropertiesEdit.DisplayFormatString = "d-M-yyyy HH:mm";
            });

            settings.Columns.Add(column => {
                column.FieldName = "remark";
                column.Caption = Extensions.dict(Usr.Language, "remark");
            });

            settings.Columns.Add(column => {
                column.FieldName = "cask";
                column.Caption = Extensions.dict(Usr.Language, "cask");
            });

            settings.Columns.Add(column => {
                column.FieldName = "status";
                column.Caption = Extensions.dict(Usr.Language, "status");
            });

            settings.Columns.Add(column => {
                column.FieldName = "desc";
                column.Caption = Extensions.dict(Usr.Language, "desc");
            });
            settings.Columns.Add(column => {
                column.FieldName = "length";
                column.Caption = Extensions.dict(Usr.Language, "length");
            });

            settings.Columns.Add(column => {
                column.FieldName = "custcode";
                column.Caption = Extensions.dict(Usr.Language, "custcode");
            });

            settings.Columns.Add(column => {
                column.FieldName = "items";
                column.Caption = Extensions.dict(Usr.Language, "items");
            });

            settings.Columns.Add(column => {
                column.FieldName = "units";
                column.Caption = Extensions.dict(Usr.Language, "units");
            });

            settings.Columns.Add(column => {
                column.FieldName = "ipu";
                column.Caption = Extensions.dict(Usr.Language, "ipu");
            });

            settings.Columns.Add(column => {
                column.FieldName = "itemprice";
                column.Caption = Extensions.dict(Usr.Language, "pricepritem");
                column.PropertiesEdit.DisplayFormatString = "c";
            });

            settings.Columns.Add(column => {
                column.FieldName = "price";
                column.Caption = Extensions.dict(Usr.Language, "totalprice");
                column.PropertiesEdit.DisplayFormatString = "c";
            });

            settings.Columns.Add(column => {
                column.FieldName = "deliverydate";
                column.Caption = Extensions.dict(Usr.Language, "deliverydate");
                column.Width = Unit.Pixel(80);
                
                column.PropertiesEdit.DisplayFormatString = "d-M-yyyy HH:mm";
            });
            settings.Columns.Add(column => {
                column.FieldName = "transporter";
                column.Caption = Extensions.dict(Usr.Language, "transporter");
            });
            return settings;
        }
        static GridViewSettings CreateExportGridViewSettingsGrower() {
            GridViewSettings settings = new GridViewSettings();

            settings.Name = "gvExport";
            settings.KeyFieldName = "seq";
            settings.Width = Unit.Percentage(100);
            settings.CallbackRouteValues = new { Controller = "Order", Action = "ListGrower" };

            settings.SettingsPager.Position = PagerPosition.TopAndBottom;

            settings.Settings.ShowFilterRow = true;
            settings.Settings.ShowGroupPanel = true;
            settings.Settings.ShowFooter = true;

            settings.Settings.ShowHeaderFilterButton = true;
            settings.SettingsPopup.HeaderFilter.Height = 200;

            settings.SettingsPager.PageSize = 15;
            settings.SettingsPager.PageSizeItemSettings.Visible = true;

            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "price");
            settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, "price");
            settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, "units").DisplayFormat = "{0} Eenheden";

            settings.SettingsCookies.Enabled = true;

            settings.HtmlRowPrepared = (sender, e) => {
                string status = (string)e.GetValue("status");
                if (status == "Geweigerd") {
                    e.Row.Style.Add("background-color", "rgb(255, 150, 150)");
                }
            };


            //settings.PreRender


            settings.Columns.Add(column => {
                column.FieldName = "buyer";
                column.Caption = "Koper";
                column.GroupIndex = 0;
            });
            settings.Columns.Add(column => {
                column.FieldName = "orheno";
                column.Caption = "Order.Nr";
            });

            settings.Columns.Add(column => {
                column.FieldName = "date";
                column.Caption = "Orderdatum";
                column.Width = Unit.Pixel(80);
                
                column.PropertiesEdit.DisplayFormatString = "d-M-yyyy HH:mm";
            });

            settings.Columns.Add(column => {
                column.FieldName = "remark";
                column.Caption = "Order";
            });

            settings.Columns.Add(column => {
                column.FieldName = "cask";
                column.Caption = "Fust";
            });

            settings.Columns.Add(column => {
                column.FieldName = "status";
                column.Caption = "Status";
                column.VisibleIndex = 0;
            });

            settings.Columns.Add(column => {
                column.FieldName = "desc";
                column.Caption = "CAB Omschrijving";
            });
            settings.Columns.Add(column => {
                column.FieldName = "length";
                column.Caption = "Lengte";
            });

            settings.Columns.Add(column => {
                column.FieldName = "custcode";
                column.Caption = "Klant kode";
            });

            settings.Columns.Add(column => {
                column.FieldName = "items";
                column.Caption = "Aantal";
            });

            settings.Columns.Add(column => {
                column.FieldName = "units";
                column.Caption = "Eenheden";
            });

            settings.Columns.Add(column => {
                column.FieldName = "ipu";
                column.Caption = "APE";
            });

            settings.Columns.Add(column => {
                column.FieldName = "itemprice";
                column.Caption = "Item Prijs";
                column.PropertiesEdit.DisplayFormatString = "c";
            });

            settings.Columns.Add(column => {
                column.FieldName = "price";
                column.Caption = "Regelbedrag";
                column.PropertiesEdit.DisplayFormatString = "c";
            });

            settings.Columns.Add(column => {
                column.FieldName = "deliverydate";
                column.Caption = "Deliverydate";
                column.Width = Unit.Pixel(80);
                
                column.PropertiesEdit.DisplayFormatString = "d-M-yyyy HH:mm";
            });
            settings.Columns.Add(column => {
                column.FieldName = "transporter";
                column.Caption = "Transporteur";
            });
            return settings;
        }

        static GridViewSettings CreateExportGridViewSettings() {
            GridViewSettings settings = new GridViewSettings();

            settings.Name = "gvExport";
            settings.KeyFieldName = "seq";
            settings.Width = System.Web.UI.WebControls.Unit.Percentage(100);
            settings.CallbackRouteValues = new { Controller = "Order", Action = "List" };

            settings.SettingsPager.Position = System.Web.UI.WebControls.PagerPosition.TopAndBottom;

            settings.Settings.ShowFilterRow = true;
            settings.Settings.ShowGroupPanel = true;
            settings.Settings.ShowFooter = true;

            settings.Settings.ShowHeaderFilterButton = true;
            settings.SettingsPopup.HeaderFilter.Height = 200;

            settings.SettingsPager.PageSize = 15;
            settings.SettingsPager.PageSizeItemSettings.Visible = true;

            settings.TotalSummary.Add(DevExpress.Data.SummaryItemType.Sum, "price");
            settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, "price");
            settings.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, "units").DisplayFormat = "{0} Eenheden";

//            settings.SettingsCookies.Enabled = true;

            settings.HtmlRowPrepared = (sender, e) => {
                string status = (string)e.GetValue("status");
                if (status == "Geweigerd") {
                    e.Row.Style.Add("background-color", "rgb(255, 150, 150)");
                }
            };

            if (Usr.OrganisationType == OrgaType.Buyer) {
                settings.Columns.Add(column => {
                    column.FieldName = "grower";
                    column.Caption = Extensions.dict(Usr.Language, "grower");
                    column.GroupIndex = 0;
                });
            } else if (Usr.OrganisationType == OrgaType.Grower) {
                settings.Columns.Add(column => {
                    column.FieldName = "buyer";
                    column.Caption = Extensions.dict(Usr.Language, "buyer");
                    column.GroupIndex = 0;
                });
            }
            settings.Columns.Add(column => {
                column.FieldName = "orheno";
                column.Caption = Extensions.dict(Usr.Language, "ordernr");
            });

            settings.Columns.Add(column => {
                column.FieldName = "date";
                column.Caption = Extensions.dict(Usr.Language, "orderdate");
                column.Width = System.Web.UI.WebControls.Unit.Pixel(80);
                column.PropertiesEdit.DisplayFormatString = "d-M-yyyy HH:mm";
            });

            settings.Columns.Add(column => {
                column.FieldName = "status";
                column.Caption = "Status";
                column.VisibleIndex = 0;
            });
            settings.Columns.Add(column => {
                column.FieldName = "remark";
                column.Caption = Extensions.dict(Usr.Language, "remark");
            });

            settings.Columns.Add(column => {
                column.FieldName = "cask";
                column.Caption = Extensions.dict(Usr.Language, "cask");
            });

            settings.Columns.Add(column => {
                column.FieldName = "desc";
                column.Caption = Extensions.dict(Usr.Language, "desc");
            });
            settings.Columns.Add(column => {
                column.FieldName = "length";
                column.Caption = Extensions.dict(Usr.Language, "length");
            });

            settings.Columns.Add(column => {
                column.FieldName = "custcode";
                column.Caption = Extensions.dict(Usr.Language, "custcode");
            });

            settings.Columns.Add(column => {
                column.FieldName = "items";
                column.Caption = Extensions.dict(Usr.Language, "numitems");
            });

            settings.Columns.Add(column => {
                column.FieldName = "units";
                column.Caption = Extensions.dict(Usr.Language, "numunits");
            });

            settings.Columns.Add(column => {
                column.FieldName = "ipu";
                column.Caption = Extensions.dict(Usr.Language, "ipu");
            });

            settings.Columns.Add(column => {
                column.FieldName = "itemprice";
                column.Caption = Extensions.dict(Usr.Language, "pricepritem");
                column.PropertiesEdit.DisplayFormatString = "c";
            });

            settings.Columns.Add(column => {
                column.FieldName = "price";
                column.Caption = Extensions.dict(Usr.Language, "totalprice");
                column.PropertiesEdit.DisplayFormatString = "c";
            });

            settings.Columns.Add(column => {
                column.FieldName = "deliverydate";
                column.Caption = Extensions.dict(Usr.Language, "deliverydate");
                column.Width = System.Web.UI.WebControls.Unit.Pixel(80);
                column.PropertiesEdit.DisplayFormatString = "d-M-yyyy HH:mm";
            });
            settings.Columns.Add(column => {
                column.FieldName = "transporter";
                column.Caption = Extensions.dict(Usr.Language, "transporter");
            });
            return settings;
        }

        public ActionResult setSelectedDate(string startDate, string endDate, string startTime, string endTime) {
            Session["ordersStartDate"] = DateTime.Parse(startDate).Add(TimeSpan.Parse(startTime));
            Session["ordersEndDate"] = DateTime.Parse(endDate).Add(TimeSpan.Parse(endTime));
            return null;
        }

        public delegate ActionResult ExportMethod(GridViewSettings settings, object dataObject);

        public class ExportType {
            public string Title { get; set; }
            public ExportMethod Method { get; set; }
        }

        static Dictionary<string, ExportType> CreateExportTypes() {
            Dictionary<string, ExportType> types = new Dictionary<string, ExportType>();
            types.Add("PDF", new ExportType { Title = "Export to PDF", Method = GridViewExtension.ExportToPdf });
            types.Add("XLS", new ExportType { Title = "Export to XLS", Method = GridViewExtension.ExportToXls });
            types.Add("XLSX", new ExportType { Title = "Export to XLSX", Method = GridViewExtension.ExportToXlsx });
            types.Add("RTF", new ExportType { Title = "Export to RTF", Method = GridViewExtension.ExportToRtf });
            types.Add("CSV", new ExportType { Title = "Export to CSV", Method = GridViewExtension.ExportToCsv });
            return types;
        }

        public static DateTime NextWorkday(DateTime currentDay) {
            List<DateTime> holidays = EFHelper.ctx.F_CAB_SPECIAL_DATES.Select(x => x.SPDA_DATETIME).ToList();
            var nextDay = currentDay;
            var validDay = false;

            while (!validDay) {
                validDay = true;

                while (nextDay.DayOfWeek == DayOfWeek.Saturday || nextDay.DayOfWeek == DayOfWeek.Sunday)
                    nextDay = nextDay.AddDays(1);

                foreach (DateTime holiday in holidays) {
                    if (holiday.Date == nextDay.Date) {
                        nextDay = nextDay.AddDays(1);
                        validDay = false;
                        break;
                    }
                }
            }

            return nextDay;
        }
        public static DateTime PreviousWorkday(DateTime currentDay) {
            List<DateTime> holidays = EFHelper.ctx.F_CAB_SPECIAL_DATES.Select(x => x.SPDA_DATETIME).ToList();
            var prevDay = currentDay;
            var validDay = false;

            while (!validDay) {
                validDay = true;

                while (prevDay.DayOfWeek == DayOfWeek.Saturday || prevDay.DayOfWeek == DayOfWeek.Sunday)
                    prevDay = prevDay.AddDays(-1);

                foreach (DateTime holiday in holidays) {
                    if (holiday.Date == prevDay.Date) {
                        prevDay = prevDay.AddDays(-1);
                        validDay = false;
                        break;
                    }
                }
            }

            return prevDay;
        }

        private IEnumerable getOrders() {
            EFHelper.ctx.F_CAB_ORDER_DET.OrderBy(x => x.ORDE_CREATED_ON).Select(x => x.ORDE_CREATED_ON);
            DateTime startDateTime = new DateTime();
            DateTime endDateTime = new DateTime();

            if (DateTime.Now.Hour > 9) {
                startDateTime = PreviousWorkday(DateTime.Today).AddHours(9);
                endDateTime = NextWorkday(DateTime.Today.AddDays(1)).AddHours(8);
            } else {
                startDateTime = PreviousWorkday(DateTime.Today.AddDays(-1)).AddHours(9);
                endDateTime = NextWorkday(DateTime.Today).AddHours(8);
            }
        
            try {
                startDateTime = (DateTime)Session["ordersStartDate"];
            } catch {
            }
            try {
                endDateTime = (DateTime)Session["ordersEndDate"];
            } catch {
            }

            if (Usr.OrganisationType == OrgaType.Buyer || Usr.OrganisationType == OrgaType.Comm) { // Consolidate into one selct.
                return (from h in EFHelper.ctx.F_CAB_ORDER_HEA.Where(x => 
                    x.ORHE_FK_BUYER == Usr.OrgaTypeSequence && 
                    x.ORHE_CREATED_ON >= startDateTime && 
                    x.ORHE_CREATED_ON <= endDateTime && 
                    x.ORHE_FK_MAIN_GROUP == Usr.SelectedMaingroup &&
                    x.ORHE_FK_ORDER_STATUS != OrderHeaderStatus.NEW
                            
                    )

                        join o in EFHelper.ctx.F_CAB_ORDER_DET
                        on h.ORHE_SEQUENCE equals o.ORDE_FK_ORDER_HEA

                        join cdca in EFHelper.ctx.F_CAB_CD_CASK
                        on o.ORDE_FK_CASK_CODE equals cdca.CDCA_SEQUENCE

                        join caca in EFHelper.ctx.F_CAB_CAB_CASK_MATRIX
                        on o.ORDE_FK_CACA_SEQ equals caca.CACA_SEQUENCE

                        join cabc in EFHelper.ctx.F_CAB_CD_CAB_CODE
                        on o.ORDE_FK_CAB_CODE equals cabc.CABC_SEQUENCE

                        join prop in EFHelper.ctx.F_CAB_CAB_PROP.Where(x => x.PROP_FK_CDLA_SEQ == Usr.Language)
                        on cabc.CABC_SEQUENCE equals prop.PROP_FK_CABC_SEQ

                        select new Order {
                            seq = o.ORDE_SEQUENCE,
                            buyer = h.F_CAB_BUYER.F_CAB_ORGANISATION.ORGA_NAME,
                            grower = h.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_NAME,
                            date = h.ORHE_CREATED_ON,
                            remark = h.ORHE_DESC,
                            orheno = h.ORHE_NO,
                            caskdesc = cdca.CDCA_CASK_DESC,
                            cask = cdca.CDCA_CASK_CODE,
                            desc = cabc.CABC_CAB_DESC,
                            items = o.ORDE_NUM_OF_ITEMS,
                            units = o.ORDE_NUM_OF_UNITS,
                            ipu = caca.CACA_NUM_OF_UNITS,
                            itemprice = o.ORDE_ITEM_PRICE,
                            price = o.ORDE_NETT_LINE_VALUE,
                            deliverydate = h.ORHE_DELIVERY_DATE,
                            custcode = o.ORDE_CUSTOMER_CODE,
                            cabprops = prop.PROP_DESC_ALL_PROPS,
                            length = prop.PROP_VAL1,
                            status = o.F_CAB_CD_ORD_DET_STATUS.CDDS_STATUS_DESC,
                            transporter = h.ORHE_FK_ORGA_LOGP.HasValue ? EFHelper.ctx.F_CAB_ORGANISATION.FirstOrDefault(t => t.ORGA_SEQUENCE == h.ORHE_FK_ORGA_LOGP.Value).ORGA_NAME : string.Empty
                        }).ToList();
            } else if (Usr.OrganisationType == OrgaType.Grower) {
                return (from h in EFHelper.ctx.F_CAB_ORDER_HEA.Where(x => 
                    x.ORHE_FK_GROWER == Usr.OrgaTypeSequence && 
                    x.ORHE_CREATED_ON >= startDateTime && 
                    x.ORHE_CREATED_ON <= endDateTime && 
                    x.ORHE_FK_MAIN_GROUP == Usr.SelectedMaingroup &&
                    x.ORHE_FK_ORDER_STATUS != OrderHeaderStatus.NEW
                    )

                        join o in EFHelper.ctx.F_CAB_ORDER_DET
                        on h.ORHE_SEQUENCE equals o.ORDE_FK_ORDER_HEA

                        join cdca in EFHelper.ctx.F_CAB_CD_CASK
                        on o.ORDE_FK_CASK_CODE equals cdca.CDCA_SEQUENCE

                        join caca in EFHelper.ctx.F_CAB_CAB_CASK_MATRIX
                        on o.ORDE_FK_CACA_SEQ equals caca.CACA_SEQUENCE

                        join cabc in EFHelper.ctx.F_CAB_CD_CAB_CODE
                        on o.ORDE_FK_CAB_CODE equals cabc.CABC_SEQUENCE

                        join prop in EFHelper.ctx.F_CAB_CAB_PROP.Where(x => x.PROP_FK_CDLA_SEQ == Usr.Language)
                        on cabc.CABC_SEQUENCE equals prop.PROP_FK_CABC_SEQ

                        select new Order {
                            seq = o.ORDE_SEQUENCE,
                            buyer = h.F_CAB_BUYER.F_CAB_ORGANISATION.ORGA_NAME,
                            grower = h.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_NAME,
                            date = h.ORHE_CREATED_ON,
                            remark = h.ORHE_DESC,
                            orheno = h.ORHE_NO,
                            caskdesc = cdca.CDCA_CASK_DESC,
                            cask = cdca.CDCA_CASK_CODE,
                            desc = cabc.CABC_CAB_DESC,
                            items = o.ORDE_NUM_OF_ITEMS,
                            units = o.ORDE_NUM_OF_UNITS,
                            ipu = caca.CACA_NUM_OF_UNITS,
                            itemprice = o.ORDE_ITEM_PRICE,
                            price = o.ORDE_NETT_LINE_VALUE,
                            deliverydate = h.ORHE_DELIVERY_DATE,
                            custcode = o.ORDE_CUSTOMER_CODE,
                            cabprops = prop.PROP_DESC_ALL_PROPS,
                            length = prop.PROP_VAL1,
                            status = o.F_CAB_CD_ORD_DET_STATUS.CDDS_STATUS_DESC,
                            transporter = h.ORHE_FK_ORGA_LOGP.HasValue ? EFHelper.ctx.F_CAB_ORGANISATION.FirstOrDefault(t => t.ORGA_SEQUENCE == h.ORHE_FK_ORGA_LOGP.Value).ORGA_NAME : string.Empty
                        }).ToList();
            } else {
                return null;
            }
        }
        private IEnumerable getOrdersBuyer() {

            DateTime startDateTime = new DateTime();
            DateTime endDateTime = new DateTime();

            if (DateTime.Now.Hour > 9) {
                startDateTime = PreviousWorkday(DateTime.Today).AddHours(9);
                endDateTime = NextWorkday(DateTime.Today.AddDays(1)).AddHours(8);
            } else {
                startDateTime = PreviousWorkday(DateTime.Today.AddDays(-1)).AddHours(9);
                endDateTime = NextWorkday(DateTime.Today).AddHours(8);
            }
            try {
                startDateTime = (DateTime)Session["ordersStartDate"];
            } catch {
            }
            try {
                endDateTime = (DateTime)Session["ordersEndDate"];
            } catch {
            }

            return (from h in EFHelper.ctx.F_CAB_ORDER_HEA.Where(x =>
                x.ORHE_FK_BUYER == Usr.OrgaTypeSequence
                && x.ORHE_CREATED_ON >= startDateTime
                && x.ORHE_CREATED_ON <= endDateTime
                && x.ORHE_FK_MAIN_GROUP == Usr.SelectedMaingroup)

                    join o in EFHelper.ctx.F_CAB_ORDER_DET
                    on h.ORHE_SEQUENCE equals o.ORDE_FK_ORDER_HEA

                    join cdca in EFHelper.ctx.F_CAB_CD_CASK
                    on o.ORDE_FK_CASK_CODE equals cdca.CDCA_SEQUENCE

                    join caca in EFHelper.ctx.F_CAB_CAB_CASK_MATRIX
                    on o.ORDE_FK_CACA_SEQ equals caca.CACA_SEQUENCE

                    join cabc in EFHelper.ctx.F_CAB_CD_CAB_CODE
                    on o.ORDE_FK_CAB_CODE equals cabc.CABC_SEQUENCE

                    join prop in EFHelper.ctx.F_CAB_CAB_PROP.Where(x => x.PROP_FK_CDLA_SEQ == Usr.Language)
                    on cabc.CABC_SEQUENCE equals prop.PROP_FK_CABC_SEQ

                    select new Order {
                        seq = o.ORDE_SEQUENCE,
                        buyer = h.F_CAB_BUYER.F_CAB_ORGANISATION.ORGA_NAME,
                        grower = h.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_NAME,
                        date = h.ORHE_CREATED_ON,
                        remark = h.ORHE_DESC,
                        orheno = h.ORHE_NO,
                        caskdesc = cdca.CDCA_CASK_DESC,
                        cask = cdca.CDCA_CASK_CODE,
                        desc = cabc.CABC_CAB_DESC,
                        items = o.ORDE_NUM_OF_ITEMS,
                        units = o.ORDE_NUM_OF_UNITS,
                        ipu = caca.CACA_NUM_OF_UNITS,
                        itemprice = o.ORDE_ITEM_PRICE,
                        price = o.ORDE_NETT_LINE_VALUE,
                        deliverydate = h.ORHE_DELIVERY_DATE,
                        custcode = o.ORDE_CUSTOMER_CODE,
                        cabprops = prop.PROP_DESC_ALL_PROPS,
                        length = prop.PROP_VAL1,
                        status = o.F_CAB_CD_ORD_DET_STATUS.CDDS_STATUS_DESC,
                        transporter = h.ORHE_FK_ORGA_LOGP.HasValue ? EFHelper.ctx.F_CAB_ORGANISATION.FirstOrDefault(t => t.ORGA_SEQUENCE == h.ORHE_FK_ORGA_LOGP.Value).ORGA_NAME : string.Empty
                    }).ToList();
        }
        private IEnumerable getOrdersGrower() {
            DateTime startDateTime = new DateTime();
            DateTime endDateTime = new DateTime();

            if (DateTime.Now.Hour > 9) {
                startDateTime = PreviousWorkday(DateTime.Today).AddHours(9);
                endDateTime = NextWorkday(DateTime.Today.AddDays(1)).AddHours(8);
            } else {
                startDateTime = PreviousWorkday(DateTime.Today.AddDays(-1)).AddHours(9);
                endDateTime = NextWorkday(DateTime.Today).AddHours(8);
            }

            try {
                startDateTime = (DateTime)Session["ordersStartDate"];
            } catch {
            }
            try {
                endDateTime = (DateTime)Session["ordersEndDate"];
            } catch {
            }
            return (from h in EFHelper.ctx.F_CAB_ORDER_HEA.Where(x =>
                               x.ORHE_FK_GROWER == Usr.GrowerSequence
                               && x.ORHE_CREATED_ON >= startDateTime
                               && x.ORHE_CREATED_ON <= endDateTime
                               && x.ORHE_FK_MAIN_GROUP == Usr.SelectedMaingroup)

                    join o in EFHelper.ctx.F_CAB_ORDER_DET
                    on h.ORHE_SEQUENCE equals o.ORDE_FK_ORDER_HEA

                    join cdca in EFHelper.ctx.F_CAB_CD_CASK
                    on o.ORDE_FK_CASK_CODE equals cdca.CDCA_SEQUENCE

                    join caca in EFHelper.ctx.F_CAB_CAB_CASK_MATRIX
                    on o.ORDE_FK_CACA_SEQ equals caca.CACA_SEQUENCE

                    join cabc in EFHelper.ctx.F_CAB_CD_CAB_CODE
                    on o.ORDE_FK_CAB_CODE equals cabc.CABC_SEQUENCE

                    join prop in EFHelper.ctx.F_CAB_CAB_PROP.Where(x => x.PROP_FK_CDLA_SEQ == Usr.Language)
                    on cabc.CABC_SEQUENCE equals prop.PROP_FK_CABC_SEQ

                    select new Order {
                        seq = o.ORDE_SEQUENCE,
                        buyer = h.F_CAB_BUYER.F_CAB_ORGANISATION.ORGA_NAME,
                        grower = h.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_NAME,
                        date = h.ORHE_CREATED_ON,
                        remark = h.F_CAB_BUYER.F_CAB_ORGANISATION.ORGA_TYPE == "COMM" ? h.F_CAB_BUYER.F_CAB_ORGANISATION.ORGA_NAME : h.ORHE_DESC,
                        orheno = h.ORHE_NO,
                        caskdesc = cdca.CDCA_CASK_DESC,
                        cask = cdca.CDCA_CASK_CODE,
                        desc = cabc.CABC_CAB_DESC,
                        items = o.ORDE_NUM_OF_ITEMS,
                        units = o.ORDE_NUM_OF_UNITS,
                        ipu = caca.CACA_NUM_OF_UNITS,
                        itemprice = o.ORDE_ITEM_PRICE,
                        price = o.ORDE_NETT_LINE_VALUE,
                        deliverydate = h.ORHE_DELIVERY_DATE,
                        custcode = o.ORDE_CUSTOMER_CODE,
                        cabprops = prop.PROP_DESC_ALL_PROPS,
                        length = prop.PROP_VAL1,
                        status = o.F_CAB_CD_ORD_DET_STATUS.CDDS_STATUS_DESC,
                        transporter = h.ORHE_FK_ORGA_LOGP.HasValue ? EFHelper.ctx.F_CAB_ORGANISATION.FirstOrDefault(t => t.ORGA_SEQUENCE == h.ORHE_FK_ORGA_LOGP.Value).ORGA_NAME : string.Empty
                    }).ToList();
        }
        public static string MapPath(string path) {
            if (System.Web.HttpContext.Current != null)
                return System.Web.HttpContext.Current.Server.MapPath(path);

            return HttpRuntime.AppDomainAppPath + path.Replace("~", string.Empty).Replace('/', '\\');
        }
        public ActionResult Transporter() {
            
            ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            Log.Info("Controller: Order, Action: Transporter");
#if DEBUG            
            string exceldir = MapPath("~/Content/Transport");
            Log.InfoFormat("[DEBUG]: excelDir: {0}", exceldir);
#else
            //            string exceldir = System.Web.HttpContext.Current.Server.MapPath("C:/inetpub/Applications/Dev/LogiCAB/Content/Transport/");
            string exceldir = MapPath("~/Content/Transport");
            Log.InfoFormat("[RELEASE]: excelDir: {0}", exceldir);
#endif
            List<string> files = new List<string>();
            var excelfiles = Directory.GetFiles(exceldir);
            foreach (var f in excelfiles.Where(x => x.Contains(String.Format("_{0}_", Usr.OrganisationSequence)))) {
                files.Add(System.IO.Path.GetFileName(f));
            }
            return View(files);
        }

        public ActionResult ExcelOverview() {
            ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            return View(_getExcelList());
        }
        public ActionResult ExcelOverviewPartial()
        {
            return PartialView(_getExcelList());
        }

        private List<Tuple<string, string, string>> _getExcelList() {
            try { 
                string exceldir = "C:\\inetpub\\Applications\\Prod\\LogiOffer\\Content\\OrderExcelOverview\\" + Usr.GrowerSequence;
                List<FileInfo> growerExcels = new List<FileInfo>();

                var excelfiles = Directory.GetFiles(exceldir);
                foreach (var f in excelfiles)
                {
                    FileInfo file = new FileInfo(f);
                    growerExcels.Add(file);
                }

                string OrderExcelOverviewStore = ConfigurationManager.AppSettings["OrderExcelOverviewStore"];

                string baseFileUrl = OrderExcelOverviewStore + Usr.GrowerSequence;
                List<Tuple<string, string, string>> excelModel = new List<Tuple<string, string, string>>();

                foreach (var f in growerExcels.OrderByDescending(x => x.CreationTime))
                {
                    var fileUrl = String.Format("{0}/{1}", baseFileUrl, f.Name);
                    excelModel.Add(new Tuple<string, string, string>(f.Name, f.CreationTime.ToString(), fileUrl));
                }
                return excelModel;
            } catch (Exception ex) {
                return new List<Tuple<string, string, string>>();
            }
        }
    }
}
