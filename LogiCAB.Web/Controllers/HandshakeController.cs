﻿using LogiCAB.Web.Domain;
using CAB.Data.EF.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LogiCAB.Web.DAL;
using CAB.Domain.Helpers;
using CAB.Domain.Enums;
using log4net;
using System.Data.Entity.Core.Objects;
using System.Net.Mail;
using System.Configuration;
using System.Threading;
using System.Globalization;
/*
 * Add small Logistic-signup form for transporters.
 * Orga-type: "LOGI". Basic info with Emset-ail, Type
 * 
 * Show this list at the Handshake-detail in Relations.
 * Add the LOGI-organisation to a HandshakeOption-field: Logistics.
 * 
 */
namespace LogiCAB.Web.Controllers {
    [Authorize]
    [HandleError]
    public class HandshakeController : BaseController {
        private string mailOverride;
        public HandshakeController(ILog log) : base(log) {
            mailOverride = ConfigurationManager.AppSettings["mailOverride"];

        
        }
        public ActionResult Index() {
            return View(getHandshakes());
        }

        public ActionResult IndexAdmin() {
            return View(getHandshakesAdmin());
        }

        public ActionResult ListAdmin() {
            return PartialView("ListAdmin", getHandshakesAdmin());
        }

        private static List<Handshake> adminHandshakeCache;
        private static bool adminHandshakeCacheDirty = false;     // Set to true when updated.
        public List<Handshake> getHandshakesAdmin() {
            if (adminHandshakeCache != null && !adminHandshakeCacheDirty) {
                return adminHandshakeCache;
            }
            
            var handshakes = EFHelper.ctx.F_CAB_HANDSHAKE.Select(x => new Handshake {
                seq = x.HAND_SEQUENCE,
                active = x.HAND_BLOCKED_YN,
                request_date = x.HAND_REQUEST_DATE,
                accept_date = x.HAND_ACCEPT_DATE,
                _growerOrga = x.F_CAB_ORGANISATION,
                _buyerOrga = x.F_CAB_ORGANISATION1,
                hndo = x.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault()
            }).ToList();
            adminHandshakeCache = handshakes;
            return adminHandshakeCache;

        }
        public ActionResult IndexAsGrower() {
            return View(getCommHandshakesAsGrower());
        }
        public ActionResult IndexAsBuyer() {
            return View(getCommHandshakesAsBuyer());
        }

        public ActionResult ListAsGrower() {
            return PartialView("ListAsGrower", getCommHandshakesAsGrower());
        }

        public ActionResult ListAsBuyer() {
            return PartialView("ListAsBuyer", getCommHandshakesAsBuyer());
        }

        public ActionResult List() {
            return PartialView("List", getHandshakes());
        }

        public ActionResult ListPending() {
            return PartialView("ListPending", getHandshakes());
        }

        private IEnumerable getCommHandshakesAsGrower() {
            return (from h in EFHelper.ctx.F_CAB_HANDSHAKE.Where(x => x.HAND_FK_ORGA_SEQ_GROWER == Usr.OrganisationSequence)
                    select new Handshake {
                        seq = h.HAND_SEQUENCE,
                        active = h.HAND_BLOCKED_YN,
                        request_date = h.HAND_REQUEST_DATE,
                        accept_date = h.HAND_ACCEPT_DATE,
                        _buyerOrga = h.F_CAB_ORGANISATION1,
                        hndo = EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault(x => x.HNDO_FK_HAND_SEQ == h.HAND_SEQUENCE)
                    }).Distinct().ToList();
        }
        private IEnumerable getCommHandshakesAsBuyer() {
            return (from h in EFHelper.ctx.F_CAB_HANDSHAKE.Where(x => x.HAND_FK_ORGA_SEQ_BUYER == Usr.OrganisationSequence)
                    where
                        h.F_CAB_ORGANISATION.F_CAB_ORGA_MAIN_GROUP_MATRIX.Any(x => x.ORMA_FK_MAIN_GROUP == Usr.SelectedMaingroup) &&
                        h.F_CAB_ORGANISATION.F_CAB_ORGANISATION_ADMIN.FirstOrDefault().ORAD_STATUS != ORAD_Status.AFGEMELD
                    select new Handshake {
                        seq = h.HAND_SEQUENCE,
                        active = h.HAND_BLOCKED_YN,
                        request_date = h.HAND_REQUEST_DATE,
                        accept_date = h.HAND_ACCEPT_DATE,
                        _growerOrga = h.F_CAB_ORGANISATION,
                        hndo = EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault(x => x.HNDO_FK_HAND_SEQ == h.HAND_SEQUENCE)
                    }).Distinct().ToList();
        }

        public ActionResult HandshakeEditPartial(int id) {
            var h = EFHelper.ctx.F_CAB_HANDSHAKE.SingleOrDefault(x => x.HAND_SEQUENCE == id);
            if (h == null) return PartialView(null);
            return PartialView(new Handshake {
                seq = h.HAND_SEQUENCE,
                active = h.HAND_BLOCKED_YN,
                request_date = h.HAND_REQUEST_DATE,
                accept_date = h.HAND_ACCEPT_DATE,
                _growerOrga = h.F_CAB_ORGANISATION,
                _buyerOrga = h.F_CAB_ORGANISATION1,
                hndo = EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault(x => x.HNDO_FK_HAND_SEQ == h.HAND_SEQUENCE)
            });
        }

        public ActionResult UpdateHandshake(int key, string field, string value) {
            //if (field == "active") { // HAND_BLOCKED_YN                
            //    EFHelper.ctx.F_CAB_HANDSHAKE.FirstOrDefault(x => x.HAND_SEQUENCE == key).HAND_BLOCKED_YN = value == "true" ? true : false; 
            //} else if (field == "hndo.HNDO_AUTO_CONFIRM_YN") {
            //    EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault(x => x.HNDO_FK_HAND_SEQ == key).HNDO_AUTO_CONFIRM_YN = value == "true" ? true : false; 
            //} else if (field == "hndo.HNDO_AUTO_KWB_YN") {
            //    EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault(x => x.HNDO_FK_HAND_SEQ == key).HNDO_AUTO_KWB_YN = value == "true" ? true : false; 
            //} else if (field == "hndo.HNDO_DELIVERY_TYPE") {
            //    EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault(x => x.HNDO_FK_HAND_SEQ == key).HNDO_DELIVERY_TYPE = value.ToString(); 
            //} else if (field == "hndo.HNDO_AUTO_EKT_YN") {
            //    EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault(x => x.HNDO_FK_HAND_SEQ == key).HNDO_AUTO_EKT_YN = value == "true" ? true : false; 
            //} else if (field == "hndo.HNDO_LATEST_DELIVERY_TIME") {
            //    TimeSpan time = TimeSpan.Parse(value);
            //    DateTime selDate = DateTime.Now.Date + time;

            //    EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault(x => x.HNDO_FK_HAND_SEQ == key).HNDO_LATEST_DELIVERY_TIME = selDate; // 
            //} else if (field == "hndo.HNDO_FK_ADDR_SEQ") {
            //    EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault(x => x.HNDO_FK_HAND_SEQ == key).HNDO_FK_ADDR_SEQ = -1; //
            //}
            //EFHelper.ctx.SaveChanges();
            //return null;
            Log.InfoFormat("UpdateHandshake called: Username: {0},\n\t Key: {1}, Field: {2}, Value: {3}", Usr.LoginName, key, field, value);
            var dbObjH = EFHelper.ctx.F_CAB_HANDSHAKE.FirstOrDefault(x => x.HAND_SEQUENCE == key);
            var dbObj = EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault(x => x.HNDO_FK_HAND_SEQ == key);
            if (field == "active") { // HAND_BLOCKED_YN                
                dbObjH.HAND_BLOCKED_YN = value == "true";
                short ofhdDeletedYN = value == "true" ? (short)1 : (short)0;
                DateTime nowTime = DateTime.Now;
                //                var typeSequence = userinfo.orgaTypeSequence;
                if (Usr.OrganisationType == OrgaType.Grower) {
                    var buyerSequence = dbObjH.F_CAB_ORGANISATION1.F_CAB_BUYER.FirstOrDefault().BUYR_SEQUENCE;
                    var validOffers = (from o in EFHelper.ctx.F_CAB_OFFER_HEADERS
                                       where
                                         o.OFHD_VALID_TO > nowTime
                                         && o.OFHD_FK_GROWER == Usr.OrgaTypeSequence
                                         && o.OFHD_FK_BUYER == buyerSequence
                                       select o.OFHD_SEQUENCE).ToList();
                    foreach (var o in EFHelper.ctx.F_CAB_OFFER_HEADERS.Where(x => validOffers.Contains(x.OFHD_SEQUENCE))) {
                        o.OFHD_DELETED_YN = ofhdDeletedYN;
                        o.OFHD_OFFER_CAPTION_SEQ = (Int16)0;
                    }
                } else if (Usr.OrganisationType == OrgaType.Buyer) {
                    var growerSequence = dbObjH.F_CAB_ORGANISATION.F_CAB_GROWER.FirstOrDefault().GROW_SEQUENCE;
                    var validOffers = (from o in EFHelper.ctx.F_CAB_OFFER_HEADERS
                                       where
                                         o.OFHD_VALID_TO > nowTime
                                         && o.OFHD_FK_GROWER == growerSequence
                                         && o.OFHD_FK_BUYER == Usr.OrgaTypeSequence
                                       select o.OFHD_SEQUENCE).ToList();
                    foreach (var o in EFHelper.ctx.F_CAB_OFFER_HEADERS.Where(x => validOffers.Contains(x.OFHD_SEQUENCE))) {
                        o.OFHD_DELETED_YN = ofhdDeletedYN;
                        o.OFHD_OFFER_CAPTION_SEQ = (Int16)0;
                    }
                }
            } else if (field == "hndo.HNDO_AUTO_CONFIRM_YN") {
                dbObj.HNDO_AUTO_CONFIRM_YN = value == "true";
            } else if (field == "hndo.HNDO_AUTO_KWB_YN") {
                dbObj.HNDO_AUTO_KWB_YN = value == "true";
            } else if (field == "hndo.HNDO_DELIVERY_TYPE") {
                dbObj.HNDO_DELIVERY_TYPE = value.ToString();
            } else if (field == "hndo.HNDO_AUTO_EKT_YN") {
                dbObj.HNDO_AUTO_EKT_YN = value == "true";
            } else if (field == "hndo.HNDO_LATEST_DELIVERY_TIME") {
                TimeSpan time = TimeSpan.Parse(value);
                DateTime selDate = DateTime.Now.Date + time;

                dbObj.HNDO_LATEST_DELIVERY_TIME = selDate; // 
            } else if (field == "hndo.HNDO_FK_ADDR_SEQ") {
                dbObj.HNDO_FK_ADDR_SEQ = -1; //
            } else if (field == "hndo.HNDO_SEND_CONFIRM_MAIL_YN") {
                dbObj.HNDO_SEND_CONFIRM_MAIL_YN = value == "true";
            } else if (field == "hndo.HNDO_RECV_OFFER_MAIL_YN") {
                dbObj.HNDO_RECV_OFFER_MAIL_YN = value == "true";
            }
            EFHelper.ctx.SaveChanges();
            return null;
        }

        private IEnumerable getHandshakes() {
            if (Usr.OrganisationType == OrgaType.Buyer || Usr.OrganisationType == OrgaType.Comm) {
                return (from h in EFHelper.ctx.F_CAB_HANDSHAKE.Where(x => x.HAND_FK_ORGA_SEQ_BUYER == Usr.OrganisationSequence)
                        join orad in EFHelper.ctx.F_CAB_ORGANISATION_ADMIN on h.F_CAB_ORGANISATION.ORGA_SEQUENCE equals orad.ORAD_FK_ORGANISATION
                        where h.F_CAB_ORGANISATION.F_CAB_ORGA_MAIN_GROUP_MATRIX.Any(x => x.ORMA_FK_MAIN_GROUP == Usr.SelectedMaingroup
                        && orad.ORAD_STATUS == SubscriptionStatus.ACTIEF)
                        select new Handshake {
                            seq = h.HAND_SEQUENCE,
                            active = h.HAND_BLOCKED_YN,
                            request_date = h.HAND_REQUEST_DATE,
                            accept_date = h.HAND_ACCEPT_DATE,
                            _growerOrga = h.F_CAB_ORGANISATION,
                            hndo = EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault(x => x.HNDO_FK_HAND_SEQ == h.HAND_SEQUENCE)
                        }).Distinct().ToList();
            } else if (Usr.OrganisationType == OrgaType.Grower) {
                return (from h in EFHelper.ctx.F_CAB_HANDSHAKE.Where(x => x.HAND_FK_ORGA_SEQ_GROWER == Usr.OrganisationSequence)
                                                join orad in EFHelper.ctx.F_CAB_ORGANISATION_ADMIN on h.F_CAB_ORGANISATION1.ORGA_SEQUENCE equals orad.ORAD_FK_ORGANISATION
                        where 
                            h.F_CAB_ORGANISATION1.F_CAB_ORGA_MAIN_GROUP_MATRIX.Any(x => x.ORMA_FK_MAIN_GROUP == Usr.SelectedMaingroup)
                            && (h.F_CAB_ORGANISATION1.ORGA_TYPE == OrgaType.Buyer ||  h.F_CAB_ORGANISATION1.ORGA_TYPE == OrgaType.Comm)
                            && orad.ORAD_STATUS == SubscriptionStatus.ACTIEF
                        select new Handshake {
                            seq = h.HAND_SEQUENCE,
                            active = h.HAND_BLOCKED_YN,
                            request_date = h.HAND_REQUEST_DATE,
                            accept_date = h.HAND_ACCEPT_DATE,
                            _buyerOrga = h.F_CAB_ORGANISATION1,
                            hndo = EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault(x => x.HNDO_FK_HAND_SEQ == h.HAND_SEQUENCE)
                        }).Distinct().ToList();
            }
            return null;
        }

        public ActionResult DeliveryMoments(int id) {
            ViewBag.CurrentHandshake = id;
            return PartialView(_getDeliveryMoments(id));
        }

        [HttpPost]
        public ActionResult UpdateDeliveryMoments(F_CAB_DELIVERY_MOMENT_DETAILS detail, int id) {
            if (ModelState.IsValid) {
                F_CAB_DELIVERY_MOMENT_DETAILS toUpdate = EFHelper.ctx.F_CAB_DELIVERY_MOMENT_DETAILS.Single(x => x.DMDT_SEQUENCE == detail.DMDT_SEQUENCE);
                toUpdate.DMDT_MAX_ORDER_TIME = detail.DMDT_MAX_ORDER_TIME;
                toUpdate.DMDT_MAX_DELIVERY_TIME = detail.DMDT_MAX_DELIVERY_TIME;
                toUpdate.DMDT_DELIVERY_TYPE = detail.DMDT_DELIVERY_TYPE;
                EFHelper.ctx.SaveChanges();
            } else {
                ViewData["UpdateError"] = String.Format("Incorrect handshake posted.[{0}]", id);
            }
            ViewBag.CurrentHandshake = id;
            return PartialView("DeliveryMoments", _getDeliveryMoments(id));
        }

        [HttpPost]
        public ActionResult AddDeliveryMoments(F_CAB_DELIVERY_MOMENT_DETAILS detail, int id) {

            if (ModelState.IsValid) {
                DateTime now = DateTime.Now;
                var hndo = EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.SingleOrDefault(x => x.HNDO_FK_HAND_SEQ == id);
                if (hndo.HNDO_FK_DMHD_SEQ == null) {
                    F_CAB_DELIVERY_MOMENT_HEADERS dmhd = new F_CAB_DELIVERY_MOMENT_HEADERS {
                        DMHD_CODE = String.Format("{0} Template", Usr.Organisation.Name),
                        DMHD_DESCRIPTION = "Template created based on Base-template",
                        DMHD_FK_GROW_SEQ = Usr.OrgaTypeSequence,
                        DWHD_CREATED_ON = now,
                        DWHD_MODIFIED_ON = now,
                        DWHD_CREATED_BY = "sa",
                        DWHD_MODIFIED_BY = "sa"
                    };
                    EFHelper.ctx.F_CAB_DELIVERY_MOMENT_HEADERS.AddObject(dmhd);
                    EFHelper.ctx.SaveChanges();
                    hndo.HNDO_FK_DMHD_SEQ = dmhd.DMHD_SEQUENCE;
                    EFHelper.ctx.SaveChanges();

                    List<F_CAB_DELIVERY_MOMENT_DETAILS> baseDetails = EFHelper.ctx.F_CAB_DELIVERY_MOMENT_DETAILS.Where(x => x.DMDT_FK_DMHD_SEQ == 1).ToList();

                    var addDay = detail.DMDT_WEEKDAY_NUMBER;
                    for (int i = 0; i < 7; i++) {
                        F_CAB_DELIVERY_MOMENT_DETAILS baseDetail = baseDetails.Single(x => x.DMDT_WEEKDAY_NUMBER == i);
                        F_CAB_DELIVERY_MOMENT_DETAILS dmdt = new F_CAB_DELIVERY_MOMENT_DETAILS {
                            DMDT_FK_ADDR_SEQ_DELIVERY = null,
                            DMDT_TMP_VALID_TO = null,
                            DMDT_TMP_WEEKDAY_NUMBER = null,
                            DMDT_TMP_ORDER_NUMBER = null,
                            DMDT_TMP_WEEK_NUMBER = null,
                            DMDT_TMP_MAX_ORDER_TIME = null,
                            DMDT_TMP_MAX_DELIVERY_TIME = null,
                            DMDT_TMP_DELIVERY_TYPE = detail.DMDT_DELIVERY_TYPE,
                            DMDT_TMP_FK_ADDR_SEQ_DELIVERY = null,
                            DMDT_CREATED_ON = now,
                            DMDT_CREATED_BY = "sa",
                            DMDT_MODIFIED_ON = now,
                            DMDT_MODIFIED_BY = "sa",
                            DMDT_FK_DMHD_SEQ = dmhd.DMHD_SEQUENCE,
                            DMDT_WEEKDAY_NUMBER = i,
                            //                            DMDT_ORDER_NUMBER = 1,
                            DMDT_WEEK_NUMBER = null
                        };
                        if (i == addDay) {
                            dmdt.DMDT_MAX_ORDER_TIME = detail.DMDT_MAX_ORDER_TIME;
                            dmdt.DMDT_MAX_DELIVERY_TIME = detail.DMDT_MAX_DELIVERY_TIME;
                            dmdt.DMDT_DELIVERY_TYPE = detail.DMDT_DELIVERY_TYPE;
                        } else {
                            dmdt.DMDT_MAX_ORDER_TIME = baseDetail.DMDT_MAX_ORDER_TIME;
                            dmdt.DMDT_MAX_DELIVERY_TIME = baseDetail.DMDT_MAX_DELIVERY_TIME;
                            dmdt.DMDT_DELIVERY_TYPE = baseDetail.DMDT_DELIVERY_TYPE;
                        }
                        EFHelper.ctx.F_CAB_DELIVERY_MOMENT_DETAILS.AddObject(dmdt);
                    }
                    EFHelper.ctx.SaveChanges();
                } else {
                    F_CAB_DELIVERY_MOMENT_DETAILS dmdt = new F_CAB_DELIVERY_MOMENT_DETAILS {
                        DMDT_FK_ADDR_SEQ_DELIVERY = null,
                        DMDT_TMP_VALID_TO = null,
                        DMDT_TMP_WEEKDAY_NUMBER = null,
                        DMDT_TMP_ORDER_NUMBER = null,
                        DMDT_TMP_WEEK_NUMBER = null,
                        DMDT_TMP_MAX_ORDER_TIME = null,
                        DMDT_TMP_MAX_DELIVERY_TIME = null,
                        DMDT_TMP_DELIVERY_TYPE = detail.DMDT_DELIVERY_TYPE,
                        DMDT_TMP_FK_ADDR_SEQ_DELIVERY = null,
                        DMDT_CREATED_ON = now,
                        DMDT_CREATED_BY = "sa",
                        DMDT_MODIFIED_ON = now,
                        DMDT_MODIFIED_BY = "sa",
                        DMDT_FK_DMHD_SEQ = hndo.HNDO_FK_DMHD_SEQ.Value,
                        DMDT_WEEKDAY_NUMBER = detail.DMDT_WEEKDAY_NUMBER,
                        //                        DMDT_ORDER_NUMBER = 1,
                        DMDT_WEEK_NUMBER = null,
                        DMDT_MAX_ORDER_TIME = detail.DMDT_MAX_ORDER_TIME,
                        DMDT_MAX_DELIVERY_TIME = detail.DMDT_MAX_DELIVERY_TIME,
                        DMDT_DELIVERY_TYPE = detail.DMDT_DELIVERY_TYPE
                    };
                    EFHelper.ctx.F_CAB_DELIVERY_MOMENT_DETAILS.AddObject(dmdt);
                    EFHelper.ctx.SaveChanges();
                }
            } else {
                ViewData["UpdateError"] = String.Format("Incorrect handshake posted.[{0}]", id);
            }
            ViewBag.CurrentHandshake = id;
            return PartialView("DeliveryMoments", _getDeliveryMoments(id));
        }

        [HttpPost]
        public ActionResult DeleteDeliveryMoments(F_CAB_DELIVERY_MOMENT_DETAILS detail, int id) {
            if (ModelState.IsValid) {
                F_CAB_DELIVERY_MOMENT_DETAILS toDelete = EFHelper.ctx.F_CAB_DELIVERY_MOMENT_DETAILS.Single(x => x.DMDT_SEQUENCE == detail.DMDT_SEQUENCE);
                EFHelper.ctx.F_CAB_DELIVERY_MOMENT_DETAILS.DeleteObject(toDelete);
                EFHelper.ctx.SaveChanges();
            } else {
                ViewData["UpdateError"] = String.Format("Incorrect handshake posted.[{0}]", id);
            }
            ViewBag.CurrentHandshake = id;
            return PartialView("DeliveryMoments", _getDeliveryMoments(id));
        }

        private IQueryable<F_CAB_DELIVERY_MOMENT_DETAILS> _getDeliveryMoments(int id) {
            var hndo = EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.SingleOrDefault(x => x.HNDO_FK_HAND_SEQ == id);
            if (hndo.HNDO_FK_DMHD_SEQ != null) {
                var deliveryDetails = EFHelper.ctx.F_CAB_DELIVERY_MOMENT_DETAILS.Where(x => x.DMDT_FK_DMHD_SEQ == hndo.HNDO_FK_DMHD_SEQ).AsQueryable();
                if (!deliveryDetails.Any()) {
                    var deliveryHeader = EFHelper.ctx.F_CAB_DELIVERY_MOMENT_HEADERS.FirstOrDefault(x => x.DMHD_SEQUENCE == hndo.HNDO_FK_DMHD_SEQ);
                    EFHelper.ctx.F_CAB_DELIVERY_MOMENT_HEADERS.DeleteObject(deliveryHeader);
                    EFHelper.ctx.SaveChanges();
                    return EFHelper.ctx.F_CAB_DELIVERY_MOMENT_DETAILS.Where(x => x.F_CAB_DELIVERY_MOMENT_HEADERS.DMHD_FK_GROW_SEQ == null).AsQueryable();
                }
                return deliveryDetails;
            }
            return EFHelper.ctx.F_CAB_DELIVERY_MOMENT_DETAILS.Where(x => x.F_CAB_DELIVERY_MOMENT_HEADERS.DMHD_FK_GROW_SEQ == null).AsQueryable();
        }


        public ActionResult Transporters(int handshake = -1) {
            if (handshake != -1)
                Usr.SelectedHandshake = handshake;
            return PartialView(_getTransporters());
        }


        private IEnumerable<Handshake> getHandshakesTransporters() {

            return (from h in EFHelper.ctx.F_CAB_HANDSHAKE.Where(x => x.HAND_FK_ORGA_SEQ_GROWER == Usr.OrganisationSequence)
                    where
//                        h.F_CAB_ORGANISATION1.F_CAB_ORGA_MAIN_GROUP_MATRIX.Any(x => x.ORMA_FK_MAIN_GROUP == Usr.SelectedMaingroup)
                         h.F_CAB_ORGANISATION1.ORGA_TYPE == OrgaType.Logp
                    select new Handshake {
                        seq = h.HAND_SEQUENCE,
                        active = h.HAND_BLOCKED_YN,
                        request_date = h.HAND_REQUEST_DATE,
                        accept_date = h.HAND_ACCEPT_DATE,
                        _buyerOrga = h.F_CAB_ORGANISATION1,
                        hndo = EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault(x => x.HNDO_FK_HAND_SEQ == h.HAND_SEQUENCE)
                    }).Distinct().ToList();
        }

        public ActionResult TransportersList() {
//            var ret = EFHelper.ctx.F_CAB_ORGANISATION.Where(x => x.ORGA_TYPE == OrgaType.Logp).ToList();
//            var allTransporters = ret.Select(x => x.ORGA_SEQUENCE).ToList();
//            var currentHandshakesActivated = EFHelper.ctx.F_CAB_HANDSHAKE.Where(x => x.HAND_FK_ORGA_SEQ_GROWER == Usr.OrganisationSequence).Select(x => x.HAND_FK_ORGA_SEQ_BUYER).ToList();
            //var currentHandshakes = 
//            var activeTransportRelations = currentHandshakesActivated.Where(x => allTransporters.Contains(x.Value)).ToList();
//            ViewBag.RelationsToTag = activeTransportRelations;
//            ViewData["RelationsToTag"] = activeTransportRelations;
            return PartialView(getHandshakesTransporters().Where(x => x.accept_date != null).ToList());
        }
        public ActionResult TransportersListPending() {
            return PartialView(getHandshakesTransporters().Where(x => x.accept_date == null).ToList());
        }



        private List<F_CAB_ORGANISATION> _getTransporters() {
            var ret = from o in EFHelper.ctx.F_CAB_ORGANISATION
                      join h in EFHelper.ctx.F_CAB_HANDSHAKE on o.ORGA_SEQUENCE equals h.HAND_FK_ORGA_SEQ_BUYER
                      where h.HAND_FK_ORGA_SEQ_GROWER == Usr.OrganisationSequence
                      select o;
            return ret.Where(x => x.ORGA_TYPE == OrgaType.Logp).ToList();
        }

        public JsonResult SendHandshakeRequest(int id) {
            var handshake = EFHelper.ctx.F_CAB_HANDSHAKE.Single(x => x.HAND_SEQUENCE == id);

//            if (exists) return Json(false);
            // Handshake does not yet exists, create it and send the request to the specified organisation
            var targetPerson = handshake.F_CAB_ORGANISATION.F_CAB_PERSON.FirstOrDefault();
            var targetOrga = handshake.F_CAB_ORGANISATION;
            SendHandshakeEmail(targetPerson.PERS_EMAIL ?? targetOrga.ORGA_EMAIL, Usr.Person.Email, Usr.Person.FullName, targetOrga.ORGA_NAME, id, "BUYR", "Re-request");
            handshake.HAND_REQUEST_DATE = DateTime.Now;
            return Json(
                new {
                    RequestDate = handshake.HAND_REQUEST_DATE.Value.ToShortDateString(),
                    AcceptDate = handshake.HAND_ACCEPT_DATE,
                    Remark = handshake.HAND_REQUEST_REMARK,
                    Sequence = handshake.HAND_SEQUENCE
                }, JsonRequestBehavior.AllowGet);
        }

                                    


        public JsonResult processLogisticsChange(int orgaseq) {
            var hndo = EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.SingleOrDefault(x => x.HNDO_FK_HAND_SEQ == Usr.SelectedHandshake);
            if (hndo != null) {
                hndo.HNDO_FK_ORGA_SEQ = orgaseq;
                EFHelper.ctx.SaveChanges();
                return Json(true);
            } else {
                Log.ErrorFormat("Attempted to add Logistic Provider, but Handshake: {0} has no F_CAB_HANDSHAKE_OPTIONS-entry.", Usr.SelectedHandshake);
                return Json(false);
            }
        }

        public JsonResult UpdateDeliveryProfile(int profile) {
            var hndo = EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.SingleOrDefault(x => x.HNDO_FK_HAND_SEQ == Usr.SelectedHandshake);
            if (hndo != null) {
                hndo.HNDO_FK_DMHD_SEQ = profile;
                EFHelper.ctx.SaveChanges();
                return Json(true);
            } else {
                Log.ErrorFormat("Attempted to add DeliveryProfile, but Handshake: {0} has no F_CAB_HANDSHAKE_OPTIONS-entry.", Usr.SelectedHandshake);
                return Json(false);
            }
        }

        public ActionResult UpdatePersonFilter(string toggled, string maingroup, string grower, string buyer, string person) {
            bool blocked = Boolean.Parse(toggled);
            int _mg = Int32.Parse(maingroup);
            int _grower = Int32.Parse(grower);
            int _buyer = Int32.Parse(buyer);
            int _person = Int32.Parse(person);

            var currentRecordExists = EFHelper.ctx.F_CAB_PERSON_FILTERS.Any(x => x.PERF_FK_CDMG_SEQ == _mg && x.PERF_FK_GROW_SEQ == _grower && x.PERF_FK_BUYR_SEQ == _buyer && x.PERF_FK_PERS_SEQ == _person);
            if (blocked) {
                if (!currentRecordExists) {
                    DateTime now = DateTime.Now;
                    EFHelper.ctx.F_CAB_PERSON_FILTERS.AddObject(
                        new F_CAB_PERSON_FILTERS {
                            PERF_FK_CDMG_SEQ = _mg,
                            PERF_FK_BUYR_SEQ = _buyer,
                            PERF_FK_PERS_SEQ = _person,
                            PERF_FK_GROW_SEQ = _grower,
                            PERF_FK_CDGR_SEQ = null,
                            PERF_CREATED_ON = now,
                            PERF_CREATED_BY = "sa",
                            PERF_MODIFIED_ON = now,
                            PERF_MODIFIED_BY = "sa"
                        });
                    EFHelper.ctx.SaveChanges();
                }
            } else {
                if (currentRecordExists) {
                    var currentRecord = EFHelper.ctx.F_CAB_PERSON_FILTERS.SingleOrDefault(x => x.PERF_FK_CDMG_SEQ == _mg && x.PERF_FK_GROW_SEQ == _grower && x.PERF_FK_BUYR_SEQ == _buyer && x.PERF_FK_PERS_SEQ == _person);
                    EFHelper.ctx.F_CAB_PERSON_FILTERS.DeleteObject(currentRecord);
                    EFHelper.ctx.SaveChanges();
                }
            }
            return null;
        }

        public ActionResult HandshakeBuyers() {
            return PartialView();
        }

        public ActionResult HandshakeTransporters() {
            return PartialView();
        }

        [HttpPost]
        public JsonResult SendHandshake(string senderName, string orgaName, string orgaMail, string remark) {
            try {
                var selectedOrgaSequence = -1;
                var selectedOrgaType = "BUYR";
                var orgaSequence = -1;
                var orgaType = Usr.OrganisationType;
                var persEmail = Usr.Person.Email;

//                bool success = SendHandshakeInternal(senderName, orgaName, orgaMail, remark, selectedOrgaSequence, selectedOrgaType.ToString(), orgaSequence, orgaType.ToString(), persEmail);
            } catch (Exception ex) {
                var test = ex;
            }
//            return RedirectToAction("Index", "Handshakes");
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        private bool SendHandshakeInternal(
            string senderName, string orgaName, string orgaMail, string remark,
            int selectedOrgaSequence, string selectedOrgaType, int orgaSequence, string orgaType, string persEmail) {

            String sessionKey = orgaSequence + " " + selectedOrgaSequence;
            if (Session[sessionKey] != null) {
                Session.Add("HandError", "Handshake bestaat al.");
                return false;
            }
            Entities cab = EFHelper.ctx;
//            using (EFHelper.ctx cab = new EFHelper.ctx()) {
                if (selectedOrgaType.ToString() == "GROW") {
                    Session[sessionKey] = true;
                    var handshakeExists = cab.F_CAB_HANDSHAKE.FirstOrDefault(x => x.HAND_FK_ORGA_SEQ_BUYER == orgaSequence && x.HAND_FK_ORGA_SEQ_GROWER == selectedOrgaSequence);
                    if (handshakeExists != null) {
                        Session.Add("HandError", "Handshake bestaat al.");
                        return false;
                    }
                    F_CAB_ORGANISATION buyerOrg = cab.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == orgaSequence);
                    F_CAB_ORGANISATION growerOrg = cab.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == selectedOrgaSequence);
                    F_CAB_ORGANISATION_ADMIN selectedOrgaAdmin = cab.F_CAB_ORGANISATION_ADMIN.FirstOrDefault(x => x.F_CAB_ORGANISATION.ORGA_SEQUENCE == selectedOrgaSequence);

                    F_CAB_BUYER buyer = cab.F_CAB_BUYER.FirstOrDefault(x => x.BUYR_FK_ORGA == buyerOrg.ORGA_SEQUENCE);
                    F_CAB_GROWER grower = cab.F_CAB_GROWER.FirstOrDefault(x => x.GROW_FK_ORGA == growerOrg.ORGA_SEQUENCE);

                    if (buyer == null || grower == null) {
                        SendHandshakeErrorEmail(buyerOrg.ORGA_EMAIL, "support@logiflora.nl", buyerOrg.ORGA_NAME, growerOrg.ORGA_NAME);
                        return false;
                    }

                    F_CAB_HANDSHAKE newHandshake = new F_CAB_HANDSHAKE {
                        HAND_FK_ORGA_SEQ_BUYER = buyerOrg.ORGA_SEQUENCE,
                        HAND_FK_ORGA_SEQ_GROWER = growerOrg.ORGA_SEQUENCE,
                        HAND_FK_HATY_SEQ = 1,
                        HAND_BLOCKED_YN = true,
                        HAND_REQUEST_REMARK = remark,
                        HAND_REQUEST_DATE = DateTime.Now,
                        HAND_CREATED_ON = DateTime.Now,
                        HAND_CREATED_BY = "LogiCAB:SendHandshakeInternal"
                    };
                    cab.F_CAB_HANDSHAKE.AddObject(newHandshake);
                    cab.SaveChanges();

                    /*
                     * 
                     * Create HANDSHAKE_OPTIONS
                     * 
                     */

                    F_CAB_HANDSHAKE_OPTIONS newHandshakeOptions = new F_CAB_HANDSHAKE_OPTIONS {
                        HNDO_AUTO_EKT_YN = false,
                        HNDO_FK_HAND_SEQ = newHandshake.HAND_SEQUENCE,
                        HNDO_CREATED_ON = DateTime.Now,
                        HNDO_CREATED_BY = "LogiCAB:SendHandshakeInternal",
                        HNDO_MODIFIED_ON = DateTime.Now,
                        HNDO_MODIFIED_BY = "LogiCAB:SendHandshakeInternal",
                        HNDO_LATEST_DELIVERY_TIME = DateTime.Today.AddHours(12),
                        HNDO_AUTO_CONFIRM_YN = false,
                        HNDO_FK_ADDR_SEQ = null,
                        HNDO_DELIVERY_TYPE = ""
                    };

                    cab.F_CAB_HANDSHAKE_OPTIONS.AddObject(newHandshakeOptions);
                    cab.SaveChanges();

                    if (buyer != null && grower != null) {
                        F_CAB_GROWER_BUYER_MATRIX growBuyMatrix = new F_CAB_GROWER_BUYER_MATRIX {
                            GAMA_FK_BUYER = buyer.BUYR_SEQUENCE,
                            GAMA_FK_GROWER = grower.GROW_SEQUENCE,
                            GAMA_BLOCKED_YN = 0
                        };
                        cab.F_CAB_GROWER_BUYER_MATRIX.AddObject(growBuyMatrix);
                        cab.SaveChanges();
                        cab.Refresh(RefreshMode.ClientWins, newHandshake);
                        if (newHandshake.HAND_SEQUENCE > 0) {
                            if (selectedOrgaAdmin.ORAD_STATUS != 1) {
                                SendHandshakeEmail(persEmail ?? buyerOrg.ORGA_EMAIL, growerOrg.ORGA_EMAIL, Session["FullName"], buyerOrg.ORGA_NAME, newHandshake.HAND_SEQUENCE, "BUYR", remark);
                            } else {
                                String growerOrgaInfo = String.Format("GrowerOrga: {0}<br />Phone: {1}<br />Email: {2}", growerOrg.ORGA_NAME, growerOrg.ORGA_PHONE_NO, growerOrg.ORGA_EMAIL);
                                SendHandshakeEmail(persEmail ?? buyerOrg.ORGA_EMAIL, String.Format("{0};{1}", "support@logiflora.nl", growerOrg.ORGA_EMAIL), Session["FullName"], buyerOrg.ORGA_NAME, newHandshake.HAND_SEQUENCE, "BUYR", growerOrgaInfo);
                            }
                        }
                    }
                } else if (selectedOrgaType.ToString() == "BUYR" || selectedOrgaType.ToString() == "LOGP") {
                    Session[sessionKey] = true;
                    var handshakeExists = cab.F_CAB_HANDSHAKE.FirstOrDefault(x => x.HAND_FK_ORGA_SEQ_BUYER == selectedOrgaSequence && x.HAND_FK_ORGA_SEQ_GROWER == orgaSequence && x.HAND_ACCEPT_DATE != null && x.HAND_APPROVE_DATE != null);
                    if (handshakeExists != null)
                        return false;
                    F_CAB_ORGANISATION buyerOrg = cab.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == selectedOrgaSequence);
                    F_CAB_ORGANISATION growerOrg = cab.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == orgaSequence);
                    F_CAB_ORGANISATION_ADMIN selectedOrgaAdmin = cab.F_CAB_ORGANISATION_ADMIN.FirstOrDefault(x => x.F_CAB_ORGANISATION.ORGA_SEQUENCE == selectedOrgaSequence);

                    F_CAB_BUYER buyer = cab.F_CAB_BUYER.FirstOrDefault(x => x.BUYR_FK_ORGA == buyerOrg.ORGA_SEQUENCE);
                    F_CAB_GROWER grower = cab.F_CAB_GROWER.FirstOrDefault(x => x.GROW_FK_ORGA == growerOrg.ORGA_SEQUENCE);

                    if ((buyer == null || grower == null) && selectedOrgaType.ToString() == "BUYR") {
                        SendHandshakeErrorEmail(growerOrg.ORGA_EMAIL, "support@logiflora.nl", growerOrg.ORGA_NAME, buyerOrg.ORGA_NAME);
                        return false;
                    }

                    F_CAB_HANDSHAKE newHandshake = new F_CAB_HANDSHAKE {
                        HAND_FK_ORGA_SEQ_BUYER = buyerOrg.ORGA_SEQUENCE,
                        HAND_FK_ORGA_SEQ_GROWER = growerOrg.ORGA_SEQUENCE,
                        HAND_FK_HATY_SEQ = 1,
                        HAND_BLOCKED_YN = true,
                        HAND_REQUEST_REMARK = remark,
                        HAND_REQUEST_DATE = DateTime.Now,
                        HAND_CREATED_ON = DateTime.Now,
                        HAND_CREATED_BY = "LogiCAB:SendHandshakeInternal"
                    };
                    try {
                        cab.F_CAB_HANDSHAKE.AddObject(newHandshake);
                        cab.SaveChanges();

                    } catch (Exception ex) {
                        var exception = ex.Message;

                    }


                    F_CAB_HANDSHAKE_OPTIONS newHandshakeOptions = new F_CAB_HANDSHAKE_OPTIONS {
                        HNDO_AUTO_EKT_YN = false,
                        HNDO_FK_HAND_SEQ = newHandshake.HAND_SEQUENCE,
                        HNDO_CREATED_ON = DateTime.Now,
                        HNDO_CREATED_BY = "LogiCAB:SendHandshakeInternal",
                        HNDO_MODIFIED_ON = DateTime.Now,
                        HNDO_MODIFIED_BY = "LogiCAB:SendHandshakeInternal",
                        HNDO_LATEST_DELIVERY_TIME = DateTime.Today.AddHours(12),
                        HNDO_AUTO_CONFIRM_YN = false,
                        HNDO_FK_ADDR_SEQ = null,
                        HNDO_DELIVERY_TYPE = ""
                    };
                    cab.F_CAB_HANDSHAKE_OPTIONS.AddObject(newHandshakeOptions);
                    cab.SaveChanges();


                    if (buyer != null && grower != null) {
                        F_CAB_GROWER_BUYER_MATRIX growBuyMatrix = new F_CAB_GROWER_BUYER_MATRIX {
                            GAMA_FK_BUYER = buyer.BUYR_SEQUENCE,
                            GAMA_FK_GROWER = grower.GROW_SEQUENCE,
                            GAMA_BLOCKED_YN = 0
                        };
                        cab.F_CAB_GROWER_BUYER_MATRIX.AddObject(growBuyMatrix);
                        cab.SaveChanges();
                        cab.Refresh(RefreshMode.ClientWins, newHandshake);
                    }


                    if (newHandshake.HAND_SEQUENCE > 0) {
                        if (selectedOrgaAdmin.ORAD_STATUS != 1) {
                            SendHandshakeEmail(persEmail ?? growerOrg.ORGA_EMAIL, buyerOrg.ORGA_EMAIL, Session["FullName"], growerOrg.ORGA_NAME, newHandshake.HAND_SEQUENCE, "GROW", remark);
                        } else {
                            String buyerOrgaInfo = String.Format("BuyerOrga: {0}<br />Phone: {1}<br />Email: {2}", buyerOrg.ORGA_NAME, buyerOrg.ORGA_PHONE_NO, buyerOrg.ORGA_EMAIL);
                            SendHandshakeEmail(persEmail ?? buyerOrg.ORGA_EMAIL, String.Format("{0};{1}", "support@logiflora.nl", buyerOrg.ORGA_EMAIL), Session["FullName"], buyerOrg.ORGA_NAME, newHandshake.HAND_SEQUENCE, "BUYR", buyerOrgaInfo);
                        }
                    }
                }
            //}
            return true;
        }

        private void SendHandshakeErrorEmail(string fromEmail, string toEmail, string inviterOrgaName, string invitedOrgaName) {
            using (SmtpClient SMTP = new SmtpClient("mail.i-trade.nl")) {
                var mail = new MailMessage {
                    From = new MailAddress(fromEmail),
                    Subject = "[LogiPlaza: Error] Handshake failure",
                    IsBodyHtml = true,
                    Body = String.Format("An handshake attempt was made from {0} to {1}, but one of the organisations were missing information(grower/buyer-information)", inviterOrgaName, invitedOrgaName)
                };
                if (String.IsNullOrEmpty(mailOverride))
                    mail.To.Add(toEmail);
                else
                    mail.To.Add(mailOverride);
                try {
                    SMTP.Send(mail);
                } catch (Exception ex) {
                    throw new Exception(ex.Message);
                }
            }
        }
        private void SendHandshakeEmail(string fromEmail, string toEmail, object inviterFullName, string inviterOrgaName, int newHandSequence, string inviterOrgaType, string remark) {
            using (SmtpClient SMTP = new SmtpClient("mail.i-trade.nl")) {
                var urlHelper = new UrlHelper(Request.RequestContext);
                var activationLink = String.Format("http://www.logiplaza.nl/Handshakes/Activate?handshakeSequence={0}&inviterOrgaType={1}", newHandSequence, inviterOrgaType);

                var mail = new MailMessage {
                    From = new MailAddress(fromEmail),
                    Subject = "[LogiPlaza] (Re)Uitnodiging samenwerking LogiCAB",
                    IsBodyHtml = true,
                    Body = String.Format(
                        "Hierbij nodigt {0} u uit voor een samenwerking via Logi<i>CAB</i><sup>&reg;</sup><br />{3}<br />U kunt deze uitnodiging accepteren door op onderstaande link te klikken:<br /><a href=\"{1}\">Accepteer deze uitnodiging</a><br /><br />" +
                        "Na acceptatie kunt u direct zaken doen met {2}.<br />Met de acceptatie van deze handshake geeft u ook toestemming om mail te ontvangen vanuit en betreffende de dienst Logi<i>CAB</i><sup>&reg;</sup>"
                        , inviterFullName, activationLink, inviterOrgaName, remark),
                };
                if (String.IsNullOrEmpty(mailOverride))
                    mail.To.Add(toEmail);
                else
                    mail.To.Add(mailOverride);
                mail.ToString();
                try {
#if DEBUG
                    SMTP.Credentials = new System.Net.NetworkCredential("sigurd.boaseter@logiflora.nl", "!@#lf");
#endif

                    SMTP.Send(mail);
                } catch (Exception ex) {
                    //throw new Exception(ex.Message);
                }
            }
        }

    }
}
