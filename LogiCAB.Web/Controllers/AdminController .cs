﻿using DevExpress.Web;
using DevExpress.Web.Mvc;
using LogiCAB.Web.DAL;
using LogiCAB.Web.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DevExpress.Web.Mvc.UI;


namespace LogiCAB.Web.Controllers {
    public class AdminController : Controller {

        public string OfferDetail(int id) {
            var ofdt = EFHelper.ctx.F_CAB_OFFER_DETAILS.FirstOrDefault(x => x.OFDT_SEQUENCE == id);
            var picture = ofdt.Picture;
            return picture.ToString();
        }

        public ActionResult Order() {
//            var orderDetails = .ctx.F_CAB_ORDER_DET.Select(x => x.ORDE_CREATED_ON).OrderBy(x => x).ToList();
            
            return View();


        }


        public JsonResult GetOrders() {
            var orderDetails = EFHelper.ctx.F_CAB_ORDER_DET.OrderBy(z => z.ORDE_SEQUENCE).Select(x => new OrderChart { Hour = x.ORDE_CREATED_ON.Hour, Value = x.ORDE_NETT_LINE_VALUE }).Take(1000).ToList();
            //List<OrderChart> 
            return Json(orderDetails, JsonRequestBehavior.AllowGet);
        }
    }
    public class OrderChart {
        public int Hour { get; set; }
        public decimal Value {get;set;}
    }
}
