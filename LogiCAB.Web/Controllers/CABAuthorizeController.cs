﻿using CAB.Data.EF.Models;
using CAB.Domain.Helpers;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using log4net;
using LogiCAB.Web.DAL;
using LogiCAB.Web.Domain;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Dapper;

namespace LogiCAB.Web.Controllers {
    [HandleError]
    [AuthorizeWithSessionAttribute]
    public class CABAuthorizeController : BaseController {
        public CABAuthorizeController(ILog log) : base(log) { }
        public ActionResult Index() {
            Session["currentUser"] = Usr.Person.FirstName;
            return View();
        }

        public ActionResult List() {
            return PartialView("List", _getCABRequestList());
        }

        public ActionResult Update([ModelBinder(typeof(DevExpressEditorsBinder))] CABRequest req) {
            if (ModelState.IsValid) {
                var test1 = req.cdgr;
                Debug.WriteLine("cdgr: " + test1.CDGR_DESC);
            } else {
                ViewData["updateerrors"] = "Error occured while trying to save CABRequest changes";
            }
            return PartialView("List", _getCABRequestList());
        }

        public enum RequestStatus {
            Failed,
            Success,
            MissingInfo,
            MissingRequest,
            MissingProperties,
            MissingCasks,
            MissingPicture,
            MissingVBNCode
        }

        private RequestStatus ValidateRequest(F_CAB_QUEUE_CODE_REQUEST req, IQueryable<F_CAB_QUEUE_CODE_REQ_DETAIL> det, IQueryable<F_CAB_QUEUE_CODE_REQ_CASK> cask) {
            if (req.QCRQ_FK_VBN_CODE == 1 && req.QCRQ_FK_VBN_CODE == null) {
                return RequestStatus.MissingVBNCode;
            }
            if (req.QCRQ_FK_PICTURE == null) {
                return RequestStatus.MissingPicture;
            }
            if (CABCodeExists(req) > 0)
                return RequestStatus.Failed;
            return RequestStatus.Success;
        }

        private int CreateCABCodeFromRequest(F_CAB_QUEUE_CODE_REQUEST req) {
            //only for existing request with no cab code assigned
            if (req.QCRQ_FK_CAB_CODE_ASSIGNED == null)  {
                var newcode = new F_CAB_CD_CAB_CODE();
                newcode.CABC_CREATED_ON = DateTime.Now;
                newcode.CABC_MODIFIED_ON = DateTime.Now;
                newcode.CABC_CREATED_BY = Usr.Person.FullName;
                newcode.CABC_MODIFIED_BY = Usr.Person.FullName;
                newcode.CABC_CAB_DESC = req.QCRQ_CAB_DESC_ASSIGNED;
                newcode.CABC_CAB_DESC_UPPER = req.QCRQ_CAB_DESC_ASSIGNED.ToUpper();
                newcode.CABC_IS_AUTHORISED = 1;
                newcode.CABC_FK_CAB_GROUP = (int)req.QCRQ_FK_CAB_GROUP_ASSIGNED;
                newcode.CABC_FK_PICTURE = req.QCRQ_FK_PICTURE;
                newcode.CABC_FK_PD_GROUP = req.QCRQ_FK_PD_GROUP_ASSIGNED;
                var maxNum = EFHelper.ctx.F_CAB_CD_CAB_CODE.Max(x => x.CABC_CAB_CODE);
                var intMaxNum = Int32.Parse(maxNum) + 1;
                var sCabCode = String.Format("{0:00000000}", intMaxNum);
                newcode.CABC_CAB_CODE = sCabCode;
                EFHelper.ctx.F_CAB_CD_CAB_CODE.AddObject(newcode);
                EFHelper.ctx.SaveChanges();

                //Add the new cab code to the matrix
                var matrix = new F_CAB_CAB_VBN_MATRIX();
                matrix.CVMA_FK_CAB_CODE = newcode.CABC_SEQUENCE;
                matrix.CVMA_FK_VBN_CODE = req.QCRQ_FK_VBN_CODE;

                EFHelper.ctx.F_CAB_CAB_VBN_MATRIX.AddObject(matrix);
                EFHelper.ctx.SaveChanges();

                //copy the detail properties
                int i = 0;
                foreach (var prop in req.F_CAB_QUEUE_CODE_REQ_DETAIL.OrderBy(x => x.F_CAB_CD_CAB_PROP_TYPE.CDPT_DESC)) {
                    i++;
                    var newprop = new F_CAB_CAB_DETAIL();
                    newprop.CABD_FK_CAB_CODE = newcode.CABC_SEQUENCE;
                    newprop.CABD_FK_TYPE = prop.QCRD_FK_CAB_PROP_TYPE;
                    newprop.CABD_VALUE = prop.QCRD_ASS_PROP_VALUE.Replace("  ", " ").Trim();
                    //check if the assigned value exists in the prop lookup. If not add it to the lookup
                    if (newprop.CABD_VALUE != null && newprop.CABD_VALUE.Trim().Length > 0) {
                        var check = EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.Any(x => x.CDPL_LOOKUP_VALUE == newprop.CABD_VALUE && x.CDPL_FK_PROP_TYPE == prop.QCRD_FK_CAB_PROP_TYPE);
                        if (check == false) {
                            var maxExists = EFHelper.ctx.F_CAB_CD_CAB_PROP_TYPE.FirstOrDefault(x => x.CDPT_SEQUENCE == prop.QCRD_FK_CAB_PROP_TYPE);
                            int icounter = 1;
/*                            try { 
                                var max = maxExists == null ? 1 : maxExists.F_CAB_CD_CAB_PROP_LOOKUP.Max(x => x.CDPL_ORDER);
                                icounter = max;
                                icounter = icounter + 1;
                            } catch (Exception ex) {
                                Log.ErrorFormat("Error occured in CABAuthorizeController: CreateCABCodeFromRequest: getting CDPL_ORDER: ex: {0}", ex.ToString());
                            }*/
                            var lookup = new F_CAB_CD_CAB_PROP_LOOKUP();
                            lookup.CDPL_FK_PROP_TYPE = newprop.CABD_FK_TYPE;
                            lookup.CDPL_LOOKUP_DESC = newprop.CABD_VALUE;
                            lookup.CDPL_LOOKUP_VALUE = newprop.CABD_VALUE;
//                            lookup.CDPL_ORDER = icounter;
                            EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.AddObject(lookup);

                            //newprop.F_CAB_CD_CAB_PROP_TYPE.F_CAB_CD_CAB_PROP_LOOKUP.Add(lookup);
                        }
                    }
                    newprop.CABD_VALUE_DESC = prop.QCRD_ASS_PROP_DESC.Trim();
                    newprop.CABD_VALUE_DESC_UPPER = prop.QCRD_ASS_PROP_DESC.ToUpper();
                    newprop.CABD_ORDER = i;
                    newcode.F_CAB_CAB_DETAIL.Add(newprop);
                }

                //update the request
                req.QCRQ_FK_CAB_CODE_ASSIGNED = newcode.CABC_SEQUENCE;
                req.QCRQ_STATUS = "ADDED";
                req.QCRQ_HANDLED_BY = Usr.Person.FullName;
                req.QCRQ_HANDLED_DATE = DateTime.Now;

                EFHelper.ctx.SaveChanges();
                EFHelper.ctx.ExecuteStoreCommand(String.Format("exec proc_fill_and_sync_properties {0}", newcode.CABC_SEQUENCE));
                Log.InfoFormat("Ran: exec proc_fill_and_sync_properties {0}", newcode.CABC_SEQUENCE);
                addToAssortiment(req);
                return 1;
            }
            return -1;
        }

        // TODO: Unify
        private string addToAssortiment(F_CAB_QUEUE_CODE_REQUEST request) {
            int growerSequence = request.QCRQ_FK_GROWER;
            int cab_seq = (int)request.QCRQ_FK_CAB_CODE_ASSIGNED;
            string result = "";
            try {
                // Add image-upload and cask
                string cabArticle = EFHelper.ctx.F_CAB_CD_CAB_CODE.Single(x => x.CABC_SEQUENCE == cab_seq).CABC_CAB_DESC;
                bool exists = EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.Any(x => x.GRAS_FK_GROWER == growerSequence && x.GRAS_FK_CAB_CODE == cab_seq);
                if (exists) {
                    result = String.Format("{0} is al aanwezig in uw assortiment!", cabArticle);
                    addRequestsCasks(request, EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.FirstOrDefault(x => x.GRAS_FK_GROWER == growerSequence && x.GRAS_FK_CAB_CODE == cab_seq));
                } else {
                    F_CAB_GROWER_ASSORTIMENT newGras = new F_CAB_GROWER_ASSORTIMENT {
                        GRAS_DELETED_YN = false,
                        GRAS_FK_CAB_CODE = cab_seq,
                        GRAS_FK_GROWER = growerSequence,
                        GRAS_NUM_OF_M2 = 0,
                        GRAS_SELECTED_YN = true,
                        GRAS_START_DATE = DateTime.Now,
                        GRAS_NUM_OF_ITEMS = 0,
                        GRAS_FK_PICTURE = request.QCRQ_FK_PICTURE ?? null
                    };
                    EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.AddObject(newGras);
                    EFHelper.ctx.SaveChanges();

                    foreach (var cask in newGras.F_CAB_CD_CAB_CODE.F_CAB_CAB_CASK_MATRIX.OrderBy(x => x.F_CAB_CD_CASK.CDCA_CASK_DESC)) {
                        // Check if casks exists in grower_cask_matrix, if not, add it and set show_cask to true
                        var gcm = newGras.F_CAB_GROWER_CASK_MATRIX.FirstOrDefault(x => x.F_CAB_CAB_CASK_MATRIX.F_CAB_CD_CASK.CDCA_SEQUENCE == cask.F_CAB_CD_CASK.CDCA_SEQUENCE && x.CGCM_FK_CACA == cask.CACA_SEQUENCE);
                        if (gcm == null) {
                            newGras.F_CAB_GROWER_CASK_MATRIX.Add(new F_CAB_GROWER_CASK_MATRIX {
//                                CGCM_FK_CASK = cask.CACA_FK_CASK,
                                CGCM_SHOW_CASK = true,
                                CGCM_FK_GROWER_ASSORTIMENT = newGras.GRAS_SEQUENCE,
                                CGCM_FK_CACA = cask.CACA_SEQUENCE
                            });
                        }
                        EFHelper.ctx.SaveChanges();
                    }
                    addRequestsCasks(request, newGras);
                    result = String.Format("{0} toegevoegd.", newGras.GRAS_SEQUENCE);
                    Log.InfoFormat("Auth: CABCode->Assortiment: {0}: Added CAB-Article to his assortiment: {1}, {2}", request.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_NAME, cab_seq, cabArticle);
                    EFHelper.ctx.ExecuteStoreCommand(String.Format("exec proc_fill_and_sync_properties_assortment {0}", newGras.GRAS_SEQUENCE));
                }
                
                return result;
            } catch (Exception ex) {
                Log.Error("Error in addToAssortiment: ", ex);
                return "Error occured while adding an article to your assortiment";
            }

        }

        private void addRequestsCasks(F_CAB_QUEUE_CODE_REQUEST request, F_CAB_GROWER_ASSORTIMENT gras) {

            foreach (var cask in request.F_CAB_QUEUE_CODE_REQ_CASK) {
                var cgcm = EFHelper.ctx.F_CAB_GROWER_CASK_MATRIX.FirstOrDefault(x =>
                    x.CGCM_FK_GROWER_ASSORTIMENT == gras.GRAS_SEQUENCE &&
                    x.F_CAB_CAB_CASK_MATRIX.F_CAB_CD_CASK.CDCA_SEQUENCE == cask.QCRC_FK_CASK &&
                    x.F_CAB_CAB_CASK_MATRIX.CACA_NUM_OF_UNITS == cask.QCRC_NUM_OF_UNITS &&
                    x.F_CAB_CAB_CASK_MATRIX.CACA_UNITS_PER_LAYER == cask.QCRC_UNITS_PER_LAYER &&
                    x.F_CAB_CAB_CASK_MATRIX.CACA_LAYERS_PER_TROLLEY == cask.QCRC_LAYERS_PER_TROLLEY);
                if (cgcm != null) continue;
                var newCaca = new F_CAB_CAB_CASK_MATRIX {
                    CACA_FK_CAB_CODE = gras.GRAS_FK_CAB_CODE,
                    CACA_FK_CASK = cask.QCRC_FK_CASK,
                    CACA_NUM_OF_UNITS = cask.QCRC_NUM_OF_UNITS,
                    CACA_UNITS_PER_LAYER = cask.QCRC_UNITS_PER_LAYER,
                    CACA_LAYERS_PER_TROLLEY = cask.QCRC_LAYERS_PER_TROLLEY
                };
                EFHelper.ctx.F_CAB_CAB_CASK_MATRIX.AddObject(newCaca);
                EFHelper.ctx.SaveChanges();
                EFHelper.ctx.F_CAB_GROWER_CASK_MATRIX.AddObject(new F_CAB_GROWER_CASK_MATRIX {
                    CGCM_FK_CACA = newCaca.CACA_SEQUENCE,
//                    CGCM_FK_CASK = cask.QCRC_FK_CASK,
                    CGCM_FK_GROWER_ASSORTIMENT = gras.GRAS_SEQUENCE,
                    CGCM_SHOW_CASK = true,
                });
                EFHelper.ctx.SaveChanges();
            }

        }

        public JsonResult SavePropertyToDetail(int propType, string propValue) {
            var currentRequest = (int)Session["currentrequest"];
            var det = EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.SingleOrDefault(x => x.QCRD_FK_CODE_REQUEST == currentRequest && x.QCRD_FK_CAB_PROP_TYPE == propType);
            if (det == null) return Json("notok", JsonRequestBehavior.AllowGet);
            det.QCRD_ASS_PROP_VALUE = propValue;
            det.QCRD_PROP_VALUE = propValue;
            EFHelper.ctx.SaveChanges();
            return Json("ok", JsonRequestBehavior.AllowGet);
        }

        [OutputCache(Duration = 0, VaryByParam = "None")]
        [HttpPost]
        public JsonResult RejectRequest(int id, string remark) {
            var retData = new {
                Error = string.Empty
            };

            try { 
                var request = EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.FirstOrDefault(x => x.QCRQ_SEQUENCE == id);
                request.QCRQ_REMARK = remark;
                request.QCRQ_HANDLED_BY = Usr.Person.FullName;
                request.QCRQ_HANDLED_DATE = DateTime.Now;
                request.QCRQ_STATUS = "REJECTED";
                EFHelper.ctx.SaveChanges();
            } catch (Exception ex) {
                retData = new { Error = "An error occured!" };
            }
            return Json(retData, JsonRequestBehavior.AllowGet);
        }



        [OutputCache(Duration = 0, VaryByParam = "None")]
        [HttpPost]
        public JsonResult ExistingRequest(int id) {
            var cabCode = EFHelper.ctx.F_CAB_CD_CAB_CODE.SingleOrDefault(x => x.CABC_SEQUENCE == id);
            var reqId = (int)Session["currentrequest"];
            var currentRequest = EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.SingleOrDefault(x => x.QCRQ_SEQUENCE == reqId);

            if (cabCode != null && currentRequest != null) {
                // Perform actions: Status to "bestat al", add to assortiment, casks
                // First assign the CABCode to the Request:
                currentRequest.QCRQ_FK_CAB_CODE_ASSIGNED = id;
                currentRequest.QCRQ_STATUS = "EXIST";
                currentRequest.QCRQ_HANDLED_BY = Usr.Person.FullName;
                currentRequest.QCRQ_HANDLED_DATE = DateTime.Now;
                EFHelper.ctx.SaveChanges();
                addToAssortiment(currentRequest);
                var grower = currentRequest.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_NAME;
                var retData = new {
                    Grower = grower,
                    CABDesc = String.Format("{0}: {1}", cabCode.CABC_CAB_CODE, cabCode.CABC_CAB_DESC),
                    Notify = "Yes",
                    Error = string.Empty
                };
                return Json(retData, JsonRequestBehavior.AllowGet);
            } else {
                var retData = new {
                    Grower = string.Empty,
                    CABDesc = string.Empty,
                    Notify = string.Empty,
                    Error = String.Format("Internal error occured: Request: {0}, CABCode: {1}", reqId, id)
                };
                return Json(retData, JsonRequestBehavior.AllowGet);               
            }
        }

        [OutputCache(Duration = 0, VaryByParam = "None")]
        [HttpPost]
        public string SaveRequest(string req)
        {
            try
            {
                var submittedRequest = new JavaScriptSerializer().Deserialize<CABRequestJSON>(req);
                var requestSequence = Int32.Parse(submittedRequest.Seq);
                var storedRequest = EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.FirstOrDefault(x => x.QCRQ_SEQUENCE == requestSequence);
                if (storedRequest != null)
                {
                    storedRequest.QCRQ_FK_CAB_GROUP_TYPE = Int32.Parse(submittedRequest.CABGroupTypeSeq);
                    storedRequest.QCRQ_FK_CAB_GROUP_ASSIGNED = Int32.Parse(submittedRequest.CABGroupSeq);
                    storedRequest.QCRQ_FK_PD_GROUP_ASSIGNED = Int32.Parse(submittedRequest.PDGroupSeq);
                    storedRequest.QCRQ_REMARK = submittedRequest.AuthorizerRemark;
                    storedRequest.QCRQ_CAB_DESC = submittedRequest.CABDescRequested;
                    storedRequest.QCRQ_CAB_DESC_ASSIGNED = submittedRequest.CABDescRequested;
                    storedRequest.QCRQ_STATUS = "TEMP";
                    EFHelper.ctx.SaveChanges();
                }
                return String.Format("CABRequest saved as Temp. {0}", storedRequest.QCRQ_SEQUENCE);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("An error occured while attempting to Temp.Save a CABRequest, Req: {0}", req);
                return String.Format("An error occured while attempting to Temp.Save a CABRequest, Req: {0}", req);
            }
        }

        [OutputCache(Duration = 0, VaryByParam = "None")]
        [HttpPost]
        public JsonResult ApproveRequest(string req) {
            var submittedRequest = new JavaScriptSerializer().Deserialize<CABRequestJSON>(req);
            var requestSequence = Int32.Parse(submittedRequest.Seq);
            var storedRequest = EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.FirstOrDefault(x => x.QCRQ_SEQUENCE == requestSequence);
            if (storedRequest != null)
            {
                storedRequest.QCRQ_FK_CAB_GROUP_TYPE = Int32.Parse(submittedRequest.CABGroupTypeSeq);
                storedRequest.QCRQ_FK_CAB_GROUP_ASSIGNED = Int32.Parse(submittedRequest.CABGroupSeq);
                storedRequest.QCRQ_FK_PD_GROUP_ASSIGNED = Int32.Parse(submittedRequest.PDGroupSeq);
                storedRequest.QCRQ_REMARK = submittedRequest.AuthorizerRemark;
                storedRequest.QCRQ_CAB_DESC = submittedRequest.CABDescRequested;
                storedRequest.QCRQ_CAB_DESC_ASSIGNED = submittedRequest.CABDescRequested;
                EFHelper.ctx.SaveChanges();


                var storedRequestDetails = EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.Where(x => x.QCRD_FK_CODE_REQUEST == requestSequence);
                var storedRequestCasks = EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_CASK.Where(x => x.QCRC_FK_CODE_REQUEST == requestSequence);

                if (!storedRequestDetails.Any())
                    return Json(
                        RequestStatus.MissingProperties, 
                        JsonRequestBehavior.AllowGet);
                if (!storedRequestCasks.Any())
                    return Json(
                        RequestStatus.MissingCasks, 
                        JsonRequestBehavior.AllowGet);

                // Should allready be validated at the time of sending, but re-validate on the authorizer-side to be sure.
                Log.InfoFormat("Request: {0} has all 3 tables of information present, proceding to validate information within these tables.", requestSequence);

                var validationResult = ValidateRequest(storedRequest, storedRequestDetails, storedRequestCasks);

                switch (validationResult) {
                    case RequestStatus.Success:
                        CreateCABCodeFromRequest(storedRequest);
                        return Json(RequestStatus.Success, JsonRequestBehavior.AllowGet);
                    default:
                        return Json(validationResult, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(RequestStatus.Failed, JsonRequestBehavior.AllowGet);
        }
        private int CABCodeExists(F_CAB_QUEUE_CODE_REQUEST request) {
            var existingCodes = getRelatedCABCodes(request.QCRQ_FK_VBN_CODE);
            if (existingCodes == null) return -1; // related cab-codes found
            foreach (F_CAB_CD_CAB_CODE code in existingCodes) {
                IEnumerable<F_CAB_CAB_DETAIL> details = EFHelper.ctx.F_CAB_CAB_DETAIL.Where(x => x.CABD_FK_CAB_CODE == code.CABC_SEQUENCE);
                bool identialProps = true;
                foreach (F_CAB_QUEUE_CODE_REQ_DETAIL detail in request.F_CAB_QUEUE_CODE_REQ_DETAIL) {
                    if (!details.Any(x => x.CABD_FK_TYPE == detail.QCRD_FK_CAB_PROP_TYPE && x.CABD_VALUE.Trim() == detail.QCRD_PROP_VALUE.Trim())) {
                        identialProps = false; // A propertyvalue in the request is not found the the cab-cab-details
                    }
                }
                if (identialProps) return code.CABC_SEQUENCE;
            }
            return -1;
        }

        
        public JsonResult getCABRequestListJson() {
            var reqList = from qcrq in EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST
                   join grow in EFHelper.ctx.F_CAB_GROWER on qcrq.QCRQ_FK_GROWER equals grow.GROW_SEQUENCE
                   join orga in EFHelper.ctx.F_CAB_ORGANISATION on grow.GROW_FK_ORGA equals orga.ORGA_SEQUENCE
                   join vbnc in EFHelper.ctx.F_CAB_CD_VBN_CODE on qcrq.QCRQ_FK_VBN_CODE equals vbnc.VBNC_SEQUENCE
                   join cdgt in EFHelper.ctx.F_CAB_CD_GROUP_TYPE on qcrq.QCRQ_FK_CAB_GROUP_TYPE equals cdgt.CDGT_SEQUENCE

                   join cdgr in EFHelper.ctx.F_CAB_CD_CAB_GROUP on qcrq.QCRQ_FK_CAB_GROUP_ASSIGNED equals cdgr.CDGR_SEQUENCE into cdgrO
                   from cdgrC in cdgrO.DefaultIfEmpty()

                   join pdgr in EFHelper.ctx.F_CAB_CD_PD_GROUP on qcrq.QCRQ_FK_PD_GROUP_ASSIGNED equals pdgr.CDPD_SEQUENCE into pdgrO
                   from pdgrC in pdgrO.DefaultIfEmpty()

                   where
                       qcrq.QCRQ_CAB_DESC != null &&
                       qcrq.QCRQ_FK_CAB_GT_ASSIGNED != null &&
                       qcrq.QCRQ_CAB_DESC_ASSIGNED != null

                   select new CABRequest {
                       request_date = qcrq.QCRQ_REQUEST_DATE,
                       status = qcrq.QCRQ_STATUS,
//                       vbn = vbnc,
                       requester = orga.ORGA_NAME,
                       cabdesc = qcrq.QCRQ_CAB_DESC,
                       cabgroup = cdgt.CDGT_DESC,
  //                     cdgt = cdgt,
//                       cdgr = cdgrC != null ? cdgrC : null,
//                       pdgr = pdgrC != null ? pdgrC : null,
                       handledby = qcrq.QCRQ_HANDLED_BY,
                       handledon = qcrq.QCRQ_HANDLED_DATE,
                       authremark = qcrq.QCRQ_REMARK,
                       remark = qcrq.QCRQ_GROWER_REMARK,
  //                     seq = qcrq.QCRQ_SEQUENCE,
                       pictureseq = qcrq.QCRQ_FK_PICTURE,
                       cabcode = qcrq.QCRQ_FK_CAB_CODE_ASSIGNED
                   };
            return Json(reqList.ToList(), JsonRequestBehavior.AllowGet);


            //            return CompiledQueries.s_compiledCABRequests.Invoke(EFHelper.ctx);
        }


        private IQueryable<CABRequest> _getCABRequestList() {
            return from qcrq in EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST
                   join grow in EFHelper.ctx.F_CAB_GROWER on qcrq.QCRQ_FK_GROWER equals grow.GROW_SEQUENCE
                   join orga in EFHelper.ctx.F_CAB_ORGANISATION on grow.GROW_FK_ORGA equals orga.ORGA_SEQUENCE
                   join vbnc in EFHelper.ctx.F_CAB_CD_VBN_CODE on qcrq.QCRQ_FK_VBN_CODE equals vbnc.VBNC_SEQUENCE
                   join cdgt in EFHelper.ctx.F_CAB_CD_GROUP_TYPE on qcrq.QCRQ_FK_CAB_GROUP_TYPE equals cdgt.CDGT_SEQUENCE

                   join cdgr in EFHelper.ctx.F_CAB_CD_CAB_GROUP on qcrq.QCRQ_FK_CAB_GROUP_ASSIGNED equals cdgr.CDGR_SEQUENCE into cdgrO
                   from cdgrC in cdgrO.DefaultIfEmpty()

                   join pdgr in EFHelper.ctx.F_CAB_CD_PD_GROUP on qcrq.QCRQ_FK_PD_GROUP_ASSIGNED equals pdgr.CDPD_SEQUENCE into pdgrO
                   from pdgrC in pdgrO.DefaultIfEmpty()

                   where
                       qcrq.QCRQ_CAB_DESC != null &&
                       qcrq.QCRQ_FK_CAB_GT_ASSIGNED != null &&
                       qcrq.QCRQ_CAB_DESC_ASSIGNED != null 
                       //qcrq.F_CAB_CD_CAB_GROUP.F_CAB_CD_GROUP_TYPE.CDGT_FK_MAIN_GROUP == Usr.SelectedMaingroup

                   select new CABRequest {
                       request_date = qcrq.QCRQ_REQUEST_DATE,
                       status = qcrq.QCRQ_STATUS,
                       vbn = vbnc,
                       requester = orga.ORGA_NAME,
                       cabdesc = qcrq.QCRQ_CAB_DESC,
                       cabgroup = cdgt.CDGT_DESC,
                       cdgt = cdgt,
                       cdgr = cdgrC != null ? cdgrC : null,
                       pdgr = pdgrC != null ? pdgrC : null,
                       handledby = qcrq.QCRQ_HANDLED_BY,
                       handledon = qcrq.QCRQ_HANDLED_DATE,
                       authremark = qcrq.QCRQ_REMARK,
                       remark = qcrq.QCRQ_GROWER_REMARK,
                       seq = qcrq.QCRQ_SEQUENCE,
                       pictureseq = qcrq.QCRQ_FK_PICTURE,
                       cabcode = qcrq.QCRQ_FK_CAB_CODE_ASSIGNED
                   };
            

//            return CompiledQueries.s_compiledCABRequests.Invoke(EFHelper.ctx);
        }
        public static IEnumerable<F_CAB_CD_CAB_CODE> getRelatedCABCodes(int vbnseq) {
            var listofcodes = (from cvma in EFHelper.ctx.F_CAB_CAB_VBN_MATRIX.Where(x => x.CVMA_FK_VBN_CODE == vbnseq) select cvma.F_CAB_CD_CAB_CODE).ToList();
            return listofcodes;
        }

        public ActionResult CABRequestTemplatePartial(int id) {
            Session["currentrequest"] = id;
            // Replace with Dapper : TODO!
            return PartialView(
                //CompiledQueries.s_compiledCABRequests.Invoke(EFHelper.ctx)
                (from qcrq in EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST
                 join grow in EFHelper.ctx.F_CAB_GROWER on qcrq.QCRQ_FK_GROWER equals grow.GROW_SEQUENCE
                 join orga in EFHelper.ctx.F_CAB_ORGANISATION on grow.GROW_FK_ORGA equals orga.ORGA_SEQUENCE
                 join vbnc in EFHelper.ctx.F_CAB_CD_VBN_CODE on qcrq.QCRQ_FK_VBN_CODE equals vbnc.VBNC_SEQUENCE
                 join cdgt in EFHelper.ctx.F_CAB_CD_GROUP_TYPE on qcrq.QCRQ_FK_CAB_GROUP_TYPE equals cdgt.CDGT_SEQUENCE

                where
                    qcrq.QCRQ_CAB_DESC != null &&
                    qcrq.QCRQ_FK_CAB_GT_ASSIGNED != null &&
                    qcrq.QCRQ_CAB_DESC_ASSIGNED != null

                select new CABRequest {
                    request_date = qcrq.QCRQ_REQUEST_DATE,
                    status = qcrq.QCRQ_STATUS,
                    vbn = vbnc,
                    requester = orga.ORGA_NAME,
                    cabdesc = qcrq.QCRQ_CAB_DESC,
                    cabgroup = cdgt.CDGT_DESC,
                    cdgt = cdgt,
                    handledby = qcrq.QCRQ_HANDLED_BY,
                    handledon = qcrq.QCRQ_HANDLED_DATE,
                    authremark = qcrq.QCRQ_REMARK,
                    remark = qcrq.QCRQ_GROWER_REMARK,
                    seq = qcrq.QCRQ_SEQUENCE,
                    pictureseq = qcrq.QCRQ_FK_PICTURE
                })


                .FirstOrDefault(x => x.seq == id)
                );
        }

        public ActionResult RelatedCABCodesPartial(int id) {
            var relatedCABCodes = (from cvma in EFHelper.ctx.F_CAB_CAB_VBN_MATRIX
                                join cabc in EFHelper.ctx.F_CAB_CD_CAB_CODE
                                    on cvma.CVMA_FK_CAB_CODE equals cabc.CABC_SEQUENCE
                                where
                                    cvma.CVMA_FK_VBN_CODE == id
                                select new CABCode {
                                    CABC_SEQUENCE = cabc.CABC_SEQUENCE,
                                    CABC_CAB_CODE = cabc.CABC_CAB_CODE,
                                    CABC_CAB_DESC = cabc.CABC_CAB_DESC,
                                    CABC_IS_AUTHORISED = cabc.CABC_IS_AUTHORISED,
                                    CABC_FK_PICTURE = cabc.CABC_FK_PICTURE,
                                    details = cabc.F_CAB_CAB_DETAIL.ToList()
                                }).ToList();
            return PartialView(relatedCABCodes);
        }

        public ActionResult CABRequestDetailsPartial(int id) {
            return PartialView(_getCABRequestDetails(id));
        }

        [HttpPost, ValidateInput(true)]
        public ActionResult CABRequestDetailsPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] F_CAB_CD_CAB_PROP_LOOKUP det) {
            if (ModelState.IsValid) {
                try {
                } catch (Exception e) {
                    // TODO: Update Prop-table
                    ViewData["EditError"] = e.Message;
                }
            } else
                ViewData["EditError"] = "Please, correct all errors.";
            int qcrq_seq = !string.IsNullOrEmpty(Request.Params["qcrq_sequence"]) ? int.Parse(Request.Params["qcrq_sequence"]) : 0;
            return PartialView("CABRequestDetailsPartial", _getCABRequestDetails(qcrq_seq));
        }

        public static IQueryable<F_CAB_QUEUE_CODE_REQ_DETAIL> _getCABRequestDetails(int id) {
            return from qcrd in EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL
                   where qcrd.QCRD_FK_CODE_REQUEST == id
                   select qcrd;
        }
        public ActionResult getCABRequestDetails(int id) {
            return PartialView("CABRequestDetailsPartial", _getCABRequestDetails(id));
        }

        public ActionResult CABRequestCasksPartial(int id) {
            return PartialView(_getCABRequestCasks(id));
        }

        public static IQueryable<F_CAB_QUEUE_CODE_REQ_CASK> _getCABRequestCasks(int id) {
            if (EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_CASK.Any(x => x.QCRC_FK_CODE_REQUEST == id)) { 
                return from qcrc in EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_CASK
                       where qcrc.QCRC_FK_CODE_REQUEST == id
                       select qcrc;
            } else {
                return null;
            }
        }
        public ActionResult getCABRequestCasks(int id) {
            return PartialView("CABRequestCasksPartial", _getCABRequestCasks(id));
        }


        /* Comboboxes start */
        public ActionResult GroupTypeCBPartial(int id = -1) {
            var CABGroupTypes = from cdgt in EFHelper.ctx.F_CAB_CD_GROUP_TYPE
                                where cdgt.CDGT_FK_MAIN_GROUP == Usr.SelectedMaingroup
                                select cdgt;
            if (id != -1)
                TempData["cbGroupType"] = id;
            return PartialView(CABGroupTypes);
        }

        public ActionResult CABGroupCBPartial(int id = -1) {
            var CABGroups = from cdgr in EFHelper.ctx.F_CAB_CD_CAB_GROUP
                            where cdgr.F_CAB_CD_GROUP_TYPE.CDGT_FK_MAIN_GROUP == Usr.SelectedMaingroup
                            select cdgr;
            if (id != -1)
                TempData["cbCabGroup"] = id;
            return PartialView(CABGroups);
        }
        
        public ActionResult PDGroupCBPartial(int id = -1) {
            var PDGroups = from cdpd in EFHelper.ctx.F_CAB_CD_PD_GROUP
                           join cdpy in EFHelper.ctx.F_CAB_CD_PD_TYPE on cdpd.CDPD_FK_PD_TYPE equals cdpy.CDPY_SEQUENCE
                           select cdpd;
            if (id != -1)
                TempData["cbPDGroup"] = id;
            return PartialView(PDGroups);
        }

        public ActionResult CABPropLookupCBPartial(int id, string propvalue ) {
            var CABProperties = from p in EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP
                                where p.CDPL_FK_PROP_TYPE == id
                                select p;
            ViewData["propertytype"] = id;
            ViewData["propValue"] = propvalue;
            return PartialView(CABProperties);
        }



        /* Comboboxes end */

        [HttpPost]
        public string addNewCaskToRequest(int cdca_seq, int prcask, int prlayer, int prtrolley, int qcrq) {

            try {
                F_CAB_CD_CASK cask = EFHelper.ctx.F_CAB_CD_CASK.FirstOrDefault(x => x.CDCA_SEQUENCE == cdca_seq);
                if (cask == null) return "Invalid CDCA_SEQUENCE";
                F_CAB_QUEUE_CODE_REQ_CASK newCask = new F_CAB_QUEUE_CODE_REQ_CASK {
                    QCRC_FK_CODE_REQUEST = qcrq,
                    F_CAB_CD_CASK = cask,
                    QCRC_NUM_OF_UNITS = prcask,
                    QCRC_UNITS_PER_LAYER = prlayer,
                    QCRC_LAYERS_PER_TROLLEY = prtrolley,
                };
                EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_CASK.AddObject(newCask);
                EFHelper.ctx.SaveChanges();
                return "";
            } catch {
                return "Error";

            }

        }

        public ActionResult CallbacksImageUpload() {
            int id = (int)System.Web.HttpContext.Current.Session["currentrequest"];
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ucCallbacks", null, ucCallbacks_FileUploadComplete);
            UploadedFile file = files[0];
            MemoryStream ms = new MemoryStream(file.FileBytes);
            Image tempImg = Image.FromStream(ms);
            Image newImage = ImageUtils.resizeImage(tempImg, new Size { Height = 1024, Width = 1024 });
            MemoryStream ms2 = new MemoryStream();
            newImage.Save(ms2, ImageFormat.Jpeg);
            F_CAB_PICTURE newPic = new F_CAB_PICTURE {
                PICT_PICTURE = ms2.ToArray()
            };
            EFHelper.ctx.F_CAB_PICTURE.AddObject(newPic);
            EFHelper.ctx.SaveChanges();
            EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.FirstOrDefault(x => x.QCRQ_SEQUENCE == id).QCRQ_FK_PICTURE = newPic.PICT_SEQUENCE;
            EFHelper.ctx.SaveChanges();
            return null;
        }

        public static void ucCallbacks_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
            int newPic = -1;
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                newPic = conn.Query<int>("select top 1 PICT_SEQUENCE FROM F_CAB_PICTURE ORDER BY 1 DESC").First();
            }
            newPic++;
            e.CallbackData = newPic.ToString();
        }

        public ActionResult PropValueComboPartial(int proptype)
        {
            return PartialView(cbPropValEdit(proptype));
        }

        public List<PropertyLookup> cbPropValEdit(int proptype)
        {

            var propValues = EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.Where(x => x.CDPL_FK_PROP_TYPE == proptype).Select(y => new PropertyLookup  { 
                CDPL_SEQUENCE = y.CDPL_SEQUENCE,
                CDPL_LOOKUP_VALUE = y.CDPL_LOOKUP_VALUE
                 }).ToList();
            return propValues;
        }

    }
    public class PropertyLookup
    {
        public int CDPL_SEQUENCE { get; set; }
        public string CDPL_LOOKUP_VALUE { get; set; }
    }
 //y.CDPL_SEQUENCE, y.CDPL_LOOKUP_VALUE

}
