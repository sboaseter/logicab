﻿using CAB.Domain.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using DevExpress.Web.Mvc;
using CAB.Domain.Repositories;

namespace LogiCAB.Web.Controllers {
    public class TranslationController : BaseController {
        private ITranslationRepository _translationRepository;
        public TranslationController(ILog log, ITranslationRepository translationRepository)
            : base(log) {
            _translationRepository = translationRepository;
        }
        public ActionResult Index() {
            return View();
        }
        public ActionResult List() {
            return PartialView(getTranslations());
        }

        public ActionResult ListMissing() {
            return PartialView(getMissingTranslations());
        }





        private List<Translation> getMissingTranslations() {
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                var res = conn.Query<Translation>(@"
                    SELECT 
	                    CMTR_SEQUENCE as Sequence, 
	                    CMTR_FK_LANGUAGE as LanguageSeq, 
	                    CDLA_LANGUAGE_DESC as Language,
	                    CMTR_LABEL_NAME as Name,
                    '' as TranslationText 
                    FROM F_CAB_MISSING_TRANSLATION
                    JOIN F_CAB_CD_LANGUAGE ON CMTR_FK_LANGUAGE = CDLA_SEQUENCE");
                return res.ToList();
            }
        }
        private List<Translation> getTranslations() {
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                var res = conn.Query<Translation>(@"
                    SELECT                        
	                    CATR_SEQUENCE as Sequence, 
	                    CATR_FK_LANGUAGE as LanguageSeq, 
	                    CDLA_LANGUAGE_DESC as Language,
	                    CATR_LABEL_NAME as Name,
                        CATR_LABEL_DESCRIPTION as TranslationText 
                    FROM F_CAB_TRANSLATION
                    JOIN F_CAB_CD_LANGUAGE ON CATR_FK_LANGUAGE = CDLA_SEQUENCE");
                return res.ToList();
            }
        }
        public ActionResult UpdateMissingTranslation([ModelBinder(typeof(DevExpressEditorsBinder))] Translation model) {
            if (ModelState.IsValid) {
                using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                    conn.Open();
                    // Insert into F_CAB_TRANSLATION, USING MODEL.NAME AND MODEL.LANG
                    // Delete from F_CAB_MISSING_TRANSLATION, USING MODEL.SEQ
                    var ins = conn.Query(@"
                        insert into F_CAB_TRANSLATION(CATR_FK_LANGUAGE, CATR_WINDOW_NAME, CATR_LABEL_ID, CATR_LABEL_NAME, CATR_LABEL_DESCRIPTION, CATR_CREATED_BY) 
                        values 
                        (@Language, 'Translation', @MaxId, @Text, @TranslationText, @CreatedBy)",
                        new { Language = model.LanguageSeq, MaxId = 0, Text = model.Name, TranslationText = model.TranslationText, CreatedBy = "LogiCAB" });
                    var res = conn.Query("DELETE FROM F_CAB_MISSING_TRANSLATION WHERE CMTR_SEQUENCE = @Id", new { Id = model.Sequence });
                }
            }
            return PartialView("ListMissing", getMissingTranslations());
        }
        public ActionResult UpdateTranslation([ModelBinder(typeof(DevExpressEditorsBinder))] Translation model) {
            if (ModelState.IsValid) {
                using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                    conn.Open();
                    // Update F_CAB_TRANSLATION
                    var upd = conn.Query(@"
                        update F_CAB_TRANSLATION set CATR_LABEL_DESCRIPTION = @TranslationText, CATR_MODIFIED_BY = @ModifiedBy WHERE CATR_SEQUENCE = @Id",
                        new { TranslationText = model.TranslationText, ModifiedBy = "LogiCAB", Id = model.Sequence });
                    _translationRepository.Clear();

                }
            }
            return PartialView("List", getMissingTranslations());
        }
    }
}