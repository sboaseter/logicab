﻿using CAB.Data.EF.Models;
using CAB.Domain.Enums;
using CAB.Domain.Models;
using CAB.Domain.Providers;
using CAB.Domain.Repositories;
using log4net;
using LogiCAB.Web.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Reflection;
using LogiCAB.Web.ViewModels;
using System.Security.Cryptography;
using System.Text;
using LogiCAB.Web.Domain;
//using System.Web.Mvc;

namespace LogiCAB.Web.Controllers {

    public class NewOrganisationInfo {
        public string CompanyName { get; set; }
        public string CompanyType { get; set; }
        public string AddrStreet { get; set; }
        public string AddrNr { get; set; }
        public string AddrCity { get; set; }
        public string AddrZip { get; set; }
        public string PhoneNr { get; set; }
        public string FaxNr { get; set; }
        public string Email { get; set; }
        public string PersFirstName { get; set; }
        public string PersMiddleName { get; set; }
        public string PersLastName { get; set; }
        public string PersPhoneNr { get; set; }
        public string PersEmail { get; set; }
        public string LoginName { get; set; }
        public string LoginSequence { get; set; }
    }

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class OrganisationApiController : ApiController {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private IOrganisationRepository _organisationRepository;
        private IFunctionsProvider _functionsProvider;
        public static List<Function> Functions;
        public OrganisationApiController(IOrganisationRepository organisationRepository, IFunctionsProvider functionsProvider) {
            _organisationRepository = organisationRepository;
            _functionsProvider = functionsProvider;
            if (Functions == null)
                Functions = _functionsProvider.List();
        }
        public NewOrganisationInfo CheckAccess(string testx, string testy) {
            return new NewOrganisationInfo {
            };
        }
        public NewOrganisationInfo CreateNewOrganisation(NewOrganisationInfo organisation) {
            Log.InfoFormat("Received request to register organisation: {0}", organisation.CompanyName);
            try { 
            string  orgaName = organisation.CompanyName,
                    orgaEmail = organisation.Email,
                    orgaType = organisation.CompanyType, 
                    orgaMaingroups = "1,2,3,4", 
                    orgaFunctions = "6", 
                    orgaSkin = "1", 
                    addrStreet = organisation.AddrStreet,
                    addrNr = organisation.AddrNr,
                    addrZip = organisation.AddrZip, 
                    addrCity = organisation.AddrCity,
                    persFirstName = organisation.PersFirstName,
                    persMiddleName = organisation.PersMiddleName,
                    persLastName = organisation.PersLastName, 
                    persEmail = organisation.PersEmail, 
                    logiUsername = String.Format("{0}{1}", organisation.PersFirstName[0], organisation.PersLastName).ToLower(), 
                    logiPassword = "Logiflora1", 
                    logiLanguage = "1";

            OrganisationModel orga = new OrganisationModel {
                Name = orgaName,
                Email = orgaEmail,
                Type = orgaType,
                Maingroups = orgaMaingroups.Split(',').Select(x => Int32.Parse(x)).ToList(),
                Functions = orgaFunctions.Split(',').Select(x => Int32.Parse(x)).ToList(),
                Skin = 1,
                Address = new CAB.Domain.Models.Address() {
                    Street = addrStreet,
                    StreetNr = addrNr,
                    Zipcode = addrZip,
                    City = addrCity
                },
                Person = new PersonModel() {
                    FirstName = persFirstName,
                    MiddleName = persMiddleName,
                    LastName = persLastName,
                    Email = persEmail
                },
                Login = new LoginModel() {
                    Name = logiUsername,
                    Password = logiPassword,
                    Language = Int32.Parse(logiLanguage)
                }
            };
            EFHelper.ctx.ExecuteStoreCommand(String.Format("insert into LogiPlaza_Sub_Registration (Organisation) values ('{0}')", Extensions.SerializeToXml(orga)));
            var newOrga = CreateOrganisation(orga.Name, orga.Email, orga.Type, orga.Skin);
            var newSpon = CreateShopOrganisation(newOrga, 1);
            var newOrma = CreateOrgaMainGroupMatrix(newOrga, orga.Maingroups);
            var newOrgaInfo = CreateOrganisationInfo(newOrga);
            var newOrgaAdmin = CreateOrganisationAdmin(newOrga);
            var newOradArray = CreateOrganisationFunctions(newOrga, orga.Functions);
            var newAddress = CreateAddress(newOrga, orga.Address.Street, orga.Address.StreetNr, orga.Address.Zipcode, orga.Address.City);
            if (newOrga.ORGA_TYPE == "BUYR") {
                var newBuyer = CreateBuyer(newOrga);
                // Grower setup
            } else if (newOrga.ORGA_TYPE == "GROW") {
                var newGrower = CreateGrower(newOrga);
                // Comm setup
            } else if (newOrga.ORGA_TYPE == "COMM" || newOrga.ORGA_TYPE == "LOGP") {
                var newBuyer = CreateBuyer(newOrga);
                var newGrower = CreateGrower(newOrga);
            } 
            var newPerson = CreatePerson(orga.Person, newOrga);
            var newLogin = CreateLogin(orga.Login.Name, orga.Login.Password, orga.Login.Language.ToString(), newPerson);
            var newHandshake = CreateHandshake(newOrga);
            var newHandshakeOption = CreateHandshakeOption(newHandshake);
            var newGama = CreateGrowerBuyerMatrix(newHandshake);
            _organisationRepository.Get(newOrga.ORGA_SEQUENCE); // Refreshing cache.


            return new NewOrganisationInfo {
                Email = newOrga.ORGA_EMAIL,
                CompanyName = newOrga.ORGA_NAME,
                CompanyType = newOrga.ORGA_TYPE,
                PersFirstName = newPerson.PERS_FIRST_NAME,
                PersMiddleName = newPerson.PERS_MIDDLE_NAME,
                PersLastName = newPerson.PERS_LAST_NAME,
                PersPhoneNr = newPerson.PERS_PHONE_NO,
                PersEmail = newPerson.PERS_EMAIL,
                LoginSequence = newLogin.LOGI_SEQUENCE.ToString()
            };
            } catch (Exception ex) {
                Log.ErrorFormat("Failed registering organisation: {0}", organisation.CompanyName);
                Log.ErrorFormat("Exception: ", ex.ToString());
                return new NewOrganisationInfo();
            }
        }


        #region Create/Delete Organisation CRUD-Methods
        private F_CAB_ORGANISATION CreateOrganisation(string name, string email, string type, int skin) {
            F_CAB_ORGANISATION newOrga = new F_CAB_ORGANISATION();
            newOrga.ORGA_CREATED_ON = DateTime.Now;
            newOrga.ORGA_CREATED_BY = "maual";
            newOrga.ORGA_MODIFIED_ON = DateTime.Now;
            newOrga.ORGA_MODIFIED_BY = "manual";
            newOrga.ORGA_FK_SHOP_SKIN = skin;
            newOrga.ORGA_REFER = null;
            newOrga.ORGA_FK_PICTURE = null;
            newOrga.ORGA_CODE = "12345678";
            newOrga.ORGA_END_DATE = DateTime.Now.AddYears(100);
            newOrga.ORGA_START_DATE = DateTime.Now;
            newOrga.ORGA_EAN_CODE = "12345678";
            newOrga.ORGA_BVH_NUMBER = "12345678";
            newOrga.ORGA_TYPE = type;
            newOrga.ORGA_VBA_NUMBER = "";
            newOrga.ORGA_MAILBOX = email;
            newOrga.ORGA_VAT_NUMBER = "12345678";
            newOrga.ORGA_EMAIL_2 = "";
            newOrga.ORGA_EMAIL = email;
            newOrga.ORGA_FAX_NO = "";
            newOrga.ORGA_PHONE_NO = "87654321";
            newOrga.ORGA_NAME = name;
            newOrga.ORGA_NAME_UPPER = name.ToUpper();
            EFHelper.ctx.F_CAB_ORGANISATION.AddObject(newOrga);
            EFHelper.ctx.SaveChanges();
            return newOrga;
        }
        private F_CAB_SHOP_ORGANISATION CreateShopOrganisation(F_CAB_ORGANISATION orga, int shop) {
            F_CAB_SHOP_ORGANISATION newSpon = new F_CAB_SHOP_ORGANISATION();
            newSpon.SPON_FK_SHOP = shop;
            newSpon.SPON_FK_ORGA = orga.ORGA_SEQUENCE;
            EFHelper.ctx.F_CAB_SHOP_ORGANISATION.AddObject(newSpon);
            EFHelper.ctx.SaveChanges();
            return newSpon;
        }
        private List<F_CAB_ORGA_MAIN_GROUP_MATRIX> CreateOrgaMainGroupMatrix(F_CAB_ORGANISATION orga, List<int> maingroups) {
            int i = 1;
            foreach (var mg in maingroups) {
                F_CAB_ORGA_MAIN_GROUP_MATRIX newOrma = new F_CAB_ORGA_MAIN_GROUP_MATRIX() {
                    ORMA_FK_ORGA = orga.ORGA_SEQUENCE,
                    ORMA_FK_MAIN_GROUP = mg,
                    ORMA_ORDER = i
                };
                EFHelper.ctx.F_CAB_ORGA_MAIN_GROUP_MATRIX.AddObject(newOrma);
                i++;
            }
            EFHelper.ctx.SaveChanges();
            return EFHelper.ctx.F_CAB_ORGA_MAIN_GROUP_MATRIX.Where(x => x.ORMA_FK_ORGA == orga.ORGA_SEQUENCE).ToList();
        }
        private F_CAB_ORGANISATION_INFO CreateOrganisationInfo(F_CAB_ORGANISATION orga) {
            F_CAB_ORGANISATION_INFO newOrgi = new F_CAB_ORGANISATION_INFO();
            newOrgi.ORGI_FK_ORGA_SEQ = orga.ORGA_SEQUENCE;
            newOrgi.ORGI_FK_PERS_BUYER_SEQ = null;
            newOrgi.ORGI_FK_CDCU_SEQ = null;
            newOrgi.ORGI_SHIPPING_DAYS = "1";
            newOrgi.ORGI_ORDER_DELIVER_DAYS = 1;
            newOrgi.ORGI_OFFER_DELIVER_DAYS = 1;
            newOrgi.ORGI_ORDER_OFFSET_DAYS = 0;
            newOrgi.ORGI_OFFER_OFFSET_DAYS = 0;
            newOrgi.ORGI_DEADLINE = TimeSpan.Parse("06:00:00");
            newOrgi.ORGI_DEADLINE_OFFER = TimeSpan.Parse("06:00:00.0000000");
            newOrgi.ORGI_CONTACT_NAME = "";
            newOrgi.ORGI_CONTACT_PHONE_NO = "";
            newOrgi.ORGI_CONTACT_FAX_NO = "";
            newOrgi.ORGI_CONTACT_EMAIL = "";
            newOrgi.ORGI_LATEST_DELIVERY_DATE_TIME = DateTime.Parse("2011-12-07 12:00:00.000");
            newOrgi.ORGI_ORDER_NAME = "";
            newOrgi.ORGI_CREATED_ON = DateTime.Now;
            newOrgi.ORGI_CREATED_BY = "sa";
            newOrgi.ORGI_MODIFIED_ON = DateTime.Now;
            newOrgi.ORGI_MODIFIED_BY = "sa";
            EFHelper.ctx.F_CAB_ORGANISATION_INFO.AddObject(newOrgi);
            EFHelper.ctx.SaveChanges();
            return newOrgi;
        }
        private F_CAB_ORGANISATION_ADMIN CreateOrganisationAdmin(F_CAB_ORGANISATION orga) {
            F_CAB_ORGANISATION_ADMIN newOrad = new F_CAB_ORGANISATION_ADMIN();
            newOrad.ORAD_FK_ORGANISATION = orga.ORGA_SEQUENCE;
            newOrad.ORAD_STATUS = 3;
            newOrad.ORAD_MEMBERTYPE = 3;
            newOrad.ORAD_DATE = DateTime.Now;
            EFHelper.ctx.F_CAB_ORGANISATION_ADMIN.AddObject(newOrad);
            EFHelper.ctx.SaveChanges();
            return newOrad;
        }
        private List<F_CAB_ORGANISATION_FUNCTION> CreateOrganisationFunctions(F_CAB_ORGANISATION orga, List<int> f) {
            foreach (var func in f) {
                F_CAB_ORGANISATION_FUNCTION newOrfu = new F_CAB_ORGANISATION_FUNCTION() {
                    ORFU_FK_ORGA = orga.ORGA_SEQUENCE,
                    ORFU_FK_FUNC = func
                };
                EFHelper.ctx.F_CAB_ORGANISATION_FUNCTION.AddObject(newOrfu);
            }
            EFHelper.ctx.SaveChanges();
            return EFHelper.ctx.F_CAB_ORGANISATION_FUNCTION.Where(x => x.ORFU_FK_ORGA == orga.ORGA_SEQUENCE).ToList();
        }
        private F_CAB_ADDRESS CreateAddress(F_CAB_ORGANISATION orga, string street, string streetnr, string zipcode, string city) {
            F_CAB_ADDRESS newAddr = new F_CAB_ADDRESS();
            newAddr.ADDR_FK_ORGANISATION = orga.ORGA_SEQUENCE;
            newAddr.ADDR_FK_CDCO_SEQ = null;
            newAddr.ADDR_FK_CDAT_SEQ = 1;
            newAddr.ADDR_STREET = street;
            newAddr.ADDR_NO = streetnr;
            newAddr.ADDR_NO_SUFFIX = "";
            newAddr.ADDR_ZIPCODE = zipcode;
            newAddr.ADDR_CITY = city;
            newAddr.ADDR_GLN_LOCATION_CODE = "";
            newAddr.ADDR_LOCATION_DESCRIPTION = "";
            newAddr.ADDR_ENTRY_DATE_FLORECOM = null;
            newAddr.ADDR_EXPIRE_DATE_FLORECOM = null;
            newAddr.ADDR_CHANGE_DATE_FLORECOM = null;
            newAddr.ADDR_COMPANY_ENTRY_DATE_FLORECOM = null;
            newAddr.ADDR_GPS_LATITUDE_FLORECOM = 0;
            newAddr.ADDR_GPS_LONGTITUDE_FLORECOM = 0;
            newAddr.ADDR_AUTO_SYNC_FLORECOM = false;
            newAddr.ADDR_LOCATION_PRIORITY = 0;
            newAddr.ADDR_CREATED_BY = "sa";
            newAddr.ADDR_CREATED_ON = DateTime.Now;
            newAddr.ADDR_MODIFIED_BY = "sa";
            newAddr.ADDR_MODIFIED_ON = DateTime.Now;

            EFHelper.ctx.F_CAB_ADDRESS.AddObject(newAddr);
            EFHelper.ctx.SaveChanges();
            return newAddr;
        }
        private F_CAB_BUYER CreateBuyer(F_CAB_ORGANISATION orga) {
            F_CAB_BUYER newBuyer = new F_CAB_BUYER();
            newBuyer.BUYR_CREATED_ON = DateTime.Now;
            newBuyer.BUYR_CREATED_BY = "manual";
            newBuyer.BUYR_MODIFIED_ON = DateTime.Now;
            newBuyer.BUYR_MODIFIED_BY = "manual";
            newBuyer.BUYR_TRANSPORT_TYPE = null;
            newBuyer.BUYR_DEMO_YN = true;
            newBuyer.BUYR_MPS_GAP = false;
            newBuyer.BUYR_FLORIMARK_YN = null;
            newBuyer.BUYR_ISO_YN = false;
            newBuyer.BUYR_MPS = 0;
            newBuyer.BUYR_MOBILE_NO = "87654321";
            newBuyer.BUYR_FK_ORGA = orga.ORGA_SEQUENCE;
            //F_CAB_BUYER.InsertOnSubmit(newBuyer);
            EFHelper.ctx.F_CAB_BUYER.AddObject(newBuyer);
            EFHelper.ctx.SaveChanges();
            //SubmitChanges();
            return newBuyer;
        }
        private F_CAB_GROWER CreateGrower(F_CAB_ORGANISATION orga) {
            F_CAB_GROWER newGrower = new F_CAB_GROWER();
            newGrower.GROW_CREATED_ON = DateTime.Now;
            newGrower.GROW_CREATED_BY = "manual";
            newGrower.GROW_MODIFIED_ON = DateTime.Now;
            newGrower.GROW_MODIFIED_BY = "manual";
            newGrower.GROW_FK_ORGA = orga.ORGA_SEQUENCE;
            newGrower.GROW_NUM_OF_M2 = 0;
            newGrower.GROW_FK_BULK_CASK = 385;
            newGrower.GROW_MOBILE_NO = "";
            newGrower.GROW_MPS = 0;
            newGrower.GROW_ISO_YN = 0;
            newGrower.GROW_FLORIMARK_YN = 0;
            newGrower.GROW_MPS_GAP = null;
            EFHelper.ctx.F_CAB_GROWER.AddObject(newGrower);
            EFHelper.ctx.SaveChanges();
            return newGrower;
        }
        public F_CAB_PERSON CreatePerson(PersonModel person, F_CAB_ORGANISATION orga) {
            F_CAB_PERSON newPerson = new F_CAB_PERSON();
            newPerson.PERS_CREATED_ON = DateTime.Now;
            newPerson.PERS_CREATED_BY = "maual";
            newPerson.PERS_MODIFIED_ON = DateTime.Now;
            newPerson.PERS_MODIFIED_BY = "manual";
            newPerson.PERS_REF_EKT = "";
            newPerson.PERS_EXT_REF = null;
            newPerson.PERS_EMAIL = person.Email;
            newPerson.PERS_FIRST_NAME = person.FirstName;
            newPerson.PERS_MIDDLE_NAME = person.MiddleName;
            newPerson.PERS_LAST_NAME = person.LastName;
            newPerson.PERS_PHONE_NO = "12345678";
            newPerson.PERS_FK_ORGANISATION = orga.ORGA_SEQUENCE;
            EFHelper.ctx.F_CAB_PERSON.AddObject(newPerson);
            EFHelper.ctx.SaveChanges();
            return newPerson;
        }
        private F_CAB_LOGIN CreateLogin(string username, string password, string language, F_CAB_PERSON person) {
            int langSeq = Int32.Parse(language);
            F_CAB_LOGIN newLogin = new F_CAB_LOGIN();
            newLogin.LOGI_CREATED_ON = DateTime.Now;
            newLogin.LOGI_CREATED_BY = "manual";
            newLogin.LOGI_MODIFIED_ON = DateTime.Now;
            newLogin.LOGI_MODIFIED_BY = "manual";
            newLogin.LOGI_LAST_LOGIN = null;
//            int languageSequence = EFHelper.ctx.F_CAB_CD_LANGUAGE.FirstOrDefault(x => x.CDLA_SEQUENCE == ).CDLA_SEQUENCE;
            newLogin.LOGI_FK_CDLA_SEQ = langSeq;
            newLogin.LOGI_PASS = password;
            newLogin.LOGI_BLOCKED_YN = 0;
            newLogin.LOGI_FK_LOGIN_GROUP = 2;
            newLogin.LOGI_FK_PERSON = person.PERS_SEQUENCE;
            newLogin.LOGI_PASSWORD = CreateMD5Hash("This is my password: " + password);
            newLogin.LOGI_USER_NAME = username;
            EFHelper.ctx.F_CAB_LOGIN.AddObject(newLogin);
            EFHelper.ctx.SaveChanges();
            return newLogin;
        }
        private string CreateMD5Hash(string input) {
            byte[] hashBytes = MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(input));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++) {
                sb.Append(hashBytes[i].ToString("x2"));
            }
            return sb.ToString();
        }
        private F_CAB_HANDSHAKE CreateHandshake(F_CAB_ORGANISATION orga) {
            F_CAB_HANDSHAKE newHand = new F_CAB_HANDSHAKE();
            newHand.HAND_FK_ORGA_SEQ_GROWER = 1155;
            newHand.HAND_FK_ORGA_SEQ_BUYER = orga.ORGA_SEQUENCE;
            newHand.HAND_FK_HATY_SEQ = 1;
            newHand.HAND_REQUEST_DATE = DateTime.Now;
            newHand.HAND_ACCEPT_DATE = DateTime.Now;
            newHand.HAND_APPROVE_DATE = DateTime.Now;
            newHand.HAND_BLOCKED_YN = false;
            newHand.HAND_REQUEST_MAIL = null;
            newHand.HAND_REQUEST_REMARK = "";
            newHand.HAND_CREATED_ON = DateTime.Now;
            newHand.HAND_CREATED_BY = "manual";
            EFHelper.ctx.F_CAB_HANDSHAKE.AddObject(newHand);
            EFHelper.ctx.SaveChanges();
            return newHand;
        }
        private F_CAB_HANDSHAKE_OPTIONS CreateHandshakeOption(F_CAB_HANDSHAKE hand) {
            F_CAB_HANDSHAKE_OPTIONS newHndo = new F_CAB_HANDSHAKE_OPTIONS();
            newHndo.HNDO_FK_HAND_SEQ = hand.HAND_SEQUENCE;
            newHndo.HNDO_FK_ADDR_SEQ = null;
            newHndo.HNDO_FK_CDPM_SEQ = null;
            newHndo.HNDO_FK_DMHD_SEQ = null;
            newHndo.HNDO_FK_ORGA_SEQ = null;
            newHndo.HNDO_AUTO_CONFIRM_YN = false;
            newHndo.HNDO_AUTO_EKT_YN = false;
            newHndo.HNDO_AUTO_KWB_YN = false;
            newHndo.HNDO_SEND_CONFIRM_MAIL_YN = true;
            newHndo.HNDO_RECV_OFFER_MAIL_YN = true;
            newHndo.HNDO_AUTO_PUBLISH_STOCK_YN = true;
            newHndo.HNDO_AUTO_PUBLISH_OFFER_YN = true;
            newHndo.HNDO_LATEST_DELIVERY_TIME = DateTime.Parse("2012-08-07 12:00:00.000");
            newHndo.HNDO_DELIVERY_TYPE = "";
            newHndo.HNDO_DELIVERY_STOCK_ALL_WORKDAYS_YN = true;
            newHndo.HNDO_DELIVERY_OFFER_ALL_WORKDAYS_YN = false;
            newHndo.HNDO_CREATED_ON = DateTime.Now;
            newHndo.HNDO_CREATED_BY = "LogiCAB:OrganisationAPI";
            newHndo.HNDO_MODIFIED_ON = DateTime.Now;
            newHndo.HNDO_MODIFIED_BY = "LogiCAB:OrganisationAPI";
            EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.AddObject(newHndo);
//            F_CAB_HANDSHAKE_OPTIONS.InsertOnSubmit(newHndo);
  //          SubmitChanges();
            EFHelper.ctx.SaveChanges();
            return newHndo;
        }
        private F_CAB_GROWER_BUYER_MATRIX CreateGrowerBuyerMatrix(F_CAB_HANDSHAKE hand) {
            // Retrieve Buyer & Grower sequences based on handshake:
            var buyer = EFHelper.ctx.F_CAB_BUYER.FirstOrDefault(x => x.BUYR_FK_ORGA == hand.HAND_FK_ORGA_SEQ_BUYER);
            var grower = EFHelper.ctx.F_CAB_GROWER.FirstOrDefault(x => x.GROW_FK_ORGA == hand.HAND_FK_ORGA_SEQ_GROWER);
            if (buyer == null || grower == null) {
                Log.Debug("Failed to aquire Grower/Buyer-sequence from Handshake");
                return null;
            }
            F_CAB_GROWER_BUYER_MATRIX newGama = new F_CAB_GROWER_BUYER_MATRIX();
            newGama.GAMA_FK_BUYER = buyer.BUYR_SEQUENCE;
            newGama.GAMA_FK_GROWER = grower.GROW_SEQUENCE;
            newGama.GAMA_LAST_ORDER_DATE = null;
            newGama.GAMA_BLOCKED_YN = 0;
            //F_CAB_GROWER_BUYER_MATRIX.InsertOnSubmit(newGama);
            EFHelper.ctx.F_CAB_GROWER_BUYER_MATRIX.AddObject(newGama);
            //SubmitChanges();
            EFHelper.ctx.SaveChanges();
            return newGama;
        }
        #endregion

    }
}
