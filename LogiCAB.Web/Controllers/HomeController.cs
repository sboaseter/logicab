﻿using CAB.Domain.Helpers;
using log4net;
using LogiCAB.Web.DAL;
using LogiCAB.Web.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace LogiCAB.Web.Controllers {
    [AuthorizeWithSessionAttribute]
    [HandleError]
    public class HomeController : Controller {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public ActionResult Index() {
            var newCabCodes = EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.Where(x => x.QCRQ_STATUS == "ADDED" && x.F_CAB_CD_CAB_CODE.F_CAB_CD_CAB_GROUP.F_CAB_CD_GROUP_TYPE.F_CAB_CD_MAIN_GROUP_TYPE.CDMG_SEQUENCE == Usr.SelectedMaingroup).OrderByDescending(y => y.QCRQ_HANDLED_DATE).Take(12);
            List<SpecialArticle> items = new List<SpecialArticle>();
            foreach (var c in newCabCodes) {
                SpecialArticle sa = new SpecialArticle();
                sa.CAB = new Tuple<string, string>(c.F_CAB_CD_CAB_CODE.CABC_CAB_DESC, c.F_CAB_CD_CAB_CODE.CABC_CAB_CODE);
                sa.VBN = new Tuple<string, string>(c.F_CAB_CD_VBN_CODE.VBNC_DESC, c.F_CAB_CD_VBN_CODE.VBNC_VBN_CODE);
                sa.RequestInfo = new Tuple<DateTime, string>((DateTime)c.QCRQ_REQUEST_DATE, c.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_NAME);
                sa.ApproveInfo = new Tuple<DateTime, string>((DateTime)c.QCRQ_HANDLED_DATE, c.QCRQ_HANDLED_BY);
                sa.Properties = new List<Tuple<string, string>>();
                foreach (var d in c.F_CAB_CD_CAB_CODE.F_CAB_CAB_DETAIL) {
                    sa.Properties.Add(new Tuple<string, string>(d.F_CAB_CD_CAB_PROP_TYPE.CDPT_DESC, d.CABD_VALUE));
                }
                sa.PictureSequence = (int)c.QCRQ_FK_PICTURE;
                items.Add(sa);

            }
            return View(items);
        }
        public ActionResult ErrorTest() {
            return View();
        }

        public void TestJob(int num) {
            var ep = DependencyResolver.Current.GetService(typeof(IEmailProvider)) as EmailProvider;
            ep.Show(num);
        }
    }
}
