﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Ionic.Zip;
using log4net;
using LogiCAB.Web.DAL;
using CAB.Data.EF.Models;
//using LogiControlCenter.Models;
//using LogiControlCenter.Logging;

namespace LogiCAB.Web.Controllers {
    public class LocationAddressRecord {
        private String _EntryDate = "";
        private String _ExpiryDate = "";
        private String _ChangeDateTime = "";
        private String _CompanyEntryDate = "";
        private String _GPSLatitude = "";
        private String _GPSLongitude = "";
        public String GLNLocatieCode { get; set; } //Locatiecode
        public String GLNCompanyCode { get; set; } //Bedrijfscode waartoe locatie behoort.
        public String StreetName { get; set; } //Straatnaam
        public String StreetNumber { get; set; } //Huisnummer
        public String StreetNumberSuffix { get; set; } //Huisnummer toevoeging.
        public String PostalIdentificationCode { get; set; } //Postcode
        public String CityName { get; set; } //Plaatsnaam
        public String CountryNameCode { get; set; } //Zie ISO 3166-1 code lijst (2 posities).
        public String LocationTypeCode { get; set; } //Zie Florecom locatie type codelijst -Box, -Tuin, -..
        public String LocatieName { get; set; } // Specifieke naam van de locatie: Voorbeelden ‘C003 – 15B’ ‘C003 – 36D’ ‘Dock 12’ ‘Dock 1 t/m 3’
        public String LocationDescription { get; set; } //Aanduiding: Voorbeelden: - ‘Bloemenveiling Centrum’ - ‘Oude kopersgebied’ - ‘Tussen 7A en 9’ - TPW-Jupiter
        public String EntryDate //Ingangsdatum (ccyymmdd) GLN locatiecode
        {
            get {
                return _EntryDate;
            }
            set {

                DateTime convDateTime;


                if (DateTime.TryParse(value, out convDateTime))
                    _EntryDate = convDateTime.ToShortDateString();

            }
        }
        public String ExpiryDate //Vervaldatum (ccyymmdd) 
        {

            get {
                return _ExpiryDate;
            }
            set {
                DateTime convDateTime;

                if (DateTime.TryParse(value, out convDateTime))
                    _ExpiryDate = convDateTime.ToShortDateString();

            }
        }
        public String ChangeDateTime//Mutatiedatum/tijd (ccyymmddhhmm)
        {
            get {
                return _ChangeDateTime;
            }
            set {
                DateTime convDateTime;

                if (DateTime.TryParse(value, out convDateTime))
                    _ChangeDateTime = convDateTime.ToShortDateString();

            }
        }
        public String CompanyEntryDate //Ingangsdatum (ccyymmdd) GLN bedrijfscode 
        {
            get {
                return _CompanyEntryDate;
            }
            set {
                DateTime convDateTime;

                if (DateTime.TryParse(value, out convDateTime))
                    _CompanyEntryDate = convDateTime.ToShortDateString();

            }
        }
        public String GPSLatitude //-90.0 <= waarde <= 90.0 Voorbeeld: 50.754500
        {
            get {
                return _GPSLatitude;
            }
            set {
                Decimal convDecimal;

                if (Decimal.TryParse(value, out convDecimal))
                    _GPSLatitude = convDecimal.ToString();

            }
        }
        public String GPSLongitude  //-180.0 <= waarde < 180.0 Voorbeeld:6.021100
        {
            get {
                return _GPSLongitude;
            }
            set {
                Decimal convDecimal;

                if (Decimal.TryParse(value, out convDecimal))
                    _GPSLongitude = convDecimal.ToString();

            }
        }
        public Boolean Validated { get; set; } //Object gevalideerd.
    }

    [Authorize]
    public class LocationController : BaseController {

        public LocationController(ILog log) : base(log) { }


        public ActionResult Index() {
            ViewBag.Test = "Test viewbag";
            return View();
        }


/*      
 *      Not used
 *      
 *      public PartialViewResult LocationLookup() {
            using (Entities cab = new Entities()) {
                var locationLookups = from ll in cab.F_CAB_ADDRESS
                                      select ll;
                ViewBag.LocationAddressLookups = locationLookups.ToList();
            }
            return PartialView();
        }
*/
        private void ExtractFile(string zipToUnpack, string unpackDirectory) {
            using (ZipFile zip1 = ZipFile.Read(zipToUnpack)) {
                foreach (ZipEntry e in zip1) {
                    e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);
                }
            }
        }

        static IEnumerable<string> ReadFrom(string file) {
            string line;

            using (StreamReader reader = System.IO.File.OpenText(file)) {
                while ((line = reader.ReadLine()) != null) {
                    yield return line;
                }
            }
        }

        private static LocationAddressRecord ParseLineLocationAddress(string line) {
            string[] elements = line.Split(';');
            DateTime convDateTime;
            Decimal convDecimal;

            return new LocationAddressRecord {
                GLNLocatieCode = elements[1].Trim(), //Locatiecode
                GLNCompanyCode = elements[2].Trim(), //Bedrijfscode waartoe locatie behoort.
                StreetName = elements[3].Trim(), //Straatnaam
                StreetNumber = elements[4].Trim(), //Huisnummer
                StreetNumberSuffix = elements[5].Trim(), //Huisnummer toevoeging.
                PostalIdentificationCode = elements[6].Trim(), //Postcode
                CityName = elements[7], //Plaatsnaam
                CountryNameCode = elements[8], //Zie ISO 3166-1 code lijst (2 posities).
                LocationTypeCode = elements[9], //Zie Florecom locatie type codelijst -Box, -Tuin, -..
                LocatieName = elements[10].Trim(), // Specifieke naam van de locatie: Voorbeelden ‘C003 – 15B’ ‘C003 – 36D’ ‘Dock 12’ ‘Dock 1 t/m 3’
                LocationDescription = elements[11].Trim(), //Aanduiding: Voorbeelden: - ‘Bloemenveiling Centrum’ - ‘Oude kopersgebied’ - ‘Tussen 7A en 9’ - TPW-Jupiter
                EntryDate = elements[12].Trim(), //Ingangsdatum (ccyymmdd) GLN locatiecode
                ExpiryDate = elements[13].Trim(), //Vervaldatum (ccyymmdd)
                ChangeDateTime = elements[14].Trim(),//Mutatiedatum/tijd (ccyymmddhhmm)
                CompanyEntryDate = elements[15].Trim(), //Ingangsdatum (ccyymmdd) GLN bedrijfscode
                GPSLatitude = elements[16].Trim(), //-90.0 <= waarde <= 90.0 Voorbeeld: 50.754500
                GPSLongitude = elements[17].Trim(), //-180.0 <= waarde < 180.0 Voorbeeld:6.021100
            };
        }

        public PartialViewResult LocationUpdate() {
            ViewBag.LocatiUpdate = "Use this to update the location";
            return PartialView();
        }

        public JsonResult downloadLocations() {
            try {
                var listOfFiles = Directory.GetFiles(Server.MapPath("~/tmp/"));

                foreach (string oneFile in listOfFiles) {
                    try {
                        System.IO.File.Delete(oneFile);
                    } catch (Exception ex) {
                        Log.Error(String.Format("Could not delete file: {0}, {1}", oneFile, ex.ToString()));
                    }
                }

                //Directory.Delete(Server.MapPath("~/tmp/"), true);
                WebClient Client = new WebClient();
                Client.Credentials = new NetworkCredential("Ad", "7PT26ZD4");
                string saveToFile = String.Format("{0}{1}", Server.MapPath("~/tmp/"), "codelist.zip");

                try {
                    Client.DownloadFile("ftp://codes.florecom.nl/codes/FEC020103.ZIP", saveToFile);
                    ExtractFile(saveToFile, Server.MapPath("~/tmp/"));
                } catch (WebException e) {
                    Console.WriteLine(e.ToString());
                }

            } catch (Exception ex) {
                Log.Error(String.Format("Error during download/extraction: {0}", ex.ToString()));
            }

            return Json("Success", JsonRequestBehavior.AllowGet);
        }


        public List<LocationAddressRecord> getCompoundListLocations() {
            downloadLocations();
            string[] filesCode = Directory.GetFiles(Server.MapPath("~/tmp/"), "FL*.txt");
            string fileCode = filesCode[filesCode.Length - 1];

//            using (Entities cab = new Entities()) {
            using(var cab = EFHelper.ctx) {
                var locationAddress =
                    from locAdr in cab.F_CAB_ADDRESS
                    join orga in cab.F_CAB_ORGANISATION on locAdr.ADDR_FK_ORGANISATION equals orga.ORGA_SEQUENCE
                    select new { orga.ORGA_EAN_CODE, locAdr };

                var locationsTxt =
                    from line in ReadFrom(fileCode)
                    let record = ParseLineLocationAddress(line)
                    select record;

                var compoundList = (
                    from locationsTx in locationsTxt
                    join locationsDb in locationAddress
                    on locationsTx.GLNCompanyCode equals locationsDb.ORGA_EAN_CODE
                    into joinedList
                    orderby locationsTx.GLNCompanyCode ascending
                    select new LocationAddressRecord {
                        ChangeDateTime = locationsTx.ChangeDateTime,
                        CityName = locationsTx.CityName,
                        CompanyEntryDate = locationsTx.CompanyEntryDate,
                        CountryNameCode = locationsTx.CountryNameCode,
                        EntryDate = locationsTx.EntryDate,
                        ExpiryDate = locationsTx.ExpiryDate,
                        GLNCompanyCode = locationsTx.GLNCompanyCode,
                        GLNLocatieCode = locationsTx.GLNLocatieCode,
                        GPSLatitude = locationsTx.GPSLatitude,
                        GPSLongitude = locationsTx.GPSLongitude,
                        LocatieName = locationsTx.LocatieName,
                        LocationDescription = locationsTx.LocationDescription,
                        LocationTypeCode = locationsTx.LocationTypeCode,
                        PostalIdentificationCode = locationsTx.PostalIdentificationCode,
                        StreetName = locationsTx.StreetName,
                        StreetNumber = locationsTx.StreetNumber,
                        StreetNumberSuffix = locationsTx.StreetNumberSuffix
                    });

                return compoundList.ToList();
            }
        }
        [HttpPost]
        public JsonResult insertLocations() {
            //Entities cab = new Entities();
            var cab = EFHelper.ctx;
            List<LocationAddressRecord> compoundList = getCompoundListLocations();
            var organisations = cab.F_CAB_ORGANISATION.ToList();
            Func<string, int> getOrganisation = gOrg => organisations.Where(x => x.ORGA_EAN_CODE == gOrg).FirstOrDefault() == null ? 0 : organisations.Where(x => x.ORGA_EAN_CODE == gOrg).FirstOrDefault().ORGA_SEQUENCE;
            Func<int, string, F_CAB_ADDRESS> getAddress = (gOrgSeq, gLocCode) => cab.F_CAB_ADDRESS.Where(x => x.ADDR_FK_ORGANISATION == gOrgSeq && x.ADDR_GLN_LOCATION_CODE == gLocCode).FirstOrDefault();

            Func<LocationAddressRecord, F_CAB_ADDRESS> getLocationEntry = le => new F_CAB_ADDRESS {
                ADDR_AUTO_SYNC_FLORECOM = true,
                ADDR_CHANGE_DATE_FLORECOM = GetDateTime(le.ChangeDateTime, false),
                ADDR_CITY = le.CityName,
                ADDR_COMPANY_ENTRY_DATE_FLORECOM = GetDateTime(le.EntryDate, false),
                ADDR_ENTRY_DATE_FLORECOM = GetDateTime(le.EntryDate, false),
                ADDR_EXPIRE_DATE_FLORECOM = GetDateTime(le.ExpiryDate, false),
                //ADDR_FK_ORGANISATION = getOrganisation(le.GLNCompanyCode),
                ADDR_GLN_LOCATION_CODE = le.GLNLocatieCode,
                ADDR_GPS_LATITUDE_FLORECOM = Decimal.Parse(String.IsNullOrEmpty(le.GPSLatitude) ? "0" : le.GPSLatitude),
                ADDR_GPS_LONGTITUDE_FLORECOM = Decimal.Parse(String.IsNullOrEmpty(le.GPSLongitude) ? "0" : le.GPSLongitude),
                ADDR_LOCATION_DESCRIPTION = le.LocationDescription,
                ADDR_MODIFIED_BY = "AutoSync",
                ADDR_MODIFIED_ON = DateTime.Now,
                ADDR_NO = le.StreetNumber,
                ADDR_NO_SUFFIX = le.StreetNumberSuffix,
                ADDR_STREET = le.StreetName,
                ADDR_ZIPCODE = le.PostalIdentificationCode,
            };

            int orgKey;
            F_CAB_ADDRESS addressOrg;

            foreach (LocationAddressRecord item in compoundList) {
                if (string.IsNullOrEmpty(item.GLNCompanyCode) || string.IsNullOrEmpty(item.GLNLocatieCode))
                    continue;
                F_CAB_ADDRESS newAddress = getLocationEntry(item);
                //F_CAB_ADDRESS orgAddress = new F_CAB_ADDRESS();
                orgKey = getOrganisation(item.GLNCompanyCode);

                if (orgKey != 0) {
                    addressOrg = getAddress(orgKey, item.GLNLocatieCode);

                    if (addressOrg != null) {
                        //Nog even naar kijken wat we hier precies willen doen...
                        //Het adres bestaat reeds, misschien bepaalde velden bijwerken..
                        //Voorlopig maar even negeren..
                        //addressOrg.ADDR_LOCATION_DESCRIPTION = newAddress.ADDR_LOCATION_DESCRIPTION;
                        //cab.SaveChanges();
                    } else {
                        newAddress.ADDR_FK_ORGANISATION = orgKey;
                        newAddress.ADDR_CREATED_BY = "AutoSync";
                        newAddress.ADDR_CREATED_ON = DateTime.Now;
                        cab.F_CAB_ADDRESS.AddObject(newAddress);
                    }
                }
            }

            cab.SaveChanges();
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ContentResult parseLocations() {
            var serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };

            var result = new ContentResult {
                Content = serializer.Serialize(getCompoundListLocations().ToList()),
                ContentType = "application/json"
            };

            return result;
        }

        /// <summary>
        /// Converteert een (lege) string naar een datum.
        /// Zet datum op null als string leeg is.
        /// Optioneel wordt de datum op Now gezet als de string leeg is.
        /// </summary>
        /// <returns></returns>
        private static DateTime? GetDateTime(String dateTimeValue, Boolean getNowWhenEmpty) {
            DateTime? returnValue = null;
            DateTime convDateTime;

            if (DateTime.TryParse(dateTimeValue, out convDateTime))
                returnValue = convDateTime;
            else
                if (getNowWhenEmpty)
                    returnValue = System.DateTime.Now;

            return returnValue;
        }
    }
}
