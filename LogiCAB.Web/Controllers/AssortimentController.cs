﻿using CAB.Data.EF.Models;
using CAB.Domain.Helpers;
using CAB.Domain.Providers;
using CAB.Domain.Repositories;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using log4net;
using LogiCAB.Web.DAL;
using LogiCAB.Web.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Dapper;
using System.Data.SqlClient;

namespace LogiCAB.Web.Controllers {
    [AuthorizeWithSessionAttribute]
    [HandleError]
    public class AssortimentController : BaseController {
        private ILoginRepository _loginRepository;
        private IFunctionsProvider _functionsProvider;
        private IPersonRepository _personRepository;
        private IOrganisationRepository _organisationRepository;
        private ISettingsProvider _settingsProvider;

        public AssortimentController(
            ILoginRepository loginRepository,
            IFunctionsProvider functionsProvider,
            IPersonRepository personRepository,
            IOrganisationRepository organisationRepository,
            ISettingsProvider settingsProvider,
            ILog log) : base(log) {
            _loginRepository = loginRepository;
            _functionsProvider = functionsProvider;
            _personRepository = personRepository;
            _organisationRepository = organisationRepository;
            _settingsProvider = settingsProvider;
        }
        public ActionResult Index() {
            return View(getAssortiment());
        }

        public ActionResult List() {
            return PartialView("List", getAssortiment());
        }

        public ActionResult CABPropertiesPartial(int id = -1) {
            if (id != -1)
                Session["cabdetail_id"] = id;
            else
                id = Int32.Parse(Session["cabdetail_id"].ToString());
            return PartialView(_getCABProperties(id));
        }
        public ActionResult CABPropertiesVBNPartial(int id = -1) {
            if (id != -1)
                Session["cabdetail_id"] = id;
            else
                id = Int32.Parse(Session["cabdetail_id"].ToString());
            return PartialView(_getCABProperties(id));
        }

        public ActionResult CABPropertiesVBNPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] VBNSpecifiedProperties  model) {
            int id = Int32.Parse(Session["cabdetail_id"].ToString()); // GRAS_SEQUENCE
            
            if (ModelState.IsValid) {
                if (model.VBN_PROP_LOOKUP != null) {
                    int cdgl_seq = Int32.Parse(model.VBN_PROP_LOOKUP);
                    F_CAB_CD_GRAS_PROP_LOOKUP newValue = EFHelper.ctx.F_CAB_CD_GRAS_PROP_LOOKUP.FirstOrDefault(x => x.CDGL_SEQUENCE == cdgl_seq);
                    var q1 =       from cagd in EFHelper.ctx.F_CAB_GRAS_DETAIL
                                   join cdgp in EFHelper.ctx.F_CAB_CD_GRAS_PROP_TYPE
                                        on cagd.CAGD_FK_CDGP_SEQ equals cdgp.CDGP_SEQUENCE
                                   join cdvp in EFHelper.ctx.F_CAB_CD_VBN_PROP_TYPE
                                        on cdgp.CDGP_FK_VBN_PROP_TYPE equals cdvp.CDVP_SEQUENCE
                                   where cagd.CAGD_FK_GRAS_SEQ == id && cdvp.CDVP_SEQUENCE == newValue.CDGL_FK_CDGP_SEQ
                                   select cdgp;

                    int cdgp_seq = q1.Count() > 0 ? q1.FirstOrDefault().CDGP_SEQUENCE : -1;
                    var gDet = EFHelper.ctx.F_CAB_GRAS_DETAIL.FirstOrDefault(x => x.CAGD_FK_GRAS_SEQ == id && x.CAGD_FK_CDGP_SEQ == newValue.CDGL_FK_CDGP_SEQ);
                    if (gDet != null)
                        cdgp_seq = gDet.CAGD_FK_CDGP_SEQ;
                    
                    
                    if (cdgp_seq != -1) { // Propertytype exists for gras-article. adjust it.
                        F_CAB_GRAS_DETAIL det = EFHelper.ctx.F_CAB_GRAS_DETAIL.FirstOrDefault(x => x.CAGD_FK_GRAS_SEQ == id && x.CAGD_FK_CDGP_SEQ == cdgp_seq);
                        det.CAGD_VALUE = newValue.CDGL_LOOKUP_VALUE;
                        det.CAGD_VALUE_DESC = newValue.CDGL_LOOKUP_DESC;
                        det.CAGD_VALUE_DESC_UPPER = newValue.CDGL_LOOKUP_DESC.ToUpper();
                        EFHelper.ctx.SaveChanges();
                        String queryExpr = String.Format("exec [dbo].[proc_Fill_And_Sync_Properties_Assortment] {0}", det.CAGD_FK_GRAS_SEQ);
                        EFHelper.ctx.ExecuteStoreCommand(queryExpr);
                    } else { // Proptype does not exist for gras-article. create new.                    
                        var q2 = 
                                 (from cdgp in EFHelper.ctx.F_CAB_CD_GRAS_PROP_TYPE
                                 join cdvp in EFHelper.ctx.F_CAB_CD_VBN_PROP_TYPE
                                      on cdgp.CDGP_FK_VBN_PROP_TYPE equals cdvp.CDVP_SEQUENCE
                                  where cdgp.CDGP_SEQUENCE == newValue.CDGL_FK_CDGP_SEQ
                                 select cdgp).FirstOrDefault();
                        F_CAB_GRAS_DETAIL det = new F_CAB_GRAS_DETAIL {
                            CAGD_FK_CDGP_SEQ = q2.CDGP_SEQUENCE,
                            CAGD_FK_GRAS_SEQ = id,
                            CAGD_VALUE = newValue.CDGL_LOOKUP_VALUE,
                            CAGD_VALUE_DESC = newValue.CDGL_LOOKUP_DESC,
                            CAGD_VALUE_DESC_UPPER = newValue.CDGL_LOOKUP_DESC.ToUpper(),
                            CAGD_ORDER = 9
                        };
                        EFHelper.ctx.F_CAB_GRAS_DETAIL.AddObject(det);
                        EFHelper.ctx.SaveChanges();
                        String queryExpr = String.Format("exec [dbo].[proc_Fill_And_Sync_Properties_Assortment] {0}", det.CAGD_FK_GRAS_SEQ);
                        EFHelper.ctx.ExecuteStoreCommand(queryExpr);
                    }



                }

                Debug.WriteLine(String.Format("Some model info: {0}, {1}", model.Desc, model.VBN_PROP_LOOKUP));
            } else {
                ViewData["updErrors"] = "Modelstate not valid";
            }
            return PartialView("CABPropertiesVBNPartial", _getCABProperties(id));

        }

        public List<CABDetail> _getCABPropertiesAsync(int id) {
            CABProperties result = new CABProperties();
            List<CABDetail> cabdetails = new List<CABDetail>();
            List<VBNSpecifiedProperties> vbnproperties = new List<VBNSpecifiedProperties>();
            List<GRASPropType> grasPropTypes = new List<GRASPropType>();
            // 3 Tasks

            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                cabdetails= conn.Query<CABDetail>(@"
SELECT *
FROM   f_cab_cab_detail
       INNER JOIN
       f_cab_cd_cab_code
       ON cabd_fk_cab_code = cabc_sequence
       INNER JOIN
       f_cab_grower_assortiment
       ON GRAS_FK_CAB_CODE = cabc_sequence
       INNER JOIN
       f_cab_cd_cab_prop_type
       ON CABD_FK_TYPE = CDPT_SEQUENCE
WHERE  GRAS_SEQUENCE = @id;", new { id = id }).ToList();
            }
            return cabdetails;


        }

        // Refactor
        public CABProperties _getCABProperties(int id) {

            int cabSequence = EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.SingleOrDefault(x => x.GRAS_SEQUENCE == id).GRAS_FK_CAB_CODE;

            CABProperties result = new CABProperties();
            result.cabdetails = from cabd in EFHelper.ctx.F_CAB_CAB_DETAIL
                where
                    cabd.CABD_FK_CAB_CODE == cabSequence
                orderby cabd.CABD_ORDER ascending
                select new CABDetail {
                    CABD_SEQUENCE = cabd.CABD_SEQUENCE,
                    CABD_FK_CAB_CODE = cabd.CABD_FK_CAB_CODE,
                    CDPT_DESC = cabd.F_CAB_CD_CAB_PROP_TYPE.CDPT_DESC,
                    CDPT_MANDATORY_YN = cabd.F_CAB_CD_CAB_PROP_TYPE.CDPT_MANDATORY_YN,
                    CDPT_FK_VBN_PROP_TYPE = cabd.F_CAB_CD_CAB_PROP_TYPE.CDPT_FK_VBN_PROP_TYPE,
                    CABD_VALUE = cabd.CABD_VALUE,
                    CABD_VALUE_DESC = cabd.CABD_VALUE_DESC,
                    CABD_ORDER = cabd.CABD_ORDER
                };

            // Current VBNCode 
            var vbnCodeStringObj = (from x in EFHelper.ctx.F_CAB_CAB_VBN_MATRIX.Where(x => x.CVMA_FK_CAB_CODE == cabSequence)
                                    select x).FirstOrDefault();
            if (vbnCodeStringObj != null) {
                var vbnCodeString = vbnCodeStringObj.F_CAB_CD_VBN_CODE.VBNC_VBN_CODE;
                var vbnCodeDouble = Double.Parse(vbnCodeString);

                // VBN Property Types present in the CAB_DETAIL of the selected CABCode
                int?[] vbnPropTypes = (from x in result.cabdetails select x.CDPT_FK_VBN_PROP_TYPE).ToArray();

                result.vbnproperties = from cvps in EFHelper.ctx.F_CAB_VBN_PROPERTIES_SPECIFIED
                                       join cdvp in EFHelper.ctx.F_CAB_CD_VBN_PROP_TYPE
                                       on cvps.CVPS_VBN_PROP_CODE equals cdvp.CDVP_VBN_PROP_CODE
                                       // Eliminate proptypes that are connected to the CAB-Code in question:
                                       join cdgp in EFHelper.ctx.F_CAB_CD_GRAS_PROP_TYPE.Where(x => !vbnPropTypes.Contains(x.CDGP_SEQUENCE))

                                       on cdvp.CDVP_SEQUENCE equals cdgp.CDGP_FK_VBN_PROP_TYPE
                                       join cadg in EFHelper.ctx.F_CAB_GRAS_DETAIL
                                       on id equals cadg.CAGD_FK_GRAS_SEQ into cadgTable
                                       from cadgData in cadgTable.Where(x => x.F_CAB_CD_GRAS_PROP_TYPE.CDGP_FK_VBN_PROP_TYPE == cdvp.CDVP_FK_VBN_PROP_GROUP).DefaultIfEmpty()
                                       where cvps.CVPS_VBN_CODE == vbnCodeDouble
                                       select new VBNSpecifiedProperties {
                                           CVPS_SEQUENCE = cvps.CVPS_SEQUENCE,
                                           CVPS_VBN_CODE = cvps.CVPS_VBN_CODE,
                                           CVPS_VBN_PROP_CODE = cvps.CVPS_VBN_PROP_CODE,
                                           Desc = cdvp.CDVP_PROP_DESC,
                                           CVPS_MANDATORY_YN = cvps.CVPS_MANDATORY_YN,
                                           CVPS_SORT_ORDER = cvps.CVPS_SORT_ORDER,
                                           CVPS_VALID_FROM = cvps.CVPS_VALID_FROM,
                                           VBN_PROP_LOOKUP = EFHelper.ctx.F_CAB_GRAS_DETAIL
                                               .FirstOrDefault(x => x.CAGD_FK_GRAS_SEQ == id &&
                                                   x.F_CAB_CD_GRAS_PROP_TYPE.F_CAB_CD_VBN_PROP_TYPE.CDVP_SEQUENCE == cdvp.CDVP_SEQUENCE).CAGD_VALUE,
                                           VBN_PROP_LOOKUP_DESC = EFHelper.ctx.F_CAB_GRAS_DETAIL
                                               .FirstOrDefault(x => x.CAGD_FK_GRAS_SEQ == id &&
                                                   x.F_CAB_CD_GRAS_PROP_TYPE.F_CAB_CD_VBN_PROP_TYPE.CDVP_SEQUENCE == cdvp.CDVP_SEQUENCE).CAGD_VALUE_DESC,
                                       };
                result.grasPropTypes = from cdgp in EFHelper.ctx.F_CAB_CD_GRAS_PROP_TYPE
                                       select new GRASPropType {
                                           CDGP_SEQUENCE = cdgp.CDGP_SEQUENCE,
                                           CDGP_DESC = cdgp.CDGP_DESC
                                       };
            }
            return result;
            
                
        }

        public static IQueryable<GRASPropLookup> getGRASPropLookup(int CDGP_SEQUENCE) {
            return from x in EFHelper.ctx.F_CAB_CD_GRAS_PROP_LOOKUP 
                   where x.CDGL_FK_CDGP_SEQ == CDGP_SEQUENCE 
                   select new GRASPropLookup {
                       CDGL_SEQUENCE = x.CDGL_SEQUENCE,
                       CDGL_FK_CDGP_SEQ = x.CDGL_FK_CDGP_SEQ,
                       CDGL_LOOKUP_VALUE = x.CDGL_LOOKUP_VALUE,
                       CDGL_LOOKUP_DESC = x.CDGL_LOOKUP_DESC,
                       CDGL_ORDER = x.CDGL_ORDER
                   };
        }

        public static IQueryable<F_CAB_CD_GRAS_PROP_LOOKUP> getVBNPropCodeValues(string propType) {
            return from cdgp in EFHelper.ctx.F_CAB_CD_GRAS_PROP_TYPE
                   join cdvp in EFHelper.ctx.F_CAB_CD_VBN_PROP_TYPE
                   on cdgp.CDGP_FK_VBN_PROP_TYPE equals cdvp.CDVP_SEQUENCE
                   join cdgl in EFHelper.ctx.F_CAB_CD_GRAS_PROP_LOOKUP
                    on cdgp.CDGP_SEQUENCE equals cdgl.CDGL_FK_CDGP_SEQ
                   where cdvp.CDVP_VBN_PROP_CODE == propType
                   select cdgl;
        }

        public ActionResult GRASPropertyLookupPartial() {
            int grasPropType = (Request.Params["grasPropTypeSeq"] != null) ? int.Parse(Request.Params["grasPropTypeSeq"]) : 70;
            return PartialView("GRASPropertyLookupPartial", getGRASPropLookup(grasPropType));
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateAssortimentModel([ModelBinder(typeof(DevExpressEditorsBinder))] Assortiment model) {
            return PartialView("List", getAssortiment());

        }

        public ActionResult UpdateAssortiment(int key, string field, object value) {
            string svalue = "";
            foreach (object item in (Array)value)
                svalue = (string)item;

            if (field == "start_date") {
                string preparedDateString = svalue.Substring(0, svalue.IndexOf("GMT"));
                EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.FirstOrDefault(x => x.GRAS_SEQUENCE == key).GRAS_START_DATE = DateTime.Parse(preparedDateString);
            } else if (field == "end_date") {
                if (!String.IsNullOrEmpty(svalue)) {
                    string preparedDateString = svalue.Substring(0, svalue.IndexOf("GMT"));
                    EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.FirstOrDefault(x => x.GRAS_SEQUENCE == key).GRAS_END_DATE = DateTime.Parse(preparedDateString);
                } else {
                    EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.FirstOrDefault(x => x.GRAS_SEQUENCE == key).GRAS_END_DATE = null;
                }
            } else if (field == "numitems") {
                using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                    conn.Open();
                    var res = conn.Query("[proc_UPD_Assortiment_Stock] @GRAS_SEQ, @NEW_ITEMS", new { GRAS_SEQ = key, NEW_ITEMS = Int32.Parse(svalue) });
                } 
            } else if (field == "active") {
                EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.FirstOrDefault(x => x.GRAS_SEQUENCE == key).GRAS_SELECTED_YN = svalue == "true" ? true : false;
            } else if (field == "showcask") {
                EFHelper.ctx.F_CAB_GROWER_CASK_MATRIX.FirstOrDefault(x => x.CGCM_SEQUENCE == key).CGCM_SHOW_CASK = svalue == "true" ? true : false;
            }
            EFHelper.ctx.SaveChanges();
            return null;
        }

        [HttpPost]
        public string AddCaskToAssortiment(int cdca_seq, int prcask, int prlayer, int prtrolley, int grasseq, int prbunch) {

            F_CAB_GROWER_ASSORTIMENT gras = EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.FirstOrDefault(x => x.GRAS_SEQUENCE == grasseq);

            // Todo: check: Move to cask-provider to centralize control of adding/editing/removing casks. ICaskProvider ( Also for F_CAB_GROWER_CASK_MATRIX )
            F_CAB_GROWER_CASK_MATRIX cgcm = EFHelper.ctx.F_CAB_GROWER_CASK_MATRIX.FirstOrDefault(x =>
                x.F_CAB_CAB_CASK_MATRIX.F_CAB_CD_CASK.CDCA_SEQUENCE == cdca_seq &&
                x.F_CAB_CAB_CASK_MATRIX.CACA_NUM_OF_UNITS == prcask &&
                x.F_CAB_CAB_CASK_MATRIX.CACA_UNITS_PER_LAYER == prlayer &&
                x.F_CAB_CAB_CASK_MATRIX.CACA_LAYERS_PER_TROLLEY == prtrolley &&
                x.F_CAB_CAB_CASK_MATRIX.CACA_FK_CAB_CODE == gras.GRAS_FK_CAB_CODE &&
                x.CGCM_FK_GROWER_ASSORTIMENT == grasseq &&
                x.CGCM_NUM_ITEMS_BUNCH == prbunch);
            if (cgcm != null)
                return "Exists";
            F_CAB_CAB_CASK_MATRIX caca = EFHelper.ctx.F_CAB_CAB_CASK_MATRIX.FirstOrDefault(x =>
                x.CACA_FK_CAB_CODE == gras.GRAS_FK_CAB_CODE &&
                x.CACA_FK_CASK == cdca_seq &&
                x.CACA_NUM_OF_UNITS == prcask &&
                x.CACA_UNITS_PER_LAYER == prlayer &&
                x.CACA_LAYERS_PER_TROLLEY == prtrolley
                );

            if (caca == null) { 
                F_CAB_CAB_CASK_MATRIX newCaca = new F_CAB_CAB_CASK_MATRIX {
                    CACA_FK_CAB_CODE = gras.GRAS_FK_CAB_CODE,
                    CACA_FK_CASK = cdca_seq,
                    CACA_NUM_OF_UNITS = prcask,
                    CACA_UNITS_PER_LAYER = prlayer,
                    CACA_LAYERS_PER_TROLLEY = prtrolley
                };
                EFHelper.ctx.F_CAB_CAB_CASK_MATRIX.AddObject(newCaca);
                EFHelper.ctx.SaveChanges();
                caca = EFHelper.ctx.F_CAB_CAB_CASK_MATRIX.Single(x => x.CACA_SEQUENCE == newCaca.CACA_SEQUENCE);
            }

            EFHelper.ctx.F_CAB_GROWER_CASK_MATRIX.AddObject(new F_CAB_GROWER_CASK_MATRIX {
                CGCM_FK_CACA = caca.CACA_SEQUENCE,
                CGCM_FK_GROWER_ASSORTIMENT = gras.GRAS_SEQUENCE,
                CGCM_SHOW_CASK = true,
                CGCM_NUM_ITEMS_BUNCH = prbunch
            });
            EFHelper.ctx.SaveChanges();
            return "";
        }

        [HttpPost]
        public string setVKCImage(int gras_seq) {
            var cab_seq = EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.FirstOrDefault(x => x.GRAS_SEQUENCE == gras_seq).GRAS_FK_CAB_CODE;
            var vbn_seq = EFHelper.ctx.F_CAB_CAB_VBN_MATRIX.FirstOrDefault(x => x.CVMA_FK_CAB_CODE == cab_seq).F_CAB_CD_VBN_CODE.VBNC_VBN_CODE;
            string vkccode = ImageUtils.getVKCImage(vbn_seq);
            if (!String.IsNullOrEmpty(vkccode)) {
                Log.InfoFormat("User did a reset on his assortiment-picture of article(GRAS_SEQUNCE): {0}", gras_seq);
                string vkcphoto = String.Format("http://www.logicab.nl/logicab/vkc/{0}.jpg", vkccode);
                var webClient = new WebClient();
                byte[] imageBytes = webClient.DownloadData(vkcphoto);
                MemoryStream ms = new MemoryStream(imageBytes);
                Image tempImg = Image.FromStream(ms);
                Image newImage = ImageUtils.resizeImage(tempImg, new Size { Height = 1024, Width = 1024 });
                MemoryStream ms2 = new MemoryStream();
                newImage.Save(ms2, ImageFormat.Jpeg);

                F_CAB_PICTURE newPic = new F_CAB_PICTURE {
                    PICT_PICTURE = ms2.ToArray()
                };
                EFHelper.ctx.F_CAB_PICTURE.AddObject(newPic);
                EFHelper.ctx.SaveChanges();
                EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.FirstOrDefault(x => x.GRAS_SEQUENCE == gras_seq).GRAS_FK_PICTURE = newPic.PICT_SEQUENCE;
                EFHelper.ctx.SaveChanges();
                return vkcphoto;
            } else {
                return "http://www.logicab.nl/logicab/vkc/nopic.jpg";
            }
        }

        public List<Assortiment> getAssortiment() {
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                var res = conn.Query<Assortiment>("[logicab_getAssortiment] @Grower, @Maingroup, @Language", new { Grower = Usr.OrgaTypeSequence, Maingroup = Usr.SelectedMaingroup, Language = Usr.Language });
                return res.ToList();
            } 
        }

        public ActionResult Casks() {
            int assortSeq = (int)Session["currentAssort"];
            return PartialView("Casks", getAssortimentCasks(assortSeq));
        }

        public ActionResult EditCask([ModelBinder(typeof(DevExpressEditorsBinder))] Cask model) {
            int assortSeq = (int)Session["currentAssort"];
            if (ModelState.IsValid) {
                try {
                    Debug.WriteLine(model.ToString());
                    using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                        conn.Open();
                        var res = conn.Query("update f_Cab_grower_cask_matrix set cgcm_num_items_bunch = @PrBunch where cgcm_sequence = @Id", new { PrBunch = model.cask_prbunch, Id = model.seq });
                    } 
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;

                }
            }
            return PartialView("Casks", getAssortimentCasks(assortSeq));
        }

        public ActionResult DeleteCask([ModelBinder(typeof(DevExpressEditorsBinder))] Cask model) {
            int assortSeq = (int)Session["currentAssort"];
            if (ModelState.IsValid) {
                try {
                    using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                        conn.Open();
                        var res = conn.Query("delete from F_CAB_GROWER_CASK_MATRIX where CGCM_SEQUENCE = @Id", new { Id = model.seq });
                        Log.InfoFormat("{0} deleted {1} (F_CAB_GROWER_CASK_MATRIX", Usr.LoginName, model.seq);
                    } 
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            return PartialView("Casks", getAssortimentCasks(assortSeq));
        }

        public List<Cask> getAssortimentCasks(int id) {
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                var res = conn.Query<Cask>("[logicab_getAssortimentCasks] @Id", new { Id = id });
                return res.ToList();
            } 

        }

        public ActionResult CaskComboPartial() {
            List<CAB.Domain.Models.Cask> casks = new List<CAB.Domain.Models.Cask>();
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                casks = conn.Query<CAB.Domain.Models.Cask>(@"
select 
	CDCA_SEQUENCE as Id, 
	CDCA_CASK_CODE as Code, 
	CDCA_CASK_DESC as [Desc], 
	CDCA_TRANSPORT_TYPE as Transport 
from F_CAB_CD_CASK
order by Code").ToList();
            }
            return PartialView(casks);
        }

        public ActionResult CallbacksImageUpload() {
            string UC_grasSeq = Request.Params["DXUploadingCallback"];
            int gras_sequence = Int32.Parse(UC_grasSeq.Split('_')[1]);
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles(UC_grasSeq, null, ucCallbacks_FileUploadComplete);
            UploadedFile file = files[0];

            MemoryStream ms = new MemoryStream(file.FileBytes);
            Image tempImg = Image.FromStream(ms);
            Image newImage = ImageUtils.resizeImage(tempImg, new Size { Height = 1024, Width = 1024 });
            MemoryStream ms2 = new MemoryStream();
            newImage.Save(ms2, ImageFormat.Jpeg);

            F_CAB_PICTURE newPic = new F_CAB_PICTURE {
                PICT_PICTURE = ms2.ToArray()
            };
                EFHelper.ctx.F_CAB_PICTURE.AddObject(newPic);
            EFHelper.ctx.SaveChanges();

            EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.FirstOrDefault(x => x.GRAS_SEQUENCE == gras_sequence).GRAS_FK_PICTURE = newPic.PICT_SEQUENCE;
            EFHelper.ctx.SaveChanges();
            return null;
        }

        public static void ucCallbacks_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
            int lastPicSeq = (from n in EFHelper.ctx.F_CAB_PICTURE orderby n.PICT_SEQUENCE descending select n.PICT_SEQUENCE).FirstOrDefault();
            lastPicSeq++;
            e.CallbackData = lastPicSeq.ToString();
        }
    }

}
