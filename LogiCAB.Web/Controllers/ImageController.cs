﻿using CAB.Data.EF.Models;
using LogiCAB.Web.Domain;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;

namespace LogiCAB.Web.Controllers {
    public class ImageController : Controller {
        private Entities db = new Entities();

        [OutputCache(Duration = 600, VaryByParam = "id", Location = OutputCacheLocation.ServerAndClient)]
        public FileContentResult ShowImage(int id = -1) {
            if (id == -1)
                return NoPic();
            try {
                return File(db.F_CAB_PICTURE.SingleOrDefault(x => x.PICT_SEQUENCE == id).PICT_PICTURE, "image/jpeg");
            } catch {
                return NoPic();
            }
        }

        [OutputCache(Duration = 600, VaryByParam = "id", Location = OutputCacheLocation.ServerAndClient)]
        public FileContentResult ShowImageSmall(int? id = null) {
            if (id == null)
                return NoPic();
            try {
                return File(ImageUtils.CreateThumbnail(db.F_CAB_PICTURE.SingleOrDefault(x => x.PICT_SEQUENCE == id).PICT_PICTURE, 50), "image/jpeg");
            } catch {
                return NoPic();
            }
        }

        [OutputCache(Duration = 600, VaryByParam = "", Location = OutputCacheLocation.ServerAndClient)]
        public FileContentResult NoPic() {
            return File(ImageUtils.NoPic(Server.MapPath("~/")), "image/jpeg");
        }
    }
}
