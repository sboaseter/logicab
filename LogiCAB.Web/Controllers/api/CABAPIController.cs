﻿using log4net;
using LogiCAB.Web.Domain;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using Dapper;
using CAB.Data.EF.Models;
using LogiCAB.Web.DAL;
using System.Data;

namespace LogiCAB.Web.Controllers {
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CABAPIController : ApiController {
        private ILog Log;
        public CABAPIController(ILog log) {
            Log = log;
        }
        [HttpGet]
        public List<CABPropGroup> CABPropGroups() {
            Log.Info("CAB API: CABPropGroups() called.");
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                var res = conn.Query<CABPropGroup>("select CDPG_SEQUENCE as Sequence, CDPG_DESC as Description, CDPG_Order as [Order] from F_CAB_CD_CAB_PROP_GROUP");
                return res.ToList();
            }              
        }
        [HttpGet]
        public List<CABPropType> CABPropTypes(int id) {
            Log.InfoFormat("CAB API: CABPropTypes({0}) called.", id);
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                var res = conn.Query<CABPropType>("select CDPT_SEQUENCE as Sequence, CDPT_DESC as Description, CDPT_MANDATORY_YN as Mandatory from F_CAB_CD_CAB_PROP_TYPE where CDPT_FK_PROP_GROUP = @id", new { id = id });
                return res.ToList();
            }              
        }
        [HttpGet]
        public List<CABPropLookup> CABPropLookups(int id) {
            Log.InfoFormat("CAB API: CABPropLookups({0}) called.", id);
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                var res = conn.Query<CABPropLookup>("select CDPL_SEQUENCE as Sequence, CDPL_LOOKUP_VALUE as Value, CDPL_ORDER as [Order] from F_CAB_CD_CAB_PROP_LOOKUP where CDPL_FK_PROP_TYPE = @id order by CDPL_LOOKUP_VALUE", new { id = id });
                return res.ToList();
            }              
        }

        [HttpGet]
        public void syncProperties() {
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Query("proc_fill_and_sync_properties", commandType: CommandType.StoredProcedure, commandTimeout: 600);
            }
        }

        [HttpGet]
        public bool addProperty(string type, string description, string value) {
            try { 
                int t = Int32.Parse(type);
                description = description.Replace("  ", " ").Trim();
                value = value.Replace("  ", " ").Trim();
                //check if exists
                var exists = EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.Any(x => x.CDPL_FK_PROP_TYPE == t && x.CDPL_LOOKUP_VALUE == value);
                if (exists) {
                    var currentProp = EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.FirstOrDefault(x => x.CDPL_FK_PROP_TYPE == t && x.CDPL_LOOKUP_VALUE == value);
                    currentProp.CDPL_LOOKUP_VALUE = value;                    
                } else { 
                    F_CAB_CD_CAB_PROP_LOOKUP newProp = new F_CAB_CD_CAB_PROP_LOOKUP
                    {
                        CDPL_FK_PROP_TYPE = t,
                        CDPL_LOOKUP_VALUE = value
//                        CDPL_LOOKUP_DESC = description,
//                        CDPL_ORDER = 1
                    };
                    EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.AddObject(newProp);
                }
                EFHelper.ctx.SaveChanges();
                return true;
            } catch (Exception ex) {
                return false;
            }
        }


        [HttpGet]
        public int executePropertyChange(int idCorrect, int idNotCorrect) {

            F_CAB_CD_CAB_PROP_LOOKUP cdpl_correct = EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.SingleOrDefault(x => x.CDPL_SEQUENCE == idCorrect);
            F_CAB_CD_CAB_PROP_LOOKUP cdpl_notcorrect = EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.SingleOrDefault(x => x.CDPL_SEQUENCE == idNotCorrect);
            int propertyType = cdpl_correct.CDPL_FK_PROP_TYPE;

            var detailUsingWrong = from cabd in EFHelper.ctx.F_CAB_CAB_DETAIL
                                       .Where(x => 
                                           x.CABD_VALUE == cdpl_notcorrect.CDPL_LOOKUP_VALUE && 
                                           x.CABD_FK_TYPE == propertyType) 
                                   select cabd;

            int detailUsingWrongCount = detailUsingWrong.Count();
            foreach (var detail in detailUsingWrong) {
                detail.CABD_VALUE = cdpl_correct.CDPL_LOOKUP_VALUE;
                detail.CABD_VALUE_DESC = cdpl_correct.CDPL_LOOKUP_VALUE;
                detail.CABD_VALUE_DESC_UPPER = cdpl_correct.CDPL_LOOKUP_VALUE.ToUpper();
            }
            EFHelper.ctx.SaveChanges();
            var vlkmat = EFHelper.ctx.F_CAB_CD_CAB_VBN_LK_MATRIX.Where(x => x.CVLM_FK_CAB_LOOKUP_VALUE == cdpl_notcorrect.CDPL_SEQUENCE).ToList();
            if (vlkmat != null) {
                for (int i = 0; i < vlkmat.Count(); i++) {
                    if(!EFHelper.ctx.F_CAB_CD_CAB_VBN_LK_MATRIX.Any(x => x.CVLM_FK_CAB_LOOKUP_VALUE == cdpl_correct.CDPL_SEQUENCE)) {
                        vlkmat[i].CVLM_FK_CAB_LOOKUP_VALUE = cdpl_correct.CDPL_SEQUENCE;
                    } else {                  
                        EFHelper.ctx.F_CAB_CD_CAB_VBN_LK_MATRIX.DeleteObject(vlkmat[i]);
                    }
                }
            }
            EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.DeleteObject(cdpl_notcorrect);
            EFHelper.ctx.SaveChanges();

            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString))
            {
                conn.Query("proc_fill_and_sync_properties", new { CDPL_SEQ = idCorrect }, commandType: CommandType.StoredProcedure, commandTimeout: 60);
            }
            return detailUsingWrongCount;
        }

    }
    public class TwoInts {
        public int idCorrect { get; set; }
        public int idNotCorrect { get; set; }

    }
}
