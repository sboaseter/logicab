﻿using DevExpress.Web;
using DevExpress.Web.Mvc;
using LogiCAB.Web.DAL;
using LogiCAB.Web.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DevExpress.Web.Mvc.UI;


namespace LogiCAB.Web.Controllers {
    public enum OrgaTypeBilling {
        Grower,
        Buyer,
        KVKServive
    }
    [Authorize]
    [HandleError]
    public class BillingController : Controller {

        public ActionResult IndexBuyers() {
            if (Session["billinglist_b"] == null)
                Session["billinglist_b"] = GetBillingList(OrgaTypeBilling.Buyer);
            return View((List<BillingRecordL>)Session["billinglist_b"]);

        }
        public ActionResult IndexGrowers() {
            if (Session["billinglist_g"] == null)
                Session["billinglist_g"] = GetBillingList(OrgaTypeBilling.Grower);
            return View((List<BillingRecordL>)Session["billinglist_g"]);

        }

        public ActionResult IndexKVKService() {
            if (Session["billinglist_kvks"] == null)
                Session["billinglist_kvks"] = GetBillingList(OrgaTypeBilling.KVKServive);
            return View((List<BillingRecordL>)Session["billinglist_kvks"]);
        }



        public ActionResult ListBuyers() {
            if (Session["billinglist_b"] == null)
                Session["billinglist_b"] = GetBillingList(OrgaTypeBilling.Buyer);
            return PartialView((List<BillingRecordL>)Session["billinglist_b"]);
        }
        public ActionResult ListGrowers() {
            if (Session["billinglist_g"] == null)
                Session["billinglist_g"] = GetBillingList(OrgaTypeBilling.Grower);
            return PartialView((List<BillingRecordL>)Session["billinglist_g"]);
        }

        public ActionResult ListKVKService() {
            if (Session["billinglist_kvks"] == null)
                Session["billinglist_kvks"] = GetBillingList(OrgaTypeBilling.KVKServive);
            var retList = (List<BillingRecordL>)Session["billinglist_kvks"];
            return PartialView(retList);
        }


        public ActionResult ExportToBuyer() {
            GridViewSettings gvs = null;
            if (Session["billinglist_b"] == null)
                Session["billinglist_b"] = GetBillingList(OrgaTypeBilling.Buyer);
            var dt = (List<BillingRecordL>)Session["billinglist_b"];
            return GridViewExtension.ExportToXls(gvs, dt);
        }
        public ActionResult ExportToGrower(GridViewSettings gvs) {
            if (Session["billinglist_g"] == null)
                Session["billinglist_g"] = GetBillingList(OrgaTypeBilling.Grower);
            var dt = (List<BillingRecordL>)Session["billinglist_g"];
            return GridViewExtension.ExportToXls(gvs, dt);
        }


        public List<BillingRecordL> GetBillingList(OrgaTypeBilling orgaType) {
            DateTime startDateTime = new DateTime();
            DateTime endDateTime = new DateTime();

            if (DateTime.Now.Hour > 9) {
                startDateTime = DateTime.Today.AddHours(0);
                endDateTime = DateTime.Today.AddDays(1).AddMinutes(24 * 60 - 1);
            } else {
                startDateTime = DateTime.Today.AddDays(-1).AddHours(0);
                endDateTime = DateTime.Today.AddMinutes(24 * 60 - 1);
            }

            try {
                if (Session["Billing_ordersStartDate"] != null)
                    startDateTime = (DateTime)Session["Billing_ordersStartDate"];
                else
                    Session["Billing_ordersStartDate"] = startDateTime;
            } catch {
            }
            try {
                if (Session["Billing_ordersEndDate"] != null)
                    endDateTime = (DateTime)Session["Billing_ordersEndDate"];
                else
                    Session["Billing_ordersEndDate"] = endDateTime;
            } catch {
            }

            // Default values
            int FuncLOGISERVICE = 18;
            int FuncKVK = 20;

            // Correct default values incase they differ from db to db
            FuncLOGISERVICE = EFHelper.ctx.F_CAB_FUNCTION.SingleOrDefault(x => x.FUNC_CODE == "LOGISERVICE").FUNC_SEQUENCE;
            FuncKVK = EFHelper.ctx.F_CAB_FUNCTION.SingleOrDefault(x => x.FUNC_CODE == "KVK_USER").FUNC_SEQUENCE;

            // All orderheaders within the selected timespan
            var iList = from o in EFHelper.ctx.F_CAB_ORDER_HEA
                        where o.ORHE_CREATED_ON >= startDateTime && o.ORHE_CREATED_ON <= endDateTime
                        select o;

            /*
             * Improvement potential: 
             * -Seperate selection for commisionaires, and exclude them from buyers and grower selection
             * 
             */ 
            // All buyers whos BuyerSequence occurs in the selected Orderheaders.
            var buyers = from b in EFHelper.ctx.F_CAB_BUYER.Where(x => iList.Select(y => y.ORHE_FK_BUYER).Contains(x.BUYR_SEQUENCE))
                         join o in EFHelper.ctx.F_CAB_ORGANISATION on b.BUYR_FK_ORGA equals o.ORGA_SEQUENCE
                         join a in EFHelper.ctx.F_CAB_ORGANISATION_ADMIN on o.ORGA_SEQUENCE equals a.ORAD_FK_ORGANISATION
                         join f in EFHelper.ctx.F_CAB_ORGANISATION_FUNCTION.Where(x => x.ORFU_FK_FUNC == FuncLOGISERVICE) on o.ORGA_SEQUENCE equals f.ORFU_FK_ORGA into ServiceTable
                         from Service in ServiceTable.DefaultIfEmpty()
                         select new BillingRecordL {
                             Organisation = o.ORGA_NAME,
                             _Total = iList.Where(x => x.ORHE_FK_BUYER == b.BUYR_SEQUENCE).Sum(oh => oh.ORHE_NETT_HEADER_VALUE), // Sum of all orderheaders from the BuyerSequence
                             _Subscription = a.ORAD_MEMBERTYPE,
                             LogiService = Service != null ? true : false
                         };
            // All growers whos GrowerSequence occurs in the selected Orderheaders.
            var growers = from b in EFHelper.ctx.F_CAB_GROWER.Where(x => iList.Select(y => y.ORHE_FK_GROWER).Contains(x.GROW_SEQUENCE))
                          join o in EFHelper.ctx.F_CAB_ORGANISATION on b.GROW_FK_ORGA equals o.ORGA_SEQUENCE
                          join a in EFHelper.ctx.F_CAB_ORGANISATION_ADMIN on o.ORGA_SEQUENCE equals a.ORAD_FK_ORGANISATION
                          join f in EFHelper.ctx.F_CAB_ORGANISATION_FUNCTION.Where(x => x.ORFU_FK_FUNC == FuncLOGISERVICE) on o.ORGA_SEQUENCE equals f.ORFU_FK_ORGA into ServiceTable
                          from Service in ServiceTable.DefaultIfEmpty()
                          select new BillingRecordL {
                              Organisation = o.ORGA_NAME,
                              _Total = iList.Where(x => x.ORHE_FK_GROWER == b.GROW_SEQUENCE).Sum(oh => oh.ORHE_NETT_HEADER_VALUE), // Sum of all orderheaders from the GrowerSequence
                              _Subscription = a.ORAD_MEMBERTYPE,
                              LogiService = Service != null ? true : false
                          };
            // All buyers who are KVKs and who have the ServiceFunction, regardless of orders placed or not, meaning _Total can result in 0 for kvk's who have no orders placed in selected timespan
            var kvkservive = from k in EFHelper.ctx.F_CAB_BUYER //.Where(x => iList.Select(y => y.ORHE_FK_BUYER).Contains(x.BUYR_SEQUENCE))
                             join o in EFHelper.ctx.F_CAB_ORGANISATION on k.BUYR_FK_ORGA equals o.ORGA_SEQUENCE
                             where
                                o.F_CAB_ORGANISATION_FUNCTION.Any(x => x.ORFU_FK_FUNC == FuncKVK) && 
                                o.F_CAB_ORGANISATION_FUNCTION.Any(x => x.ORFU_FK_FUNC == FuncLOGISERVICE) &&
                                o.F_CAB_ORGANISATION_ADMIN.FirstOrDefault().ORAD_MEMBERTYPE == 0 // KVK
                             select new BillingRecordL {
                                 Organisation = o.ORGA_NAME,
                                 Commisionaire = o.F_CAB_HANDSHAKE1.FirstOrDefault().F_CAB_ORGANISATION.ORGA_NAME,
                                 _Total = iList.Any(x => x.ORHE_FK_BUYER == k.BUYR_SEQUENCE) ? 
                                            iList.Where(x => x.ORHE_FK_BUYER == k.BUYR_SEQUENCE).Sum(oh => oh.ORHE_NETT_HEADER_VALUE) 
                                            : 0,
                                 _Subscription = o.F_CAB_ORGANISATION_ADMIN.FirstOrDefault().ORAD_MEMBERTYPE,
                                 LogiService = true
                             };

                             

            var g_gold = growers.Count(x => x._Subscription == ORAD_MemberType.GOUD);
            var g_silver = growers.Count(x => x._Subscription == ORAD_MemberType.ZILVER);
            var g_bronze = growers.Count(x => x._Subscription == ORAD_MemberType.BRONZE);

            var b_gold = buyers.Count(x => x._Subscription == ORAD_MemberType.GOUD);
            var b_silver = buyers.Count(x => x._Subscription == ORAD_MemberType.ZILVER);
            var b_bronze = buyers.Count(x => x._Subscription == ORAD_MemberType.BRONZE);

            // Add a summary of number of buyers in selected time period with LogiService to show a total inc/ ServiceCost ( 75eur pr month)

            Session["Subscriptions"] = new int[]{
                g_gold, g_silver, g_bronze, b_gold, b_silver, b_bronze
            };

            TimeSpan ts = endDateTime - startDateTime;
            if (orgaType == OrgaTypeBilling.Buyer)
                return buyers.ToList();
            else if (orgaType == OrgaTypeBilling.Grower)
                return growers.ToList();
            else if (orgaType == OrgaTypeBilling.KVKServive)
                return kvkservive.ToList();
            return null;
        }


        public List<BillingRecord> GetBillingListOld() {
            DateTime startDateTime = new DateTime();
            DateTime endDateTime = new DateTime();

            if (DateTime.Now.Hour > 9) {
                startDateTime = DateTime.Today.AddHours(0);
                endDateTime = DateTime.Today.AddDays(1).AddMinutes(24 * 60 - 1);
            } else {
                startDateTime = DateTime.Today.AddDays(-1).AddHours(0);
                endDateTime = DateTime.Today.AddMinutes(24 * 60 - 1);
            }

            try {
                if (Session["Billing_ordersStartDate"] != null)
                    startDateTime = (DateTime)Session["Billing_ordersStartDate"];
            } catch {
            }
            try {
                if (Session["Billing_ordersEndDate"] != null)
                    endDateTime = (DateTime)Session["Billing_ordersEndDate"];
            } catch {
            }
            var iList = EFHelper.ctx.F_CAB_ORDER_HEA.Where(x =>
                x.ORHE_DATE >= startDateTime
                && x.ORHE_DATE <= endDateTime
                );
            var buyers = EFHelper.ctx.F_CAB_BUYER.Where(x => iList.Select(y => y.ORHE_FK_BUYER).Contains(x.BUYR_SEQUENCE));

            var list = from orhe in EFHelper.ctx.F_CAB_ORDER_HEA
                       join buyer in EFHelper.ctx.F_CAB_BUYER on orhe.ORHE_FK_BUYER equals buyer.BUYR_SEQUENCE
                       join bOrga in EFHelper.ctx.F_CAB_ORGANISATION on buyer.BUYR_FK_ORGA equals bOrga.ORGA_SEQUENCE
                       join baOrga in EFHelper.ctx.F_CAB_ORGANISATION_ADMIN on bOrga.ORGA_SEQUENCE equals baOrga.ORAD_FK_ORGANISATION
                       join bOrgaService in EFHelper.ctx.F_CAB_ORGANISATION_FUNCTION on bOrga.ORGA_SEQUENCE equals bOrgaService.ORFU_FK_ORGA into ServiceTable
                       from Service in ServiceTable.DefaultIfEmpty()

                       join grower in EFHelper.ctx.F_CAB_GROWER on orhe.ORHE_FK_GROWER equals grower.GROW_SEQUENCE
                       join gOrga in EFHelper.ctx.F_CAB_ORGANISATION on grower.GROW_FK_ORGA equals gOrga.ORGA_SEQUENCE
                       join gaOrga in EFHelper.ctx.F_CAB_ORGANISATION_ADMIN on gOrga.ORGA_SEQUENCE equals gaOrga.ORAD_FK_ORGANISATION

                       join person in EFHelper.ctx.F_CAB_PERSON on orhe.ORHE_FK_PERSON equals person.PERS_SEQUENCE
                       join orderstatus in EFHelper.ctx.F_CAB_CD_ORD_HEA_STATUS on orhe.ORHE_FK_ORDER_STATUS equals orderstatus.CDOS_SEQUENCE

                       where

                        orhe.ORHE_DATE >= startDateTime &&
                        orhe.ORHE_DATE <= endDateTime

                       select new BillingRecord {
                           Shop = "CAB", // For now
                           BuyerName = bOrga.ORGA_NAME,
                           BuyerContract = baOrga.ORAD_MEMBERTYPE,
                           BuyerService = Service != null,
                           OrderHeaderSequence = orhe.ORHE_SEQUENCE,
                           OrderDate = orhe.ORHE_DATE,
                           OrderNo = orhe.ORHE_NO,
                           OrderDesc = orhe.ORHE_DESC,
                           OrderDeliveryDate = orhe.ORHE_DELIVERY_DATE ?? orhe.ORHE_DATE,
                           OrderNettValue = orhe.ORHE_NETT_HEADER_VALUE,
                           PersonFirstName = person.PERS_FIRST_NAME,
                           PersonMiddleName = person.PERS_MIDDLE_NAME,
                           PersonLastName = person.PERS_LAST_NAME,
                           OrderRemark = orhe.ORHE_REMARK,
                           BuyerOrgaSequence = bOrga.ORGA_SEQUENCE,
                           BuyerOrgaName = bOrga.ORGA_NAME,
                           BuyerOrgaCode = bOrga.ORGA_CODE,

                           GrowerOrgaSequence = gOrga.ORGA_SEQUENCE,
                           GrowerOrgaName = gOrga.ORGA_NAME,
                           GrowerOrgaCode = gOrga.ORGA_CODE,
                           GrowerContract = gaOrga.ORAD_MEMBERTYPE,

                           MainGroup = orhe.F_CAB_CD_MAIN_GROUP_TYPE.CDMG_DESC,
                           VBNCode = "0"
                       };
            return list.ToList();
        }

        public JsonResult setSelectedDate(string startDate, string endDate, string startTime, string endTime) {
            Session["Billing_ordersStartDate"] = DateTime.Parse(startDate).Add(TimeSpan.Parse(startTime));
            Session["Billing_ordersEndDate"] = DateTime.Parse(endDate).Add(TimeSpan.Parse(endTime));
            Session["billinglist_b"] = null;
            Session["billinglist_g"] = null;
            Session["billinglist_kvks"] = null;

            return Json(true, JsonRequestBehavior.AllowGet);
        }
        private string _rootFolder() {
            return System.Web.HttpContext.Current.Server.MapPath("~");
        }
    }
}
