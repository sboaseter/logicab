﻿using log4net;
using LogiCAB.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LogiCAB.Web.Controllers {
    public class ErrorController : BaseController {
        public ErrorController(ILog log) : base(log) { }
        public ActionResult Error(string id)  {
            Log.ErrorFormat("Error occured: {0}", id);
            ViewBag.ErrorCode = id;
            return View();
        }
    }
}
