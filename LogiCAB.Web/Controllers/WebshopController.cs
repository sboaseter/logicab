﻿using LogiCAB.Web.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LogiCAB.Web.Controllers {
    [AuthorizeWithSessionAttribute]
    [HandleError]
    public class WebshopController : Controller {
        public ActionResult Version() {
            return View();
        }

    }
}
