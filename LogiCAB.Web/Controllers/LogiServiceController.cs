﻿using CAB.Domain.Helpers;
using log4net;
using LogiCAB.Service.Domain;
using LogiCAB.Service.Models;
using LogiCAB.Web.Controllers;
using LogiCAB.Web.DAL;
using LogiCAB.Web.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace LogiCAB.Web.Controllers {
    [AuthorizeWithSessionAttribute]
    [HandleError]
    public class LogiServiceController : BaseController {
        public LogiServiceController(ILog log) : base(log) {
        }
        public ActionResult Index() {
            return View();
        }
        public ActionResult List() {
            string endpoint = "http://devservice.logicab.nl/Florecom/Jul2012/CommercialCustomer.asmx";
            string username = !string.IsNullOrEmpty(Request.Params["username"]) ? Request.Params["username"] : "";
            string password = !string.IsNullOrEmpty(Request.Params["password"]) ? Request.Params["password"] : "";
//            string version = !string.IsNullOrEmpty(Request.Params["version"]) ? Request.Params["version"] : "0.7";
            endpoint = !string.IsNullOrEmpty(Request.Params["endpoint"]) ? Request.Params["endpoint"] : endpoint; 
//            string username = Usr.LoginName;
//            string password = Usr.LoginPass;
            string version = "0.7";

            return PartialView(_getLogiServiceOfferList(username, password, version, endpoint));
        }
        public JsonResult PutOrder(string id, string url, string delivery, string numItems, string username, string password) {
//            Thread.Sleep(3000);
            Log.InfoFormat("PutOrder received for ID: {0}, Items: {1}", id, numItems);

//            string username = !string.IsNullOrEmpty(Request.Params["username"]) ? Request.Params["username"] : "";
//            string password = !string.IsNullOrEmpty(Request.Params["password"]) ? Request.Params["password"] : "";


            ServiceOrderResponse res = _putLogiServiceOrder(username, password, "0.7", url, id, delivery, numItems);
            return Json(new {
                Status = res.Status,
                Price = res.Price,
                Quantity = res.Quantity,
                LineDateTime = res.LineDateTime.ToString()
            }, JsonRequestBehavior.AllowGet);
        }
        private ServiceOrderResponse _putLogiServiceOrder(
            String username = "", 
            String password = "", 
            String version = "0.7", 
            string endpoint = "http://devservice.logicab.nl/Florecom/Jul2012/CommercialCustomer.asmx", 
            string id = "",
            string delivery = "",
            string numItems = "1"
            ) {
            Service2012 service = new Service2012();
            var ret = service.PutOrder(username, password, endpoint, id, delivery, numItems);
            return ret;
        }
        private IEnumerable<ServiceOffer> _getLogiServiceOfferList(String username = "", String password = "", String version = "0.7", string endpoint = "http://devservice.logicab.nl/Florecom/Jul2012/CommercialCustomer.asmx") {
            var currentTime = DateTime.Now;

            //#if DEBUG
            String sessionKey = new Random().Next(1000).ToString();
            // TODO: Change to interval determinator
            //#else
            //            String sessionKey = String.Format("serviceoffers_{0}_{1}_{2}_{3}", username, password, version, currentTime.Minute / 5);
            //#endif
            if (Session[sessionKey] == null) {
                if (version == "0.7") {
                    Service2012 service = new Service2012();
                    Session[sessionKey] = service.GetSupply(username, password, endpoint);
                } else if (version == "0.5") {
                    Service2010 service = new Service2010();
                    Session[sessionKey] = service.GetSupply(username, password);
                }

            }
            String sessionKeySuppliers = "suppliers";
            if (Session[sessionKeySuppliers] == null) {
                Session[sessionKeySuppliers] = EFHelper.ctx.F_CAB_ORGANISATION.Where(x => x.ORGA_EAN_CODE != "" && x.ORGA_NAME != null && x.ORGA_EAN_CODE != null).Select(x => new Supplier {
                    SupplierEAN = x.ORGA_EAN_CODE,
                    SupplierName = x.ORGA_NAME
                }).ToList();
            }
            List<ServiceOffer> offerlist = (List<ServiceOffer>)Session[sessionKey];
            List<Supplier> suppliers = (List<Supplier>)Session[sessionKeySuppliers];
            foreach (var item in offerlist) {
                var supplierEan = item.SupplierEAN;
                var supplierObj = suppliers.FirstOrDefault(x => x.SupplierEAN == supplierEan);
                if (supplierObj != null) {
                    if (supplierObj.SupplierName != null)
                        item.SupplierName = supplierObj.SupplierName;
                }
            }
            return offerlist;
        }

    }
}
