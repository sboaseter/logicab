﻿using CAB.Data.EF.Models;
using log4net;
using LogiCAB.Web.Domain;
using LogiCAB.Web.Controllers;
using LogiCAB.Web.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using CAB.Domain.Helpers;

namespace LogiCAB.Web.Controllers {
    public class TransportController : BaseController {
        private TransportProvider _transportProvider { get; set; }
        static Func<int, string> StatusText;
        static Func<int, string> OrderDetailInfo;
        
        int outputs = 0;
        public TransportController(TransportProvider transportProvider, ILog log) : base(log) {
                _transportProvider = transportProvider;
        }
        public ActionResult Index() {
            return View(getHandshakesTransporter());
        }
        public ActionResult List() {
            return PartialView("List", getHandshakesTransporter());
        }

        private IEnumerable getHandshakesTransporter() {
            return (
                from ho in EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.Where(x => x.HNDO_FK_ORGA_SEQ != null && x.HNDO_FK_ORGA_SEQ == Usr.OrganisationSequence)
                select new Handshake {
                    seq = ho.HNDO_FK_HAND_SEQ,
                    _buyerOrga = ho.F_CAB_HANDSHAKE.F_CAB_ORGANISATION1,
                    hndo = ho
                }).Distinct().ToList();

        }


    }


}
