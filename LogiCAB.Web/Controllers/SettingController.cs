﻿using CAB.Data.EF.Models;
using LogiCAB.Web.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LogiCAB.Web.Controllers
{
    public class SettingController : Controller
    {
        //
        // GET: /Setting/

        public ActionResult Index()
        {
            return View(_getOptionFields());
        }

        public ActionResult List() {
            return PartialView(_getOptionFields());
        }

        private IQueryable<F_CAB_HANDSHAKE_OPTION_FIELDS> _getOptionFields() {
            return EFHelper.ctx.F_CAB_HANDSHAKE_OPTION_FIELDS;
        }

    }
}
