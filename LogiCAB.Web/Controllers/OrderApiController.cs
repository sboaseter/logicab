﻿using CAB.Data.EF.Models;
using CAB.Domain.Providers;
using CAB.Domain.Repositories;
using log4net;
using LogiCAB.Web.DAL;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace LogiCAB.Web.Controllers {
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class OrderApiController : ApiController {
        private ILog Log;
        private IOrganisationRepository OrganisationRepository;
        private IPersonRepository PersonRepository;
        private IOrderProvider OrderProvider;
        private string smtpServer;
        private string mailOverride;
        private string logifloraFromAddress;
        public OrderApiController(
            IOrganisationRepository organisationRepository,
            IPersonRepository personRepository,
            IOrderProvider orderProvider,
            ILog log) {
                OrganisationRepository = organisationRepository;
                PersonRepository = personRepository;
                OrderProvider = orderProvider;
                Log = log;
                smtpServer = ConfigurationManager.AppSettings["SMTP"];
                mailOverride = ConfigurationManager.AppSettings["mailOverride"];
                logifloraFromAddress = ConfigurationManager.AppSettings["LogiFloraFromAddress"];

        }
        [HttpPost]
        public string SendEKT([FromBody]int id) {
            Log.InfoFormat("LogiCAB WebAPI: OrderApi/SendEKT OrderHeader: [{0}]", id);
            var anyEKTSentMissing = EFHelper.ctx.F_CAB_ORDER_DET.Any(x => x.ORDE_FK_ORDER_HEA == id && x.ORDE_EKT_SEND_YN == 0);
            if (anyEKTSentMissing) {
                var res = SendEKTMail(id);
                return String.Format("SendEKT: {0}, Result: {1}", id, res);
            } else {
                return String.Format("SendEKT: {0}, Result: {1}", id, "No EKT with Flag: 0");
            }
        }

        [HttpPost]
        public string SendKWB([FromBody]int id) {
            Log.InfoFormat("LogiCAB WebAPI: OrderApi/SendKWB OrderHeader: [{0}]", id);            
            var anyKWBSentMissing = EFHelper.ctx.F_CAB_ORDER_DET.Any(x => x.ORDE_FK_ORDER_HEA == id && x.ORDE_KWB_SEND_YN == 0);
            if (anyKWBSentMissing) {
                Log.InfoFormat("Check shows anyKWBSentMissing: {0}", anyKWBSentMissing);
                //Task.Factory.StartNew(() => SendKWBMail(id));
                var res = SendKWBMail(id);
                return String.Format("SendKWB: {0}", id);
                //return ""
            } else {
                return String.Format("SendKWB: {0}, Result: {1}", id, "No KWB with Flag: 0");
            }
        }

        private string SendKWBMail(int id) {
            Log.InfoFormat("Collecting data for orderheader: {0}", id);
            F_CAB_ORDER_HEA orderHeader = EFHelper.ctx.F_CAB_ORDER_HEA.Single(x => x.ORHE_SEQUENCE == id);
            string toAdressStr = "";
            string fromAdressStr = "";
            if (String.IsNullOrEmpty(orderHeader.F_CAB_BUYER.F_CAB_ORGANISATION.ORGA_MAILBOX))
                toAdressStr = orderHeader.F_CAB_BUYER.F_CAB_ORGANISATION.ORGA_EMAIL;
            else
                toAdressStr = orderHeader.F_CAB_BUYER.F_CAB_ORGANISATION.ORGA_MAILBOX;

            if (String.IsNullOrEmpty(orderHeader.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_MAILBOX))
                fromAdressStr = orderHeader.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_EMAIL;
            else
                fromAdressStr = orderHeader.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_MAILBOX;
            string toAdressKWB = orderHeader.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_MAILBOX;
            if (string.IsNullOrEmpty(toAdressKWB))
                toAdressKWB = fromAdressStr;
            Log.InfoFormat("Result: Orderheader: {0}, toAdressStr: {1}, fromAdressStr: {2}, toAdressKWB: {3}", id, toAdressStr, fromAdressStr, toAdressKWB);


            MailMessage Email = new MailMessage();
            Email.From = new MailAddress(fromAdressStr.Split(';')[0]);
            Email.To.Add(new MailAddress(toAdressKWB.Split(';')[0]));
            if (toAdressKWB.Split(';').Count() > 1) {
                var toAdresses = toAdressKWB.Split(';').Skip(1);
                foreach (var toadd in toAdresses) {
                    Email.To.Add(new MailAddress(toadd));
                }
            }
            Email.Subject = "ORDERS;CAB";
            var kwbContent = OrderProvider.CreateKWB(id);

            


            var logroot = HttpContext.Current.Server.MapPath("~/Logs/ektkwb");
            var kwbfilename = id.ToString() + "_"  + DateTime.Now.ToLongTimeString().Replace(":", string.Empty) + ".kwb.txt";
            FileStream fs = new FileStream(logroot + "/" + kwbfilename, FileMode.Append);
            byte[] kwbData = Encoding.ASCII.GetBytes(kwbContent);
            fs.Write(kwbData, 0, kwbData.Length);
            fs.Close();
            Log.InfoFormat("KWB Generated: File: ~/Logs/" + kwbfilename);

            Byte[] b = System.Text.Encoding.ASCII.GetBytes(kwbContent);
            MemoryStream ms = new MemoryStream(b);

            Attachment att = new Attachment(ms, new System.Net.Mime.ContentType("Application/EDIFACT"));
            System.Net.Mime.ContentDisposition cd = att.ContentDisposition;
            cd.FileName = orderHeader.ORHE_NO + DateTime.Now.ToString("_yyyyMMdd_hhmm") + ".txt";
            att.TransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;

            Email.Attachments.Add(att);
            SmtpClient SMTP = new SmtpClient();
            SMTP.Host = smtpServer;
            try {
                if (!String.IsNullOrEmpty(mailOverride)) {
                    Email.From = new MailAddress(mailOverride);
                    Email.To.Clear();
                    Email.To.Add(new MailAddress(mailOverride));
                }
                Email.From = new MailAddress(logifloraFromAddress, orderHeader.F_CAB_BUYER.F_CAB_ORGANISATION.ORGA_NAME);

                // While exception exists -> redo SMTP.Send x-times.
                SMTP.Send(Email);
                if (!String.IsNullOrEmpty(mailOverride))
                    Log.Info(string.Format("KWB (MailOverride: {0}). From: {1}, To: {2}\n\r{3}", mailOverride, logifloraFromAddress, toAdressKWB, kwbContent));
                else
                    Log.Info(string.Format("KWB From: {0}, To: {1}\n\r{2}", logifloraFromAddress, toAdressKWB, kwbContent));
                //orderHeader.ORHE_FK_ORDER_STATUS = orderHeader.ORHE_FK_ORDER_STATUS == OrderHeaderStatus.EKTSENT ? OrderHeaderStatus.EKTKWBSENT : OrderHeaderStatus.KWBSENT;
                EFHelper.ctx.SaveChanges();
                // KWB Sent
                OrderProvider.UpdateDetailsKWBSent(id);

                return "1";
            } catch (Exception ex) {
                Log.WarnFormat("Failed sending KWB: OrderHeader: {0}, Error: {1}", id, ex.ToString());
                return "-1";
            }
        }
        private string SendEKTMail(int id) {
            F_CAB_ORDER_HEA orderHeader = EFHelper.ctx.F_CAB_ORDER_HEA.Single(x => x.ORHE_SEQUENCE == id);
            string toAdressStr = "";
            string fromAdressStr = "";
            if (String.IsNullOrEmpty(orderHeader.F_CAB_BUYER.F_CAB_ORGANISATION.ORGA_MAILBOX))
                toAdressStr = orderHeader.F_CAB_BUYER.F_CAB_ORGANISATION.ORGA_EMAIL;
            else
                toAdressStr = orderHeader.F_CAB_BUYER.F_CAB_ORGANISATION.ORGA_MAILBOX;

            if (String.IsNullOrEmpty(orderHeader.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_MAILBOX))
                fromAdressStr = orderHeader.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_EMAIL;
            else
                fromAdressStr = orderHeader.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_MAILBOX;

            MailMessage Email = new MailMessage();
            Email.From = new MailAddress(fromAdressStr.Split(';')[0]);
            Email.To.Add(new MailAddress(toAdressStr.Split(';')[0]));
            if (toAdressStr.Split(';').Count() > 1) {
                var toAdresses = toAdressStr.Split(';').Skip(1);
                foreach (var toadd in toAdresses) {
                    Email.To.Add(new MailAddress(toadd));
                }
            }
            Email.Subject = "CLOCKT;";

            var ektContent = OrderProvider.CreateEKT(id);
            Byte[] b = System.Text.Encoding.ASCII.GetBytes(ektContent);
            MemoryStream ms = new MemoryStream(b);

            Attachment att = new Attachment(ms, new System.Net.Mime.ContentType("Application/EDIFACT"));
            System.Net.Mime.ContentDisposition cd = att.ContentDisposition;
            cd.FileName = orderHeader.ORHE_NO + DateTime.Now.ToString("_yyyyMMdd_hhmm") + ".txt";
            att.TransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;

            Email.Attachments.Add(att);
            SmtpClient SMTP = new SmtpClient();
            SMTP.Host = smtpServer;
            try {
                if (!String.IsNullOrEmpty(mailOverride)) {
                    Email.From = new MailAddress(mailOverride);
                    Email.To.Clear();
                    Email.To.Add(new MailAddress(mailOverride));
                }
                Email.From = new MailAddress(logifloraFromAddress, orderHeader.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_NAME);
                SMTP.Send(Email);
                if (!String.IsNullOrEmpty(mailOverride))
                    Log.Info(string.Format("EKT (MailOverride: {0}). From: {1}, To: {2}\n\r{3}", mailOverride, fromAdressStr, toAdressStr, ektContent));
                else
                    Log.Info(string.Format("EKT From: {0}, To: {1}\n\r{2}", fromAdressStr, toAdressStr, ektContent));

                //orderHeader.ORHE_FK_ORDER_STATUS = orderHeader.ORHE_FK_ORDER_STATUS == OrderHeaderStatus.KWBSENT ? OrderHeaderStatus.EKTKWBSENT : OrderHeaderStatus.EKTSENT; 
                EFHelper.ctx.SaveChanges();
                // EKT Sent
                OrderProvider.UpdateDetailsEKTSent(id);

                return "1";
            } catch (Exception ex) {
                Log.WarnFormat("Failed sending EKT: OrderHeader: {0}, Error: {1}", id, ex.ToString());
                return "-1";
            }
        }
        public string CheckAccess() {
            return String.Format("CheckAccess: {0}", Request.Headers.FirstOrDefault().ToString());
        }


    }
}
