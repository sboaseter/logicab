﻿using CAB.Data.EF.Models;
using CAB.Domain.Enums;
using CAB.Domain.Providers;
using CAB.Domain.Repositories;
using log4net;
using LogiCAB.Web.ViewModels;
using LogiCAB.Web.DAL;
using LogiCAB.Web.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Web.Http.Cors;
using DevExpress.Web.Mvc;
using CAB.Domain.Models;
using System.IO;
using DevExpress.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data.Entity.Core.Objects;
using Dapper;
using CAB.Domain.Helpers;

namespace LogiCAB.Web.Controllers {
    [AuthorizeWithSessionAttribute]
    public class OrganisationController : BaseController {
        private IOrganisationRepository _organisationRepository;
        private IFunctionsProvider _functionsProvider;
        public static List<Function> Functions;
        public OrganisationController(IOrganisationRepository organisationRepository, IFunctionsProvider functionsProvider, ILog log) : base(log) {
            _organisationRepository = organisationRepository;
            _functionsProvider = functionsProvider;
            if (Functions == null)
                Functions = _functionsProvider.List();
        }
        public bool RefreshOrganisations() {
            try {
                _organisationRepository.FillCache();
            }catch(Exception ex) {
                return false;
            }
            return true;
        }
        public ActionResult Index() {
            var model = _organisationRepository.List(true);
            var test = model.FirstOrDefault();
            var functions = _functionsProvider.List();
            ViewBag.Functions = functions;
            return View(model);
        }
        public ActionResult List() {
            var model = _organisationRepository.List(true);
            return PartialView(model);
        }

        public ActionResult OrganisationUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] Organisation model) {
            if (ModelState.IsValid) {
                try {                    
                    var orga = EFHelper.ctx.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == model.Sequence);
                    orga.ORGA_PHONE_NO = model.Phone ?? string.Empty;
                    orga.ORGA_EMAIL = model.Email ?? string.Empty;
                    orga.ORGA_VAT_NUMBER = model.VAT ?? string.Empty;
                    orga.ORGA_MAILBOX = model.Mailbox ?? string.Empty;
                    orga.ORGA_BVH_NUMBER = model.BVH ?? string.Empty;
                    orga.ORGA_EAN_CODE = model.EAN ?? string.Empty;
                    orga.ORGA_CODE = model.Code ?? string.Empty;
                    orga.ORGA_VBA_NUMBER = model.VBA ?? string.Empty;
                    EFHelper.ctx.SaveChanges();
                    _organisationRepository.Refresh(model.Sequence);
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            return PartialView("List", _organisationRepository.List(true));
        }

        [HttpPost]
        public ActionResult UpdateORAD(string type, string value, int orad) {
            return Json(_organisationRepository.UpdateOrad(orad, type, value));
        }

        public ActionResult CreateOrganisation() {
            return PartialView();
        }

        public ActionResult OrgaMaingroupPartial() {
            int orgaSeq = (int)System.Web.HttpContext.Current.Session["currentOrga"];
            var orgaMaingroups = EFHelper.ctx.F_CAB_ORGA_MAIN_GROUP_MATRIX.Where(x => x.ORMA_FK_ORGA == orgaSeq).ToList();
            return PartialView(orgaMaingroups);
        }

        public ActionResult OrgaMaingroupPartialAdd([ModelBinder(typeof(DevExpressEditorsBinder))] F_CAB_ORGA_MAIN_GROUP_MATRIX model) {
            int orgaSeq = (int)System.Web.HttpContext.Current.Session["currentOrga"];
            if (ModelState.IsValid) {
                try {
                    // Check for existing connection
                    var exists = EFHelper.ctx.F_CAB_ORGA_MAIN_GROUP_MATRIX.Any(x => x.ORMA_FK_ORGA == orgaSeq && x.ORMA_FK_MAIN_GROUP == model.ORMA_FK_MAIN_GROUP);
                    if (!exists) {
                        F_CAB_ORGA_MAIN_GROUP_MATRIX newOrma = new F_CAB_ORGA_MAIN_GROUP_MATRIX {
                            ORMA_FK_ORGA = orgaSeq,
                            ORMA_FK_MAIN_GROUP = model.ORMA_FK_MAIN_GROUP,
                            ORMA_ORDER = model.ORMA_ORDER
                        };
                        EFHelper.ctx.F_CAB_ORGA_MAIN_GROUP_MATRIX.AddObject(newOrma);
                        EFHelper.ctx.SaveChanges();
                    }
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            var orgaMaingroups = EFHelper.ctx.F_CAB_ORGA_MAIN_GROUP_MATRIX.Where(x => x.ORMA_FK_ORGA == orgaSeq).ToList();
            return PartialView("OrgaMaingroupPartial", orgaMaingroups);
        }

        public ActionResult OrgaMaingroupPartialDelete([ModelBinder(typeof(DevExpressEditorsBinder))] F_CAB_ORGA_MAIN_GROUP_MATRIX model) {
            int orgaSeq = (int)System.Web.HttpContext.Current.Session["currentOrga"];
            if (ModelState.IsValid) {
                try {
                    var orma = EFHelper.ctx.F_CAB_ORGA_MAIN_GROUP_MATRIX.FirstOrDefault(x => x.ORMA_SEQUENCE == model.ORMA_SEQUENCE);
                    EFHelper.ctx.F_CAB_ORGA_MAIN_GROUP_MATRIX.DeleteObject(orma);
                    EFHelper.ctx.SaveChanges();                    
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            var orgaMaingroups = EFHelper.ctx.F_CAB_ORGA_MAIN_GROUP_MATRIX.Where(x => x.ORMA_FK_ORGA == orgaSeq).ToList();
            return PartialView("OrgaMaingroupPartial", orgaMaingroups);
        }


        public ActionResult AddressTemplatePartial(int id) {
            var orga = _organisationRepository.Get(id);
            return PartialView(orga.Addresses);
        }

        public ActionResult AddressTemplatePartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] CAB.Domain.Models.Address model) {
            int id = (int)Session["currentOrga"];
            if (ModelState.IsValid) {
                try {
                    var address = EFHelper.ctx.F_CAB_ADDRESS.FirstOrDefault(x => x.ADDR_SEQUENCE == model.Sequence);
                    address.ADDR_CITY = model.City;
                    address.ADDR_ZIPCODE = model.Zipcode;
                    address.ADDR_STREET = model.Street;
                    address.ADDR_NO = model.StreetNr;
                    EFHelper.ctx.SaveChanges();
                    _organisationRepository.Refresh(id);

                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            var orga = _organisationRepository.Get(id);
            return PartialView("AddressTemplatePartial",orga.Addresses);
        }

        public ActionResult CallbacksImageUpload() {
            int id = (int)System.Web.HttpContext.Current.Session["currentOrga"];
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ucCallbacks", null, ucCallbacks_FileUploadComplete);
            UploadedFile file = files[0];
            MemoryStream ms = new MemoryStream(file.FileBytes);
            Image tempImg = Image.FromStream(ms);
            Image newImage = ImageUtils.resizeImage(tempImg, new Size { Height = 1024, Width = 1024 });
            MemoryStream ms2 = new MemoryStream();
            newImage.Save(ms2, ImageFormat.Jpeg);
            F_CAB_PICTURE newPic = new F_CAB_PICTURE {
                PICT_PICTURE = ms2.ToArray()
            };
            EFHelper.ctx.F_CAB_PICTURE.AddObject(newPic);
            EFHelper.ctx.SaveChanges();
            EFHelper.ctx.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == id).ORGA_FK_PICTURE = newPic.PICT_SEQUENCE;
            _organisationRepository.Refresh(id);
            EFHelper.ctx.SaveChanges();
            return null;
        }

        public static void ucCallbacks_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
            int newPic = -1;
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                newPic = conn.Query<int>("select top 1 PICT_SEQUENCE FROM F_CAB_PICTURE ORDER BY 1 DESC").First();
            }
            newPic++;
            e.CallbackData = newPic.ToString();
        }

        public ActionResult OrgaModulesPartial(int id) {
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                var ret = conn.Query<OrganisationFunction>(@"
select
ORFU_SEQUENCE AS Sequence,
ORFU_FK_FUNC as [FunctionSequence],
FUNC_CODE as [Code],
FUNC_DESCRIPTION as [Description]
from F_CAB_ORGANISATION_FUNCTION
join F_CAB_FUNCTION on orfu_fk_func = func_Sequence
where ORFU_FK_ORGA = @Id and  CHARINDEX('_MODULE',FUNC_CODE) > 0
", new { Id = id }).ToList();
                return PartialView(ret);
            }
            
        }


        public ActionResult OrgaContactPersonsPartial(int id) {
            var cps = _getOrgaContactPerson(id);
            return PartialView(cps);            
        }
        public ActionResult OrgaContactPersonsPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] ContactPerson model) {
            if (ModelState.IsValid) {
                try {
                    var person = EFHelper.ctx.F_CAB_PERSON.FirstOrDefault(x => x.PERS_SEQUENCE == model.Sequence);
                    var login = EFHelper.ctx.F_CAB_LOGIN.FirstOrDefault(x => x.LOGI_FK_PERSON == model.Sequence);

                    person.PERS_FIRST_NAME = model.FirstName;
                    person.PERS_LAST_NAME = model.LastName;
                    person.PERS_MIDDLE_NAME = model.MiddleName ?? string.Empty;
                    person.PERS_PHONE_NO = model.PhoneNo;
                    person.PERS_EMAIL = model.Email;

                    login.LOGI_USER_NAME = model.Login;
                    login.LOGI_PASSWORD = CreateMD5Hash("This is my password: " + model.LoginPass);
                    login.LOGI_PASS = model.LoginPass;
                    login.LOGI_BLOCKED_YN = model.LoginBlocked;

                    EFHelper.ctx.SaveChanges();
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            var orgaSequence = EFHelper.ctx.F_CAB_PERSON.FirstOrDefault(x => x.PERS_SEQUENCE == model.Sequence).PERS_FK_ORGANISATION;
            return PartialView("OrgaContactPersonsPartial", _getOrgaContactPerson(orgaSequence));
        }

        private List<ContactPerson> _getOrgaContactPerson(int id) {
            var cps = (from o in EFHelper.ctx.F_CAB_ORGANISATION
                       join p in EFHelper.ctx.F_CAB_PERSON on o.ORGA_SEQUENCE equals p.PERS_FK_ORGANISATION
                       join l in EFHelper.ctx.F_CAB_LOGIN on p.PERS_SEQUENCE equals l.LOGI_FK_PERSON into loginJoin
                       from lObj in loginJoin.DefaultIfEmpty()
                       where o.ORGA_SEQUENCE == id
                       select new ContactPerson {
                           Sequence = p.PERS_SEQUENCE,
                           LastName = p.PERS_LAST_NAME,
                           MiddleName = p.PERS_MIDDLE_NAME,
                           FirstName = p.PERS_FIRST_NAME,
                           PhoneNo = p.PERS_PHONE_NO,
                           Email = p.PERS_EMAIL,
                           Login = lObj != null ? lObj.LOGI_USER_NAME : string.Empty,
                           LoginPass = lObj != null ? lObj.LOGI_PASS : string.Empty,
                           LoginBlocked = lObj != null ? lObj.LOGI_BLOCKED_YN : (short)0
                       }).ToList();
            return cps;
        }

        public bool CheckLoginName(string loginName) {
            return EFHelper.ctx.F_CAB_LOGIN.Any(x => x.LOGI_USER_NAME.ToLower() == loginName.ToLower());
        }


        [HttpPost]
        [AllowAnonymous]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public JsonResult CreateNewLoginOnly(string pers, string logiUsername, string logiPassword, string logiLanguage) {

            var persSeq = Int32.Parse(pers);
            F_CAB_PERSON newPerson = EFHelper.ctx.F_CAB_PERSON.FirstOrDefault(x => x.PERS_SEQUENCE == persSeq);
            LoginModel Login = new LoginModel() {
                Name = logiUsername,
                Password = logiPassword,
                Language = Int32.Parse(logiLanguage)
            };
            var newLogin = CreateLogin(Login.Name, Login.Password, Login.Language.ToString(), newPerson);
            return Json(
                new {
                    Login = String.Format("{0} / {1}", newLogin.LOGI_USER_NAME, newLogin.LOGI_PASS)
                },
                JsonRequestBehavior.AllowGet
            );
        }

        [HttpPost]
        [AllowAnonymous]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public JsonResult CreateNewLogin(string orga, string persFirstName, string persMiddleName, string persLastName, string persEmail, string persPhone, string logiUsername, string logiPassword, string logiLanguage) {

            var orgaSeq = Int32.Parse(orga);            
            F_CAB_ORGANISATION orgaObj = EFHelper.ctx.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == orgaSeq);
                PersonModel Person = new PersonModel() {
                    FirstName = persFirstName,
                    MiddleName = persMiddleName,
                    LastName = persLastName,
                    Email = persEmail,
                    Phone = persPhone
                };
                LoginModel Login = new LoginModel() {
                    Name = logiUsername,
                    Password = logiPassword,
                    Language = Int32.Parse(logiLanguage)
                };

            var newPerson = CreatePerson(Person, orgaObj);
            var newLogin = CreateLogin(Login.Name, Login.Password, Login.Language.ToString(), newPerson);
            return Json(
                new {
                    Person = String.Format("{0} {1}", newPerson.PERS_FIRST_NAME, newPerson.PERS_LAST_NAME),
                    Login = String.Format("{0} / {1}", newLogin.LOGI_USER_NAME, newLogin.LOGI_PASS)
                },
                JsonRequestBehavior.AllowGet
            );
        }

        [HttpPost]
        [AllowAnonymous]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public JsonResult CreateNewOrganisation(string orgaName, string orgaEmail, string orgaType, string orgaMaingroups, string orgaFunctions, string orgaSkin, string addrStreet, string addrNr, string addrZip, string addrCity, string persName, string persEmail, string logiUsername, string logiPassword, string logiLanguage) {
            OrganisationModel orga = new OrganisationModel {
                Name = orgaName,
                Email = orgaEmail,
                Type = orgaType,
                Maingroups = orgaMaingroups.Split(',').Select(x => Int32.Parse(x)).ToList(),
                Functions = orgaFunctions.Split(',').Select(x => Int32.Parse(x)).ToList(),
                Skin = 1,
                Address = new CAB.Domain.Models.Address() {
                    Street = addrStreet,
                    StreetNr = addrNr,
                    Zipcode = addrZip,
                    City = addrCity
                },
                Person = new PersonModel() {
                    FirstName = persName,
                    MiddleName = string.Empty,
                    LastName = string.Empty,
                    Email = persEmail
                },
                Login = new LoginModel() {
                    Name = logiUsername,
                    Password = logiPassword,
                    Language = Int32.Parse(logiLanguage)
                }
            };
            var newOrga = CreateOrganisation(orga.Name, orga.Email, orga.Type, orga.Skin);
            var newSpon = CreateShopOrganisation(newOrga, 1);
            var newOrma = CreateOrgaMainGroupMatrix(newOrga, orga.Maingroups);
            var newOrgaInfo = CreateOrganisationInfo(newOrga);
            var newOrgaAdmin = CreateOrganisationAdmin(newOrga);
            var newOradArray = CreateOrganisationFunctions(newOrga, orga.Functions);
            var newAddress = CreateAddress(newOrga, orga.Address.Street, orga.Address.StreetNr, orga.Address.Zipcode, orga.Address.City);
            if (newOrga.ORGA_TYPE == "BUYR") {
                var newBuyer = CreateBuyer(newOrga);
                // Grower setup
            } else if (newOrga.ORGA_TYPE == "GROW") {
                var newGrower = CreateGrower(newOrga);
                // Comm setup
            } else if (newOrga.ORGA_TYPE == "COMM") {
                var newBuyer = CreateBuyer(newOrga);
                var newGrower = CreateGrower(newOrga);
            } else if (newOrga.ORGA_TYPE == "LOGP") {
                var newGrower = CreateGrower(newOrga);
            }
            var newPerson = CreatePerson(orga.Person, newOrga);
            var newLogin = CreateLogin(orga.Login.Name, orga.Login.Password, orga.Login.Language.ToString(), newPerson);
            var newHandshake = CreateHandshake(newOrga);
            var newHandshakeOption = CreateHandshakeOption(newHandshake);
            var newGama = CreateGrowerBuyerMatrix(newHandshake);
            _organisationRepository.Get(newOrga.ORGA_SEQUENCE); // Refreshing cache.
            return Json(
                new {
                    Organisation = String.Format("{0}: {1}",newOrga.ORGA_NAME, newOrga.ORGA_TYPE),
                    Person = String.Format("{0} {1}", newPerson.PERS_FIRST_NAME, newPerson.PERS_LAST_NAME),
                    Login = String.Format("{0} / {1}", newLogin.LOGI_USER_NAME, newLogin.LOGI_PASS)
                }, 
                JsonRequestBehavior.AllowGet
            );
        }

        public ActionResult Delete(int Sequence) {
            Log.InfoFormat("DeleteOrganisation: {0}", Sequence);
            var param1 = new SqlParameter("ORGA_SEQ", SqlDbType.Int);
            param1.Value = Sequence;
            EFHelper.ctx.ExecuteStoreCommand("exec proc_deleteOrganisation @ORGA_SEQ", param1);
            _organisationRepository.DeleteFromCache(Sequence);
            var model = _organisationRepository.List(true);
            return PartialView("List", model);
        }
        #region Create/Delete Organisation CRUD-Methods
        public F_CAB_ORGANISATION CreateOrganisation(string name, string email, string type, int skin) {
            F_CAB_ORGANISATION newOrga = new F_CAB_ORGANISATION();
            newOrga.ORGA_CREATED_ON = DateTime.Now;
            newOrga.ORGA_CREATED_BY = "maual";
            newOrga.ORGA_MODIFIED_ON = DateTime.Now;
            newOrga.ORGA_MODIFIED_BY = "manual";
            newOrga.ORGA_FK_SHOP_SKIN = skin;
            newOrga.ORGA_REFER = null;
            newOrga.ORGA_FK_PICTURE = null;
            newOrga.ORGA_CODE = "12345678";
            newOrga.ORGA_END_DATE = DateTime.Now.AddYears(100);
            newOrga.ORGA_START_DATE = DateTime.Now;
            newOrga.ORGA_EAN_CODE = "12345678";
            newOrga.ORGA_BVH_NUMBER = "12345678";
            newOrga.ORGA_TYPE = type;
            newOrga.ORGA_VBA_NUMBER = "";
            newOrga.ORGA_MAILBOX = email;
            newOrga.ORGA_VAT_NUMBER = "12345678";
            newOrga.ORGA_EMAIL_2 = "";
            newOrga.ORGA_EMAIL = email;
            newOrga.ORGA_FAX_NO = "";
            newOrga.ORGA_PHONE_NO = "87654321";
            newOrga.ORGA_NAME = name;
            newOrga.ORGA_NAME_UPPER = name.ToUpper();
            EFHelper.ctx.F_CAB_ORGANISATION.AddObject(newOrga);
            EFHelper.ctx.SaveChanges();
            return newOrga;
        }
        public F_CAB_SHOP_ORGANISATION CreateShopOrganisation(F_CAB_ORGANISATION orga, int shop) {
            F_CAB_SHOP_ORGANISATION newSpon = new F_CAB_SHOP_ORGANISATION();
            newSpon.SPON_FK_SHOP = shop;
            newSpon.SPON_FK_ORGA = orga.ORGA_SEQUENCE;
            EFHelper.ctx.F_CAB_SHOP_ORGANISATION.AddObject(newSpon);
            EFHelper.ctx.SaveChanges();
            return newSpon;
        }
        public List<F_CAB_ORGA_MAIN_GROUP_MATRIX> CreateOrgaMainGroupMatrix(F_CAB_ORGANISATION orga, List<int> maingroups) {
            int i = 1;
            foreach (var mg in maingroups) {
                F_CAB_ORGA_MAIN_GROUP_MATRIX newOrma = new F_CAB_ORGA_MAIN_GROUP_MATRIX() {
                    ORMA_FK_ORGA = orga.ORGA_SEQUENCE,
                    ORMA_FK_MAIN_GROUP = mg,
                    ORMA_ORDER = i
                };
                EFHelper.ctx.F_CAB_ORGA_MAIN_GROUP_MATRIX.AddObject(newOrma);
                i++;
            }
            EFHelper.ctx.SaveChanges();
            return EFHelper.ctx.F_CAB_ORGA_MAIN_GROUP_MATRIX.Where(x => x.ORMA_FK_ORGA == orga.ORGA_SEQUENCE).ToList();
        }
        public F_CAB_ORGANISATION_INFO CreateOrganisationInfo(F_CAB_ORGANISATION orga) {
            F_CAB_ORGANISATION_INFO newOrgi = new F_CAB_ORGANISATION_INFO();
            newOrgi.ORGI_FK_ORGA_SEQ = orga.ORGA_SEQUENCE;
            newOrgi.ORGI_FK_PERS_BUYER_SEQ = null;
            newOrgi.ORGI_FK_CDCU_SEQ = null;
            newOrgi.ORGI_SHIPPING_DAYS = "1";
            newOrgi.ORGI_ORDER_DELIVER_DAYS = 1;
            newOrgi.ORGI_OFFER_DELIVER_DAYS = 1;
            newOrgi.ORGI_ORDER_OFFSET_DAYS = 0;
            newOrgi.ORGI_OFFER_OFFSET_DAYS = 0;
            newOrgi.ORGI_DEADLINE = TimeSpan.Parse("06:00:00");
            newOrgi.ORGI_DEADLINE_OFFER = TimeSpan.Parse("06:00:00.0000000");
            newOrgi.ORGI_CONTACT_NAME = "";
            newOrgi.ORGI_CONTACT_PHONE_NO = "";
            newOrgi.ORGI_CONTACT_FAX_NO = "";
            newOrgi.ORGI_CONTACT_EMAIL = "";
            newOrgi.ORGI_LATEST_DELIVERY_DATE_TIME = DateTime.Parse("2011-12-07 12:00:00.000");
            newOrgi.ORGI_ORDER_NAME = "";
            newOrgi.ORGI_CREATED_ON = DateTime.Now;
            newOrgi.ORGI_CREATED_BY = "sa";
            newOrgi.ORGI_MODIFIED_ON = DateTime.Now;
            newOrgi.ORGI_MODIFIED_BY = "sa";
            EFHelper.ctx.F_CAB_ORGANISATION_INFO.AddObject(newOrgi);
            EFHelper.ctx.SaveChanges();
            return newOrgi;
        }
        public F_CAB_ORGANISATION_ADMIN CreateOrganisationAdmin(F_CAB_ORGANISATION orga) {
            F_CAB_ORGANISATION_ADMIN newOrad = new F_CAB_ORGANISATION_ADMIN();
            newOrad.ORAD_FK_ORGANISATION = orga.ORGA_SEQUENCE;
            newOrad.ORAD_STATUS = 3;
            newOrad.ORAD_MEMBERTYPE = 3;
            newOrad.ORAD_DATE = DateTime.Now;
            EFHelper.ctx.F_CAB_ORGANISATION_ADMIN.AddObject(newOrad);
            EFHelper.ctx.SaveChanges();
            return newOrad;
        }
        public List<F_CAB_ORGANISATION_FUNCTION> CreateOrganisationFunctions(F_CAB_ORGANISATION orga, List<int> f) {
            foreach (var func in f) {
                F_CAB_ORGANISATION_FUNCTION newOrfu = new F_CAB_ORGANISATION_FUNCTION() {
                    ORFU_FK_ORGA = orga.ORGA_SEQUENCE,
                    ORFU_FK_FUNC = func
                };
                EFHelper.ctx.F_CAB_ORGANISATION_FUNCTION.AddObject(newOrfu);
            }
            EFHelper.ctx.SaveChanges();
            return EFHelper.ctx.F_CAB_ORGANISATION_FUNCTION.Where(x => x.ORFU_FK_ORGA == orga.ORGA_SEQUENCE).ToList();
        }
        public F_CAB_ADDRESS CreateAddress(F_CAB_ORGANISATION orga, string street, string streetnr, string zipcode, string city) {
            F_CAB_ADDRESS newAddr = new F_CAB_ADDRESS();
            newAddr.ADDR_FK_ORGANISATION = orga.ORGA_SEQUENCE;
            newAddr.ADDR_FK_CDCO_SEQ = null;
            newAddr.ADDR_FK_CDAT_SEQ = 1;
            newAddr.ADDR_STREET = street;
            newAddr.ADDR_NO = streetnr;
            newAddr.ADDR_NO_SUFFIX = "";
            newAddr.ADDR_ZIPCODE = zipcode;
            newAddr.ADDR_CITY = city;
            newAddr.ADDR_GLN_LOCATION_CODE = "";
            newAddr.ADDR_LOCATION_DESCRIPTION = "";
            newAddr.ADDR_ENTRY_DATE_FLORECOM = null;
            newAddr.ADDR_EXPIRE_DATE_FLORECOM = null;
            newAddr.ADDR_CHANGE_DATE_FLORECOM = null;
            newAddr.ADDR_COMPANY_ENTRY_DATE_FLORECOM = null;
            newAddr.ADDR_GPS_LATITUDE_FLORECOM = 0;
            newAddr.ADDR_GPS_LONGTITUDE_FLORECOM = 0;
            newAddr.ADDR_AUTO_SYNC_FLORECOM = false;
            newAddr.ADDR_LOCATION_PRIORITY = 0;
            newAddr.ADDR_CREATED_BY = "sa";
            newAddr.ADDR_CREATED_ON = DateTime.Now;
            newAddr.ADDR_MODIFIED_BY = "sa";
            newAddr.ADDR_MODIFIED_ON = DateTime.Now;

            EFHelper.ctx.F_CAB_ADDRESS.AddObject(newAddr);
            EFHelper.ctx.SaveChanges();
            return newAddr;
        }
        public F_CAB_BUYER CreateBuyer(F_CAB_ORGANISATION orga) {
            F_CAB_BUYER newBuyer = new F_CAB_BUYER();
            newBuyer.BUYR_CREATED_ON = DateTime.Now;
            newBuyer.BUYR_CREATED_BY = "manual";
            newBuyer.BUYR_MODIFIED_ON = DateTime.Now;
            newBuyer.BUYR_MODIFIED_BY = "manual";
            newBuyer.BUYR_TRANSPORT_TYPE = null;
            newBuyer.BUYR_DEMO_YN = true;
            newBuyer.BUYR_MPS_GAP = false;
            newBuyer.BUYR_FLORIMARK_YN = null;
            newBuyer.BUYR_ISO_YN = false;
            newBuyer.BUYR_MPS = 0;
            newBuyer.BUYR_MOBILE_NO = "87654321";
            newBuyer.BUYR_FK_ORGA = orga.ORGA_SEQUENCE;
            //F_CAB_BUYER.InsertOnSubmit(newBuyer);
            EFHelper.ctx.F_CAB_BUYER.AddObject(newBuyer);
            EFHelper.ctx.SaveChanges();
            //SubmitChanges();
            return newBuyer;
        }
        public F_CAB_GROWER CreateGrower(F_CAB_ORGANISATION orga) {
            F_CAB_GROWER newGrower = new F_CAB_GROWER();
            newGrower.GROW_CREATED_ON = DateTime.Now;
            newGrower.GROW_CREATED_BY = "manual";
            newGrower.GROW_MODIFIED_ON = DateTime.Now;
            newGrower.GROW_MODIFIED_BY = "manual";
            newGrower.GROW_FK_ORGA = orga.ORGA_SEQUENCE;
            newGrower.GROW_NUM_OF_M2 = 0;
            newGrower.GROW_FK_BULK_CASK = 385;
            newGrower.GROW_MOBILE_NO = "";
            newGrower.GROW_MPS = 0;
            newGrower.GROW_ISO_YN = 0;
            newGrower.GROW_FLORIMARK_YN = 0;
            newGrower.GROW_MPS_GAP = null;
            EFHelper.ctx.F_CAB_GROWER.AddObject(newGrower);
            EFHelper.ctx.SaveChanges();
            return newGrower;
        }
        public F_CAB_PERSON CreatePerson(PersonModel person, F_CAB_ORGANISATION orga) {
            F_CAB_PERSON newPerson = new F_CAB_PERSON();
            newPerson.PERS_CREATED_ON = DateTime.Now;
            newPerson.PERS_CREATED_BY = "maual";
            newPerson.PERS_MODIFIED_ON = DateTime.Now;
            newPerson.PERS_MODIFIED_BY = "manual";
            newPerson.PERS_REF_EKT = "";
            newPerson.PERS_EXT_REF = null;
            newPerson.PERS_EMAIL = person.Email;
            newPerson.PERS_FIRST_NAME = person.FirstName;
            newPerson.PERS_MIDDLE_NAME = person.MiddleName;
            newPerson.PERS_LAST_NAME = person.LastName ?? string.Empty;
            newPerson.PERS_PHONE_NO = person.Phone ?? string.Empty;
            newPerson.PERS_FK_ORGANISATION = orga.ORGA_SEQUENCE;
            EFHelper.ctx.F_CAB_PERSON.AddObject(newPerson);
            EFHelper.ctx.SaveChanges();
            return newPerson;
        }
        public F_CAB_LOGIN CreateLogin(string username, string password, string language, F_CAB_PERSON person) {
            int langSeq = Int32.Parse(language);
            F_CAB_LOGIN newLogin = new F_CAB_LOGIN();
            newLogin.LOGI_CREATED_ON = DateTime.Now;
            newLogin.LOGI_CREATED_BY = "manual";
            newLogin.LOGI_MODIFIED_ON = DateTime.Now;
            newLogin.LOGI_MODIFIED_BY = "manual";
            newLogin.LOGI_LAST_LOGIN = null;
//            int languageSequence = EFHelper.ctx.F_CAB_CD_LANGUAGE.FirstOrDefault(x => x.CDLA_SEQUENCE == ).CDLA_SEQUENCE;
            newLogin.LOGI_FK_CDLA_SEQ = langSeq;
            newLogin.LOGI_PASS = password;
            newLogin.LOGI_BLOCKED_YN = 0;
            newLogin.LOGI_FK_LOGIN_GROUP = 2;
            newLogin.LOGI_FK_PERSON = person.PERS_SEQUENCE;
            newLogin.LOGI_PASSWORD = CreateMD5Hash("This is my password: " + password);
            newLogin.LOGI_USER_NAME = username;
            EFHelper.ctx.F_CAB_LOGIN.AddObject(newLogin);
            EFHelper.ctx.SaveChanges();
            return newLogin;
        }
        public string CreateMD5Hash(string input) {
            byte[] hashBytes = MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(input));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++) {
                sb.Append(hashBytes[i].ToString("x2"));
            }
            return sb.ToString();
        }
        public F_CAB_HANDSHAKE CreateHandshake(F_CAB_ORGANISATION orga) {
            F_CAB_HANDSHAKE newHand = new F_CAB_HANDSHAKE();
            if (orga.ORGA_TYPE == OrgaType.Grower) {
                newHand.HAND_FK_ORGA_SEQ_GROWER = orga.ORGA_SEQUENCE;
                newHand.HAND_FK_ORGA_SEQ_BUYER = 1411; // koper1
            } else {
                newHand.HAND_FK_ORGA_SEQ_GROWER = 1155; // kweker
                newHand.HAND_FK_ORGA_SEQ_BUYER = orga.ORGA_SEQUENCE;
            }
            newHand.HAND_FK_HATY_SEQ = 1;
            newHand.HAND_REQUEST_DATE = DateTime.Now;
            newHand.HAND_ACCEPT_DATE = DateTime.Now;
            newHand.HAND_APPROVE_DATE = DateTime.Now;
            newHand.HAND_BLOCKED_YN = false;
            newHand.HAND_REQUEST_MAIL = null;
            newHand.HAND_REQUEST_REMARK = "";
            newHand.HAND_CREATED_ON = DateTime.Now;
            newHand.HAND_CREATED_BY = "manual";
            EFHelper.ctx.F_CAB_HANDSHAKE.AddObject(newHand);
            EFHelper.ctx.SaveChanges();
            return newHand;
        }
        public F_CAB_HANDSHAKE_OPTIONS CreateHandshakeOption(F_CAB_HANDSHAKE hand) {
            F_CAB_HANDSHAKE_OPTIONS newHndo = new F_CAB_HANDSHAKE_OPTIONS();
            newHndo.HNDO_FK_HAND_SEQ = hand.HAND_SEQUENCE;
            newHndo.HNDO_FK_ADDR_SEQ = null;
            newHndo.HNDO_FK_CDPM_SEQ = null;
            newHndo.HNDO_FK_DMHD_SEQ = null;
            newHndo.HNDO_FK_ORGA_SEQ = null;
            newHndo.HNDO_AUTO_CONFIRM_YN = false;
            newHndo.HNDO_AUTO_EKT_YN = false;
            newHndo.HNDO_AUTO_KWB_YN = false;
            newHndo.HNDO_SEND_CONFIRM_MAIL_YN = true;
            newHndo.HNDO_RECV_OFFER_MAIL_YN = true;
            newHndo.HNDO_AUTO_PUBLISH_STOCK_YN = true;
            newHndo.HNDO_AUTO_PUBLISH_OFFER_YN = true;
            newHndo.HNDO_LATEST_DELIVERY_TIME = DateTime.Parse("2012-08-07 12:00:00.000");
            newHndo.HNDO_DELIVERY_TYPE = "";
            newHndo.HNDO_DELIVERY_STOCK_ALL_WORKDAYS_YN = true;
            newHndo.HNDO_DELIVERY_OFFER_ALL_WORKDAYS_YN = false;
            newHndo.HNDO_CREATED_ON = DateTime.Now;
            newHndo.HNDO_CREATED_BY = "LogiCAB:OrganisationAPI";
            newHndo.HNDO_MODIFIED_ON = DateTime.Now;
            newHndo.HNDO_MODIFIED_BY = "LogiCAB:OrganisationAPI";
            EFHelper.ctx.F_CAB_HANDSHAKE_OPTIONS.AddObject(newHndo);
//            F_CAB_HANDSHAKE_OPTIONS.InsertOnSubmit(newHndo);
  //          SubmitChanges();
            EFHelper.ctx.SaveChanges();
            return newHndo;
        }
        public F_CAB_GROWER_BUYER_MATRIX CreateGrowerBuyerMatrix(F_CAB_HANDSHAKE hand) {
            // Retrieve Buyer & Grower sequences based on handshake:
            var buyer = EFHelper.ctx.F_CAB_BUYER.FirstOrDefault(x => x.BUYR_FK_ORGA == hand.HAND_FK_ORGA_SEQ_BUYER);
            var grower = EFHelper.ctx.F_CAB_GROWER.FirstOrDefault(x => x.GROW_FK_ORGA == hand.HAND_FK_ORGA_SEQ_GROWER);
            if (buyer == null || grower == null) {
                Log.Debug("Failed to aquire Grower/Buyer-sequence from Handshake");
                return null;
            }
            F_CAB_GROWER_BUYER_MATRIX newGama = new F_CAB_GROWER_BUYER_MATRIX();
            newGama.GAMA_FK_BUYER = buyer.BUYR_SEQUENCE;
            newGama.GAMA_FK_GROWER = grower.GROW_SEQUENCE;
            newGama.GAMA_LAST_ORDER_DATE = null;
            newGama.GAMA_BLOCKED_YN = 0;
            //F_CAB_GROWER_BUYER_MATRIX.InsertOnSubmit(newGama);
            EFHelper.ctx.F_CAB_GROWER_BUYER_MATRIX.AddObject(newGama);
            //SubmitChanges();
            EFHelper.ctx.SaveChanges();
            return newGama;
        }
        #endregion

    }
}
