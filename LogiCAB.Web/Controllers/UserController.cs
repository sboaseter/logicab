﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Dapper;
using LogiCAB.Web.Domain;
using System.Security.Cryptography;
using System.Text;
using System.Collections;
using DevExpress.Web;
using DevExpress.Web.Mvc;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using log4net;
using CAB.Data.EF.Models;
using CAB.Domain.Repositories;
using CAB.Domain.Providers;
using CAB.Domain.Models;
using CAB.Domain.Helpers;
using CAB.Domain.Enums;
using LogiCAB.Web.DAL;
using System.Globalization;

namespace LogiCAB.Web.Controllers {
    [HandleError]
    public class UserController : BaseController {
        private ILoginRepository _loginRepository;
        private IFunctionsProvider _functionsProvider;
        private IPersonRepository _personRepository;
        private IOrganisationRepository _organisationRepository;
        private ISettingsProvider _settingsProvider;

        public UserController(
            ILoginRepository loginRepository,
            IFunctionsProvider functionsProvider,
            IPersonRepository personRepository,
            IOrganisationRepository organisationRepository,
            ISettingsProvider settingsProvider,
            ILog log) : base(log)
        {
            _loginRepository = loginRepository;
            _functionsProvider = functionsProvider;
            _personRepository = personRepository;
            _organisationRepository = organisationRepository;
            _settingsProvider = settingsProvider;
        }

        public ActionResult Login() {
            return View(new Login());
        }

        public ActionResult Index() {
            ViewBag.Orga = EFHelper.ctx.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == Usr.OrganisationSequence);
            return View();
        }

        public ActionResult Profile()
        {
            return View();
        }

        public static IEnumerable<DeliveryMomentHeader> _getDMHDList() {
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                Stopwatch sw1 = new Stopwatch();
                sw1.Start();
                conn.Open();
                var res = conn.Query<DeliveryMomentHeader>(
@"select DMHD_SEQUENCE as Sequence
,      DMHD_DESCRIPTION as Description
,      DMHD_CODE as Code
,      DMHD_FK_GROW_SEQ as Grower
from f_cab_delivery_moment_headers
where dmhd_fk_grow_seq = @Grower OR dmhd_fk_grow_seq is null", new { Grower = Usr.GrowerSequence });
                sw1.Stop();
                return res;
            }


        }

        private IEnumerable<DeliveryMomentDetail> _getDMDT(int id)
        {
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                Stopwatch sw1 = new Stopwatch();
                sw1.Start();
                conn.Open();
                var res = conn.Query<DeliveryMomentDetail>(
@"select DMDT_SEQUENCE as Sequence
,      DMDT_FK_DMHD_SEQ as Header
,      DMDT_WEEKDAY_NUMBER as Day
,      DMDT_MAX_ORDER_TIME as MaxOrder
,      DMDT_MAX_DELIVERY_TIME as MaxDelivery
,      DMDT_DELIVERY_TYPE as Type
from f_cab_delivery_moment_details
where DMDT_FK_DMHD_SEQ = @HeaderSequence", new { HeaderSequence = id });
                sw1.Stop();
                Log.InfoFormat("Getting DeliveryMomentDetails-list took: {0}ms", sw1.ElapsedMilliseconds);
                return res;
            }            
        }

        public ActionResult DeliveryMomentHeadersPartial()
        {
            return PartialView(_getDMHDList());

        }

        public ActionResult DeliveryMomentDetailsPartial()
        {
            return PartialView(_getDMDT(Int32.Parse(Usr.DeliveryHeader)));
        }

        public JsonResult getDeliveryMomentDetails(int id)
        {
            Usr.DeliveryHeader = id.ToString();
            return Json(true, JsonRequestBehavior.AllowGet);

        }

        public JsonResult getDeliveryMomentHeaders()
        {
            return Json(_getDMHDList(), JsonRequestBehavior.AllowGet);
        }
        #region DeliveryMomentHeaderCRUD
        private void _insertDeliveryMomentHeader(DeliveryMomentHeader dmh) {
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                Stopwatch sw1 = new Stopwatch();
                sw1.Start();
                conn.Open();
                string sqlQuery = @"INSERT INTO [dbo].[F_CAB_DELIVERY_MOMENT_HEADERS]
                                   ([DMHD_DESCRIPTION]
                                   ,[DMHD_CODE]
                                   ,[DMHD_FK_GROW_SEQ])
                                 VALUES
                                   (@Description
                                   ,@Code
                                   ,@Grower)";

                var res = conn.Execute(sqlQuery, dmh);

                var header = conn.Query<DeliveryMomentHeader>(
                                        @"select top 1 DMHD_SEQUENCE as Sequence
                                        ,      DMHD_DESCRIPTION as Description
                                        ,      DMHD_CODE as Code
                                        ,      DMHD_FK_GROW_SEQ as Grower
                                        from f_cab_delivery_moment_headers
                                        where dmhd_fk_grow_seq = @Grower order by DMHD_SEQUENCE desc", new { Grower = dmh.Grower });

                sqlQuery = @"INSERT INTO F_CAB_DELIVERY_MOMENT_DETAILS (
                            [DMDT_FK_DMHD_SEQ]
                            ,[DMDT_FK_OFDT_SEQ]
                            ,[DMDT_WEEKDAY_NUMBER]
                            ,[DMDT_ORDER_NUMBER]
                            ,[DMDT_WEEK_NUMBER]
                            ,[DMDT_MAX_ORDER_TIME]
                            ,[DMDT_MAX_DELIVERY_TIME]
                            ,[DMDT_DELIVERY_TYPE]
                            ,[DMDT_FK_ADDR_SEQ_DELIVERY]
                            ,[DMDT_TMP_VALID_TO]
                            ,[DMDT_TMP_WEEKDAY_NUMBER]
                            ,[DMDT_TMP_ORDER_NUMBER]
                            ,[DMDT_TMP_WEEK_NUMBER]
                            ,[DMDT_TMP_MAX_ORDER_TIME]
                            ,[DMDT_TMP_MAX_DELIVERY_TIME]
                            ,[DMDT_TMP_DELIVERY_TYPE]
                            ,[DMDT_TMP_FK_ADDR_SEQ_DELIVERY]
                            )
                            SELECT 
                             @Sequence
                            ,[DMDT_FK_OFDT_SEQ]
                            ,[DMDT_WEEKDAY_NUMBER]
                            ,[DMDT_ORDER_NUMBER]
                            ,[DMDT_WEEK_NUMBER]
                            ,[DMDT_MAX_ORDER_TIME]
                            ,[DMDT_MAX_DELIVERY_TIME]
                            ,[DMDT_DELIVERY_TYPE]
                            ,[DMDT_FK_ADDR_SEQ_DELIVERY]
                            ,[DMDT_TMP_VALID_TO]
                            ,[DMDT_TMP_WEEKDAY_NUMBER]
                            ,[DMDT_TMP_ORDER_NUMBER]
                            ,[DMDT_TMP_WEEK_NUMBER]
                            ,[DMDT_TMP_MAX_ORDER_TIME]
                            ,[DMDT_TMP_MAX_DELIVERY_TIME]
                            ,[DMDT_TMP_DELIVERY_TYPE]
                            ,[DMDT_TMP_FK_ADDR_SEQ_DELIVERY]
                            FROM F_CAB_DELIVERY_MOMENT_DETAILS
                            where DMDT_FK_DMHD_SEQ = 1";
                res = conn.Execute(sqlQuery, header.FirstOrDefault());
                sw1.Stop();
                Log.InfoFormat("Added DeliveryMomentHeader & Details took: {0}ms", sw1.ElapsedMilliseconds);
            }
        }
        private bool _deleteDeliveryMomentHeader(DeliveryMomentHeader dmh) {
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                Stopwatch sw1 = new Stopwatch();
                sw1.Start();
                conn.Open();
                string sqlQuery = @"DELETE FROM [dbo].[F_CAB_DELIVERY_MOMENT_DETAILS]
                                    WHERE DMDT_FK_DMHD_SEQ = @Sequence";
                var res = conn.Execute(sqlQuery, dmh);

                sqlQuery = @"DELETE FROM [dbo].[F_CAB_DELIVERY_MOMENT_HEADERS]
                                    WHERE DMHD_SEQUENCE = @Sequence";
                res = conn.Execute(sqlQuery, dmh);
                sw1.Stop();
                Log.InfoFormat("Deleted DeliveryMomentHeader & Details took: {0}ms", sw1.ElapsedMilliseconds);
                return true;
            }
            return false;
        }

        private bool _updateDeliveryMomentHeader(DeliveryMomentHeader dmh) {
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                Stopwatch sw1 = new Stopwatch();
                sw1.Start();
                conn.Open();
                string sqlQuery = @"UPDATE [dbo].[F_CAB_DELIVERY_MOMENT_HEADERS]
                                  SET  [DMHD_DESCRIPTION] = @Description
                                      ,[DMHD_CODE] = @Code
                                      ,[DMHD_FK_GROW_SEQ] = @Grower
                                 WHERE [DMHD_SEQUENCE] = @Sequence";
                var res = conn.Execute(sqlQuery, dmh);
                sw1.Stop();
                Log.InfoFormat("Updated DeliveryMomentHeader took: {0}ms", sw1.ElapsedMilliseconds);
                return true;
            }
            return false;
        }

        private bool _updateDeliveryMomentDetail(DeliveryMomentDetail dmd) {
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                Stopwatch sw1 = new Stopwatch();
                sw1.Start();
                conn.Open();
                string sqlQuery = @"UPDATE [dbo].[F_CAB_DELIVERY_MOMENT_DETAILS]
                                   SET [DMDT_MAX_ORDER_TIME] = @MaxOrder
                                      ,[DMDT_MAX_DELIVERY_TIME] = @MaxDelivery 
                                      ,[DMDT_DELIVERY_TYPE] = @Type
                                 WHERE [DMDT_SEQUENCE] = @Sequence";
                var res = conn.Execute(sqlQuery, dmd);
                sw1.Stop();
                Log.InfoFormat("Updated DeliveryMomentDetail took: {0}ms", sw1.ElapsedMilliseconds);
                return true;
            }
            return false;
        }

        #endregion

        [HttpPost, ValidateInput(false)]
        public ActionResult AddDeliveryMomentHeaders([ModelBinder(typeof(DevExpress.Web.Mvc.DevExpressEditorsBinder))] DeliveryMomentHeader dmh) {
            if(ModelState.IsValid) {
                try {
                    dmh.Grower = Usr.GrowerSequence;
                    dmh.Code = dmh.Description;
                    _insertDeliveryMomentHeader(dmh);
                } catch(Exception e) {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("DeliveryMomentHeadersPartial", _getDMHDList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult DeleteDeliveryMomentHeaders([ModelBinder(typeof(DevExpress.Web.Mvc.DevExpressEditorsBinder))] DeliveryMomentHeader dmh) {
            if (ModelState.IsValid) {
                try {
                    _deleteDeliveryMomentHeader(dmh);
                } catch (Exception e) {
                    ViewData["EditError"] = e.Message;
                }
            } else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("DeliveryMomentHeadersPartial", _getDMHDList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateDeliveryMomentHeaders([ModelBinder(typeof(DevExpress.Web.Mvc.DevExpressEditorsBinder))] DeliveryMomentHeader dmh) {
            if (ModelState.IsValid) {
                try {
                    _updateDeliveryMomentHeader(dmh);
                } catch (Exception e) {
                    ViewData["EditError"] = e.Message;
                }
            } else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("DeliveryMomentHeadersPartial", _getDMHDList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateDeliveryMomentDetails([ModelBinder(typeof(DevExpress.Web.Mvc.DevExpressEditorsBinder))] DeliveryMomentDetail dmd) {
            if (ModelState.IsValid) {
                try {
//                    _updateDeliveryMomentHeader(dmh);
                    _updateDeliveryMomentDetail(dmd);
                } catch (Exception e) {
                    ViewData["EditError"] = e.Message;
                }
            } else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("DeliveryMomentDetailsPartial", _getDMDT(Int32.Parse(Usr.DeliveryHeader)));
        }

        


        public ActionResult Mail() {
            return View(_getMailMatrix());
        }

        public ActionResult MailList() {
            return View(_getMailMatrix());
        }

        public ActionResult getContactPersons() {
            return PartialView("ContactPersons", _getContactPersons());
        }

        public static List<PersonLogin> _getContactPersons() {
            using (Entities cab = new Entities()) {
                return (from p in cab.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == Usr.OrganisationSequence).F_CAB_PERSON
                       join l in cab.F_CAB_LOGIN
                       on p.PERS_SEQUENCE equals l.LOGI_FK_PERSON into pl
                       from perslog in pl.DefaultIfEmpty()
                       select new PersonLogin {
                           PERS_SEQUENCE = p.PERS_SEQUENCE,
                           PERS_FIRST_NAME = p.PERS_FIRST_NAME,
                           PERS_LAST_NAME = p.PERS_LAST_NAME,
                           PERS_PHONE_NO = p.PERS_PHONE_NO,
                           PERS_EMAIL = p.PERS_EMAIL,

                           LOGI_SEQUENCE = perslog == null ? 0 : perslog.LOGI_SEQUENCE,
                           LOGI_USER_NAME = perslog == null ? String.Empty : perslog.LOGI_USER_NAME,
                           LOGI_PASS = perslog == null ? String.Empty : perslog.LOGI_PASS,
                           LOGI_FK_LOGIN_GROUP = perslog == null ? 0 : perslog.LOGI_FK_LOGIN_GROUP,
                           LOGI_BLOCKED_YN = perslog == null ? (short)0 : perslog.LOGI_BLOCKED_YN
                       }).ToList();
            }            
        }

        public ActionResult getMailMatrix() {
            return PartialView("MailList", _getMailMatrix());
        }

        private IEnumerable _getMailMatrix() {
            return (from mgpm in EFHelper.ctx.F_CAB_MAIL_GROUP_PERSON_MATRIX
                    join cdgr in EFHelper.ctx.F_CAB_CD_CAB_GROUP
                    on mgpm.MGPM_FK_CAB_GROUP equals cdgr.CDGR_SEQUENCE

                    join cdgt in EFHelper.ctx.F_CAB_CD_GROUP_TYPE
                    on cdgr.CDGR_FK_GROUP_TYPE equals cdgt.CDGT_SEQUENCE

                    join pers in EFHelper.ctx.F_CAB_PERSON
                    on mgpm.MGPM_FK_PERSON equals pers.PERS_SEQUENCE

                    where Usr.OrganisationSequence == mgpm.F_CAB_PERSON.F_CAB_ORGANISATION.ORGA_SEQUENCE

                    select new MailMatrix {
                        Sequence = mgpm.MGPM_SEQUENCE,
                        Group = cdgr.CDGR_DESC,
                        GroupType = cdgt.CDGT_DESC,
                        PersFirstName = pers.PERS_FIRST_NAME,
                        PersLastName = pers.PERS_LAST_NAME,
                        PersEmail = pers.PERS_EMAIL
                    }).ToList();
        }

        [HttpPost]
        public ActionResult deleteMailMatrix(int Sequence) {
            var mgpm = EFHelper.ctx.F_CAB_MAIL_GROUP_PERSON_MATRIX.Where(x => x.MGPM_SEQUENCE == Sequence).FirstOrDefault();
            if (mgpm != null) {
                EFHelper.ctx.F_CAB_MAIL_GROUP_PERSON_MATRIX.DeleteObject(mgpm);
                EFHelper.ctx.SaveChanges();
                return PartialView("MailList", _getMailMatrix());
            }
            return PartialView("MailList", _getMailMatrix());
        }

        private ActionResult _performLogin(Login login) {
            login = _loginRepository.Get(login.Name);
            Organisation orga = _organisationRepository.Get(login.Organisation);
            if (orga.Orad.Status == SubscriptionStatus.AFGEMELD) {
                login.error = "Abonnement staat op 'Afgemeld', helaas kunt u niet inloggen.";
                Log.InfoFormat("Unsuccessfull login attempted with status Afgemeld or Prospect: {0}", login.Name);
                return View(login);
            }
            Usr.LoginName = login.Name;
            Usr.LoginPass = login.Password;
            Usr.Language = login.Language ?? 1;
            Usr.LoginGroup = login.LoginGroup;
            Usr.Functions = _functionsProvider.Get(orga.Sequence);
//            if(Usr.Functions.Any(x => x == "GROWER_MODULE")) Usr.GrowerModule 
            Usr.LoginSequence = login.Sequence;
            Usr.OrganisationSequence = orga.Sequence;
            Usr.OrganisationType = orga.Type;
            Usr.Organisation = orga;
            Usr.Person = _personRepository.Get(login.Person);
            Usr.SelectedMaingroup = orga.Maingroups.OrderBy(x => x.Order).First().Sequence;
            Usr.OrganisationSettings = _settingsProvider.Organisation(0);
//            var mCheck = orga/
            switch (Usr.OrganisationType) {
                case OrgaType.Grower:
                    Usr.OrgaTypeSequence = EFHelper.ctx.F_CAB_GROWER.FirstOrDefault(x => x.GROW_FK_ORGA == Usr.OrganisationSequence).GROW_SEQUENCE;
                    Usr.GrowerSequence = EFHelper.ctx.F_CAB_GROWER.FirstOrDefault(x => x.GROW_FK_ORGA == Usr.OrganisationSequence).GROW_SEQUENCE;
//                    if (Usr.LoginName == "bloemk")
//                        Usr.GrowerModule = true;
//                    else
//                        Usr.GrowerModule = false;
                    break;
                case OrgaType.Buyer:
                    Usr.OrgaTypeSequence = EFHelper.ctx.F_CAB_BUYER.FirstOrDefault(x => x.BUYR_FK_ORGA == Usr.OrganisationSequence).BUYR_SEQUENCE;
                    Usr.GrowerSequence = -1;                    
                    break;
                case OrgaType.Comm:
                    Usr.OrgaTypeSequence = EFHelper.ctx.F_CAB_BUYER.FirstOrDefault(x => x.BUYR_FK_ORGA == Usr.OrganisationSequence).BUYR_SEQUENCE;
                    Usr.GrowerSequence = EFHelper.ctx.F_CAB_GROWER.FirstOrDefault(x => x.GROW_FK_ORGA == Usr.OrganisationSequence).GROW_SEQUENCE;
                    break;
                case OrgaType.Logp:
                    Usr.OrgaTypeSequence = EFHelper.ctx.F_CAB_BUYER.FirstOrDefault(x => x.BUYR_FK_ORGA == Usr.OrganisationSequence).BUYR_SEQUENCE;
                    Usr.GrowerSequence = -1;
                    break;
                default:
                    break;
            }

            FormsAuthentication.SetAuthCookie(login.Name, true);
            log4net.LogicalThreadContext.Properties["CALO_FK_LOGIN"] = Usr.LoginSequence;
            Log.Info("==Login==");
            Log.InfoFormat("LogiCAB Login: {0}, Orga: {1}, Pers: {2}", Usr.LoginName, Usr.Organisation.Name, Usr.Person.FullName);
            Session["reqproperties"] = null;
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Login(string username, string password) {
            Login loginModel = new Login {
                Name = username,
                Password = password,
                error = ""
            };
            if (_loginRepository.Validate(username, password)) {
                var login = EFHelper.ctx.F_CAB_LOGIN.SingleOrDefault(x => x.LOGI_USER_NAME == loginModel.Name && x.LOGI_PASS == loginModel.Password && x.LOGI_BLOCKED_YN == 0);
                UserInformation userInfo = new UserInformation(login);
                Session.Add("userinfo", userInfo);
                Session["maingroup"] = userInfo.selectedMaingroup;
                Usr.SelectedMaingroup = userInfo.selectedMaingroup;

                Session["reqproperties"] = null;
                return _performLogin(loginModel);
            }
            return View(loginModel);
        }

        public ActionResult LoginExternal(string username, string password) {
            Login loginModel = new Login {
                Name = username,
                Password = password,
                error = ""
            };
            if (_loginRepository.Validate(username, password)) {
                var login = EFHelper.ctx.F_CAB_LOGIN.SingleOrDefault(x => x.LOGI_USER_NAME == loginModel.Name && x.LOGI_PASS == loginModel.Password && x.LOGI_BLOCKED_YN == 0);
                UserInformation userInfo = new UserInformation(login);
                Session.Add("userinfo", userInfo);
                Session["maingroup"] = userInfo.selectedMaingroup;
                Usr.SelectedMaingroup = userInfo.selectedMaingroup;

                Session["reqproperties"] = null;
                return _performLogin(loginModel);
                #region oldLoginExternal
                /*
                var login = EFHelper.ctx.F_CAB_LOGIN.SingleOrDefault(x => x.LOGI_USER_NAME == loginModel.username && x.LOGI_PASS == loginModel.password && x.LOGI_BLOCKED_YN == 0);
                if (login == null) {
                    loginModel.error = "Gebruikersnaam of wachtwoord is onjuist.";
                    Log.InfoFormat("Unsuccessfull login attempted: {0}", loginModel.username);
                    return View(loginModel);
                }
                UserInformation userInfo = new UserInformation(login, Request);
                Session.Add("userinfo", userInfo);
                Session["maingroup"] = userInfo.selectedMaingroup;
                Session["reqproperties"] = null;
                FormsAuthentication.SetAuthCookie(loginModel.username, false);
                log4net.LogicalThreadContext.Properties["CALO_FK_LOGIN"] = userInfo.fcablogin.LOGI_SEQUENCE;
                Log.Info("==Login==");
                return RedirectToAction("Index", "Home");
                 */ 
                #endregion
            }
            return View(loginModel);
        }

        public ActionResult LogOff() {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "User");
        }

        public ActionResult setSelectedMainGroup(int id) {
            Session["reqproperties"] = null;
            Usr.SelectedMaingroup = id;
            Session["maingroup"] = Usr.SelectedMaingroup;
            return RedirectToAction("Index", "Home");
        }

        public JsonResult setSelectedMainGroupJson(int id) {
            Usr.SelectedMaingroup = id;
            return Json(Usr.SelectedMaingroup, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string changePassword(string oldPassword, string newPassword1, string newPassword2) {
            F_CAB_LOGIN login = EFHelper.ctx.F_CAB_LOGIN.FirstOrDefault(x => x.LOGI_FK_PERSON == Usr.Person.Sequence);
            string currentpassword = login.LOGI_PASS;
            if (oldPassword != currentpassword)
                return "1";

            if (newPassword1 == newPassword2) {
                login.LOGI_PASSWORD = CreateMD5Hash("This is my password: " + newPassword1);
                login.LOGI_PASS = newPassword1;
                EFHelper.ctx.SaveChanges();
                return "0";
            }
            return "-1";
        }

        [HttpPost]
        public bool saveTransporttype(int id) {
            try {
                EFHelper.ctx.F_CAB_BUYER.FirstOrDefault(x => x.BUYR_SEQUENCE == Usr.OrgaTypeSequence).BUYR_TRANSPORT_TYPE = (short)id;
                EFHelper.ctx.SaveChanges();
            } catch (Exception ex) {
                Log.ErrorFormat("Error attempting to save TransportType for buyer: {0}. Ex: {1}", Usr.OrgaTypeSequence, ex.ToString());
                return false;
            }
            return true;
        }

        [HttpPost]
        public int saveAddress(string street, string streetNr, string zipcode, string city) {
            Log.InfoFormat("saveAddress: {0}, {1}, {2}, {3}", street, streetNr, zipcode, city);
            F_CAB_ADDRESS addr = EFHelper.ctx.F_CAB_ADDRESS.FirstOrDefault(x => x.ADDR_FK_ORGANISATION == Usr.OrganisationSequence && x.ADDR_FK_CDAT_SEQ == 1);
            if (addr == null) return -1;
            addr.ADDR_STREET = street;
            addr.ADDR_NO_SUFFIX = streetNr;
            addr.ADDR_ZIPCODE = zipcode;
            addr.ADDR_CITY = city;
            EFHelper.ctx.SaveChanges();
            return 0;
        }

        [HttpPost]
        public int saveInformation(string type, string value) {
            F_CAB_ORGANISATION orga = EFHelper.ctx.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == Usr.OrganisationSequence);
            if (orga == null) return -1;
                switch (type) {
                    case "orga_name":
                        orga.ORGA_NAME = value;
                        break;
                    case "orga_phone":
                        orga.ORGA_PHONE_NO = value;
                        break;
                    case "orga_ean":
                        orga.ORGA_EAN_CODE = value;
                        break;
                    case "orga_fax":
                        orga.ORGA_FAX_NO = value;
                        break;
                    case "orga_email":
                        orga.ORGA_EMAIL = value;
                        break;
                    case "orga_btw":
                        orga.ORGA_VAT_NUMBER = value;
                        break;
                    case "orga_kwb":
                        orga.ORGA_MAILBOX = value;
                        break;
                    case "orga_ekt":
                        orga.ORGA_MAILBOX = value;
                        break;
                    case "orga_bvhnr":
                        orga.ORGA_VBA_NUMBER = value;
                        break;
                    default:
                        break;
                }
            try {
                EFHelper.ctx.SaveChanges();
                return 1;
            } catch {
                return -1;
            }
        }

        public string CreateMD5Hash(string input) {
            byte[] hashBytes = MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(input));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++) {
                sb.Append(hashBytes[i].ToString("x2"));
            }
            return sb.ToString();
        }

        protected override void Dispose(bool disposing) {
            EFHelper.ctx.Dispose();
            base.Dispose(disposing);
        }

        public static List<CABGroup> GetCabGroups() {
            if (Usr.OrganisationType == OrgaType.Grower)
            {
                var cabGroupsAssortiment = EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.Where(x => x.GRAS_FK_GROWER == Usr.OrgaTypeSequence).Select(y => y.F_CAB_CD_CAB_CODE.CABC_FK_CAB_GROUP).ToList();

                return EFHelper.ctx.F_CAB_CD_CAB_GROUP.Where(x => x.F_CAB_CD_GROUP_TYPE.F_CAB_CD_MAIN_GROUP_TYPE.CDMG_SEQUENCE == Usr.SelectedMaingroup && cabGroupsAssortiment.Contains(x.CDGR_SEQUENCE)).Select(g => new CABGroup
                {
                    Sequence = g.CDGR_SEQUENCE,
                    Description = g.CDGR_DESC,
                    TypeDescription = g.F_CAB_CD_GROUP_TYPE.CDGT_DESC
                }).ToList();
            }
            return EFHelper.ctx.F_CAB_CD_CAB_GROUP.Where(x => x.F_CAB_CD_GROUP_TYPE.F_CAB_CD_MAIN_GROUP_TYPE.CDMG_SEQUENCE == Usr.SelectedMaingroup).Select(g => new CABGroup {
                Sequence = g.CDGR_SEQUENCE,
                Description = g.CDGR_DESC,
                TypeDescription = g.F_CAB_CD_GROUP_TYPE.CDGT_DESC
            }).ToList();

        }
        public ActionResult CabGroupComboPartial() {
            return PartialView("CabGroup", GetCabGroups());
        }

        public static List<ContactPerson> GetContactPersons() {
            return EFHelper.ctx.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == Usr.OrganisationSequence).F_CAB_PERSON.Select(p => new ContactPerson {
                Sequence = p.PERS_SEQUENCE,
                LastName = p.PERS_LAST_NAME,
                FirstName = p.PERS_FIRST_NAME,
                PhoneNo = p.PERS_PHONE_NO,
                Email = p.PERS_EMAIL
            }).ToList();
        }


        public int addMailGroupPerson(int pers_seq, int cabgroup_seq) {
            if (EFHelper.ctx.F_CAB_MAIL_GROUP_PERSON_MATRIX.Any(x => x.MGPM_FK_PERSON == pers_seq && x.MGPM_FK_CAB_GROUP == cabgroup_seq)) { 
                return -1;
            }
            F_CAB_MAIL_GROUP_PERSON_MATRIX mgpm = new F_CAB_MAIL_GROUP_PERSON_MATRIX {
                MGPM_FK_PERSON = pers_seq,
                MGPM_FK_CAB_GROUP = cabgroup_seq
            };
            EFHelper.ctx.F_CAB_MAIL_GROUP_PERSON_MATRIX.AddObject(mgpm);
            EFHelper.ctx.SaveChanges();
            return 1;

        }
        public string addContactPerson(
            string firstname,
            string middlename,
            string lastname,
            string phone,
            string email) {
            if (String.IsNullOrEmpty(firstname) || String.IsNullOrEmpty(lastname) || String.IsNullOrEmpty(email))
                return "Incomplete";
            DateTime nowtime = DateTime.Now;
            F_CAB_PERSON newPerson = new F_CAB_PERSON {
                PERS_FIRST_NAME = firstname,
                PERS_LAST_NAME = lastname,
                PERS_MIDDLE_NAME = String.IsNullOrEmpty(middlename) ? " " : middlename,
                PERS_PHONE_NO = String.IsNullOrEmpty(phone) ? " " : phone,
                PERS_EMAIL = email,
                PERS_REF_EKT = string.Empty,

                PERS_FK_ORGANISATION = Usr.OrganisationSequence,
                PERS_CREATED_ON = nowtime,
                PERS_CREATED_BY = "sa",
                PERS_MODIFIED_ON = nowtime,
                PERS_MODIFIED_BY = "sa"
            };
            EFHelper.ctx.F_CAB_PERSON.AddObject(newPerson);
            EFHelper.ctx.SaveChanges();
            return "Complete";
        }


        public ActionResult CallbacksImageUpload() {
            string UC_grasSeq = Request.Params["DXUploadingCallback"];
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles(UC_grasSeq, null, ucCallbacks_FileUploadComplete);
            UploadedFile file = files[0];
            MemoryStream ms = new MemoryStream(file.FileBytes);
            Image tempImg = Image.FromStream(ms);
            Image newImage = ImageUtils.resizeImage(tempImg, new Size { Height = 1024, Width = 1024 });
            MemoryStream ms2 = new MemoryStream();
            newImage.Save(ms2, ImageFormat.Jpeg);
            F_CAB_PICTURE newPic = new F_CAB_PICTURE {
                PICT_PICTURE = ms2.ToArray()
            };
                EFHelper.ctx.F_CAB_PICTURE.AddObject(newPic);
            EFHelper.ctx.SaveChanges();
            EFHelper.ctx.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == Usr.OrganisationSequence).ORGA_FK_PICTURE = newPic.PICT_SEQUENCE;
            EFHelper.ctx.SaveChanges();
            return null;
        }

        public static void ucCallbacks_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
            int lastPicSeq = (from n in EFHelper.ctx.F_CAB_PICTURE orderby n.PICT_SEQUENCE descending select n.PICT_SEQUENCE).FirstOrDefault();
            e.CallbackData = String.Format("/Image/ShowImage/{0}", lastPicSeq + 1);
        }

        public JsonResult BlockLogin(int id) {
            /*
             * Block Login
             * Empty person email, only if the person only has 1 login
             */
            F_CAB_LOGIN login = EFHelper.ctx.F_CAB_LOGIN.FirstOrDefault(x => x.LOGI_SEQUENCE == id);
            if (login.LOGI_BLOCKED_YN == 1) login.LOGI_BLOCKED_YN = 0;
            else login.LOGI_BLOCKED_YN = 1;

            if (login.LOGI_BLOCKED_YN == 1) {
                if (EFHelper.ctx.F_CAB_LOGIN.Count(x => x.LOGI_FK_PERSON == login.LOGI_FK_PERSON) == 1) {
                    F_CAB_PERSON person = login.F_CAB_PERSON;
                    person.PERS_EMAIL = "";
                }
            }
            EFHelper.ctx.SaveChanges();

            return Json(true);
        }

        [HttpPost]
        public void heartbeat() {
            if (Session["userinfo"] != null) {
                if (LogiCAB.Web.MvcApplication.activeUsers.Any(x => x.Sequence == Usr.LoginSequence)) {
                    LogiCAB.Web.MvcApplication.activeUsers.FirstOrDefault(x => x.Sequence == Usr.LoginSequence).time = DateTime.Now;
                } else {
                    LogiCAB.Web.MvcApplication.activeUsers.Add(new Heartbeat {
                        Sequence = Usr.LoginSequence,
                        time = DateTime.Now
                    });
                }
            }
        }

        private IQueryable<ActiveUser> _listActiveUsers() {
            List<int> currentActive = (from h in LogiCAB.Web.MvcApplication.activeUsers select h.Sequence).ToList();
            var result = (from login in EFHelper.ctx.F_CAB_LOGIN.Where(x => currentActive.Contains(x.LOGI_SEQUENCE))
                          select new ActiveUserLogin {
                             LoginName = login.LOGI_USER_NAME,
                             LoginSequence = login.LOGI_SEQUENCE,
                             OrganisationName = login.F_CAB_PERSON.F_CAB_ORGANISATION.ORGA_NAME
                         }).ToList();
            var result2 = from r in result
                          join active in LogiCAB.Web.MvcApplication.activeUsers on r.LoginSequence equals active.Sequence
                          select new ActiveUser {
                              LoginName = r.LoginName,
                              LoginSequence = r.LoginSequence,
                              OrganisationName = r.OrganisationName,
                              Time = active.time                              
                          };
            return result2.AsQueryable();
        }

        public ActionResult ListOnline() {

            return View(_listActiveUsers());
        }

        public IEnumerable<Login> _getLogins() {
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                Stopwatch sw1 = new Stopwatch();
                sw1.Start();
                conn.Open();
                var res = conn.Query<Login>(
@"select logi_sequence as Sequence
,      logi_user_name as Name
,      logi_pass as Password
,      logi_fk_cdla_seq as Language
,      logi_blocked_yn as Blocked
,      orga_name as OrganisationName
from f_cab_login       
join f_cab_person       on LOGI_FK_PERSON = pers_sequence
join f_cab_organisation on pers_fk_organisation = orga_sequence
order by logi_user_name");
                sw1.Stop();
                Log.InfoFormat("Getting Login-list took: {0}ms", sw1.ElapsedMilliseconds);
                return res;
            }            
        }

        public ActionResult List()
        {
            return View(_getLogins());
        }

        public ActionResult ListPartial()
        {
            return PartialView(_getLogins());
        }

        public ActionResult ListOnlinePartial() {
            return PartialView(_listActiveUsers());
        }

        [HttpPost]
        public string SetLanguage(string id) {

            DLanguage lang;
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                var rows = conn.Query<DLanguage>("select cdla_sequence as Sequence, cdla_iso as iso from F_CAB_CD_LANGUAGE where CDLA_ISO = @Id", new { Id = id });
                lang =  rows.FirstOrDefault();

            }
            Usr.Language = lang.Sequence;            
            Session["CurrentCulture"] = CultureInfo.GetCultureInfo(lang.iso);
            return lang.iso;
        }


    }
    public class DLanguage {
        public int Sequence { get; set; }
        public string iso { get; set; }
    }
    public class ActiveUser {
        public string LoginName { get; set; }
        public int LoginSequence { get; set; }
        public string OrganisationName { get; set; }
        public DateTime Time { get; set; }
    }
    public class ActiveUserLogin {
        public string LoginName { get; set; }
        public int LoginSequence { get; set; }
        public string OrganisationName { get; set; }
    }
}

