﻿using log4net;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;

namespace LogiCAB.Web.Controllers {
    public class BaseController : Controller {
        public ILog Log;
        public BaseController(ILog log) {
            CultureInfo ci = (CultureInfo)System.Web.HttpContext.Current.Session["CurrentCulture"];
            if (ci == null) ci = CultureInfo.GetCultureInfo("nl");
            Thread.CurrentThread.CurrentUICulture = ci;
            Thread.CurrentThread.CurrentCulture = ci;
            this.Log = log;
        }
    }
}
