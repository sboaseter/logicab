﻿using System.Collections.Generic;
using System.Diagnostics;
using CAB.Data.EF.Models;
using CAB.Domain.Helpers;
using Dapper;
using log4net;
using LogiCAB.Web.DAL;
using LogiCAB.Web.Domain;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Data.SqlClient;
using DevExpress.Web.Mvc;
using DevExpress.Web;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;

namespace LogiCAB.Web.Controllers {
    [Authorize]
    [HandleError]
    public class CABController : BaseController {
        public CABController(ILog log) : base(log) {
        }

        public ActionResult Index()
        {
            return View(_getCABList());
        }

        public ActionResult List() {
            return PartialView("List", _getCABList());
        }

        public ActionResult Test() {
            return View();
        }

        public ActionResult GroupTypePartial(int id = -1)
        {
            var CABGroupTypes = from cdgt in EFHelper.ctx.F_CAB_CD_GROUP_TYPE
                                where cdgt.CDGT_FK_MAIN_GROUP == Usr.SelectedMaingroup
                                select cdgt;
            if (id != -1)
                TempData["cbGroupType"] = id;
            return PartialView(CABGroupTypes);
        }


        public ActionResult CABUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] F_CAB_CD_CAB_CODE model) {
            if (ModelState.IsValid) {
                try {
                    var cabcode = EFHelper.ctx.F_CAB_CD_CAB_CODE.FirstOrDefault(x => x.CABC_SEQUENCE == model.CABC_SEQUENCE);
                    cabcode.CABC_CAB_DESC = model.CABC_CAB_DESC ?? cabcode.CABC_CAB_DESC;
                    cabcode.CABC_FK_CAB_GROUP = model.CABC_FK_CAB_GROUP;
                    cabcode.CABC_IS_AUTHORISED = model.CABC_IS_AUTHORISED == null ? cabcode.CABC_IS_AUTHORISED : model.CABC_IS_AUTHORISED;
                    cabcode.CABC_FK_PD_GROUP = model.CABC_FK_PD_GROUP;
                    EFHelper.ctx.SaveChanges();
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            return PartialView("List", _getCABList());
        }


        private List<F_CAB_CD_CAB_CODE> _getCABList() {
            return EFHelper.ctx.F_CAB_CD_CAB_CODE.Where(x => x.F_CAB_CD_CAB_GROUP.F_CAB_CD_GROUP_TYPE.F_CAB_CD_MAIN_GROUP_TYPE.CDMG_SEQUENCE == Usr.SelectedMaingroup).ToList();
        }

        public ActionResult CallbacksImageUpload() {
            int id = (int)System.Web.HttpContext.Current.Session["currentCABSequence"];
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles("ucCallbacks", null, ucCallbacks_FileUploadComplete);
            UploadedFile file = files[0];
            MemoryStream ms = new MemoryStream(file.FileBytes);
            Image tempImg = Image.FromStream(ms);
            Image newImage = ImageUtils.resizeImage(tempImg, new Size { Height = 1024, Width = 1024 });
            MemoryStream ms2 = new MemoryStream();
            newImage.Save(ms2, ImageFormat.Jpeg);
            F_CAB_PICTURE newPic = new F_CAB_PICTURE {
                PICT_PICTURE = ms2.ToArray()
            };
            EFHelper.ctx.F_CAB_PICTURE.AddObject(newPic);
            EFHelper.ctx.SaveChanges();
            EFHelper.ctx.F_CAB_CD_CAB_CODE.FirstOrDefault(x => x.CABC_SEQUENCE == id).CABC_FK_PICTURE = newPic.PICT_SEQUENCE;
            EFHelper.ctx.SaveChanges();
            return null;
        }

        public static void ucCallbacks_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
            int newPic = -1;
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                newPic = conn.Query<int>("select top 1 PICT_SEQUENCE FROM F_CAB_PICTURE ORDER BY 1 DESC").First();
            }
            newPic++;
            e.CallbackData = newPic.ToString();
        }


        // remember IEnumerable!
        private IEnumerable<CABCode> _getCABListDapper(bool useCache = true) {
            IEnumerable<CABCode> cablist;
            string cacheKey = "cablist_" + Usr.SelectedMaingroup + DateTime.Now.Minute;
            if (CacheHelper.Get(cacheKey, out cablist) && useCache)
            {
                return cablist;
//                cablist = _getCABList();

            }
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                Stopwatch sw1 = new Stopwatch();
                sw1.Start();
                conn.Open();
                cablist = conn.Query<CABCode>(
@"SELECT CABC_SEQUENCE
,      CABC_CAB_CODE
,      CABC_CAB_DESC
,      CABC_IS_AUTHORISED
,      CABC_FK_PICTURE
,      CDGR_SEQUENCE as GroupSequence
,      CDGR_DESC as GroupDescription
,      CDGT_DESC as GroupTypeDescription
,      CDMG_SEQUENCE as MaingroupSequence
,      CDMG_DESC as MaingroupDescription
,      CDMG_NUM_OF_DAYS_OFFER as MaingroupDaysOffer
,      PROP_DESC1 
,      PROP_VAL1
,      PROP_DESC2
,      PROP_VAL2
,      PROP_DESC3
,      PROP_VAL3
,      PROP_DESC4
,      PROP_VAL4
,      PROP_DESC5
,      PROP_VAL5
,      PROP_DESC6
,      PROP_VAL6
from f_Cab_cd_Cab_code       
join f_cab_cd_cab_group       on cdgr_sequence = CABC_FK_CAB_GROUP
join F_CAB_CD_GROUP_TYPE      on cdgt_sequence = cdgr_fk_group_type
join f_cab_cd_main_group_type on CDMG_SEQUENCE = CDGT_FK_MAIN_GROUP
JOIN F_CAB_CAB_PROP           ON PROP_FK_CABC_SEQ = CABC_SEQUENCE
WHERE PROP_FK_CDLA_SEQ = @Language AND PROP_FK_CDMG_SEQ = @Maingroup
ORDER BY CABC_SEQUENCE DESC", new { Language = 1, Maingroup = Usr.SelectedMaingroup });
//var dog = connection.Query<Dog>("select Age = @Age, Id = @Id", new { Age = (int?)null, Id = guid });

                sw1.Stop();
                Log.InfoFormat("Getting CABCode-list took: {0}ms", sw1.ElapsedMilliseconds);
                CacheHelper.Add(cablist, cacheKey);
                return cablist;
            }
        }

        public ActionResult Properties() {
            return View();
        }

        public ActionResult PropertiesKO() {
            return View();
        }

        public ActionResult PropertyCallbackPartial() {
            int propertyGroup = Session["property_group"] != null ? (int)Session["property_group"] : 1;
            return PartialView((from cdpt in EFHelper.ctx.F_CAB_CD_CAB_PROP_TYPE.Where(x => x.CDPT_FK_PROP_GROUP == propertyGroup) select cdpt).ToList());
        }

        public bool setPropGroup(int id) {
            Session["property_group"] = id;
            return true;
        }

        public bool setPropType(int id) {
            Session["property_type"] = id;
            return true;
        }

        public ActionResult PropertyLookup1Partial() {
            int propertyType = Session["property_type"] != null ? (int)Session["property_type"] : 1;
            return PartialView((from cdpl in EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.Where(x => x.CDPL_FK_PROP_TYPE == propertyType) select cdpl).ToList());
        }
        public ActionResult PropertyLookup2Partial() {
            int propertyType = Session["property_type"] != null ? (int)Session["property_type"] : 1;
            return PartialView((from cdpl in EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.Where(x => x.CDPL_FK_PROP_TYPE == propertyType) select cdpl).ToList());
        }
        public JsonResult getPropertyLookups() {
            int propertyType = Session["property_type"] != null ? (int)Session["property_type"] : 1;
            return Json((from cdpl in EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.Where(x => x.CDPL_FK_PROP_TYPE == propertyType)
                         select new {
                             CDPL_SEQUENCE = cdpl.CDPL_SEQUENCE,
                             CDPL_LOOKUP_DESC = cdpl.CDPL_LOOKUP_VALUE
                         }).ToList());
        }
        public JsonResult executePropertyChange(string correct, string notcorrect) {
            int c = Int32.Parse(correct);
            int nc = Int32.Parse(notcorrect);
            F_CAB_CD_CAB_PROP_LOOKUP cdpl_correct = EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.SingleOrDefault(x => x.CDPL_SEQUENCE == c);
            F_CAB_CD_CAB_PROP_LOOKUP cdpl_notcorrect = EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.SingleOrDefault(x => x.CDPL_SEQUENCE == nc);
            int propertyType = Session["property_type"] != null ? (int)Session["property_type"] : 1;
            var detailUsingWrong = from cabd in EFHelper.ctx.F_CAB_CAB_DETAIL.Where(x => x.CABD_VALUE == cdpl_notcorrect.CDPL_LOOKUP_VALUE && x.CABD_FK_TYPE == propertyType) select cabd;
            int detailUsingWrongCount = detailUsingWrong.Count();
            foreach (var detail in detailUsingWrong) {
                detail.CABD_VALUE = cdpl_correct.CDPL_LOOKUP_VALUE;
                detail.CABD_VALUE_DESC = cdpl_correct.CDPL_LOOKUP_VALUE;
                detail.CABD_VALUE_DESC_UPPER = cdpl_correct.CDPL_LOOKUP_VALUE.ToUpper();
            }
            EFHelper.ctx.SaveChanges();
            var vlkmat = EFHelper.ctx.F_CAB_CD_CAB_VBN_LK_MATRIX.Where(x => x.CVLM_FK_CAB_LOOKUP_VALUE == cdpl_notcorrect.CDPL_SEQUENCE).ToList();
            if (vlkmat != null) {
                for (int i = 0; i < vlkmat.Count(); i++) {
                    EFHelper.ctx.F_CAB_CD_CAB_VBN_LK_MATRIX.DeleteObject(vlkmat[i]);
                }
            }

            EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.DeleteObject(cdpl_notcorrect);
            EFHelper.ctx.SaveChanges();

            String queryExpr = String.Format("exec [dbo].[proc_fill_and_sync_properties]");
            EFHelper.ctx.ExecuteStoreCommand(queryExpr);


            return Json(detailUsingWrongCount);
        }
        public JsonResult executeAddProperty(string value) {
            int propertyType = Session["property_type"] != null ? (int)Session["property_type"] : 1;
            value = value.Replace("  ", " ").Trim();
            F_CAB_CD_CAB_PROP_LOOKUP newProp = new F_CAB_CD_CAB_PROP_LOOKUP {
                CDPL_FK_PROP_TYPE = propertyType,
                CDPL_LOOKUP_VALUE = value
//                CDPL_LOOKUP_DESC = value,
//                CDPL_ORDER = 1
            };
            EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.AddObject(newProp);
            EFHelper.ctx.SaveChanges();
            return Json(true);
        }

        public ActionResult CABVBNPartial() {
            int cabcSequence = (int)System.Web.HttpContext.Current.Session["currentCABSequence"];
            var cabvbnMatrix = EFHelper.ctx.F_CAB_CAB_VBN_MATRIX.Where(x => x.CVMA_FK_CAB_CODE == cabcSequence);
            return PartialView(cabvbnMatrix.ToList());
        }

        // Change existing CAB-VBN-connection
        public ActionResult CABVBNPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] F_CAB_CAB_VBN_MATRIX model) {
            int cabcSequence = (int)System.Web.HttpContext.Current.Session["currentCABSequence"];
            if (ModelState.IsValid) {
                try {
                    var vbn = EFHelper.ctx.F_CAB_CD_VBN_CODE.FirstOrDefault(x => x.VBNC_SEQUENCE == model.CVMA_FK_VBN_CODE);
                    var currentCABVBN = EFHelper.ctx.F_CAB_CAB_VBN_MATRIX.FirstOrDefault(x => x.CVMA_SEQUENCE == model.CVMA_SEQUENCE);
                    currentCABVBN.CVMA_FK_VBN_CODE = model.CVMA_FK_VBN_CODE;
                    EFHelper.ctx.SaveChanges();
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            var cabvbnMatrix = EFHelper.ctx.F_CAB_CAB_VBN_MATRIX.Where(x => x.CVMA_FK_CAB_CODE == cabcSequence);
            return PartialView("CABVBNPartial", cabvbnMatrix.ToList());
        }

        // Change existing CAB-VBN-connection
        public ActionResult CABVBNPartialAdd([ModelBinder(typeof(DevExpressEditorsBinder))] F_CAB_CAB_VBN_MATRIX model) {
            int cabcSequence = (int)System.Web.HttpContext.Current.Session["currentCABSequence"];
            model.CVMA_FK_CAB_CODE = cabcSequence;
            if (ModelState.IsValid) {
                try {
                    // Check for existing connection
                    var exists = EFHelper.ctx.F_CAB_CAB_VBN_MATRIX.Any(x => x.CVMA_FK_CAB_CODE == cabcSequence && x.CVMA_FK_VBN_CODE == model.CVMA_FK_VBN_CODE);
                    if (!exists) {
                        F_CAB_CAB_VBN_MATRIX newCVMA = new F_CAB_CAB_VBN_MATRIX {
                            CVMA_FK_CAB_CODE = cabcSequence,
                            CVMA_FK_VBN_CODE = model.CVMA_FK_VBN_CODE                            
                        };
                        EFHelper.ctx.F_CAB_CAB_VBN_MATRIX.AddObject(newCVMA);
                        EFHelper.ctx.SaveChanges();
                    }
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            var cabvbnMatrix = EFHelper.ctx.F_CAB_CAB_VBN_MATRIX.Where(x => x.CVMA_FK_CAB_CODE == cabcSequence);
            return PartialView("CABVBNPartial", cabvbnMatrix.ToList());
        }

        

        public ActionResult CABPropertiesPartial() {
            int cabcSequence = (int)System.Web.HttpContext.Current.Session["currentCABSequence"];
            var details = EFHelper.ctx.F_CAB_CAB_DETAIL.Where(x => x.CABD_FK_CAB_CODE == cabcSequence);
            return PartialView(details.ToList());
        }

        public static List<F_CAB_CD_CAB_GROUP> GetCABGroups() {
            return EFHelper.ctx.F_CAB_CD_CAB_GROUP.ToList();
        }

        public static List<F_CAB_CD_GROUP_TYPE> GetCABGroupTypes()
        {
            return EFHelper.ctx.F_CAB_CD_GROUP_TYPE.Where(x => x.CDGT_FK_MAIN_GROUP == Usr.SelectedMaingroup).ToList();
        }

        public static List<F_CAB_CD_PD_GROUP> GetPDGroups() {
            /*
             *  1	Planten
                2	Jonge Planten
                3	Opgepotte bloembollen
                4	Snijbloemen
             * 
             * 
             */ 
//            var mg = Usr.SelectedMaingroup;

//            if(mg == 1) {
//                return EFHelper.ctx.F_CAB_CD_PD_GROUP.Where(x => x.CDPD_FK_PD_TYPE == 1 ||x.CDPD_FK_PD_TYPE == 2).ToList();
//            } else if(mg == 2) {
//                return EFHelper.ctx.F_CAB_CD_PD_GROUP.Where(x => x.CDPD_FK_PD_TYPE == 3 ||x.CDPD_FK_PD_TYPE == 4).ToList();
//            }
            return EFHelper.ctx.F_CAB_CD_PD_GROUP.ToList();

            
        }

        public ActionResult CABPropertiesPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] F_CAB_CAB_DETAIL model) {
            var cabcSequence = -1;
            if (ModelState.IsValid) {
                try {
                    var cabdetail = EFHelper.ctx.F_CAB_CAB_DETAIL.FirstOrDefault(x => x.CABD_SEQUENCE == model.CABD_SEQUENCE);
                    var propType = EFHelper.ctx.F_CAB_CD_CAB_PROP_TYPE.FirstOrDefault(x => x.CDPT_SEQUENCE == model.CABD_FK_TYPE);
                    cabcSequence = cabdetail.CABD_FK_CAB_CODE;
                    cabdetail.CABD_VALUE = model.CABD_VALUE.Replace("  ", " ").Trim();
                    cabdetail.CABD_VALUE_DESC = propType.CDPT_DESC.Replace("  ", " ").Trim();
                    cabdetail.CABD_VALUE_DESC_UPPER = propType.CDPT_DESC.Replace("  ", " ").Trim();

                    var exists = EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.Any(x => x.CDPL_FK_PROP_TYPE == model.CABD_FK_TYPE && x.CDPL_LOOKUP_VALUE == cabdetail.CABD_VALUE);
                    if (exists) {
                        var currentProp = EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.FirstOrDefault(x => x.CDPL_FK_PROP_TYPE == cabdetail.CABD_FK_TYPE && x.CDPL_LOOKUP_VALUE == cabdetail.CABD_VALUE);
                        currentProp.CDPL_LOOKUP_VALUE = cabdetail.CABD_VALUE;
                    } else {
                        F_CAB_CD_CAB_PROP_LOOKUP newProp = new F_CAB_CD_CAB_PROP_LOOKUP {
                            CDPL_FK_PROP_TYPE = cabdetail.CABD_FK_TYPE,
                            CDPL_LOOKUP_VALUE = cabdetail.CABD_VALUE,
                            CDPL_LOOKUP_DESC = propType.CDPT_DESC
//                            CDPL_ORDER = 1
                        };
                        EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.AddObject(newProp);
                    }
                    EFHelper.ctx.SaveChanges();
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            return PartialView("CABPropertiesPartial", EFHelper.ctx.F_CAB_CAB_DETAIL.Where(x => x.CABD_FK_CAB_CODE == cabcSequence).ToList());


        }

        public static List<F_CAB_CD_CAB_PROP_TYPE> getPropertiesForMG() {
            int maingroup = Usr.SelectedMaingroup;
            var props = from p in EFHelper.ctx.F_CAB_CD_CAB_PROP_TYPE
                        join mgpt in EFHelper.ctx.F_CAB_PROP_MAIN_GROUP_MATRIX on p.CDPT_SEQUENCE equals mgpt.MGPT_FK_PROP_TYPE
                        where
                           p.CDPT_DESC_PREFIX != null &&
                           mgpt.MGPT_FK_MAIN_GROUP == Usr.SelectedMaingroup
                        select p;
            return props.ToList();

        }


        public JsonResult setSelectedPropertyType(int id) {
            System.Web.HttpContext.Current.Session["SelectedPropertyType"] = id;
            return Json(System.Web.HttpContext.Current.Session["SelectedPropertyType"], JsonRequestBehavior.AllowGet);
        }

        public JsonResult RefreshPropertyTable() {
            int cabcSequence = (int)System.Web.HttpContext.Current.Session["currentCABSequence"];
            var toDelete = EFHelper.ctx.F_CAB_CAB_PROP.Where(x => x.PROP_FK_CABC_SEQ == cabcSequence);
            foreach(var d in toDelete) {
                EFHelper.ctx.F_CAB_CAB_PROP.DeleteObject(d);
            }
            EFHelper.ctx.SaveChanges();            
            EFHelper.ctx.ExecuteStoreCommand(String.Format("exec proc_fill_and_sync_properties {0}", cabcSequence));
            return Json(cabcSequence, JsonRequestBehavior.AllowGet);
        }

        public static List<F_CAB_CD_CAB_PROP_TYPE> getPropertiesForPropType() {
            int maingroup = Usr.SelectedMaingroup;
            var props = from p in EFHelper.ctx.F_CAB_CD_CAB_PROP_TYPE
                        join mgpt in EFHelper.ctx.F_CAB_PROP_MAIN_GROUP_MATRIX on p.CDPT_SEQUENCE equals mgpt.MGPT_FK_PROP_TYPE
                        where
                           p.CDPT_DESC_PREFIX != null &&
                           mgpt.MGPT_FK_MAIN_GROUP == Usr.SelectedMaingroup
                        select p;
            return props.ToList();

        }

        public ActionResult CABPropertiesPartialAdd([ModelBinder(typeof(DevExpressEditorsBinder))] F_CAB_CAB_DETAIL model) {
            int cabcSequence = (int)System.Web.HttpContext.Current.Session["currentCABSequence"];
            if (ModelState.IsValid) {
                try {
                    var propType = EFHelper.ctx.F_CAB_CD_CAB_PROP_TYPE.FirstOrDefault(x => x.CDPT_SEQUENCE == model.CABD_FK_TYPE);
                    var maxOrder = EFHelper.ctx.F_CAB_CAB_DETAIL.Where(x => x.CABD_FK_CAB_CODE == cabcSequence).Max(y => y.CABD_ORDER);
                    F_CAB_CAB_DETAIL newDetail = new F_CAB_CAB_DETAIL {
                        CABD_FK_CAB_CODE = cabcSequence,
                        CABD_FK_TYPE = model.CABD_FK_TYPE, //todo
                        CABD_ORDER = maxOrder+1, //todo
                        CABD_VALUE = model.CABD_VALUE,
                        CABD_VALUE_DESC = propType.CDPT_DESC,
                        CABD_VALUE_DESC_UPPER = propType.CDPT_DESC.ToUpper()
                    };
                    EFHelper.ctx.F_CAB_CAB_DETAIL.AddObject(newDetail);
                    EFHelper.ctx.SaveChanges();


/*                    int icounter = 1;
                    try
                    {
                        var maxExists = EFHelper.ctx.F_CAB_CD_CAB_PROP_TYPE.FirstOrDefault(x => x.CDPT_SEQUENCE == newDetail.CABD_FK_TYPE);
                        var max = maxExists == null ? 1 : maxExists.F_CAB_CD_CAB_PROP_LOOKUP.Max(x => x.CDPL_ORDER);
                        icounter = max;
                        icounter = icounter + 1;
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Error occured in CABAuthorizeController: CreateCABCodeFromRequest: getting CDPL_ORDER: ex: {0}", ex.ToString());
                    }
                    */
                    var lookup = new F_CAB_CD_CAB_PROP_LOOKUP();
                    lookup.CDPL_FK_PROP_TYPE = newDetail.CABD_FK_TYPE;
//                    lookup.CDPL_LOOKUP_DESC = newDetail.CABD_VALUE;
                    lookup.CDPL_LOOKUP_VALUE = newDetail.CABD_VALUE;
  //                  lookup.CDPL_ORDER = icounter;
                    EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.AddObject(lookup);
                    EFHelper.ctx.SaveChanges();

                    // Move to seperate button
                    //                    EFHelper.ctx.ExecuteStoreCommand(String.Format("exec proc_fill_and_sync_properties {0}", cabcSequence));
                    //exec fill and sync props! TODO
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            return PartialView("CABPropertiesPartial", EFHelper.ctx.F_CAB_CAB_DETAIL.Where(x => x.CABD_FK_CAB_CODE == cabcSequence).ToList());
        }
        public ActionResult CABPropertiesPartialDelete([ModelBinder(typeof(DevExpressEditorsBinder))] F_CAB_CAB_DETAIL model) {
            int cabcSequence = (int)System.Web.HttpContext.Current.Session["currentCABSequence"];
            if (ModelState.IsValid) {
                try {
                    var detail = EFHelper.ctx.F_CAB_CAB_DETAIL.Single(x => x.CABD_SEQUENCE == model.CABD_SEQUENCE);
                    EFHelper.ctx.F_CAB_CAB_DETAIL.DeleteObject(detail);
                    EFHelper.ctx.SaveChanges();
                    // Move to seperate button
                    //                    EFHelper.ctx.ExecuteStoreCommand(String.Format("exec proc_fill_and_sync_properties {0}", cabcSequence));
                    //exec fill and sync props! TODO
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            return PartialView("CABPropertiesPartial", EFHelper.ctx.F_CAB_CAB_DETAIL.Where(x => x.CABD_FK_CAB_CODE == cabcSequence).ToList());
        }

        public ActionResult CABCasksPartial() {
            int cabcSequence = (int)System.Web.HttpContext.Current.Session["currentCABSequence"];
            var caca = EFHelper.ctx.F_CAB_CD_CAB_CODE.Single(x => x.CABC_SEQUENCE == cabcSequence).F_CAB_CAB_CASK_MATRIX.ToList();
            return PartialView(caca);
        }

        public ActionResult CABCasksPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] F_CAB_CAB_CASK_MATRIX model) {
            var cabcSequence = -1;
            if (ModelState.IsValid) {
                try {
                    var caca = EFHelper.ctx.F_CAB_CAB_CASK_MATRIX.FirstOrDefault(x => x.CACA_SEQUENCE == model.CACA_SEQUENCE);
                    cabcSequence = caca.CACA_FK_CAB_CODE;
                    caca.CACA_NUM_OF_UNITS = model.CACA_NUM_OF_UNITS;
                    caca.CACA_UNITS_PER_LAYER = model.CACA_UNITS_PER_LAYER;
                    caca.CACA_LAYERS_PER_TROLLEY = model.CACA_LAYERS_PER_TROLLEY;
                    caca.CACA_FK_CASK = model.CACA_FK_CASK;
                    //                    cabcode.CABC_CAB_DESC = model.CABC_CAB_DESC ?? cabcode.CABC_CAB_DESC;
                    //                    cabcode.CABC_IS_AUTHORISED = model.CABC_IS_AUTHORISED == null ? cabcode.CABC_IS_AUTHORISED : model.CABC_IS_AUTHORISED;
                    EFHelper.ctx.SaveChanges();
                    // Move to seperate button
                    //                    EFHelper.ctx.ExecuteStoreCommand(String.Format("exec proc_fill_and_sync_properties {0}", cabcSequence));
                    //exec fill and sync props! TODO
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            return PartialView("CABCasksPartial", EFHelper.ctx.F_CAB_CD_CAB_CODE.Single(x => x.CABC_SEQUENCE == cabcSequence).F_CAB_CAB_CASK_MATRIX.ToList());
            //            return PartialView("List", _getCABList());
        }
        public ActionResult CABCasksPartialAdd([ModelBinder(typeof(DevExpressEditorsBinder))] F_CAB_CAB_CASK_MATRIX model) {
            int cabcSequence = (int)System.Web.HttpContext.Current.Session["currentCABSequence"];
            if (ModelState.IsValid) {
                try {
                    F_CAB_CAB_CASK_MATRIX newCaca = new F_CAB_CAB_CASK_MATRIX {
                        CACA_FK_CAB_CODE = cabcSequence,
                        CACA_FK_CASK = model.CACA_FK_CASK,
                        CACA_NUM_OF_UNITS = model.CACA_NUM_OF_UNITS,
                        CACA_UNITS_PER_LAYER = model.CACA_UNITS_PER_LAYER,
                        CACA_LAYERS_PER_TROLLEY = model.CACA_LAYERS_PER_TROLLEY,
                        
                    };
                    EFHelper.ctx.F_CAB_CAB_CASK_MATRIX.AddObject(newCaca);
                    EFHelper.ctx.SaveChanges();
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            return PartialView("CABCasksPartial", EFHelper.ctx.F_CAB_CD_CAB_CODE.Single(x => x.CABC_SEQUENCE == cabcSequence).F_CAB_CAB_CASK_MATRIX.ToList());
            //            return PartialView("List", _getCABList());
        }

        public ActionResult VBNComboPartial() {
            return PartialView();
        }
        public static IEnumerable<F_CAB_CD_VBN_CODE> GetVBNRange(ListEditItemsRequestedByFilterConditionEventArgs args) {
            return getVBNList(args, false);
        }

        public static IEnumerable<F_CAB_CD_VBN_CODE> GetVBNByID(ListEditItemRequestedByValueEventArgs args) {
            return getVBNList(args, true);
        }
        private static IEnumerable<F_CAB_CD_VBN_CODE> getVBNList(object argsObj, bool single) {
            if (argsObj != null) {
                var query = EFHelper.ctx.F_CAB_CD_VBN_CODE.Where(z => z.F_CAB_CD_VBN_CODE_GROUP.CDVG_MAINGROUP == Usr.SelectedMaingroup);
//                           .Select(x => new F_CAB_CD_VBN_CODE_NUM_CAB {
//                               VBNC_SEQUENCE = x.VBNC_SEQUENCE,
//                               VBNC_VBN_CODE = x.VBNC_VBN_CODE,
//                               VBNC_DESC = x.VBNC_DESC,
//                               numCabCodes = x.F_CAB_CAB_VBN_MATRIX.Count(y => y.CVMA_FK_VBN_CODE == x.VBNC_SEQUENCE)
//                           });
                if (single) {
                    ListEditItemRequestedByValueEventArgs args = (ListEditItemRequestedByValueEventArgs)argsObj;
                    if (args.Value == null) return null;
                    return query.Where(x => x.VBNC_SEQUENCE == (int)args.Value).Take(1).ToList();
                } else {
                    ListEditItemsRequestedByFilterConditionEventArgs args = (ListEditItemsRequestedByFilterConditionEventArgs)argsObj;
                    var skip = args.BeginIndex;
                    var take = args.EndIndex - args.BeginIndex + 1;
                    return query.Where(x => x.VBNC_DESC.Contains(args.Filter) || x.VBNC_VBN_CODE.Contains(args.Filter))
                                .OrderBy(y => y.VBNC_DESC)
                                .Skip(skip)
                                .Take(take)
                                .ToList();
                }
            }
            return null;
        }


        public ActionResult CaskComboPartial() {
            return PartialView();
        }
        public ActionResult PropertyValueComboPartial() {
            int id = -1;
            try {
                id = (int)System.Web.HttpContext.Current.Session["SelectedPropertyType"];
            } catch (Exception ex) {
                return PartialView(new List<F_CAB_CD_CAB_PROP_LOOKUP>());
            }
            var props = EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.Where(x => x.CDPL_FK_PROP_TYPE == id);
//            return ;
            return PartialView(props.OrderBy(x => x.CDPL_LOOKUP_VALUE).ToList());
        }
        public static IEnumerable<F_CAB_CD_CASK> GetCaskRange(ListEditItemsRequestedByFilterConditionEventArgs args) {
            var skip = args.BeginIndex;
            var take = args.EndIndex - args.BeginIndex + 1;
            return (from cdca in EFHelper.ctx.F_CAB_CD_CASK
                    where (cdca.CDCA_CASK_CODE).StartsWith(args.Filter)
                    orderby cdca.CDCA_CASK_CODE
                    select cdca
                    ).Skip(skip).Take(take).ToList();
        }
        public static IEnumerable<F_CAB_CD_CASK> GetCaskByID(ListEditItemRequestedByValueEventArgs args) {
            if (args.Value != null) {
                int id = (int)args.Value;
                return (from cdca in EFHelper.ctx.F_CAB_CD_CASK
                        where cdca.CDCA_SEQUENCE == id
                        select cdca).Take(1).ToList();
            }
            return null;
        }

        public string addCabGroup(int type, string group)
        {
            var exists = EFHelper.ctx.F_CAB_CD_CAB_GROUP.Any(x => x.CDGR_FK_GROUP_TYPE == type && x.CDGR_DESC == group);
            if (exists)
            {
                return String.Format("CAB Group: {0} allready exists.", group);
            }
            try { 
                F_CAB_CD_CAB_GROUP newGroup = new F_CAB_CD_CAB_GROUP();
                newGroup.CDGR_DESC = group;
                newGroup.CDGR_FK_GROUP_TYPE = type;
                newGroup.CDGR_SORT_ORDER = 0;
                newGroup.CDGR_EXT_ID = null;
                newGroup.CDGR_MODIFIED_ON = DateTime.Now;
                newGroup.CDGR_MODIFIED_BY = "LogiCAB Systeem";
                EFHelper.ctx.F_CAB_CD_CAB_GROUP.AddObject(newGroup);
                EFHelper.ctx.SaveChanges();
                return String.Format("CAB Group: {0} added.", group);
            }
            catch (Exception ex)
            {
                return String.Format("An error occured while attempting to add a CAB Group: Exception: {0}", ex.InnerException);
            }
        }
        public string addCabGroupType(int mg, string grouptype) {

            var exists = EFHelper.ctx.F_CAB_CD_GROUP_TYPE.Any(x => x.CDGT_FK_MAIN_GROUP == mg && x.CDGT_DESC.ToLower() == grouptype.ToLower());
            if (exists) return "Exists.";
            F_CAB_CD_GROUP_TYPE newgt = new F_CAB_CD_GROUP_TYPE();
            newgt.CDGT_DESC = grouptype.UppercaseFirst();
            newgt.CDGT_FK_MAIN_GROUP = mg;
            newgt.CDGT_SORT_ORDER = 0;
            newgt.CDGT_EXT_ID = null;
            newgt.CDGT_MODIFIED_ON = DateTime.Now;
            newgt.CDGT_MODIFED_BY = "LogiCAB Systeem";
            EFHelper.ctx.F_CAB_CD_GROUP_TYPE.AddObject(newgt);
            EFHelper.ctx.SaveChanges();

            try { 
                var req = (int)Session["currentrequest"];
                var reqObj = EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.FirstOrDefault(x => x.QCRQ_SEQUENCE == req);
                if (reqObj != null) {
                    reqObj.QCRQ_FK_CAB_GROUP_TYPE = newgt.CDGT_SEQUENCE;
                    EFHelper.ctx.SaveChanges();
                } else {
                    Log.ErrorFormat("Could not find current request [{0}]", req);
                    throw new NullReferenceException("Could not find request!");
                }
            } catch (Exception ex) {
                Log.ErrorFormat("An error occured while setting new GroupType for the request: {0}", ex.ToString());
                return "Error.";
            }
            return "Success";
            // do i need to add en entry in the mg-gt matrix?
        }
    }
}
