﻿using DevExpress.Web;
using DevExpress.Web.Mvc;
using LogiCAB.Web.DAL;
using LogiCAB.Web.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DevExpress.Web.Mvc.UI;
using log4net;
using CAB.Data.EF.Models;


namespace LogiCAB.Web.Controllers {
    public class TranslationsController : BaseController {
        public TranslationsController(ILog log) : base(log) { }

        public ActionResult Index() {
            var translations = _getTranslations();
            return View(translations);
        }

        public ActionResult List() {
            var translations = _getTranslations();
            return PartialView(translations);
        }

        private IEnumerable<F_CAB_TRANSLATION> _getTranslations() {
            return EFHelper.ctx.F_CAB_TRANSLATION.AsEnumerable();
        }
    }
}
