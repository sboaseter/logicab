﻿using CAB.Data.EF.Models;
using CAB.Domain.Models;
using CAB.Domain.Repositories;
using log4net;
using LogiCAB.Web.DAL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using DevExpress.Web.Mvc;
using LogiCAB.Web.Domain;

namespace LogiCAB.Web.Controllers {
    public class HandshakeAdminController : BaseController {
        private IOrganisationRepository _organisationRepository;
        private static List<HandshakeAdmin> handshakes;
        public HandshakeAdminController(IOrganisationRepository organisationRepository, ILog log)
            : base(log) {
            _organisationRepository = organisationRepository;
        }

        // Git test
        public ActionResult Index() {
            return View(_handshakes());
        }
        public ActionResult List() {

            return PartialView(_handshakes());
        }

        public ActionResult UpdateHandshake([ModelBinder(typeof(DevExpressEditorsBinder))] HandshakeAdmin model) {
            if (ModelState.IsValid) {
                try {
                    bool changed = false;
                    var handshake = EFHelper.ctx.F_CAB_HANDSHAKE.First(x => x.HAND_SEQUENCE == model.Sequence);
                    if (handshake.HAND_REQUEST_MAIL != model.Mail) changed = true;
                    if (handshake.HAND_REQUEST_REMARK != model.Remark) changed = true;
                    if (handshake.HAND_REQUEST_DATE != model.RequestDate) changed = true;
                    if (handshake.HAND_ACCEPT_DATE != model.AcceptDate) changed = true;
                    if (handshake.HAND_BLOCKED_YN != model.Blocked) changed = true;
                    if (handshake.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault().HNDO_AUTO_EKT_YN != model.Options.AutoEKT) changed = true;
                    if (handshake.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault().HNDO_AUTO_KWB_YN != model.Options.AutoKWB) changed = true;
                    handshake.HAND_REQUEST_MAIL = model.Mail ?? string.Empty;
                    handshake.HAND_REQUEST_REMARK = model.Remark ?? string.Empty;
                    handshake.HAND_REQUEST_DATE = model.RequestDate;
                    handshake.HAND_ACCEPT_DATE = model.AcceptDate ?? handshake.HAND_ACCEPT_DATE;
                    handshake.HAND_BLOCKED_YN = model.Blocked;
                    handshake.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault().HNDO_AUTO_EKT_YN = model.Options.AutoEKT;
                    handshake.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault().HNDO_AUTO_KWB_YN = model.Options.AutoKWB;
                    EFHelper.ctx.SaveChanges();
                    if (changed) {
                        handshakes = null; // Forces refetch.
                    }
                } catch (Exception ex) {
                    ViewData["EditError"] = ex.Message;
                }
            }
            return PartialView("List", _handshakes());
        }

        public ActionResult HandshakeEditPartial(int Id) {

            var handshake = handshakes.Find(x => x.Sequence == Id);
            handshake.Options = _handshakeOption(Id);
            return PartialView(handshake);
        }

        private HandshakeOption _handshakeOption(int id) {
            HandshakeOption ho;
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                var res = conn.Query<HandshakeOption>(@"
select 
HNDO_SEQUENCE as Sequence,
HNDO_FK_HAND_SEQ as Handshake,
HNDO_FK_ADDR_SEQ as [Address],
HNDO_FK_ORGA_SEQ as Transporter,
HNDO_AUTO_CONFIRM_YN as AutoConfirm,
HNDO_AUTO_EKT_YN as AutoEKT,
HNDO_AUTO_KWB_YN as AutoKWB,
HNDO_SEND_CONFIRM_MAIL_YN as SendConfirmMail,
HNDO_RECV_OFFER_MAIL_YN as ReceiveOfferMail,
HNDO_AUTO_PUBLISH_STOCK_YN as AutoPublishStock,
HNDO_AUTO_PUBLISH_OFFER_YN as AutoPublishOffer,
HNDO_LATEST_DELIVERY_TIME as LatestDeliveryTime,
HNDO_DELIVERY_STOCK_ALL_WORKDAYS_YN as DeliveryStockAllWorkdays,
HNDO_DELIVERY_OFFER_ALL_WORKDAYS_YN as DeliveryOfferallWorkdays
from F_CAB_HANDSHAKE_OPTIONS where HNDO_FK_HAND_SEQ = @Id", new { Id = id });
                if (res.Count() > 1) {
                    var hoList = res.OrderBy(x => x.Sequence).ToList();
                    for (int i = 0; i < hoList.Count - 1; i++) {
                        Log.InfoFormat("Deleting double HandshakeOption: {0}", hoList[i].Sequence);
                        var dRes = conn.Query("DELETE FROM F_CAB_HANDSHAKE_OPTIONS WHERE HNDO_SEQUENCE = @Sequence", new { Sequence = hoList[i].Sequence });
                    }
                    ho = hoList[hoList.Count - 1];
                }
                ho = res.FirstOrDefault();
                return ho;
            }
        }


        private List<HandshakeAdmin> _handshakes() {
            string cacheKey = "handshakeAdmin_list";
            if (CacheHelper.Get(cacheKey, out handshakes)) {
                return handshakes;

            }

            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                var res = conn.Query<HandshakeAdmin>(@"
SELECT
	HAND_SEQUENCE AS Sequence,
	HAND_REQUEST_MAIL AS Mail,
--	HAND_REQUEST_REMARK AS Remark,
	HAND_REQUEST_DATE AS RequestDate,
	HAND_ACCEPT_DATE AS AcceptDate,
	HAND_BLOCKED_YN AS Blocked,
--	HNDO_AUTO_EKT_YN AS AutoEKT,
--	HNDO_AUTO_KWB_YN AS AutoKWB,
	gOrga.ORGA_NAME AS Grower,
		gOrga.ORGA_SEQUENCE AS GrowerSequence,
	bOrga.ORGA_NAME AS Buyer,
		bOrga.ORGA_SEQUENCE AS BuyerSequence
		
FROM dbo.F_CAB_HANDSHAKE
--join F_CAB_HANDSHAKE_OPTIONS on HNDO_FK_HAND_SEQ = HAND_SEQUENCE
JOIN dbo.F_CAB_ORGANISATION AS gOrga ON gOrga.ORGA_SEQUENCE = HAND_FK_ORGA_SEQ_GROWER
--join F_CAB_ORGANISATION_ADMIN gOrgaAdmin on gOrga.ORGA_SEQUENCE = gOrgaAdmin.ORAD_FK_ORGANISATION
JOIN dbo.F_CAB_ORGANISATION bOrga ON bOrga.ORGA_SEQUENCE = HAND_FK_ORGA_SEQ_BUYER
--join F_CAB_ORGANISATION_ADMIN bOrgaAdmin on bOrga.ORGA_SEQUENCE = bOrgaAdmin.ORAD_FK_ORGANISATION
--where 
--	    gOrgaAdmin.ORAD_STATUS = 3 
--	and bOrgaAdmin.ORAD_STATUS = 3
--	and gOrgaAdmin.ORAD_MEMBERTYPE <> 0
--	and bOrgaAdmin.ORAD_MEMBERTYPE <> 0

");
                handshakes = res.ToList();
                CacheHelper.Add(handshakes, cacheKey, 10);
                return handshakes;
            }
        }
    }
}