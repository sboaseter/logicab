﻿using DevExpress.Web;
using DevExpress.Web.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Drawing;
using LogiCAB.Web.Domain;
using System.Net;
using System.Net.Mail;
using log4net;
using System.Reflection;
using CAB.Data.EF.Models;
using LogiCAB.Web.DAL;
using CAB.Domain.Helpers;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;

namespace LogiCAB.Web.Controllers {
    [Authorize]
    [HandleError]
    public class CABRequestController : Controller {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        //public CABRequestController(ILog log) : base(log) { }
        private UserInformation userInfo() {
            UserInformation userinfo = (UserInformation)Session["userinfo"];
            return userinfo;
        }

        public ActionResult Index() {
            return View(getCABRequestsAuthorizer());
        }

//        protected override void OnActionExecuting(ActionExecutingContext filterContext) {
//            Log.Info("OnActionExecuting triggered.");
//            base.OnActionExecuting(filterContext);
//        }

        public ActionResult Status() {
            return View(getCABRequestsGrower());
        }

        public ActionResult CaskComboPartial() {
            List<CAB.Domain.Models.Cask> casks = new List<CAB.Domain.Models.Cask>();
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                casks = conn.Query<CAB.Domain.Models.Cask>(@"
select 
	CDCA_SEQUENCE as Id, 
	CDCA_CASK_CODE as Code, 
	CDCA_CASK_DESC as [Desc], 
	CDCA_TRANSPORT_TYPE as Transport 
from F_CAB_CD_CASK
order by Code").ToList();
            }
            return PartialView(casks);
        }


        public ActionResult Req(int? id = null) {
            UserInformation userinfo = userInfo();
            if (userinfo == null)
                return RedirectToAction("Login", "User");

            userinfo.currentRequest = null;
            userinfo.RequestPictureSelected = false;
            Session["reqproperties"] = null;
            List<ReqProperties> sessionProps = (List<ReqProperties>)Session["reqproperties"];

            if (sessionProps == null) {
                IEnumerable props = getPropertiesForMG(Usr.SelectedMaingroup);
                List<ReqProperties> sProps = new List<ReqProperties>();
                foreach (ReqProperties r in props) {
                    sProps.Add(new ReqProperties {
                        desc = r.desc,
                        value = r.value,
                        required = r.required,
                        seq = r.seq
                    });
                }
                Session["reqproperties"] = sProps;
                ViewData["Properties"] = sProps;
            } else {
                ViewData["Properties"] = sessionProps;
            }

            if (id != null) {
                userinfo.currentRequest = EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.FirstOrDefault(x => x.QCRQ_SEQUENCE == id);
                ViewBag.requestID = id;
            }

            if (userinfo.currentRequest == null) {
                F_CAB_QUEUE_CODE_REQUEST newReq = new F_CAB_QUEUE_CODE_REQUEST {
                    QCRQ_FK_GROWER = userinfo.orgaTypeSequence,
                    QCRQ_FK_VBN_CODE = 1,
                    QCRQ_FK_CAB_GROUP_TYPE = 1,
                    QCRQ_REQUEST_DATE = DateTime.Now,
                    QCRQ_STATUS = "INCOMPLETE",
                    QCRQ_HIDDEN_YN = false
                };
                EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.AddObject(newReq);
                EFHelper.ctx.SaveChanges();
                userinfo.currentRequest = newReq;
            }
            ViewBag.messages = userinfo.messages;
            userinfo.messages = "";
            return View();
        }

        public ActionResult ReqRedo(int id) {
            UserInformation userinfo = userInfo();
            if (userinfo == null)
                return RedirectToAction("Login", "User");

            userinfo.currentRequest = null;
            Session["reqproperties"] = null;
            List<ReqProperties> sessionProps = (List<ReqProperties>)Session["reqproperties"];

            if (sessionProps == null) {
                IEnumerable props = getPropertiesForMG(userinfo.selectedMaingroup);
                List<ReqProperties> sProps = new List<ReqProperties>();
                foreach (ReqProperties r in props) {
                    sProps.Add(new ReqProperties {
                        desc = r.desc,
                        value = r.value,
                        required = r.required,
                        seq = r.seq
                    });
                }
                Session["reqproperties"] = sProps;
                ViewData["Properties"] = sProps;
            } else {
                ViewData["Properties"] = sessionProps;
            }
            userinfo.currentRequest = EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.FirstOrDefault(x => x.QCRQ_SEQUENCE == id);
            ViewBag.requestID = id;

            ViewBag.messages = userinfo.messages;
            userinfo.messages = "";
            Session["cab_request_type"] = "Redo";
            return View(userinfo.currentRequest);
        }

        public ActionResult VBNComboPartial() {
            return PartialView("Req/VBN");
        }

        public ActionResult PropComboPartial() {
            return PartialView("Req/PropertiesCombo");
        }

        public static IEnumerable<F_CAB_CD_CASK> getCasks() {
            return (from cdca in EFHelper.ctx.F_CAB_CD_CASK orderby cdca.CDCA_CASK_CODE select cdca).ToList();
        }

        public ActionResult CABRelatedPartial() {
            int vbnseq = !string.IsNullOrEmpty(Request.Params["vbnseq"]) ? int.Parse(Request.Params["vbnseq"]) : -1;
            return PartialView("Req/Related", getRelatedCABCodes(vbnseq));
        }

        public ActionResult CABRelatedPartialPaging() {
            int vbnseq = !string.IsNullOrEmpty(Request.Params["vbnseq"]) ? int.Parse(Request.Params["vbnseq"]) : -1;
            return PartialView("Req/CABRelatedPartial", getRelatedCABCodes(vbnseq));
        }


        public ActionResult FocusedRelatedCABPartial() {
            int cabseq = !string.IsNullOrEmpty(Request.Params["cabseq"]) ? int.Parse(Request.Params["cabseq"]) : -1;
            var retList = EFHelper.ctx.F_CAB_CD_CAB_CODE.Where(x => x.CABC_SEQUENCE == cabseq).ToList();
            return PartialView("Req/CABRelatedPartial", retList);

        }

        public static IEnumerable<F_CAB_CD_VBN_CODE_NUM_CAB> GetVBNRange(ListEditItemsRequestedByFilterConditionEventArgs args) {
            return getVBNList(args, false);
            #region oldVBNList
            /*            var skip = args.BeginIndex;
            var take = args.EndIndex - args.BeginIndex + 1;
            UserInformation userinfo = (UserInformation)System.Web.HttpContext.Current.Session["userinfo"];
            var mg = userinfo.selectedMaingroup;
            return (from vbn in EFHelper.ctx.F_CAB_CD_VBN_CODE
                    join vbng in EFHelper.ctx.F_CAB_CD_VBN_CODE_GROUP.Where(x => x.CDVG_MAINGROUP == mg)
                    on vbn.VBNC_FK_VBN_GROUP equals vbng.CDVG_SEQUENCE
                    where (vbn.VBNC_DESC).Contains(args.Filter) || (vbn.VBNC_VBN_CODE).Contains(args.Filter)
                    orderby vbn.F_CAB_CAB_VBN_MATRIX.Count(x => x.CVMA_FK_VBN_CODE == vbn.VBNC_SEQUENCE) descending
                    select new F_CAB_CD_VBN_CODE_NUM_CAB {
                        VBNC_SEQUENCE = vbn.VBNC_SEQUENCE,
                        VBNC_VBN_CODE = vbn.VBNC_VBN_CODE,
                        VBNC_DESC = vbn.VBNC_DESC,
                        numCabCodes = vbn.F_CAB_CAB_VBN_MATRIX.Count(x => x.CVMA_FK_VBN_CODE == vbn.VBNC_SEQUENCE)
                    }).Skip(skip).Take(take).ToList();*/
            #endregion

        }
        private static IEnumerable<F_CAB_CD_VBN_CODE_NUM_CAB> getVBNList(object argsObj, bool single) {
            //UserInformation userinfo = (UserInformation)System.Web.HttpContext.Current.Session["userinfo"];
            if (argsObj != null) {
                var query = EFHelper.ctx.F_CAB_CD_VBN_CODE.Where(z => z.F_CAB_CD_VBN_CODE_GROUP.CDVG_MAINGROUP == Usr.SelectedMaingroup)
                           .Select(x => new F_CAB_CD_VBN_CODE_NUM_CAB {
                               VBNC_SEQUENCE = x.VBNC_SEQUENCE,
                               VBNC_VBN_CODE = x.VBNC_VBN_CODE,
                               VBNC_DESC = x.VBNC_DESC,
                               numCabCodes = x.F_CAB_CAB_VBN_MATRIX.Count(y => y.CVMA_FK_VBN_CODE == x.VBNC_SEQUENCE)
                           });
                if (single) {
                    ListEditItemRequestedByValueEventArgs args = (ListEditItemRequestedByValueEventArgs)argsObj;
                    if (args.Value == null) return null;
                    return query.Where(x => x.VBNC_SEQUENCE == (int)args.Value).Take(1).ToList();
                } else {
                    ListEditItemsRequestedByFilterConditionEventArgs args = (ListEditItemsRequestedByFilterConditionEventArgs)argsObj;
                    var skip = args.BeginIndex;
                    var take = args.EndIndex - args.BeginIndex + 1;
                    return query.Where(x => x.VBNC_DESC.Contains(args.Filter) || x.VBNC_VBN_CODE.Contains(args.Filter))
                                .OrderByDescending(y => y.numCabCodes)
                                .Skip(skip)
                                .Take(take)
                                .ToList();
                }
            }
            return null;
        }
        public static IEnumerable<F_CAB_CD_VBN_CODE_NUM_CAB> GetVBNByID(ListEditItemRequestedByValueEventArgs args) {
            return getVBNList(args, true);
            #region oldVBNSingle
            /*
            if (args.Value != null) {
                int id = (int)args.Value;
                return (from vbn in EFHelper.ctx.F_CAB_CD_VBN_CODE
                        where vbn.VBNC_SEQUENCE == id
                        select new F_CAB_CD_VBN_CODE_NUM_CAB {
                            VBNC_SEQUENCE = vbn.VBNC_SEQUENCE,
                            VBNC_VBN_CODE = vbn.VBNC_VBN_CODE,
                            VBNC_DESC = vbn.VBNC_DESC,
                            numCabCodes = vbn.F_CAB_CAB_VBN_MATRIX.Count(x => x.CVMA_FK_VBN_CODE == vbn.VBNC_SEQUENCE)
                        }).Take(1).ToList();
            }
            return null;
 */
            #endregion
        }

        public static IEnumerable<Cask> getCabCasks(int cabseq) {
            return (from caca in EFHelper.ctx.F_CAB_CAB_CASK_MATRIX.Where(x => x.CACA_FK_CAB_CODE == cabseq)
                    join cdca in EFHelper.ctx.F_CAB_CD_CASK
                        on caca.CACA_FK_CASK equals cdca.CDCA_SEQUENCE
                    select new Cask {
                        //seq = cdca.CDCA_SEQUENCE,
                        seq = caca.CACA_SEQUENCE,
                        showcask = true,
                        caskcode = cdca.CDCA_CASK_CODE,
                        caskdesc = cdca.CDCA_CASK_DESC,
                        cask_prunit = caca.CACA_NUM_OF_UNITS,
                        cask_prlayer = caca.CACA_UNITS_PER_LAYER,
                        cask_prtrolley = caca.CACA_LAYERS_PER_TROLLEY
                    }).ToList();
        }

        public IEnumerable<Cask> getCabCasksPaging(int cabseq) {
            return getCabCasks(cabseq);

        }

        public static IEnumerable<RequestCask> getRequestCasks(int reqSeq, bool init) {
            UserInformation userinfo = (UserInformation)System.Web.HttpContext.Current.Session["userinfo"];
            string requestType = "New";
            try {
                requestType = (string)System.Web.HttpContext.Current.Session["cab_request_type"];
            } catch {
            }
            int cab_seq = -1;
            try {
                cab_seq = Int32.Parse((string)System.Web.HttpContext.Current.Session["cab_seq"]);
            } catch {
            }
            var requestCasks = (from qcrc in EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_CASK.Where(x => x.QCRC_FK_CODE_REQUEST == reqSeq) select qcrc).ToList();
            if (cab_seq != -1 && requestType == "TakeProperties") { // && type = ?
                IEnumerable<Cask> cabcasks = getCabCasks(cab_seq);
                var cabCasksList = (from c in cabcasks
                                    select new F_CAB_QUEUE_CODE_REQ_CASK {
                                        QCRC_FK_CASK = Int32.Parse(c.caskcode),
                                        QCRC_FK_CODE_REQUEST = userinfo.currentRequest.QCRQ_SEQUENCE,
                                        QCRC_LAYERS_PER_TROLLEY = c.cask_prtrolley,
                                        QCRC_NUM_OF_UNITS = c.cask_prunit,
                                        QCRC_UNITS_PER_LAYER = c.cask_prlayer,
                                        QCRC_SEQUENCE = c.seq
                                    }).ToList();
                var deleteCaskSession = System.Web.HttpContext.Current.Session["deleted_cask"];
                bool deleteIntent = false;
                if (deleteCaskSession != null) {
                    deleteIntent = (bool)deleteCaskSession;
                }
                if (!deleteIntent) {
                    foreach (F_CAB_QUEUE_CODE_REQ_CASK c1 in cabCasksList) {

                        if (!requestCasks.Exists(x =>
                            x.F_CAB_CD_CASK.CDCA_CASK_CODE == c1.QCRC_FK_CASK.ToString() &&
                            x.QCRC_FK_CODE_REQUEST == c1.QCRC_FK_CODE_REQUEST &&
                            x.QCRC_LAYERS_PER_TROLLEY == c1.QCRC_LAYERS_PER_TROLLEY &&
                            x.QCRC_NUM_OF_UNITS == c1.QCRC_NUM_OF_UNITS &&
                            x.QCRC_UNITS_PER_LAYER == c1.QCRC_UNITS_PER_LAYER)) {
                            string caskCode = c1.QCRC_FK_CASK.ToString();
                            c1.QCRC_FK_CASK = EFHelper.ctx.F_CAB_CD_CASK.Where(x => x.CDCA_CASK_CODE == caskCode).FirstOrDefault().CDCA_SEQUENCE;
                            EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_CASK.AddObject(c1);
                        }
                    }
                }
                try {
                    EFHelper.ctx.SaveChanges();
                } catch (Exception ex) {
                    Log.ErrorFormat("Problems saving new Cask-combinations: {0}", ex.ToString());
                }
                requestCasks = (from qcrc in EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_CASK.Where(x => x.QCRC_FK_CODE_REQUEST == reqSeq) select qcrc).ToList();


            }
            return (from c in requestCasks
                    select new RequestCask {
                        seq = c.QCRC_SEQUENCE,
                        caskcode = c.F_CAB_CD_CASK.CDCA_CASK_CODE,
                        caskdesc = c.F_CAB_CD_CASK.CDCA_CASK_DESC,
                        CDCA_SEQUENCE = c.QCRC_FK_CASK,
                        cask_prunit = c.QCRC_NUM_OF_UNITS,
                        cask_prlayer = c.QCRC_UNITS_PER_LAYER,
                        cask_prtrolley = c.QCRC_LAYERS_PER_TROLLEY
                    }).ToList();
            return null;

        }

        public ActionResult RequestCasksAdd([ModelBinder(typeof(DevExpressEditorsBinder))] RequestCask cask) {
            UserInformation userinfo = (UserInformation)Session["userinfo"];

            EFHelper.ctx.SaveChanges();
            return PartialView("Req/Casks", getRequestCasks(userinfo.currentRequest.QCRQ_SEQUENCE, false));

        }

        public ActionResult RequestCasksDelete([ModelBinder(typeof(DevExpressEditorsBinder))] RequestCask cask) {
            UserInformation userinfo = (UserInformation)Session["userinfo"];
            Session["deleted_cask"] = true;
            EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_CASK.DeleteObject(EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_CASK.FirstOrDefault(x => x.QCRC_SEQUENCE == cask.seq));
            EFHelper.ctx.SaveChanges();
            return PartialView("Req/Casks", getRequestCasks(userinfo.currentRequest.QCRQ_SEQUENCE, false));

        }

        [HttpPost]
        public string addNewCaskToRequest(int cdca_seq, int prcask, int prlayer, int prtrolley) {

            try {
                UserInformation userinfo = (UserInformation)Session["userinfo"];
                F_CAB_CD_CASK cask = EFHelper.ctx.F_CAB_CD_CASK.FirstOrDefault(x => x.CDCA_SEQUENCE == cdca_seq);
                if (cask == null) return "Invalid CDCA_SEQUENCE";
                F_CAB_QUEUE_CODE_REQ_CASK newCask = new F_CAB_QUEUE_CODE_REQ_CASK {
                    QCRC_FK_CODE_REQUEST = userinfo.currentRequest.QCRQ_SEQUENCE,
                    F_CAB_CD_CASK = cask,
                    QCRC_NUM_OF_UNITS = prcask,
                    QCRC_UNITS_PER_LAYER = prlayer,
                    QCRC_LAYERS_PER_TROLLEY = prtrolley,
                };
                EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_CASK.AddObject(newCask);
                EFHelper.ctx.SaveChanges();
                return "";
            } catch {
                return "Error";

            }

        }

        public ActionResult RequestCasks() {
            UserInformation userinfo = (UserInformation)Session["userinfo"];
            return PartialView("Req/Casks", getRequestCasks(userinfo.currentRequest.QCRQ_SEQUENCE, false));
        }

        public static IEnumerable<F_CAB_CAB_DETAIL> getCabProperties(int cabseq) {
            return (from cabd in EFHelper.ctx.F_CAB_CAB_DETAIL.Where(x => x.CABD_FK_CAB_CODE == cabseq) select cabd).ToList();
        }

        public static IEnumerable<F_CAB_QUEUE_CODE_REQ_DETAIL> getCabRequestProperties(int reqseq) {
            //return (from cabd in EFHelper.ctx.F_CAB_CAB_DETAIL.Where(x => x.CABD_FK_CAB_CODE == cabseq) select cabd).ToList();
            return (from qcrd in EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.Where(x => x.QCRD_FK_CODE_REQUEST == reqseq) select qcrd).ToList();
        }

        public static IEnumerable<F_CAB_CD_CAB_CODE> getRelatedCABCodes(int vbnseq) {

            var listofcodes = (from cvma in EFHelper.ctx.F_CAB_CAB_VBN_MATRIX.Where(x => x.CVMA_FK_VBN_CODE == vbnseq)
                               select cvma.F_CAB_CD_CAB_CODE).ToList();
            return listofcodes;

        }

        public ActionResult ListRequester() {
            return PartialView("ListRequester", getCABRequestsGrower());
        }


        public ActionResult ListAuthorizer() {
            return PartialView("ListAuthorizer", getCABRequestsAuthorizer());
        }

        public ActionResult Properties(int id) {

            var props = (from p in EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.Where(x => x.QCRD_SEQUENCE == id)
                         select new {
                             seq = p.QCRD_SEQUENCE,
                             proptype = p.F_CAB_CD_CAB_PROP_TYPE.CDPT_DESC,
                             propvalue = p.QCRD_PROP_VALUE,
                             proprequired = p.F_CAB_CD_CAB_PROP_TYPE.CDPT_MANDATORY_YN
                         }).ToList();
            return View("ListAuthorizer", props);

        }

        public ActionResult getPropertiesForMGAction() {
            UserInformation userinfo = (UserInformation)Session["userinfo"];
            List<ReqProperties> sessionProps = (List<ReqProperties>)Session["reqproperties"];

            if (sessionProps == null) {
                IEnumerable props = getPropertiesForMG(userinfo.selectedMaingroup);
                List<ReqProperties> sProps = new List<ReqProperties>();
                foreach (ReqProperties r in props) {
                    sProps.Add(new ReqProperties {
                        desc = r.desc,
                        value = r.value,
                        required = r.required,
                        seq = r.seq
                    });
                }
                Session["reqproperties"] = sProps;
            }
            string requestType = "New";
            try {
                requestType = (string)Session["cab_request_type"];
            } catch {
            }
            int cab_seq = -1;
            try {
                cab_seq = Int32.Parse((string)Session["cab_seq"]);
            } catch {
            }

            if (cab_seq != -1 && requestType == "TakeProperties") {
                sessionProps = (List<ReqProperties>)Session["reqproperties"];
                IEnumerable<F_CAB_CAB_DETAIL> cabprops = getCabProperties(cab_seq);

                List<ReqProperties> sProps = new List<ReqProperties>();
                foreach (F_CAB_CAB_DETAIL r in cabprops) {
                    ReqProperties reqprop = sessionProps.FirstOrDefault(x => x.seq == r.CABD_FK_TYPE);
                    if (reqprop != null) {
                        int propSeq = reqprop.seq;
                        sProps.Add(new ReqProperties {
                            desc = r.F_CAB_CD_CAB_PROP_TYPE.CDPT_DESC,
                            value = r.CABD_VALUE,
                            required = sessionProps.FirstOrDefault(x => x.seq == r.CABD_FK_TYPE).required,
                            //seq = r.CABD_SEQUENCE
                            seq = propSeq
                        });
                        var details = EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.Where(x => x.QCRD_FK_CODE_REQUEST == userinfo.currentRequest.QCRQ_SEQUENCE && x.QCRD_FK_CAB_PROP_TYPE == propSeq).FirstOrDefault();
                        if (details == null) {
                            F_CAB_QUEUE_CODE_REQ_DETAIL newDetail = new F_CAB_QUEUE_CODE_REQ_DETAIL {
                                QCRD_FK_CODE_REQUEST = userinfo.currentRequest.QCRQ_SEQUENCE,
                                QCRD_FK_CAB_PROP_TYPE = r.CABD_FK_TYPE,
                                QCRD_PROP_VALUE = r.CABD_VALUE,
                                QCRD_PROP_DESC = r.F_CAB_CD_CAB_PROP_TYPE.CDPT_DESC,
                                QCRD_ASS_PROP_VALUE = r.CABD_VALUE,
                                QCRD_ASS_PROP_DESC = r.CABD_VALUE
                            };
                            EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.AddObject(newDetail);
//                            EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.DeleteObject(newDetail);
                        } else {
                            details.QCRD_PROP_VALUE = sProps.FirstOrDefault(x => x.seq == propSeq).value;
                            EFHelper.ctx.SaveChanges(); // TODO: Check Logic.
                            //sProps.FirstOrDefault(x => x.seq == propSeq).value = details.QCRD_PROP_VALUE;
                        }
                    }
                }
                EFHelper.ctx.SaveChanges();
                Session["reqproperties"] = sProps;
            } else if (requestType == "New") {
                sessionProps = (List<ReqProperties>)Session["reqproperties"];
                IEnumerable props = getPropertiesForMG(userinfo.selectedMaingroup);
                List<ReqProperties> sProps = new List<ReqProperties>();
                foreach (ReqProperties r in props) {
                    int propSeq = sessionProps.FirstOrDefault(x => x.desc == r.desc).seq;
                    var details = EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.Where(x => x.QCRD_FK_CODE_REQUEST == userinfo.currentRequest.QCRQ_SEQUENCE && x.QCRD_FK_CAB_PROP_TYPE == propSeq).FirstOrDefault();

                    sProps.Add(new ReqProperties {
                        desc = r.desc,
                        value = r.value,
                        required = r.required,
                        seq = r.seq
                    });
                    if (details != null) {
                        sProps.FirstOrDefault(x => x.seq == propSeq).value = details.QCRD_PROP_VALUE;
                    }
                }
                Session["reqproperties"] = sProps;
            } else if (requestType == "Redo") {
                sessionProps = (List<ReqProperties>)Session["reqproperties"];
                UserInformation user = userInfo();
                IEnumerable<F_CAB_QUEUE_CODE_REQ_DETAIL> cabprops = getCabRequestProperties(user.currentRequest.QCRQ_SEQUENCE);
                List<ReqProperties> sProps = new List<ReqProperties>();
                foreach (F_CAB_QUEUE_CODE_REQ_DETAIL r in cabprops) {
                    ReqProperties reqprop = sessionProps.FirstOrDefault(x => x.desc == r.F_CAB_CD_CAB_PROP_TYPE.CDPT_DESC);
                    if (reqprop != null) {
                        int propSeq = reqprop.seq;
                        sProps.Add(new ReqProperties {
                            desc = r.F_CAB_CD_CAB_PROP_TYPE.CDPT_DESC,
                            value = r.QCRD_PROP_VALUE,
                            required = sessionProps.FirstOrDefault(x => x.desc == r.F_CAB_CD_CAB_PROP_TYPE.CDPT_DESC).required,
                            //seq = r.CABD_SEQUENCE
                            seq = propSeq
                        });
                        #region deprechated
                        /*                        var details = EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.Where(x => x.QCRD_FK_CODE_REQUEST == userinfo.currentRequest.QCRQ_SEQUENCE && x.QCRD_FK_CAB_PROP_TYPE == propSeq).FirstOrDefault();
                        if (details == null) {
                            F_CAB_QUEUE_CODE_REQ_DETAIL newDetail = new F_CAB_QUEUE_CODE_REQ_DETAIL {
                                QCRD_FK_CODE_REQUEST = userinfo.currentRequest.QCRQ_SEQUENCE,
                                QCRD_FK_CAB_PROP_TYPE = sessionProps.FirstOrDefault(x => x.desc == r.F_CAB_CD_CAB_PROP_TYPE.CDPT_DESC).seq,
                                QCRD_PROP_VALUE = r.CABD_VALUE,
                                QCRD_PROP_DESC = r.F_CAB_CD_CAB_PROP_TYPE.CDPT_DESC,
                                QCRD_ASS_PROP_VALUE = r.CABD_VALUE,
                                QCRD_ASS_PROP_DESC = r.CABD_VALUE
                            };
                            EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.DeleteObject(newDetail);
                        } else {
                            details.QCRD_PROP_VALUE = sProps.FirstOrDefault(x => x.seq == propSeq).value;
                            EFHelper.ctx.SaveChanges(); // TODO: Check Logic.
                            //sProps.FirstOrDefault(x => x.seq == propSeq).value = details.QCRD_PROP_VALUE;
                        }
 */
                        #endregion
                    }
                }
                EFHelper.ctx.SaveChanges();
                Session["reqproperties"] = sProps;
                return PartialView("ReqRedo/Properties", (List<ReqProperties>)Session["reqproperties"]);
            }
            return PartialView("Req/Properties", (List<ReqProperties>)Session["reqproperties"]);

        }
        public ActionResult getPropertiesForMGActionUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] ReqProperties model) {

            // merge also needed?

            UserInformation userinfo = (UserInformation)Session["userinfo"];

            var details = EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.Where(x => x.QCRD_FK_CODE_REQUEST == userinfo.currentRequest.QCRQ_SEQUENCE && x.QCRD_FK_CAB_PROP_TYPE == model.seq).FirstOrDefault();
            if (details == null && model.value != null) {

                F_CAB_QUEUE_CODE_REQ_DETAIL newDetail = new F_CAB_QUEUE_CODE_REQ_DETAIL {
                    QCRD_FK_CODE_REQUEST = userinfo.currentRequest.QCRQ_SEQUENCE,
                    QCRD_FK_CAB_PROP_TYPE = model.seq,
                    QCRD_PROP_VALUE = model.value,
                    QCRD_PROP_DESC = model.desc,
                    QCRD_ASS_PROP_VALUE = model.value,
                    QCRD_ASS_PROP_DESC = model.desc
                };
                EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.AddObject(newDetail);
                EFHelper.ctx.SaveChanges();
            } else {
                if (details.QCRD_PROP_VALUE != model.value && details.QCRD_FK_CAB_PROP_TYPE == model.seq) {
                    details.QCRD_PROP_VALUE = model.value;
                    details.QCRD_PROP_DESC = model.desc;
                    details.QCRD_ASS_PROP_VALUE = model.value;
                    details.QCRD_ASS_PROP_DESC = model.desc;
                    EFHelper.ctx.SaveChanges();
                }
            }

            List<ReqProperties> sessionProps = (List<ReqProperties>)Session["reqproperties"];
            foreach (ReqProperties r in sessionProps)
                if (r.desc == model.desc && model.value != null)
                    r.value = model.value;
            Session["reqproperties"] = sessionProps;
            return PartialView("Req/Properties", (List<ReqProperties>)Session["reqproperties"]);

        }

        public static IEnumerable getProperties(int id) {

            var props = (from p in EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.Where(x => x.QCRD_FK_CODE_REQUEST == id)
                         select new {
                             seq = p.QCRD_SEQUENCE,
                             proptype = p.F_CAB_CD_CAB_PROP_TYPE.CDPT_DESC,
                             propvalue = p.QCRD_PROP_VALUE,
                             proprequired = p.F_CAB_CD_CAB_PROP_TYPE.CDPT_MANDATORY_YN
                         }).ToList();
            return props;

        }

        public static IEnumerable getPropertiesForMG(int id) {
            int maingroup = (int)System.Web.HttpContext.Current.Session["maingroup"];
            var props = (from p in EFHelper.ctx.F_CAB_CD_CAB_PROP_TYPE.Where(x => x.CDPT_DESC_PREFIX != null)
                         join mgpt in EFHelper.ctx.F_CAB_PROP_MAIN_GROUP_MATRIX.Where(x => x.MGPT_FK_MAIN_GROUP == maingroup) on p.CDPT_SEQUENCE equals mgpt.MGPT_FK_PROP_TYPE
                         select new ReqProperties {
                             seq = p.CDPT_SEQUENCE,
                             desc = p.CDPT_DESC,
                             value = "",
                             required = p.CDPT_MANDATORY_YN
                         }).ToList();

            return props;

        }


        public static IEnumerable getPropertiesForPropType(int id) {
            //select * from F_CAB_CD_CAB_PROP_LOOKUP where CDPL_FK_PROP_TYPE = 6

            var props = (from p in EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.Where(x => x.CDPL_FK_PROP_TYPE == id)
                         select new LookupProperty {
                             seq = p.CDPL_SEQUENCE,
                             value = p.CDPL_LOOKUP_VALUE,
                             //desc = p.CDPL_LOOKUP_DESC
                             desc = p.CDPL_LOOKUP_VALUE
                         }).ToList();
            return props;

        }

        public ActionResult getPropertiesForPropTypeAction(int id) {
            return PartialView("Req/Properties", getPropertiesForPropType(id));
        }

        public static IEnumerable<LookupProperty> GetPropertyRange(ListEditItemsRequestedByFilterConditionEventArgs args) {
            int id = !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Params["proptype"]) ? int.Parse(System.Web.HttpContext.Current.Request.Params["proptype"]) : -1;

            var skip = args.BeginIndex;
            var take = args.EndIndex - args.BeginIndex + 1;
            return (from p in EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.Where(x => x.CDPL_FK_PROP_TYPE == id && (x.CDPL_LOOKUP_VALUE).Contains(args.Filter))
                    select new LookupProperty {
                        seq = p.CDPL_SEQUENCE,
                        value = p.CDPL_LOOKUP_VALUE
//                        desc = p.CDPL_LOOKUP_DESC
                    }).ToList().Skip(skip).Take(take);

        }
        public static IEnumerable<LookupProperty> GetPropertyByID(ListEditItemRequestedByValueEventArgs args) {
            int id = !string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Params["proptype"]) ? int.Parse(System.Web.HttpContext.Current.Request.Params["proptype"]) : -1;

            if (args.Value != null) {
                //int id = (int)args.Value;
                return (from p in EFHelper.ctx.F_CAB_CD_CAB_PROP_LOOKUP.Where(x => x.CDPL_FK_PROP_TYPE == id)
                        select new LookupProperty {
                            seq = p.CDPL_SEQUENCE,
                            value = p.CDPL_LOOKUP_VALUE
//                            desc = p.CDPL_LOOKUP_DESC
                        }).Take(1).ToList();
            }
            return null;

        }

        public ActionResult UpdateCABRequest([ModelBinder(typeof(DevExpressEditorsBinder))] CABRequest model) {
            return PartialView("ListAuthorizer", getCABRequestsAuthorizer());

        }

        public static IEnumerable getVBNList() {
            // Used?
            return (from vbn in EFHelper.ctx.F_CAB_CD_VBN_CODE select vbn).ToList();
        }

        private IEnumerable getCABRequestsAuthorizer() {
            return (from req in EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.Where(x =>
                x.QCRQ_CAB_DESC != null &&
                x.QCRQ_FK_CAB_GROUP_TYPE != null &&
                x.QCRQ_FK_CAB_GT_ASSIGNED != null &&
                x.QCRQ_CAB_DESC_ASSIGNED != null &&
                x.QCRQ_FK_VBN_CODE != null
                )
                    select new CABRequest {
                        request_date = req.QCRQ_REQUEST_DATE,
                        status = req.QCRQ_STATUS,
                        vbn = req.F_CAB_CD_VBN_CODE,
                        requester = req.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_NAME,
                        cabdesc = req.QCRQ_CAB_DESC,
                        cabgroup = req.F_CAB_CD_GROUP_TYPE.CDGT_DESC,
                        handledby = req.QCRQ_HANDLED_BY,
                        handledon = req.QCRQ_HANDLED_DATE,
                        remark = req.QCRQ_REMARK,
                        seq = req.QCRQ_SEQUENCE,
                        pictureseq = req.QCRQ_FK_PICTURE
                    }).OrderByDescending(x => x.request_date).ToList();
        }
        private IEnumerable getCABRequestsGrower() {
//            UserInformation userinfo = (UserInformation)Session["userinfo"];

            var incompleteRequests = EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.Where(x => x.QCRQ_FK_VBN_CODE == 1);
            if (incompleteRequests.Count() > 0) {
                List<F_CAB_QUEUE_CODE_REQUEST> requestsToRemove = new List<F_CAB_QUEUE_CODE_REQUEST>();
                List<F_CAB_QUEUE_CODE_REQ_CASK> casksToRemove = new List<F_CAB_QUEUE_CODE_REQ_CASK>();
                List<F_CAB_QUEUE_CODE_REQ_DETAIL> detailsToRemove = new List<F_CAB_QUEUE_CODE_REQ_DETAIL>();
                foreach (var icr in incompleteRequests) {
                    foreach (var icrCask in icr.F_CAB_QUEUE_CODE_REQ_CASK)
                        casksToRemove.Add(icrCask);
                    foreach (var icrDetails in icr.F_CAB_QUEUE_CODE_REQ_DETAIL)
                        detailsToRemove.Add(icrDetails);
                    requestsToRemove.Add(icr);
                }
                for (int i = 0; i < casksToRemove.Count; i++)
                    EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_CASK.DeleteObject(casksToRemove[i]);
                for (int i = 0; i < detailsToRemove.Count; i++)
                    EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.DeleteObject(detailsToRemove[i]);
                for (int i = 0; i < requestsToRemove.Count; i++)
                    EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.DeleteObject(requestsToRemove[i]);
                EFHelper.ctx.SaveChanges();
            }

            return (from req in EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.Where(x =>
                x.QCRQ_FK_GROWER == Usr.OrgaTypeSequence &&
                x.QCRQ_CAB_DESC != null &&
                x.QCRQ_FK_CAB_GROUP_TYPE != null &&
                x.QCRQ_FK_CAB_GT_ASSIGNED != null &&
                x.QCRQ_CAB_DESC_ASSIGNED != null &&
                x.QCRQ_FK_VBN_CODE != null &&
                x.F_CAB_CD_GROUP_TYPE.CDGT_FK_MAIN_GROUP == Usr.SelectedMaingroup &&
                x.QCRQ_HIDDEN_YN != true
                )
                    select new CABRequest {
                        request_date = req.QCRQ_REQUEST_DATE,
                        status = req.QCRQ_STATUS,
                        vbn = req.F_CAB_CD_VBN_CODE,
                        requester = req.F_CAB_GROWER.F_CAB_ORGANISATION.ORGA_NAME,
                        cabdesc = req.QCRQ_CAB_DESC,
                        cabgroup = req.F_CAB_CD_GROUP_TYPE.CDGT_DESC,
                        handledby = req.QCRQ_HANDLED_BY,
                        handledon = req.QCRQ_HANDLED_DATE,
                        remark = req.QCRQ_REMARK,
                        seq = req.QCRQ_SEQUENCE,
                        pictureseq = req.QCRQ_FK_PICTURE
                    }).OrderByDescending(x => x.request_date).ToList();

        }

        public int saveVBNChoice(int vbn_seq) {
            UserInformation userinfo = (UserInformation)Session["userinfo"];
            userinfo.currentRequest.QCRQ_FK_VBN_CODE = vbn_seq;
            getRequest().QCRQ_FK_VBN_CODE = vbn_seq;
            getVBNPicture();
            userinfo.RequestPictureSelected = true;
            EFHelper.ctx.SaveChanges();
            Session["vbn_seq"] = vbn_seq;

            return getRequest().QCRQ_FK_PICTURE ?? -1;

//            return null;

        }

        public int setVKCPhoto()
        {
            UserInformation userinfo = (UserInformation)Session["userinfo"];
            var request = userinfo.currentRequest;
            try
            {
                var vbnseq = request.QCRQ_FK_VBN_CODE;
                string vkcurl = String.Format("http://www.logicab.nl/logicab/vkc/{0}.jpg", ImageUtils.getVKCImage(EFHelper.ctx.F_CAB_CD_VBN_CODE.FirstOrDefault(x => x.VBNC_SEQUENCE == vbnseq).VBNC_VBN_CODE));
                var webClient = new WebClient();
                byte[] imageBytes = webClient.DownloadData(vkcurl);
                MemoryStream ms = new MemoryStream(imageBytes);
                Image tempImg = Image.FromStream(ms);
                Image newImage = ImageUtils.resizeImage(tempImg, new Size { Height = 1024, Width = 1024 });
                MemoryStream ms2 = new MemoryStream();
                newImage.Save(ms2, ImageFormat.Jpeg);
                F_CAB_PICTURE newPic = new F_CAB_PICTURE
                {
                    PICT_PICTURE = ms2.ToArray()
                };
                EFHelper.ctx.F_CAB_PICTURE.AddObject(newPic);
                EFHelper.ctx.SaveChanges();
                request.QCRQ_FK_PICTURE = newPic.PICT_SEQUENCE;
                using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString))
                {
                    conn.Open();
                    var upd = conn.Query(@"
                        UPDATE F_CAB_QUEUE_CODE_REQUEST SET QCRQ_FK_PICTURE = @PicSeq WHERE QCRQ_SEQUENCE = @ReqSeq",
                        new { PicSeq = newPic.PICT_SEQUENCE, ReqSeq = request.QCRQ_SEQUENCE });

                }

                EFHelper.ctx.SaveChanges();
                /* TODO: Delete the following statement when QCRQ_PICTURE is removed from Live */
                //request.QCRQ_PICTURE = newPic.PICT_PICTURE;
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Failed downloading VKC picture for VBNCode: {0}", request.QCRQ_FK_VBN_CODE);
                var cValue = String.Format("vknc: {0} missing", request.QCRQ_FK_VBN_CODE);
                DateTime cOn = DateTime.Now;
                F_CAB_LOG newLog = new F_CAB_LOG
                {
                    CALO_ACTION = "LOGICAB",
                    CALO_FK_LOGIN = 7,
                    CALO_VALUE = cValue,
                    CALO_CREATED_ON = cOn
                };
                EFHelper.ctx.F_CAB_LOG.AddObject(newLog);
                EFHelper.ctx.SaveChanges();
            }
            return request.QCRQ_FK_PICTURE ?? -1;


        }

        private F_CAB_QUEUE_CODE_REQUEST getRequest() {
            UserInformation userinfo = (UserInformation)Session["userinfo"];
            return EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.Single(x => x.QCRQ_SEQUENCE == userinfo.currentRequest.QCRQ_SEQUENCE);
        }

        public ActionResult saveCABDesc(string cab_desc) {
            UserInformation userinfo = (UserInformation)Session["userinfo"];

            userinfo.currentRequest.QCRQ_CAB_DESC = cab_desc;
            userinfo.currentRequest.QCRQ_CAB_DESC_ASSIGNED = cab_desc;
            getRequest().QCRQ_CAB_DESC = cab_desc;
            getRequest().QCRQ_CAB_DESC_ASSIGNED = cab_desc;
            EFHelper.ctx.SaveChanges();
            return null;

        }

        public string getCABRequestDesc() {
            UserInformation user = userInfo();
            return user.currentRequest.QCRQ_CAB_DESC;
        }

        public ActionResult saveCABCode(string cab_seq) {
            UserInformation userinfo = (UserInformation)Session["userinfo"];
            int iCabSeq = Int32.Parse(cab_seq);
            var selectedCabCode = EFHelper.ctx.F_CAB_CD_CAB_CODE.FirstOrDefault(x => x.CABC_SEQUENCE == iCabSeq);
            EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.Single(x => x.QCRQ_SEQUENCE == userinfo.currentRequest.QCRQ_SEQUENCE).QCRQ_FK_CAB_GROUP_ASSIGNED = selectedCabCode.CABC_FK_CAB_GROUP;
            EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.Single(x => x.QCRQ_SEQUENCE == userinfo.currentRequest.QCRQ_SEQUENCE).QCRQ_FK_PD_GROUP_ASSIGNED = selectedCabCode.CABC_FK_PD_GROUP;
            EFHelper.ctx.SaveChanges();
            userinfo.currentRequest.QCRQ_FK_CAB_GROUP_ASSIGNED = selectedCabCode.CABC_FK_CAB_GROUP;
            userinfo.currentRequest.QCRQ_FK_PD_GROUP_ASSIGNED = selectedCabCode.CABC_FK_PD_GROUP;
            EFHelper.ctx.SaveChanges();
            Session["cab_seq"] = cab_seq;
            return null;
        }

        public string getCABDesc(int seq) {

            return EFHelper.ctx.F_CAB_CD_CAB_CODE.FirstOrDefault(x => x.CABC_SEQUENCE == seq).CABC_CAB_DESC;

        }

        public ActionResult saveCABRequestType(string type) {
            UserInformation userinfo = (UserInformation)Session["userinfo"];
            Session["cab_request_type"] = type;
            if (type == "New") {
                Session["reqproperties"] = null;
                foreach (F_CAB_QUEUE_CODE_REQ_DETAIL det in EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.Where(x => x.QCRD_FK_CODE_REQUEST == userinfo.currentRequest.QCRQ_SEQUENCE)) {
                    EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.DeleteObject(det);
                }
                EFHelper.ctx.SaveChanges();
            }
            if (type == "TakeProperties") {
                int cabseq = Int32.Parse((string)Session["cab_seq"]);
                F_CAB_CD_CAB_CODE cabcode = EFHelper.ctx.F_CAB_CD_CAB_CODE.Single(x => x.CABC_SEQUENCE == cabseq);
                userinfo.currentRequest.QCRQ_FK_PICTURE = cabcode.CABC_FK_PICTURE;
                getRequest().QCRQ_FK_PICTURE = cabcode.CABC_FK_PICTURE;
                EFHelper.ctx.SaveChanges();
            }
            return null;
        }

        public ActionResult saveGroupType(int cdgt_seq) {
            UserInformation userinfo = (UserInformation)Session["userinfo"];

            userinfo.currentRequest.QCRQ_FK_CAB_GROUP_TYPE = cdgt_seq;
            userinfo.currentRequest.QCRQ_FK_CAB_GT_ASSIGNED = cdgt_seq;
            getRequest().QCRQ_FK_CAB_GROUP_TYPE = cdgt_seq;
            getRequest().QCRQ_FK_CAB_GT_ASSIGNED = cdgt_seq;
            EFHelper.ctx.SaveChanges();
            return null;

        }

        public ActionResult saveGrowerRemark(string remark) {
            UserInformation userinfo = (UserInformation)Session["userinfo"];
            userinfo.currentRequest.QCRQ_GROWER_REMARK = remark;
            getRequest().QCRQ_GROWER_REMARK = remark;
            EFHelper.ctx.SaveChanges();
            return null;
        }
        public string getGrowerRemark() {
            UserInformation user = userInfo();
            return user.currentRequest.QCRQ_GROWER_REMARK;
        }

        public string getRequestredoImage() {
            UserInformation user = userInfo();
            var req = user.currentRequest;
            if (req.QCRQ_FK_PICTURE != null) {
                return String.Format("/Image/ShowImageReq/{0}", req.QCRQ_SEQUENCE);
            } else {
                return "/Image/NoPic";
            }
        }

        public string getVBNPicture() {
            UserInformation userinfo = (UserInformation)Session["userinfo"];
            string requestType = (string)Session["cab_request_type"];
            var request = getRequest();
            int vbn_seq = -1;
            try {
                vbn_seq = (int)Session["vbn_seq"];
            } catch {
                vbn_seq = -1;
            }
            int cab_seq = -1;
            try {
                cab_seq = Int32.Parse((string)Session["cab_seq"]);
            } catch { cab_seq = -1; }
            if (cab_seq != -1 && requestType == "TakeProperties") {
                return getCABPicture(cab_seq);
            }
            if (vbn_seq != -1) {
                if (request.QCRQ_FK_PICTURE == null) {
                    try {
                        var vbnseq = request.QCRQ_FK_VBN_CODE;
                        string vkcurl = String.Format("http://www.logicab.nl/logicab/vkc/{0}.jpg", ImageUtils.getVKCImage(EFHelper.ctx.F_CAB_CD_VBN_CODE.FirstOrDefault(x => x.VBNC_SEQUENCE == vbnseq).VBNC_VBN_CODE));
                        var webClient = new WebClient();
                        byte[] imageBytes = webClient.DownloadData(vkcurl);
                        MemoryStream ms = new MemoryStream(imageBytes);
                        Image tempImg = Image.FromStream(ms);
                        Image newImage = ImageUtils.resizeImage(tempImg, new Size { Height = 1024, Width = 1024 });
                        MemoryStream ms2 = new MemoryStream();
                        newImage.Save(ms2, ImageFormat.Jpeg);
                        F_CAB_PICTURE newPic = new F_CAB_PICTURE {
                            PICT_PICTURE = ms2.ToArray()
                        };
                        EFHelper.ctx.F_CAB_PICTURE.AddObject(newPic);
                        EFHelper.ctx.SaveChanges();
                        request.QCRQ_FK_PICTURE = newPic.PICT_SEQUENCE;
                        EFHelper.ctx.SaveChanges();
                        /* TODO: Delete the following statement when QCRQ_PICTURE is removed from Live */
                        //request.QCRQ_PICTURE = newPic.PICT_PICTURE;
                        EFHelper.ctx.SaveChanges();
                    } catch (Exception ex) {
                        Log.ErrorFormat("Failed downloading VKC picture for VBNCode: {0}", request.QCRQ_FK_VBN_CODE);
                        var cValue = String.Format("vknc: {0} missing", request.QCRQ_FK_VBN_CODE);
                        DateTime cOn = DateTime.Now;
                        F_CAB_LOG newLog = new F_CAB_LOG {
                            CALO_ACTION = "LOGICAB",
                            CALO_FK_LOGIN = 7,
                            CALO_VALUE = cValue,
                            CALO_CREATED_ON = cOn
                        };
                        EFHelper.ctx.F_CAB_LOG.AddObject(newLog);
                        EFHelper.ctx.SaveChanges();
                    }
                }
                string vbnPicString = ImageUtils.getVKCImage(EFHelper.ctx.F_CAB_CD_VBN_CODE.FirstOrDefault(x => x.VBNC_SEQUENCE == vbn_seq).VBNC_VBN_CODE);
                if (!String.IsNullOrEmpty(vbnPicString)) {
                    return String.Format("http://www.logicab.nl/logicab/vkc/{0}.jpg", vbnPicString);
                }
            }
            return "http://www.logicab.nl/logicab/vkc/nopic.jpg";
        }

        public string getCABPicture(int seq) {
            //return String.Format("~Image/ShowImage/{0}", EFHelper.ctx.F_CAB_CD_CAB_CODE.FirstOrDefault(x => x.CABC_SEQUENCE == seq).CABC_FK_PICTURE);
            return Url.Content(String.Format("~/Image/ShowImage/{0}", EFHelper.ctx.F_CAB_CD_CAB_CODE.FirstOrDefault(x => x.CABC_SEQUENCE == seq).CABC_FK_PICTURE));
        }


        public static IEnumerable<F_CAB_CD_GROUP_TYPE> getGroupTypes(int id) {
            return (from gt in EFHelper.ctx.F_CAB_CD_GROUP_TYPE.Where(x => x.CDGT_FK_MAIN_GROUP == id) select gt).OrderByDescending(x => x.CDGT_SEQUENCE).ToList();
        }

        public string validateCABRequestClient() {
            UserInformation userinfo = (UserInformation)System.Web.HttpContext.Current.Session["userinfo"];
            F_CAB_QUEUE_CODE_REQUEST req = getRequest();
            bool valid = validateCABRequest(req);
            if (valid) return "Valid";
            else return "MissingProperty";
        }

        public string saveRequestDetail(int propType, string propValue) {
            UserInformation user = userInfo();
            var detail = EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL
                        .FirstOrDefault(x =>
                            x.QCRD_FK_CODE_REQUEST == user.currentRequest.QCRQ_SEQUENCE
                            && x.QCRD_FK_CAB_PROP_TYPE == propType
                        );
            propValue = propValue.Trim();
            
            if (detail != null) {
                detail.QCRD_ASS_PROP_VALUE = propValue;
                detail.QCRD_PROP_VALUE = propValue;
                EFHelper.ctx.SaveChanges();
                if (propValue == string.Empty) return "error: empty.";
                return "ok";
            } else {
                var propTypeObj = EFHelper.ctx.F_CAB_CD_CAB_PROP_TYPE.FirstOrDefault(x => x.CDPT_SEQUENCE == propType);
                if (propTypeObj == null) return "error";
                F_CAB_QUEUE_CODE_REQ_DETAIL newdetail = new F_CAB_QUEUE_CODE_REQ_DETAIL {
                    QCRD_ASS_PROP_DESC = propTypeObj.CDPT_DESC,
                    QCRD_ASS_PROP_VALUE = propValue,
                    QCRD_PROP_DESC = propTypeObj.CDPT_DESC,
                    QCRD_PROP_VALUE = propValue,
                    QCRD_FK_CODE_REQUEST = user.currentRequest.QCRQ_SEQUENCE,
                    QCRD_FK_CAB_PROP_TYPE = propTypeObj.CDPT_SEQUENCE
                };
                EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.AddObject(newdetail);
                EFHelper.ctx.SaveChanges();
                if (propValue == string.Empty) return "error: empty.";
                return "ok";
            }

        }

        public ActionResult CallbacksImageUpload() {
            string UC_qcrqSeq = Request.Params["DXUploadingCallback"];
            int qcrq_sequence = Int32.Parse(UC_qcrqSeq.Split('_')[1]);
            UploadedFile[] files = UploadControlExtension.GetUploadedFiles(UC_qcrqSeq, null, ucCallbacks_FileUploadComplete);
            UploadedFile file = files[0];

            MemoryStream ms = new MemoryStream(file.FileBytes);
            Image tempImg = Image.FromStream(ms);
            Image newImage = ImageUtils.resizeImage(tempImg, new Size { Height = 1024, Width = 1024 });
            MemoryStream ms2 = new MemoryStream();
            newImage.Save(ms2, ImageFormat.Jpeg);
            F_CAB_PICTURE newPic = new F_CAB_PICTURE {
                PICT_PICTURE = ms2.ToArray()
            };
            EFHelper.ctx.F_CAB_PICTURE.AddObject(newPic);
            EFHelper.ctx.SaveChanges();
            getRequest().QCRQ_FK_PICTURE = newPic.PICT_SEQUENCE;
            EFHelper.ctx.SaveChanges();
            /* TODO: Delete the following statement when QCRQ_PICTURE is removed from Live */
            //getRequest().QCRQ_PICTURE = newPic.PICT_PICTURE;
            EFHelper.ctx.SaveChanges();
            return null;
        }

        public static void ucCallbacks_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
            int lastPicSeq = (from n in EFHelper.ctx.F_CAB_PICTURE orderby n.PICT_SEQUENCE descending select n.PICT_SEQUENCE).FirstOrDefault();
            //e.CallbackData = String.Format("/Image/ShowImage/{0}", lastPicSeq + 1);
            UserInformation userinfo = (UserInformation)System.Web.HttpContext.Current.Session["userinfo"];
            userinfo.RequestPictureSelected = true;
            e.CallbackData = String.Format("{0}",lastPicSeq + 1);
        }

        private int Process(F_CAB_QUEUE_CODE_REQUEST request, string requestType, int cab_seq) {
            UserInformation userinfo = (UserInformation)System.Web.HttpContext.Current.Session["userinfo"];
            if (request.QCRQ_FK_PICTURE == null) { // 1 / 10: Picture
                try {
                    Log.InfoFormat("User: {0}: Submitted a request without a picture added for VBNCode: {1}", userinfo.LoginName, request.QCRQ_FK_VBN_CODE);
                    var vbnseq = request.QCRQ_FK_VBN_CODE;
                    string vkcurl = String.Format("http://www.logicab.nl/logicab/vkc/{0}.jpg", ImageUtils.getVKCImage(EFHelper.ctx.F_CAB_CD_VBN_CODE.FirstOrDefault(x => x.VBNC_SEQUENCE == vbnseq).VBNC_VBN_CODE));
                    var webClient = new WebClient();
                    byte[] imageBytes = webClient.DownloadData(vkcurl);
                    MemoryStream ms = new MemoryStream(imageBytes);
                    Image tempImg = Image.FromStream(ms);
                    Image newImage = ImageUtils.resizeImage(tempImg, new Size { Height = 1024, Width = 1024 });
                    MemoryStream ms2 = new MemoryStream();
                    newImage.Save(ms2, ImageFormat.Jpeg);
                    F_CAB_PICTURE newPic = new F_CAB_PICTURE {
                        PICT_PICTURE = ms2.ToArray()
                    };
                    EFHelper.ctx.F_CAB_PICTURE.AddObject(newPic);
                    EFHelper.ctx.SaveChanges();
                    request.QCRQ_FK_PICTURE = newPic.PICT_SEQUENCE;
                    EFHelper.ctx.SaveChanges();
                    /* TODO: Delete the following statement when QCRQ_PICTURE is removed from Live */
                    //request.QCRQ_PICTURE = newPic.PICT_PICTURE;
                    EFHelper.ctx.SaveChanges();
                } catch (Exception ex) {
                    Log.ErrorFormat("Failed downloading VKC picture for VBNCode: {0}, Exception: {1}", request.QCRQ_FK_VBN_CODE, ex.ToString());
                    var cValue = String.Format("vknc: {0} missing", request.QCRQ_FK_VBN_CODE);
                    DateTime cOn = DateTime.Now;
                    F_CAB_LOG newLog = new F_CAB_LOG {
                        CALO_ACTION = "LOGICAB",
                        CALO_FK_LOGIN = 7,
                        CALO_VALUE = cValue,
                        CALO_CREATED_ON = cOn
                    };
                    EFHelper.ctx.F_CAB_LOG.AddObject(newLog);
                    EFHelper.ctx.SaveChanges();
                    return -1;
                }

                // get VKC-picture and insert into QCRQ_FK_PICTURE
            }
            if (requestType == "New") { // Assume no cab-code selected
                IEnumerable<F_CAB_CD_CAB_CODE> existingCodes = getRelatedCABCodes(request.QCRQ_FK_VBN_CODE);
                if (existingCodes != null) { // related cab-codes found
                    foreach (F_CAB_CD_CAB_CODE code in existingCodes) {
                        IEnumerable<F_CAB_CAB_DETAIL> details = EFHelper.ctx.F_CAB_CAB_DETAIL.Where(x => x.CABD_FK_CAB_CODE == code.CABC_SEQUENCE);
                        bool identialProps = true;
                        foreach (F_CAB_QUEUE_CODE_REQ_DETAIL detail in request.F_CAB_QUEUE_CODE_REQ_DETAIL) {
                            if (!details.Any(x => x.CABD_FK_TYPE == detail.QCRD_FK_CAB_PROP_TYPE && x.CABD_VALUE == detail.QCRD_PROP_VALUE)) {
                                identialProps = false; // A propertyvalue in the request is not found the the cab-cab-details
                            }
                        }
                        if (identialProps) return code.CABC_SEQUENCE;

                    }
                } else { // no related cab-codes, send request as is.
                    // Get VKC-image and save it to QCRQ_FK_PICTURE
                    return -1;
                }
            } else if (requestType == "TakeProperties") { // only check specific cab-code
                F_CAB_CD_CAB_CODE existingCode = EFHelper.ctx.F_CAB_CD_CAB_CODE.FirstOrDefault(x => x.CABC_SEQUENCE == cab_seq);
                // take CABC_FK_PICTURE if QCRQ_FK_PICTURE is null
                IEnumerable<F_CAB_CAB_DETAIL> details = EFHelper.ctx.F_CAB_CAB_DETAIL.Where(x => x.CABD_FK_CAB_CODE == existingCode.CABC_SEQUENCE);
                bool identialProps = true;
                foreach (F_CAB_QUEUE_CODE_REQ_DETAIL detail in request.F_CAB_QUEUE_CODE_REQ_DETAIL) {
                    if (!details.Any(x => x.CABD_FK_TYPE == detail.QCRD_FK_CAB_PROP_TYPE && x.CABD_VALUE == detail.QCRD_PROP_VALUE)) {
                        identialProps = false; // A propertyvalue in the request is not found the the cab-cab-details
                    }
                }
                if (identialProps) return existingCode.CABC_SEQUENCE;
            } else if (requestType == "Redo") {
                F_CAB_CD_CAB_CODE existingCode = EFHelper.ctx.F_CAB_CD_CAB_CODE.FirstOrDefault(x => x.CABC_SEQUENCE == cab_seq);
                // take CABC_FK_PICTURE if QCRQ_FK_PICTURE is null
                /*IEnumerable<F_CAB_CAB_DETAIL> details = EFHelper.ctx.F_CAB_CAB_DETAIL.Where(x => x.CABD_FK_CAB_CODE == existingCode.CABC_SEQUENCE);
                bool identialProps = true;
                foreach (F_CAB_QUEUE_CODE_REQ_DETAIL detail in request.F_CAB_QUEUE_CODE_REQ_DETAIL) {
                    if (!details.Any(x => x.CABD_FK_TYPE == detail.QCRD_FK_CAB_PROP_TYPE && x.CABD_VALUE == detail.QCRD_PROP_VALUE)) {
                        identialProps = false; // A propertyvalue in the request is not found the the cab-cab-details
                    }
                }
                if (identialProps) return existingCode.CABC_SEQUENCE;
                */
                IEnumerable<F_CAB_CD_CAB_CODE> existingCodes = getRelatedCABCodes(request.QCRQ_FK_VBN_CODE);
                if (existingCodes != null) { // related cab-codes found
                    foreach (F_CAB_CD_CAB_CODE code in existingCodes) {
                        IEnumerable<F_CAB_CAB_DETAIL> details = EFHelper.ctx.F_CAB_CAB_DETAIL.Where(x => x.CABD_FK_CAB_CODE == code.CABC_SEQUENCE);
                        bool identialProps = true;
                        foreach (F_CAB_QUEUE_CODE_REQ_DETAIL detail in request.F_CAB_QUEUE_CODE_REQ_DETAIL) {
                            if (!details.Any(x => x.CABD_FK_TYPE == detail.QCRD_FK_CAB_PROP_TYPE && x.CABD_VALUE == detail.QCRD_PROP_VALUE)) {
                                identialProps = false; // A propertyvalue in the request is not found the the cab-cab-details
                            }
                        }
                        if (identialProps) return code.CABC_SEQUENCE;

                    }
                } else { // no related cab-codes, send request as is.
                    // Get VKC-image and save it to QCRQ_FK_PICTURE
                    return -1;
                }
            }
            return -1; // We got here, requestType = TakeProperties or New , and no properties were not identical, so we return -1 to indicate that its a brand new request

        }

        private bool validateCABRequest(F_CAB_QUEUE_CODE_REQUEST req) {

            bool result = true;
            String errorMessages = "";
            UserInformation userinfo = (UserInformation)System.Web.HttpContext.Current.Session["userinfo"];
            Log.InfoFormat("Starting validateCABRequest: Grower: {0},  Seq: {1}", userinfo.LoginSequence, req.QCRQ_SEQUENCE);
            IEnumerable<ReqProperties> props = (IEnumerable<ReqProperties>)getPropertiesForMG(userinfo.selectedMaingroup);
            foreach (var p in props.Where(x => x.required == true)) {
                if (!req.F_CAB_QUEUE_CODE_REQ_DETAIL.Any(x => x.QCRD_FK_CAB_PROP_TYPE == p.seq)) {
                    errorMessages += "Er ontbreekt een kenmerk: " + p.desc + ",";
                    result = false;
                }
            }
            foreach (var d in req.F_CAB_QUEUE_CODE_REQ_DETAIL) {
                if (d.QCRD_PROP_VALUE == string.Empty ) {
                    var propType = props.FirstOrDefault(x => x.seq == d.QCRD_FK_CAB_PROP_TYPE);
                    if (propType.required) { 
                        errorMessages += "Er ontbreekt een kenmerk: " + d.QCRD_PROP_DESC + ",";
                        result = false;
                    }
                }
            }
            if (String.IsNullOrEmpty(req.QCRQ_CAB_DESC) || req.QCRQ_CAB_DESC == null) {
                errorMessages += "Omschrijving ontbreekt,";
                result = false;
            }
            if (req.QCRQ_FK_PICTURE == null || !userinfo.RequestPictureSelected) {
                errorMessages += "Foto ontbreekt,";
                result = false;
            } else {
                //req.QCRQ_PICTURE = EFHelper.ctx.F_CAB_PICTURE.Single(x => x.PICT_SEQUENCE == req.QCRQ_FK_PICTURE).PICT_PICTURE;
                EFHelper.ctx.SaveChanges();
            }

            if (req.F_CAB_QUEUE_CODE_REQ_CASK != null) {
                if (req.F_CAB_QUEUE_CODE_REQ_CASK.Count() <= 0) {
                    errorMessages += "Fust ontbreekt,";
                    result = false;
                }
            } else {
                errorMessages += "Fust ontbreekt,";
                result = false;
            }

            if (result) {
                req.QCRQ_STATUS = "REQ";
                errorMessages = "Ok";
                EFHelper.ctx.SaveChanges();
            }
            Session["RequestValidation"] = errorMessages;
            Log.InfoFormat("Result from validateCABRequest: Grower: {0},  Seq: {1}, Messages: {2}", userinfo.LoginSequence, req.QCRQ_SEQUENCE, errorMessages);
            return result;

        }

        private void sendRequestEmail(F_CAB_QUEUE_CODE_REQUEST req) {
            try { 
                F_CAB_GROWER grower = EFHelper.ctx.F_CAB_GROWER.FirstOrDefault(x => x.GROW_SEQUENCE == req.QCRQ_FK_GROWER);
                if (grower == null)
                    return;
                var growerName = grower.F_CAB_ORGANISATION.ORGA_NAME;
                var growerMail = grower.F_CAB_ORGANISATION.ORGA_EMAIL;
                var growerPhone = grower.F_CAB_ORGANISATION.ORGA_PHONE_NO;
                var growerCabReq = req.QCRQ_GROWER_REMARK;
                var growerReqDate = req.QCRQ_REQUEST_DATE;

                //string connstring = EFHelper.ctx.Connection.ConnectionString;
                //ctx.Database.Connection.ConnectionString
                string connstring = ConfigurationSettings.CabConnectionString.ToLower();
                //string connstring = EFHelper.ctx.Connection.Database.ToLower();

                string testindicator = "";
                if (connstring.Contains("logicab_live_test") || connstring.Contains("logicab_live_dev"))
                    testindicator = "[TEST] ";
                var mailServer = ConfigurationManager.AppSettings["SMTP"] as string;
                if (String.IsNullOrEmpty(mailServer))
                    mailServer = "smtp.onlinespamfilter.nl";
                var mailobj = new Mailer(mailServer) { FromAddress = new MailAddress(growerMail) };
                mailobj.ToAddress.Add(new MailAddress("willem.geleijn@logiflora.nl"));
                mailobj.ToAddress.Add(new MailAddress("support@logiflora.nl"));
    //            mailobj.ToAddress.Add(new MailAddress("sylvia.pijper@logiflora.nl"));
                //mailobj.ToAddress.Add(new MailAddress("sigurd.boaseter@logiflora.nl,bart.boxsem@logiflora.nl,support@logiflora.nl"));

                mailobj.Subject = testindicator + "LogiCAB[NEW] - CABCode aanvraag van " + growerName;
                mailobj.Template = String.Format("<b><u>A new CABCode request has been sent from {0}:</u></b><br><br> GrowerRemarks: {1} <br> RequestDate: {2} <br> GrowerPhone: {3}", growerName, growerCabReq, growerReqDate, growerPhone);
                mailobj.SendEmail();
                Log.InfoFormat("Sent CABRequest-mail to: {0}", mailobj.ToAddressList);
            } catch (Exception ex) {
                Log.ErrorFormat("Error sending CABRequest Mail: Req: {0}, Exception: {1}",req.QCRQ_SEQUENCE, ex.ToString());
            }
        }

        public ActionResult ProcessAndNew() {
            return ProcessExtracted("Req", "CABRequest");
        }
        public ActionResult ProcessAndClose() {
            return ProcessExtracted("Index", "Home");
        }

        private ActionResult ProcessExtracted(string actionName, string controllerName) {
            UserInformation userinfo = (UserInformation)Session["userinfo"];
            int cab_seq = -1;
            try {
                cab_seq = Int32.Parse((string)Session["cab_seq"]);
            } catch {
            }
            string requestType = (string)Session["cab_request_type"];
            int result = Process(getRequest(), requestType, cab_seq);
            if (result != -1) {
                getRequest().QCRQ_STATUS = "EXIST";
                EFHelper.ctx.SaveChanges();
                addToAssortiment(userinfo.orgaTypeSequence, result, getRequest());
                userinfo.messages = "CABCode with identical properties allready exists";
            } else {
                // First check if required fields are filled
                bool validRequest = validateCABRequest(getRequest());

                if (validRequest) {
                    sendRequestEmail(getRequest());
                    Log.InfoFormat("User: {0}: Sent a valid CAB-Code Request for VBNCode: {1}", userinfo.LoginName, getRequest().QCRQ_FK_VBN_CODE);
                    getRequest().QCRQ_REQUEST_DATE = DateTime.Now;
                    EFHelper.ctx.SaveChanges();
                    userinfo.messages = "Request sent";
                }
            }
            Session["deleted_cask"] = false;
            userinfo.currentRequest = null;
            userinfo.RequestPictureSelected = false;
            return RedirectToAction(actionName, controllerName);

        }

        public JsonResult ProcessAjax() {
            UserInformation userinfo = (UserInformation)Session["userinfo"];
            bool result = validateCABRequest(getRequest());
            return Json((String)Session["RequestValidation"], JsonRequestBehavior.AllowGet);
        }


        private void addRequestsCasks(F_CAB_QUEUE_CODE_REQUEST request, F_CAB_GROWER_ASSORTIMENT gras) {

            foreach (var cask in request.F_CAB_QUEUE_CODE_REQ_CASK) {
                F_CAB_GROWER_CASK_MATRIX cgcm = EFHelper.ctx.F_CAB_GROWER_CASK_MATRIX.FirstOrDefault(x =>
                    x.F_CAB_CAB_CASK_MATRIX.F_CAB_CD_CASK.CDCA_SEQUENCE == cask.QCRC_FK_CASK &&
                    x.F_CAB_CAB_CASK_MATRIX.CACA_NUM_OF_UNITS == cask.QCRC_NUM_OF_UNITS &&
                    x.F_CAB_CAB_CASK_MATRIX.CACA_UNITS_PER_LAYER == cask.QCRC_UNITS_PER_LAYER &&
                    x.F_CAB_CAB_CASK_MATRIX.CACA_LAYERS_PER_TROLLEY == cask.QCRC_LAYERS_PER_TROLLEY);
                if (cgcm == null) {
                    //F_CAB_CAB_CASK_MATRIX newCaca = EFHelper.ctx.F_CAB_CAB_CASK_MATRIX.DeleteObject(new F);
                    F_CAB_CAB_CASK_MATRIX newCaca = new F_CAB_CAB_CASK_MATRIX {
                        CACA_FK_CAB_CODE = gras.GRAS_FK_CAB_CODE,
                        CACA_FK_CASK = cask.QCRC_FK_CASK,
                        CACA_NUM_OF_UNITS = cask.QCRC_NUM_OF_UNITS,
                        CACA_UNITS_PER_LAYER = cask.QCRC_UNITS_PER_LAYER,
                        CACA_LAYERS_PER_TROLLEY = cask.QCRC_LAYERS_PER_TROLLEY
                    };
                    EFHelper.ctx.F_CAB_CAB_CASK_MATRIX.AddObject(newCaca);
                    EFHelper.ctx.SaveChanges();
                    EFHelper.ctx.F_CAB_GROWER_CASK_MATRIX.AddObject(new F_CAB_GROWER_CASK_MATRIX {
                        CGCM_FK_CACA = newCaca.CACA_SEQUENCE,
//                        CGCM_FK_CASK = cask.QCRC_FK_CASK,
                        CGCM_FK_GROWER_ASSORTIMENT = gras.GRAS_SEQUENCE,
                        CGCM_SHOW_CASK = true,
                    });
                    EFHelper.ctx.SaveChanges();
                }
            }

        }

        // TODO: Unify
        private string addToAssortiment(int growerSequence, int cab_seq, F_CAB_QUEUE_CODE_REQUEST request) {
            UserInformation userinfo = (UserInformation)Session["userinfo"];

            string result = "";
            try {
                // Add image-upload and cask
                string cabArticle = EFHelper.ctx.F_CAB_CD_CAB_CODE.Single(x => x.CABC_SEQUENCE == cab_seq).CABC_CAB_DESC;
                bool exists = EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.Any(x => x.GRAS_FK_GROWER == growerSequence && x.GRAS_FK_CAB_CODE == cab_seq);
                if (exists) {
                    result = String.Format("{0} is al aanwezig in uw assortiment!", cabArticle);
                    addRequestsCasks(request, EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.FirstOrDefault(x => x.GRAS_FK_GROWER == growerSequence && x.GRAS_FK_CAB_CODE == cab_seq));
                    Log.InfoFormat("User: {0}: Attempted to add CAB-Article allready existing in his asssortiment: {1}, {2}", userinfo.LoginName, cab_seq, cabArticle);
                } else {
                    F_CAB_GROWER_ASSORTIMENT newGras = new F_CAB_GROWER_ASSORTIMENT {
                        GRAS_DELETED_YN = false,
                        GRAS_FK_CAB_CODE = cab_seq,
                        GRAS_FK_GROWER = growerSequence,
                        GRAS_NUM_OF_M2 = 0,
                        GRAS_SELECTED_YN = true,
                        GRAS_START_DATE = DateTime.Now,
                        GRAS_NUM_OF_ITEMS = 0,
                        GRAS_FK_PICTURE = getRequest().QCRQ_FK_PICTURE ?? null
                    };
                    EFHelper.ctx.F_CAB_GROWER_ASSORTIMENT.AddObject(newGras);
                    EFHelper.ctx.SaveChanges();

                    foreach (var cask in newGras.F_CAB_CD_CAB_CODE.F_CAB_CAB_CASK_MATRIX.OrderBy(x => x.F_CAB_CD_CASK.CDCA_CASK_DESC)) {
                        // Check if casks exists in grower_cask_matrix, if not, add it and set show_cask to true
                        var gcm = newGras.F_CAB_GROWER_CASK_MATRIX.FirstOrDefault(x => x.F_CAB_CAB_CASK_MATRIX.F_CAB_CD_CASK.CDCA_SEQUENCE == cask.F_CAB_CD_CASK.CDCA_SEQUENCE && x.CGCM_FK_CACA == cask.CACA_SEQUENCE);
                        if (gcm == null) {
                            newGras.F_CAB_GROWER_CASK_MATRIX.Add(new F_CAB_GROWER_CASK_MATRIX {
//                                CGCM_FK_CASK = cask.CACA_FK_CASK,
                                CGCM_SHOW_CASK = true,
                                CGCM_FK_GROWER_ASSORTIMENT = newGras.GRAS_SEQUENCE,
                                CGCM_FK_CACA = cask.CACA_SEQUENCE
                            });
                        }
                        EFHelper.ctx.SaveChanges();
                    }
                    addRequestsCasks(getRequest(), newGras);
                    result = String.Format("{0} toegevoegd.", newGras.GRAS_SEQUENCE);
                    Log.InfoFormat("User: {0}: Added CAB-Article to his assortiment: {1}, {2}", userinfo.LoginName, cab_seq, cabArticle);
                }
                return result;
            } catch (Exception ex) {
                Log.Error("Error in addToAssortiment: ", ex);
                return "Error occured while adding an article to your assortiment";
            }

        }

        public ActionResult ProcessAddToAssortiment() {
            UserInformation userinfo = (UserInformation)Session["userinfo"];
            int cab_seq = Int32.Parse((string)Session["cab_seq"]);
            userinfo.messages = addToAssortiment(userinfo.orgaTypeSequence, cab_seq, getRequest());
            return RedirectToAction("Index", "Home");
        }

        public string requestAction(int id, string action) {
            var request = EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.Single(x => x.QCRQ_SEQUENCE == id);
            var details = EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.Where(x => x.QCRD_FK_CODE_REQUEST == id);
            var casks = EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_CASK.Where(x => x.QCRC_FK_CODE_REQUEST == id);

            if (action == "delete") {
                foreach (var cask in casks) {
                    EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_CASK.DeleteObject(cask);
                }
                foreach (var detail in details) {
                    EFHelper.ctx.F_CAB_QUEUE_CODE_REQ_DETAIL.DeleteObject(detail);
                }
                EFHelper.ctx.F_CAB_QUEUE_CODE_REQUEST.DeleteObject(request);
                EFHelper.ctx.SaveChanges();
                return "ok";
            } else if (action == "hide") {
                request.QCRQ_HIDDEN_YN = true;
                EFHelper.ctx.SaveChanges();
                return "ok";
            } else {
                return "error";
            }
        }


    }
}
