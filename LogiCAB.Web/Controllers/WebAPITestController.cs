﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LogiCAB.Web.Controllers
{
    public class WebAPITestController : ApiController
    {
        // GET: api/WebAPITest
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/WebAPITest/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/WebAPITest
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/WebAPITest/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/WebAPITest/5
        public void Delete(int id)
        {
        }
    }
}
