﻿using CAB.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using System.Configuration;
using System.Text;
using CAB.Domain.Models;
using CAB.Domain.Providers;

namespace LogiCAB.Web.Controllers {
    public class GMController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EABOutput() {
            //ViewBag.EABContent = "Test\n\test2\test3";
            List<Auction> auctions = LogiCAB.Web.Controllers.AuctionProvider.Get();
            EDIProvider _ediProvider = new EDIProvider();
            _ediProvider.Header = new EDIHeader();
            _ediProvider.EDI = new List<EDIData>();
            
//            _ediProvider.EDI = new EDIData();
            decimal lngSenderId = 0;
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                var vbaStr = conn.Query<string>(@"SELECT ORGA_VBA_NUMBER FROM F_CAB_ORGANISATION WHERE ORGA_SEQUENCE = @Id", new { Id = Usr.Organisation.Sequence }).First();
                lngSenderId = decimal.Parse(vbaStr);
            }
            bool EDIExport = true;

            int auctionLetterInfo = 12;
            _ediProvider.Header.AuctionCode = auctions.FirstOrDefault(x => x.Code == "VBA").Code;
            string SenderIDDebtNr = "";
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                SenderIDDebtNr = conn.Query<string>(@"SELECT ORGA_VBA_NUMBER FROM F_CAB_ORGANISATION WHERE ORGA_SEQUENCE = @Id", new { Id = Usr.Organisation.Sequence }).First();
            }
            _ediProvider.Header.SenderId = Decimal.Parse(SenderIDDebtNr);
            _ediProvider.Header.lngUNHCount = 0;
            _ediProvider.Header.strVersion = "2";
            _ediProvider.Header.strRelease = "8";
            _ediProvider.Header.IsTest = true;
            _ediProvider.Header.RequestEABResponse = false;

            String strLine = "";

            // OfferSequence or AuctionLetterSequence in local DB.
            decimal uniqueIdentifier = 20707174102437;

            #region UNB_Interchange_Header
            switch (_ediProvider.Header.AuctionCode) {
                case "MVA":
                    strLine += String.Format("UNB+UNOA:2+{0:000000.##}:ZZ+{1:00}:ZZ+", _ediProvider.Header.SenderId, 1);
                    _ediProvider.Header.strVersion = "002";
                    _ediProvider.Header.strRelease = "008";
                    break;
                case "VBA":
                    strLine += String.Format("UNB+UNOA:2+{0:000000.##}:ZZ+{1:00}:ZZ+", _ediProvider.Header.SenderId, _ediProvider.Header.AuctionCode);
                    _ediProvider.Header.strVersion = "002";
                    _ediProvider.Header.strRelease = "008";
                    break;
                default:
                    strLine += String.Format("UNB+UNOA:2+{0:000000.##}:ZZ+{1:00}:ZZ+", _ediProvider.Header.SenderId, _ediProvider.Header.AuctionCode);
                    break;
            }
            strLine += String.Format("{0:yyMMdd}:{1:hhmm}+{2:00000000000000}++{3}++{4}", DateTime.Now, DateTime.Now, uniqueIdentifier, _ediProvider.Header.RequestEABResponse ? 1 : 0, _ediProvider.Header.IsTest ? 1 : 0);
            _ediProvider.InsertData(ref strLine, false);
            #endregion

            #region UNH_Message_Header

            //_ediProvider.Header.UNH_ID = 42;  // Unique number for the edi-message
            _ediProvider.Header.UNH_ID = new Random().Next(100000);  // Unique number for the edi-message
            strLine += String.Format("UNH+{0}+FLOWAV:{1}:{2}:EF'", _ediProvider.Header.UNH_ID, _ediProvider.Header.strVersion, _ediProvider.Header.strRelease);
            _ediProvider.InsertData(ref strLine, true);
            _ediProvider.Header.lngUNHCount += 1;
            #endregion

            #region BGM_Beginning_of_Message

            // Use default value of 9 (Original) for new EAB-message, 2 = Add, 3 = Delete, 5 = Replace
            _ediProvider.Header.UNCode = 9; // Original
            /*
                102 Aanvoerbrief klok
                103 Aanvoerbrief Bemiddeling
                104 Aanvoerbrief aanbod vers veilen
                106 Aanvoerbrief BLO (Buiten veiling Logistiek Om)
                107 Aanvoerbrief administratief geveild
                108 Aanvoerbrief klokservice
             */
            if (EDIExport)
                strLine += String.Format("BGM+103++{0}'", _ediProvider.Header.UNCode);// 'UNCode = 3,5 of 9 (9 = origineel, 5 = vervang voorgaande, 3 = ?)
            else
                strLine += String.Format("BGM+102++{0}'", _ediProvider.Header.UNCode); //'UNCode = 3,5 of 9 (9 = origineel, 5 = vervang voorgaande, 3 = ?)
            _ediProvider.InsertData(ref strLine, true);
//            _ediProvider.Header.lngPTYCount = 0;
            #endregion


            #region PTY_Partij
            //foreach (EDIData edi in _ediProvider.) {
            // 
            //for (int i = 0; i < 4; i++) {
            foreach(GMRecord gm in _getList().Where(x => x.InUse != null).Take(3)) {
                EDIData EDI = new EDIData();
                EDI.strQuality = gm.Quality;


                //EDI.APE = 80; // detail
                EDI.APE = decimal.Parse(gm.APE);
                EDI.EmbCode = decimal.Parse(gm.Cask);

                EDI.ArtCode = (decimal)gm.Seq;

                var partyFollowNr = 1;
                var partyNr = "A";

                Console.WriteLine("====For each Articlegroup(Pr Prop? Length? Quality? GroupType) : {0}====", gm.Seq);
                if (EDI.ALNumber == "BVH".ToString()) {
                    strLine += String.Format("PTY+{0:000000}+{1}", String.Format("{0}{1}", EDI.ALNumber, "W"), EDI.lngPTYCount);
                } else {
                    //strLine += String.Format("PTY+{0}+{1}", EDI.ALNumber, EDI.lngPTYCount.ToString().ToUpper());
                    strLine += String.Format("PTY+{0}+{1}", partyFollowNr, partyNr);
                }
                _ediProvider.InsertData(ref strLine, true);
                if (EDIExport) {
                    strLine += String.Format("NAD+DO+{0:00}'", 1);
                } else {
                    strLine += String.Format("NAD+DO+{0:00}'", _ediProvider.Header.AuctionCode.ToString());
                }
                _ediProvider.InsertData(ref strLine, true);
                strLine += String.Format("NAD+MF+{0:000000}'", lngSenderId);
                _ediProvider.InsertData(ref strLine, true);

                if (EDIExport) {
                    strLine += String.Format("NAD+BY+{0:000000}'", _ediProvider.Header.SenderId);
                    _ediProvider.InsertData(ref strLine, true);
                }


                #region PTY_Start

                bool UseAsMVAInvoice = false; // TODO: Real data

                if (EDI.strQuality != "A1" && EDI.strQuality != "A2" && EDI.strQuality != "B1")
                    EDI.strQuality = "A1";
                EDI.strPartySize = "3";
                if (!_ediProvider.IsEDIDataValid(EDI)) {
                    System.Console.WriteLine("Partijgrootte (PartySize) is onbekend. Aanmaken bestand.");
                } else {
                    if (EDIExport) {
                        strLine = String.Empty;
                        strLine += "FTX+ABM++6'";
                        _ediProvider.InsertData(ref strLine, true);
                    } else if (_ediProvider.Header.AuctionCode == "BVH" || _ediProvider.Header.AuctionCode == "VBA") {
                        int intPartySize = Int32.Parse(EDI.strPartySize);
                        int intNumOfCars = (int)EDI.NumOfCars;
                        strLine = String.Empty;
                        if (intPartySize == 2 && intNumOfCars > 2) {
                            strLine += "FTX+ABM++8'";
                        } else if (intPartySize == 4 && intNumOfCars > 2) {
                            strLine += "FTX+ABM++9'";
                        } else {
                            strLine += String.Format("FTX+ABM++{0}'", intPartySize);
                        }
                        _ediProvider.InsertData(ref strLine, true);
                    } else {
                        int intPartySize = Int32.Parse(EDI.strPartySize);
                        strLine += String.Format("FTX+ABM++{0}'", intPartySize);
                        _ediProvider.InsertData(ref strLine, true);
                    }
                    _ediProvider.Header.lngSegmentCount++;

                    if (EDIExport) {
                        strLine += "FTX+COI++J'";
                        _ediProvider.InsertData(ref strLine, true);
                    }

                    if (_ediProvider.Header.AuctionCode != "MVA" && _ediProvider.Header.AuctionCode != "BVH") { // Refine
                        strLine = String.Format("FTX+KWA++{0}'", EDI.strQuality);
                        _ediProvider.InsertData(ref strLine, true);
                    }
                    if (_ediProvider.Header.AuctionCode != "MVA") {
                        strLine = String.Format("FTX+PRD++{0}'", EDI.AuctionGroupCode);
                        _ediProvider.InsertData(ref strLine, true);
                    }
                    strLine += String.Format("DTM+97:{0:yyMMdd}:102'", EDI.dtAuction);
                    _ediProvider.InsertData(ref strLine, true);

                    strLine += String.Format("DTM+50:{0:yyMMdd}:102'", EDI.dtTransport);
                    _ediProvider.InsertData(ref strLine, true);

                    strLine += String.Format("DTM+50:{0:hhmm}:401", EDI.dtTransport);
                    _ediProvider.InsertData(ref strLine, true);

                    if (_ediProvider.Header.AuctionCode != "VBA")
                        strLine += String.Format("GDS+{0:00}'", EDI.ArtCode);
                    else
                        strLine += String.Format("GDS+{0:00000}'", EDI.ArtCode);
                    _ediProvider.InsertData(ref strLine, true);

                #endregion
                    //Segment: IMD Item Description
                    //Position: 175
                    //Group: Segment Group 1 Mandatory
                    //Level: 2
                    //Usage: Conditional (Optional)
                    //Max Use: 99
                    //Dependency Notes:
                    //Comments:
                    //Notes: Optioneel segment. Functie: opgeven van kenmerktype en waarde van:
                    //- partijsorteringen (S-codes)
                    //- negatieve keurcodes (K-codes)
                    //- positieve keurcodes (P-codes)
                    //- Informatiecodes (I-codes, Alleen FloraHolland)
                    #region IMD_Item_Description


                    /*
                     * 
                     * TODO:
                     * Implement library to generate valid VBN-property-fields based on VBN S-Codering.
                     * VBNPropertyProvider (Interface) 
                     *      SetAuction:      Interface method  
                     *      SetOther(?):     Serves as implementation-configuration
                     * 
                     * 
                     * AalsmeerPropertyProvider     : VBNPropertyProvider
                     * NaaldwijkPropertyProvider    : VBNPropertyProvider
                     * BVHPropertyProvider:         : VBNPropertyProvider
                     * 
                     */ 


                    //'### PTY.IMD (Sorteercode)
                    //'LENGTE (S-code zit al in de variabele)
                    strLine = String.Empty;
                    if (_ediProvider.Header.AuctionCode != "VBA") {
                        strLine += String.Format("IMD++{0}'", EDI.strSortCodeZN1);
                    } else {
                        strLine += String.Format("IMD++{0}'", EDI.strSortcode1);
                    }
                    if (!String.IsNullOrEmpty(EDI.strSortcode1)) {
                        _ediProvider.InsertData(ref strLine, true);
                    }
                    //'### PTY.IMD (Sorteercode)
                    //'GEWICHT (S-code zit al in de variabele)
                    strLine = String.Empty;
                    if (_ediProvider.Header.AuctionCode != "VBA")
                        strLine += String.Format("IMD++{0}'", EDI.strSortCodeZN2);
                    else
                        strLine = String.Format("IMD++{0}'", EDI.strSortcode2);
                    if (!String.IsNullOrEmpty(EDI.strSortcode2)) {
                        _ediProvider.InsertData(ref strLine, true);
                    }

                    //'### PTY.IMD (Sorteercode)
                    //'KNOPPEN (S-code zit al in de variabele)
                    strLine = String.Empty;
                    if (_ediProvider.Header.AuctionCode != "VBA")
                        strLine += String.Format("IMD++{0}'", EDI.strSortCodeZN3);
                    else
                        strLine += String.Format("IMD++{0}'", EDI.strSortcode3);
                    if (!String.IsNullOrEmpty(EDI.strSortcode3)) {
                        _ediProvider.InsertData(ref strLine, true);
                    }

                    //'### PTY.IMD (Sorteercode)
                    //'DIAMETER (S-code zit al in de variabele)
                    strLine = String.Empty;
                    if (_ediProvider.Header.AuctionCode != "VBA")
                        strLine += String.Format("IMD++{0}'", EDI.strSortCodeZN4);
                    else
                        strLine += String.Format("IMD++{0}'", EDI.strSortcode4);
                    if (!String.IsNullOrEmpty(EDI.strSortcode4)) {
                        _ediProvider.InsertData(ref strLine, true);
                    }

                    //'### PTY.IMD (Sorteercode)
                    //'KLEUR (S-code zit al in de variabele)
                    strLine = String.Empty;
                    if (_ediProvider.Header.AuctionCode != "BVH") { // != auBVH_Bleiswijk
                        if (_ediProvider.Header.AuctionCode != "VBA") {
                            strLine += String.Format("IMD++{0}'", EDI.strSortCodeZN5);
                        } else {
                            strLine += String.Format("IMD++{0}'", EDI.strSortcode5);
                        }
                        if (!String.IsNullOrEmpty(EDI.strSortcode5)) {
                            _ediProvider.InsertData(ref strLine, true);
                        }
                    }

                    //'### PTY.IMD (Sorteercode)
                    //'POTMAAT (S-code zit al in de variabele)
                    strLine = String.Empty;
                    if (_ediProvider.Header.AuctionCode != "VBA")
                        strLine += String.Format("IMD++{0}'", EDI.strSortCodeZN6);
                    else
                        strLine += String.Format("IMD++{0}'", EDI.strSortcode6);
                    if (!String.IsNullOrEmpty(EDI.strSortcode6)) {
                        _ediProvider.InsertData(ref strLine, true);
                    }

                    //'### PTY.IMD (Sorteercode)
                    //'Aantal per bos (rozen)
                    strLine = String.Empty;
                    if (_ediProvider.Header.AuctionCode != "VBA")
                        strLine += String.Format("IMD++{0}'", EDI.strSortCodeZN7);
                    else
                        strLine += String.Format("IMD++{0}'", EDI.strSortcode7);
                    if (!String.IsNullOrEmpty(EDI.strSortcode7)) {
                        _ediProvider.InsertData(ref strLine, true);
                    }

                    //'### PTY.IMD (Sorteercode)
                    //'Aantal knoppen (trosrozen)
                    strLine = String.Empty;
                    if (_ediProvider.Header.AuctionCode != "VBA")
                        strLine += String.Format("IMD++{0}'", EDI.strSortCodeZN8);
                    else
                        strLine += String.Format("IMD++{0}'", EDI.strSortcode8);
                    if (!String.IsNullOrEmpty(EDI.strSortcode8)) {
                        _ediProvider.InsertData(ref strLine, true);
                    }

                    //'### PTY.IMD (Sorteercode)
                    //'Vertakkingen (Hypericum)
                    strLine = String.Empty;
                    if (_ediProvider.Header.AuctionCode != "VBA")
                        strLine += String.Format("IMD++{0}'", EDI.strSortCodeZN9);
                    else
                        strLine += String.Format("IMD++{0}'", EDI.strSortcode9);
                    if (!String.IsNullOrEmpty(EDI.strSortcode9)) {
                        _ediProvider.InsertData(ref strLine, true);
                    }

                    //'### PTY.IMD (Sorteercode)
                    //'Aantal kleuren (Hypericum gemengd.) S55
                    strLine = String.Empty;
                    if (_ediProvider.Header.AuctionCode != "VBA")
                        strLine += String.Format("IMD++{0}'", EDI.strSortCodeZN10);
                    else
                        strLine += String.Format("IMD++{0}'", EDI.strSortcode10);
                    if (!String.IsNullOrEmpty(EDI.strSortcode10)) {
                        _ediProvider.InsertData(ref strLine, true);
                    }
                    //'### PTY.IMD (Sorteercode)
                    //'RIJPHEIDSSTADIUM (S-code zit al in de variabele)
                    strLine = String.Empty;
                    strLine += String.Format("IMD++{0}'", EDI.strMaturity);
                    if (!String.IsNullOrEmpty(EDI.strMaturity)) {
                        _ediProvider.InsertData(ref strLine, true);
                    }
                    //'### PTY.IMD K02 (Sorteercode, negatieve keurcode) VBA: niet verplicht
                    //  '4-7-2002/MVD: Negatieve Keurcodes ook in IMD. Voorlopig 1 regel, later max drie
                    //  '           regels mogelijk maken. Indien geen neg. keurcode aanwezig, 0.00 meegeven.
                    //  '           later eventueel ook positieve codes mogelijk maken
                    strLine = String.Empty;
                    if (EDI.strVet1Code != "000") {
                        strLine += String.Format("IMD++K01+{0}'", EDI.strVet1Code);
                        _ediProvider.InsertData(ref strLine, true);
                    }
                    //'### PTY.IMD K02 (Sorteercode, negatieve keurcode) VBA: niet verplicht
                    strLine = String.Empty;
                    if (EDI.strVet2Code != "000") {
                        strLine += String.Format("IMD++K02+{0}'", EDI.strVet2Code);
                        _ediProvider.InsertData(ref strLine, true);
                    }

                    #endregion

                    #region PTY_End

                    strLine = String.Empty;
                    //'### PTY PAC (Fustcode) VBA: niet verplicht
                    if (EDI.EmbCode == 0) EDI.EmbCode = 999;
                    strLine += String.Format("PAC+++{0}'", EDI.EmbCode);
                    _ediProvider.InsertData(ref strLine, true);
                    // 'PTY QTY 52 (Aantal per fust)
                    strLine += String.Format("QTY+52:{0}'", EDI.APE);// 'voor GP als KP hetzelfde: APE.
                    _ediProvider.InsertData(ref strLine, true);
                    // 'PTY QTY 46 (Aantal per partij: vervallen vanaf Flowav2.0, hebben wij nooit ondersteund)
                    // 'PTY RFF IRN foto referentie..
                    strLine = String.Empty;
                    if (!String.IsNullOrEmpty(EDI.IRNNumber)) {
                        strLine += String.Format("RFF+IRN:{0}::1'", EDI.IRNNumber);
                        _ediProvider.InsertData(ref strLine, true);
                    }
                    strLine = String.Empty;
                    if (EDIExport) {
                        //'PRI INV prijs in euro verkoopprijs.
                        //'Dit element is nodig voor factureren via heb bemiddelingsbureau.
                        strLine += String.Format("PRI+INV:{0}'", EDI.strSellPrice);
                        _ediProvider.InsertData(ref strLine, true);
                    }
                    #endregion
                }
                _ediProvider.EDI.Add(EDI);
            }
            #endregion

            #region EDIEnd
            if (!_ediProvider.Header.IsGP && (_ediProvider.Header.AuctionCode == "VBA" || _ediProvider.Header.AuctionCode == "MVA"))
                // add EQD-data
                if (_ediProvider.Header.lngNumOfPlates > 0 && _ediProvider.Header.AuctionCode != "VBA") {
                    strLine += String.Format("EQN+{0}:19'", _ediProvider.Header.lngNumOfPlates);
                    _ediProvider.InsertData(ref strLine, true);
                }
            strLine += String.Format("UNT+{0}+{1:00000000000000}'", _ediProvider.Header.lngSegmentCount, uniqueIdentifier);
            _ediProvider.InsertData(ref strLine, true);

            strLine += String.Format("UNZ+{0}+{1:00000000000000}'", _ediProvider.Header.lngUNHCount, uniqueIdentifier);
            _ediProvider.InsertData(ref strLine, true);
            #endregion



            ViewBag.EABContent = _ediProvider.Generate();
            return PartialView();
        }

        public ActionResult List() {
            return PartialView(_getList());
        }

        public List<GMRecord> _getListV1() {
            List<GMRecord> modelData = new List<GMRecord>();
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                modelData = conn.Query<GMRecord>(@"
SELECT 
CGCM_SEQUENCE AS Seq, 
PROP_DESC_ALL_PROPS AS [Desc],
PROP_VAL2 AS Quality,
GRAS_NUM_OF_ITEMS AS Num, 
GRAS_SELECTED_YN AS Rest,
Pic = 
CASE 
	WHEN GRAS_FK_PICTURE is null 
	THEN 
		(SELECT CABC_FK_PICTURE from F_CAB_CD_CAB_CODE where CABC_SEQUENCE = GRAS_FK_CAB_CODE)
	ELSE
		GRAS_FK_PICTURE
END
FROM F_CAB_GROWER_ASSORTIMENT 
JOIN F_CAB_CAB_PROP ON GRAS_FK_CAB_CODE = PROP_FK_CABC_SEQ
WHERE PROP_FK_CDLA_SEQ = 1 and GRAS_FK_GROWER = @Id", new { Id = Usr.GrowerSequence }).ToList();
            }
            return modelData;
        }

        public List<GMRecord> _getList() {
            List<GMRecord> modelData = new List<GMRecord>();
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                modelData = conn.Query<GMRecord>(@"
SELECT 
CGCM_SEQUENCE AS Seq, 
PROP_DESC_ALL_PROPS AS [Desc],
PROP_VAL2 AS Quality,
GRAS_NUM_OF_ITEMS AS Num, 
GRAS_SELECTED_YN AS Rest,
Pic = 
CASE 
	WHEN GRAS_FK_PICTURE is null 
	THEN 
		(SELECT CABC_FK_PICTURE from F_CAB_CD_CAB_CODE where CABC_SEQUENCE = GRAS_FK_CAB_CODE)
	ELSE
		GRAS_FK_PICTURE
END,
(select top 1 OFDT_NUM_OF_ITEMS from F_CAB_OFFER_DETAILS join F_CAB_OFFER_HEADERS on OFDT_FK_OFFER_HEADER = OFHD_SEQUENCE where OFDT_FK_GROWER_ASSORTIMENT = GRAS_SEQUENCE and OFHD_VALID_TO > GETDATE()) as InUse,
CACA_NUM_OF_UNITS AS APE,
CDCA_CASK_CODE as Cask

FROM F_CAB_GROWER_ASSORTIMENT 
JOIN F_CAB_CAB_PROP ON GRAS_FK_CAB_CODE = PROP_FK_CABC_SEQ
join F_CAB_GROWER_CASK_MATRIX on CGCM_FK_GROWER_ASSORTIMENT = GRAS_SEQUENCE
join F_CAB_CAB_CASK_MATRIX on CGCM_FK_CACA = CACA_SEQUENCE
join F_CAB_CD_CASK on CACA_FK_CASK = CDCA_SEQUENCE
WHERE PROP_FK_CDLA_SEQ = @Lang and GRAS_FK_GROWER = @Id", new { Lang = Usr.Language, Id = Usr.GrowerSequence }).ToList();
            }
            return modelData;
        }

/*        public ActionResult ListNew() {
            List<GMRecord> modelData = new List<GMRecord>();
            for (int i = 0; i < 50; i++) {
                modelData.Add(new GMRecord {
                    Seq = i,
                    Num = i * 2,
                    Rest = i + i
                });
            }
            return PartialView(modelData);
        }
 */ 
    }
    public static class AuctionProvider {
//        public AuctionProvider() {
//        }
        public static List<Auction> Get() {
            using (var conn = new SqlConnection(ConfigurationSettings.CabConnectionString)) {
                conn.Open();
                var res = conn.Query<Auction>(@"
                    SELECT 
                    CDAU_SEQUENCE as sequence,
                    CDAU_DESCRIPTION as description,
                    CDAU_CODE as code,
                    CDAU_EXTERNAL_CODE as externalCode,
                    CDAU_AUCTION_LETTER_NUMBER_TEXT as auctionLetterNumberNext,
                    CDAU_EDI_EMAIL as ediEmail
                    FROM F_CAB_CD_AUCTION");
                return res.ToList();
            }
        }
    }

}