﻿using CAB.Data.EF.Models;
using LogiCAB.Service.Domain;
using LogiCAB.Service.Models;
using LogiCAB.Web.DAL;
using LogiCAB.Web.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.ServiceModel.Configuration;
using log4net;

namespace LogiCAB.Web.Controllers {
    [HandleError]
    [Authorize]
    public class LogController : BaseController {
        int build = -1;
        public LogController(ILog log) : base(log) {
            var version = typeof(LogiCAB.Web.MvcApplication).Assembly.GetName().Version;
            this.build = version.Build;
        }

        public ActionResult LogiCAB() {
            return View();
        }
        public ActionResult LogiOffer() {
            return View();
        }

        public ActionResult LogiOfferte() {
            return View();
        }


        public ActionResult LogiService() {
            return View();
        }

        public ActionResult LogiCABApp() {
            return View();
        }

        public ActionResult LogiServiceOfferlist() {
            ClientSection clientSection = ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;
            ChannelEndpointElementCollection endpointCollection = clientSection.ElementInformation.Properties[string.Empty].Value as ChannelEndpointElementCollection;
            List<string> endpointNames = new List<string>();
            foreach(ChannelEndpointElement endpointElement in endpointCollection) {
                endpointNames.Add(endpointElement.Name);
            }
            ViewBag.Endpoints = endpointNames;
            return View();
        }

        public ActionResult ListLogiServiceOfferlist() {
            string username = !string.IsNullOrEmpty(Request.Params["username"]) ? Request.Params["username"] : "";
            string password = !string.IsNullOrEmpty(Request.Params["password"]) ? Request.Params["password"] : "";
            string version = !string.IsNullOrEmpty(Request.Params["version"]) ? Request.Params["version"] : "0.7";            

            return PartialView("ListLogiServiceOfferlist", _getLogiServiceOfferList(username, password, version));
        }

        public ActionResult ListLogiOffer() {
            return PartialView("ListLogiOffer", _getLogiOfferLogins());
        }

        public ActionResult ListLogiCAB() {
            return PartialView("ListLogiCAB", _getLogiCABList());
        }

        public ActionResult ListLogiOfferte() {
            return PartialView("ListLogiOfferte", _getLogiOfferteList());
        }
        public ActionResult ListLogiService() {
            return PartialView("ListLogiService", _getLogiServiceList());
        }
        public ActionResult ListLogiCABApp() {
            return PartialView("ListLogiCABApp", _getAppLogins());
        }

        private IEnumerable<ServiceOffer> _getLogiServiceOfferList(String username = "", String password = "", String version = "0.7") {
            var currentTime = DateTime.Now;

//#if DEBUG
            String sessionKey = new Random().Next(1000).ToString();
            // TODO: Change to interval determinator
//#else
//            String sessionKey = String.Format("serviceoffers_{0}_{1}_{2}_{3}", username, password, version, currentTime.Minute / 5);
//#endif



            if (Session[sessionKey] == null) {
                if (version == "0.7") {
                    Service2012 service = new Service2012();
                    Session[sessionKey] = service.GetSupply(username, password, "http://service.logicab.nl/Florecom/Jul2012/CommercialCustomer.asmx");
                } else if (version == "0.5") {
                    Service2010 service = new Service2010();
                    Session[sessionKey] = service.GetSupply(username, password);
                }

            }
            String sessionKeySuppliers = "suppliers";
            if (Session[sessionKeySuppliers] == null) {
                Session[sessionKeySuppliers] = EFHelper.ctx.F_CAB_ORGANISATION.Where(x => x.ORGA_EAN_CODE != "" && x.ORGA_NAME != null && x.ORGA_EAN_CODE != null).Select(x => new Supplier {
                    SupplierEAN = x.ORGA_EAN_CODE,
                    SupplierName = x.ORGA_NAME
                }).ToList();
            }
            List<ServiceOffer> offerlist = (List<ServiceOffer>)Session[sessionKey];
            List<Supplier> suppliers = (List<Supplier>)Session[sessionKeySuppliers];
            foreach (var item in offerlist) {
                var supplierEan = item.SupplierEAN;
                var supplierObj = suppliers.FirstOrDefault(x => x.SupplierEAN == supplierEan);
                if (supplierObj != null) {
                    if (supplierObj.SupplierName != null)
                        item.SupplierName = supplierObj.SupplierName;
                }
            }
            return offerlist;
        }

        private IQueryable<LogiLog> _getAppLogins() {
            //            var shopName = ConfigurationManager.AppSettings["LogShopName"] as String;
            //          shopName = shopName + "_log";
            //var evidence = new Evidence(Request.Url.AbsoluteUri);
            //            var shopName = evidence.SkinCode;
            //            shopName = shopName + "_log";
            var logins = from log in EFHelper.ctx.F_CAB_LOG
                         join login in EFHelper.ctx.F_CAB_LOGIN on log.CALO_FK_LOGIN equals login.LOGI_SEQUENCE
                         where log.CALO_ACTION == "App"
                         orderby log.CALO_SEQUENCE descending
                         select new LogiLog {
                             Seq = log.CALO_SEQUENCE,
                             CreatedOn = log.CALO_CREATED_ON,
                             Message = log.CALO_VALUE,
                             Login = login
                         };
            return logins;
        }

        private IEnumerable<LogiLog> _getLogiOfferList() {
            var oneDayAgo = DateTime.Now.AddDays(-30);
            var loglist = from log in EFHelper.ctx.F_CAB_LOG
                          join login in EFHelper.ctx.F_CAB_LOGIN on log.CALO_FK_LOGIN equals login.LOGI_SEQUENCE
                          where log.CALO_CREATED_ON > oneDayAgo
                          && log.CALO_ACTION == "LogiOffer"
                          && log.CALO_VALUE.Contains("logged in.")
                          orderby log.CALO_CREATED_ON descending
                          select new LogiLog {
                              Seq = log.CALO_SEQUENCE,
                              CreatedOn = log.CALO_CREATED_ON,
                              Message = log.CALO_VALUE,
                              Login = login
                          };
            return loglist;

        }
        private IQueryable<LoginLog> _getLogiOfferLogins() {
            //            var shopName = ConfigurationManager.AppSettings["LogShopName"] as String;
            //          shopName = shopName + "_log";
            //var evidence = new Evidence(Request.Url.AbsoluteUri);
            //            var shopName = evidence.SkinCode;
            //            shopName = shopName + "_log";
            var logins = from l in EFHelper.ctx.F_CAB_LOG
                         where l.CALO_ACTION.Contains("_log") && l.CALO_FK_LOGIN == null
                         orderby l.CALO_SEQUENCE descending
                         select new LoginLog {
                             Seq = l.CALO_SEQUENCE,
                             Value = l.CALO_VALUE,
                             Date = l.CALO_CREATED_ON,
                             Action = l.CALO_ACTION
                         };
            return logins;
        }

        private IEnumerable<LogiLog> _getLogiCABList() {
            var oneDayAgo = DateTime.Now.AddDays(-30);
            var loglist = from log in EFHelper.ctx.F_CAB_LOG
                          join login in EFHelper.ctx.F_CAB_LOGIN on log.CALO_FK_LOGIN equals login.LOGI_SEQUENCE
                          where log.CALO_CREATED_ON > oneDayAgo
                          && (log.CALO_ACTION == "LOGICAB" || log.CALO_ACTION == "LOGICAB LOGIN")
                          && log.CALO_VALUE.Contains("==Login==")
                          orderby log.CALO_CREATED_ON descending
                          select new LogiLog {
                              Seq = log.CALO_SEQUENCE,
                              CreatedOn = log.CALO_CREATED_ON,
                              Message = log.CALO_VALUE,
                              Login = login
                          };
            return loglist;
        }

        private IEnumerable<LogiLog> _getLogiOfferteList() {
            var oneDayAgo = DateTime.Now.AddDays(-30);
            var loglist = from log in EFHelper.ctx.F_CAB_LOG
                          join login in EFHelper.ctx.F_CAB_LOGIN on log.CALO_FK_LOGIN equals login.LOGI_SEQUENCE
                          where log.CALO_CREATED_ON > oneDayAgo
                          && log.CALO_ACTION == "LOGIOFFER LOGIN"
                          && log.CALO_VALUE.Contains("LogiCAB (LogiOfferte)")
                          orderby log.CALO_CREATED_ON descending
                          select new LogiLog {
                              Seq = log.CALO_SEQUENCE,
                              CreatedOn = log.CALO_CREATED_ON,
                              Message = log.CALO_VALUE,
                              Login = login
                          };
            return loglist;
        }

        private IEnumerable<LogiLog> _getLogiServiceList() {
            var oneDayAgo = DateTime.Now.AddDays(-30);
            var loglist = from log in EFHelper.ctx.F_CAB_LOG
                          join login in EFHelper.ctx.F_CAB_LOGIN on log.CALO_FK_LOGIN equals login.LOGI_SEQUENCE
                          //join order in EFHelper.ctx.F_CAB_ORDER_HEA on ((int)log.CALO_VALUE) equals order.ORHE_SEQUENCE
                          where log.CALO_CREATED_ON > oneDayAgo
                          && log.CALO_ACTION == "Service Order"
                          orderby log.CALO_CREATED_ON descending
                          select new LogiLog {
                              Seq = log.CALO_SEQUENCE,
                              CreatedOn = log.CALO_CREATED_ON,
                              Message = log.CALO_VALUE,
                              Login = login
                          };
            return loglist;
        }
    }
    public class LogiLog {
        public int Seq { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Message { get; set; }
        public F_CAB_LOGIN Login { get; set; }
    }
    public class LoginLog {
        //U:aablo;P:test1;IP:89.146.60.107;OK:YES 21-3-2014 13:43:14 
        public int Seq { get; set; }
        public string Value { get; set; }
        public string Username {
            get {
                return Value.Split(';')[0].Substring(2);
            }
        }
        public string Password {
            get {
                return Value.Split(';')[1].Substring(2);
            }
        }
        public string IPAddress {
            get {
                return Value.Split(';')[2].Substring(3);
            }
        }
        public bool Ok {
            get {
                return Value.Split(';')[3].Substring(3) == "YES";
            }
        }
        public string Organisation {
            get {
                //if(this.Value.)
                if (Value.IndexOf("O:") != -1)
                    return Value.Split(';')[4].Substring(2);
                return "Log-entry older than required buildversion";
            }
        }
        public String Action { get; set; }
        public string Shop {
            get {
                var sValue = Action.Substring(0, Action.Length - 4);
                if (sValue == "LogiOfferte")
                    return "LogiOffer";
                return sValue;
            }
        }
        public DateTime Date { get; set; }
    }
    public class LogiLogService {
        public int Seq { get; set; }
        public DateTime CreatedOn { get; set; }
        public F_CAB_ORDER_HEA Order { get; set; }
        public F_CAB_LOGIN Login { get; set; }
    }

    public class SupplyTradeLineItem {
        public DateTime LineDateTime { get; set; }

    }
}
