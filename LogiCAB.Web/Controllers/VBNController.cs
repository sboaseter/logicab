﻿using CAB.Domain.Providers;
using Ionic.Zip;
using log4net;
using LogiCAB.Web.Domain;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web.Mvc;
using System.Linq;
using LogiCAB.Web.DAL;
using CAB.Data.EF.Models;

namespace LogiCAB.Web.Controllers {
    [AuthorizeWithSessionAttribute]
    [HandleError]
    public class VBNController : BaseController {
        private ISettingsProvider _settingsProvider;

        /* IoC */
        public VBNController(
            ISettingsProvider settingsProvider,
            ILog log) : base(log)
        {
            _settingsProvider = settingsProvider;
        }

        public ActionResult Index() {
            string[] filesCode = Directory.GetFiles(Server.MapPath("~/tmp/"), "FP*.txt");
            if (filesCode.Length == 0) {
                try { 
                downloadVBNCodes();
                } catch (Exception ex) {
                    Log.ErrorFormat("Error downloading new VBNList: {0}", ex.ToString());
                    return View(new List<VBNCodeRecord>());
                }
            }
            return View(_getVBNList());
        }

        public ActionResult Link() {
            return View();
        }

        public int updateVBNExtraProperties() {
            string[] filesCode = Directory.GetFiles(Server.MapPath("~/tmp/"), "CY*.txt");
            string fileCode = filesCode[filesCode.Length - 1];
            var vbnExtraPropertiesTxt = (from line in ReadFrom(fileCode)
                               let record = ParseLineExtraProperty(line)
                               select record).ToList();
            var vbnPropDB = EFHelper.ctx.F_CAB_VBN_PROPERTIES_SPECIFIED.ToList();
            var vbnExtraPropertiesDB = (from vps in vbnPropDB
                                        select new ExtraPropertyRecord {
                                            CVPS_SEQUENCE = vps.CVPS_SEQUENCE,
                                            CVPS_VBN_CODE_D = vps.CVPS_VBN_CODE,
                                            CVPS_PROP_CODE = vps.CVPS_VBN_PROP_CODE,
                                            CVPS_MANDATORY_YN = Convert.ToInt32(vps.CVPS_MANDATORY_YN),
                                            CVPS_SORT_ORDER = Convert.ToInt32(vps.CVPS_SORT_ORDER),
                                            CVPS_VALID_FROM = Convert.ToInt32(vps.CVPS_VALID_FROM.Value)
                                      }).ToList();
            var newVBNExtraProperties = vbnExtraPropertiesTxt.Except(vbnExtraPropertiesDB, new VBNExtraPropComparer());
            int counter = 0;
            foreach (var np in newVBNExtraProperties) {
                F_CAB_VBN_PROPERTIES_SPECIFIED fcvps = new F_CAB_VBN_PROPERTIES_SPECIFIED {
                    CVPS_VBN_CODE = np.CVPS_VBN_CODE_D,
                    CVPS_VBN_PROP_CODE = np.CVPS_PROP_CODE,
                    CVPS_MANDATORY_YN = np.CVPS_MANDATORY_YN,
                    CVPS_SORT_ORDER = np.CVPS_SORT_ORDER,
                    CVPS_VALID_FROM = np.CVPS_VALID_FROM
                };

                EFHelper.ctx.F_CAB_VBN_PROPERTIES_SPECIFIED.AddObject(fcvps);
                counter++;
                if (counter > 50) {
                    EFHelper.ctx.SaveChanges();
                    counter = 0;
                }
            }
            EFHelper.ctx.SaveChanges();
            return 0;
        }

        private static List<VBNCodeRecord> _cachedVBNList = null;
        private List<VBNCodeRecord> _getVBNList() {
            if (_cachedVBNList != null)
                return _cachedVBNList;

            // Update f_Cab_cd_vbn_code with expired_on from textfile
            // Get codes that does not exist in table
            // merge result into projection VBNCodeMinExpired
            // Only show expired VBN-codes if they are used.
            // Add IsNew-property
            string[] filesCode = Directory.GetFiles(Server.MapPath("~/tmp/"), "FP*.txt");
            string fileCode = filesCode[filesCode.Length - 1];
            var vbnCodesTxt = (from line in ReadFrom(fileCode)
                              let record = ParseLineVBNCodes(line)
                              select record).ToList();
            DateTime now = DateTime.Now;
/*            var expiredVBNCodes = vbnCodesTxt.Where(x => x.ExpiredDate < now).ToList();
            var expiredVBNCodesOnly = expiredVBNCodes.Select(x => x.VBNC_VBN_CODE).ToList();

            var vbnCodestoUpdate = EFHelper.ctx.F_CAB_CD_VBN_CODE.Where(x => expiredVBNCodesOnly.Contains(x.VBNC_VBN_CODE));
            foreach (var ve in vbnCodestoUpdate)
            {
                ve.VBNC_EXPIRED_ON = expiredVBNCodes.Single(x => x.VBNC_VBN_CODE == ve.VBNC_VBN_CODE).ExpiredDate;
            }
            EFHelper.ctx.SaveChanges();
*/
            var vbnCodesDB = (from vbn in EFHelper.ctx.F_CAB_CD_VBN_CODE
                             select new VBNCodeRecord {
//                                 VBNC_SEQUENCE = vbn.VBNC_SEQUENCE,
                                 VBNC_VBN_CODE = vbn.VBNC_VBN_CODE,
                                 VBNC_DESC = vbn.VBNC_DESC,
                                 VBNC_FK_VBN_GROUP = vbn.VBNC_FK_VBN_GROUP,
                                 EntryDate = vbn.VBNC_CREATED_ON,
                                 ModifiedDate = vbn.VBNC_MODIFIED_ON,
                                 ExpiredDate = vbn.VBNC_EXPIRED_ON
                             }).ToList();

            var newVBNCodes = vbnCodesTxt.Except(vbnCodesDB, new VBNComparer());
            var result = newVBNCodes.ToList();

            return result;
        }

        public JsonResult AddNewVBNCodes() {
            var vbnlist = _getVBNList();
            var vbnGroupsTXT = vbnlist.Select(x => x.VBNC_FK_VBN_GROUP).ToList();
            var vbnGroupsDBtxt = EFHelper.ctx.F_CAB_CD_VBN_CODE_GROUP.Select(x => x.CDVG_CODE).ToList();
            var vbnGroupsDB = vbnGroupsDBtxt.Select(x => Int32.Parse(x)).ToList();
            var newVBNGroups = vbnGroupsTXT.Except(vbnGroupsDB).ToList();

            // Add new groups
            if (newVBNGroups.Count > 0) {
                string[] filesGroup = Directory.GetFiles(Server.MapPath("~/tmp/"), "FO*.txt");
                string fileGroup = filesGroup[filesGroup.Length - 1];
                var vbnGroupsTxt =  from line in ReadFrom(fileGroup)
                                    let record = ParseLineVBNGroups(line)
                                    select record;
                foreach (var newgroup in newVBNGroups) {
                    var newEntry = vbnGroupsTxt.FirstOrDefault(x => x.CDVG_CODE == newgroup.ToString());
                    F_CAB_CD_VBN_CODE_GROUP vg = new F_CAB_CD_VBN_CODE_GROUP {
                        CDVG_CODE = newEntry.CDVG_CODE,
                        CDVG_DESC = newEntry.CDVG_DESC,
                        CDVG_MAINGROUP = Int32.Parse(newEntry.CDVG_MAINGROUP)
                    };
                    EFHelper.ctx.F_CAB_CD_VBN_CODE_GROUP.AddObject(vg);
                    EFHelper.ctx.SaveChanges();
                }
            }

            // Add new codes
            int counter = 0;
            foreach (var newcode in vbnlist) {
                var vbnGroupTXT = newcode.VBNC_FK_VBN_GROUP.ToString();
                var vbnGroup = EFHelper.ctx.F_CAB_CD_VBN_CODE_GROUP.FirstOrDefault(x => x.CDVG_CODE == vbnGroupTXT);
                F_CAB_CD_VBN_CODE nc = new F_CAB_CD_VBN_CODE {
                    VBNC_VBN_CODE = newcode.VBNC_VBN_CODE,
                    VBNC_DESC = newcode.VBNC_DESC,
                    VBNC_DESC_UPPER = newcode.VBNC_DESC.ToUpper(),
                    VBNC_FK_VBN_GROUP = vbnGroup.CDVG_SEQUENCE,
                    VBNC_EXPIRED_ON = newcode.ExpiredDate,
                    VBNC_CREATED_ON = newcode.EntryDate,
                    VBNC_MODIFIED_ON = newcode.ModifiedDate,
                    VBNC_CREATED_BY = "LogiCAB",
                    VBNC_MODIFIED_BY = "LogiCAB"
                };
                EFHelper.ctx.F_CAB_CD_VBN_CODE.AddObject(nc);
                if (counter % 100 == 0) EFHelper.ctx.SaveChanges();                
                counter++;
            }
            EFHelper.ctx.SaveChanges();
            
            int numAdded = 42;
            return Json(new {
                NewVBNCodes = counter,
                NewVBNGroups = newVBNGroups.Count
            }, JsonRequestBehavior.AllowGet);
        }

        private static VBNCodeRecord ParseLineVBNCodes(string line) {
            //Example: 1;2;1;Agapanthus overig;AGAP OVERIG;;;0;10104101;19850101;;201006080944
            string[] elements = line.Split(';');
            return new VBNCodeRecord {
                VBNC_VBN_CODE = elements[1],
                VBNC_DESC = elements[4],
                VBNC_FK_VBN_GROUP = Int32.Parse(elements[8]),
                EntryDate = DateTime.ParseExact(String.IsNullOrEmpty(elements[9]) ? "21850101" : elements[9], "yyyyMMdd", CultureInfo.CurrentCulture),
                ExpiredDate = DateTime.ParseExact(String.IsNullOrEmpty(elements[10]) ? "21850101" : elements[10], "yyyyMMdd", CultureInfo.CurrentCulture),
                ModifiedDate = DateTime.ParseExact(String.IsNullOrEmpty(elements[11]) ? "218501010000" : elements[11], "yyyyMMddHHmm", CultureInfo.CurrentCulture),
            };
        }
        private static GroupRecord ParseLineVBNGroups(string line) {
            //Example: 16;10100302;Alstroemeria kleinbloemig;19970414;;200310010944
            string[] elements = line.Split(';');
            // VBN Group Codes starting with 1=Flowers, 2,3=Plants, 4=Misc
            string[] maingroups = new String[] { "x", "2", "1", "4" };
            int maingroup = (int)char.GetNumericValue(elements[1].ElementAt(0));
            return new GroupRecord {
                CDVG_CODE = elements[1],
                CDVG_DESC = elements[2],
                CDVG_MAINGROUP = maingroups[maingroup]
            };
        }

        private static ExtraPropertyRecord ParseLineExtraProperty(string line) {
            // 11;116975;S80;1;4;20150404;;201504021346
            string[] elements = line.Split(';');
            return new ExtraPropertyRecord {
                CVPS_SEQUENCE = 0,
                CVPS_VBN_CODE_D = Double.Parse(elements[1]),
                CVPS_PROP_CODE = elements[2],
                CVPS_MANDATORY_YN = Int32.Parse(elements[3]),
                CVPS_SORT_ORDER = Int32.Parse(elements[4]),
                CVPS_VALID_FROM = Int32.Parse(elements[5])
            };
        }

        #region UtilityMethods
        static IEnumerable<string> ReadFrom(string file) {
            string line;
            using (StreamReader reader = System.IO.File.OpenText(file)) {
                while ((line = reader.ReadLine()) != null) {
                    yield return line;
                }
            }
        }

        
        public JsonResult downloadVBNCodes() {
            try {
                var listOfFiles = Directory.GetFiles(Server.MapPath("~/tmp/"));
                foreach (string oneFile in listOfFiles) {
                    try {
                        System.IO.File.Delete(oneFile);
                    } catch (Exception ex) {
                        Log.Error(String.Format("Could not delete file: {0}, {1}", oneFile, ex.ToString()));
                    }
                }
                WebClient Client = new WebClient();
                string saveToFile = String.Format("{0}{1}", Server.MapPath("~/tmp/"), "codelist.zip");
                Client.DownloadFile("http://codes.florecom.nl/flocodis/distributie.aspx?item=100018&type=zip", saveToFile);
                ExtractFile(saveToFile, Server.MapPath("~/tmp/"));
            } catch (Exception ex) {
                Log.Error(String.Format("Error during download/extraction: {0}", ex.ToString()));
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }

            return Json("Success", JsonRequestBehavior.AllowGet);
        }
        private void ExtractFile(string zipToUnpack, string unpackDirectory) {
            using (ZipFile zip1 = ZipFile.Read(zipToUnpack)) {
                foreach (ZipEntry e in zip1) {
                    e.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);
                }
            }
        }
        #endregion



    }

    public class GroupRecord {
        public string CDVG_CODE { get; set; }
        public string CDVG_DESC { get; set; }
        public string CDVG_MAINGROUP { get; set; }
        public int exists { get; set; }
    }
    public class VBNCodeRecord {
        public int VBNC_SEQUENCE { get; set; }
        public string VBNC_VBN_CODE { get; set; }
        public string VBNC_DESC { get; set; }
//        public string VBNC_DESC_UPPER { get; set; }
        public int VBNC_FK_VBN_GROUP { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
    }

    public class ExtraPropertyRecord {
        public int CVPS_SEQUENCE { get; set; }
        public string CVPS_VBN_CODE {
            get {
                return this.CVPS_VBN_CODE_D.ToString();
            }
        }
        public double CVPS_VBN_CODE_D { get; set; }
        public string CVPS_PROP_CODE { get; set; }
        public int CVPS_MANDATORY_YN { get; set; }
        public int CVPS_SORT_ORDER { get; set; }
        public int CVPS_VALID_FROM { get; set; }
    }

    public class VBNComparer : IEqualityComparer<VBNCodeRecord>
    {
        public int GetHashCode(VBNCodeRecord vcr)
        {
            if (vcr == null)
            {
                return 0;
            }
            return vcr.VBNC_VBN_CODE.GetHashCode();
        }

        public bool Equals(VBNCodeRecord x1, VBNCodeRecord x2)
        {
		    return x1.VBNC_VBN_CODE == x2.VBNC_VBN_CODE;

        }
    }
    public class VBNExtraPropComparer : IEqualityComparer<ExtraPropertyRecord> {
        public int GetHashCode(ExtraPropertyRecord epr) {
            if (epr == null) return 0;
            return epr.CVPS_VBN_CODE.GetHashCode();
        }
        public bool Equals(ExtraPropertyRecord x1, ExtraPropertyRecord x2) {
            return x1.CVPS_VBN_CODE == x2.CVPS_VBN_CODE && x1.CVPS_PROP_CODE == x2.CVPS_PROP_CODE;
        }
    }

}
