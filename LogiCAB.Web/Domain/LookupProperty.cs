using System;
using System.Collections.Generic;
using System.Linq;

namespace LogiCAB.Web.Domain {
    public class LookupProperty {
        public int? seq { get; set; }
        public string value { get; set; }
        public string desc { get; set; }
    }
}
