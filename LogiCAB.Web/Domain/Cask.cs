using System;
using System.Collections.Generic;
using System.Linq;

namespace LogiCAB.Web.Domain {
    public class Cask {
        public int seq { get; set; }
        public bool? showcask { get; set; }
        public string caskcode { get; set; }
        public string caskdesc { get; set; }
        public decimal cask_prunit { get; set; }
        public decimal cask_prlayer { get; set; }
        public decimal cask_prtrolley { get; set; }
        public decimal cask_prbunch { get; set; }
        public short? transportType { get; set; }

        public string desc {
            get {
                return String.Format("{0} - {1}", caskcode, caskdesc);
            }
        }

        public override string ToString() {
            return String.Format("Seq: {0}, Show: {1}, Code: {2}, Desc: {3}, PrUnit: {4}, PrLayer: {5}, PrTrolley: {6}, PrBunch: {7}, Transport: {8}",
                seq, showcask, caskcode, caskdesc, cask_prunit, cask_prlayer, cask_prtrolley, cask_prbunch, transportType
                );
        }
    }
}
