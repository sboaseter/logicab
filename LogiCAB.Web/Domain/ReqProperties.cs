using System;
using System.Collections.Generic;
using System.Linq;

namespace LogiCAB.Web.Domain {
    [Serializable]
    public class ReqProperties {
        public int seq { get; set; }
        public string value { get; set; }
        public string desc { get; set; }
        public bool required { get; set; }
    }
}
