﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class VBNSpecifiedProperties {
        public int CVPS_SEQUENCE { get; set; }
        public double CVPS_VBN_CODE { get; set; }
        public string CVPS_VBN_PROP_CODE { get; set; }
        public string Desc { get; set; }
        public double CVPS_MANDATORY_YN { get; set; }
        public double? CVPS_SORT_ORDER { get; set; }
        public double? CVPS_VALID_FROM { get; set; }
        public string VBN_PROP_LOOKUP { get; set; }
        public string VBN_PROP_LOOKUP_DESC { get; set; }
    }
}

/*
 *   CVPS_SEQUENCE]
    ,[CVPS_VBN_CODE]
    ,[CVPS_VBN_PROP_CODE]
    ,[CVPS_MANDATORY_YN]
    ,[CVPS_SORT_ORDER]
    ,[CVPS_VALID_FROM]
 * 
 * 
*/