﻿using CAB.Domain.Repositories;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public interface IEmailProvider {
         void Show(int num);
    }
}