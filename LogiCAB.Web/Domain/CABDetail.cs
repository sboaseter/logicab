﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class CABDetail {
        public int CABD_SEQUENCE { get; set; }
        public int CABD_FK_CAB_CODE { get; set; }
        public string CDPT_DESC { get; set; }
        public bool CDPT_MANDATORY_YN { get; set; }
        public string CABD_VALUE { get; set; }
        public string CABD_VALUE_DESC { get; set; }
        public decimal CABD_ORDER { get; set; }
        public int? CDPT_FK_VBN_PROP_TYPE { get; set; }
    }
}