﻿using CAB.Domain.Repositories;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class EmailProvider : IEmailProvider {
        private IOrganisationRepository _organisationRepository;
        public ILog Log;
        public EmailProvider(IOrganisationRepository organisationRepository, ILog log) {
            Log = log;
            _organisationRepository = organisationRepository;
        }
        public void Show(int num) {
            Log.InfoFormat("Hello from Hangfire! Num: {0}", num);
        }
    }
}