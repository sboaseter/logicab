using CAB.Data.EF.Models;
using CAB.Domain.Helpers;
using LogiCAB.Web.DAL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LogiCAB.Web.Domain {
    public class CABRequest {

        public string getCABCode() {
            if(cabcode != null) {
                var CABCodeObj = EFHelper.ctx.F_CAB_CD_CAB_CODE.FirstOrDefault(x => x.CABC_SEQUENCE == cabcode.Value).CABC_CAB_CODE;
                return CABCodeObj;
            }
            return "0";
        }


        public int seq { get; set; }
        public int? cabcode { get; set; }
        public string requester { get; set; }
        public DateTime? request_date { get; set; }
        public string vbnimage { get; set; }
        public string picture { get; set; }
        private F_CAB_CD_VBN_CODE _vbn;
        private int? _Pictureseq;
        public int? pictureseq {
            get { return _Pictureseq; }
            set {
                if (value == null) {
                    //var vkc = Utils.getVKCImage(vbn.VBNC_VBN_CODE);
                    var vkc = "nopic.jpg";
                    if (vkc == "nopic.jpg") {
                        picture = String.Format("~/Content/nopic.jpg");
                    } else {
                        picture = String.Format("http://www.logicab.nl/logicab/vkc/{0}.jpg", ImageUtils.getVKCImage(vbn.VBNC_VBN_CODE));
                    }
                } else {
                    picture = String.Format("~/Image/ShowImageSmall/{0}", value);
                }
                _Pictureseq = value;
            }
        }
  
        public bool? approve { get; set; }
        public F_CAB_CD_VBN_CODE vbn {
            get {
                return _vbn;
            }
            set {
                vbncode = String.Format("{0} - {1}", value.VBNC_VBN_CODE, value.VBNC_DESC);
                _vbn = value;
            }
        }
        public string cabdesc { get; set; }
        public string cabgroup { get; set; }
        public F_CAB_CD_GROUP_TYPE cdgt { get; set; }
        public F_CAB_CD_CAB_GROUP cdgr { get; set; }
        public F_CAB_CD_PD_GROUP pdgr { get; set; }
        public string handledby { get; set; }
        public DateTime? handledon { get; set; }
        public string authremark { get; set; }
        public string remark { get; set; }
        private string _status;
        public string status {
            get {
                return _status;
            }
            set {
//                _status = Extensions.dict(Usr.Language, value);
                switch (value) {
                    case "REJECTED":
                    _status = "Afgekeurd";
                    break;
                    case "NEW":
                    _status = "Aangevraagd";
                    break;
                    case "ADDED":
                    _status = "Goedgekeurd";
                    break;
                    case "EXIST":
                    _status = "Bestaat al";
                    break;
                    case "REQ":
                    _status = "Aangevraagd";
                    break;
                    case "INCOMPLETE":
                    _status = "Onvolledig";
                    break;
                    case "REQEXIST":
                    _status = "Bestaat al";
                    break;
                    default:
                    _status = value;
                    break;
                }
            }
        }
        private string _vbncode;
        public string vbncode {
            set { _vbncode = value; }
            get {
                return _vbncode;
            }
        }
    }
}
