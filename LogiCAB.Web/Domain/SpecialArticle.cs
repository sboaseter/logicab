﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain
{
    public class SpecialArticle
    {
        private string IMG_BASEURL;
        public SpecialArticle() {
            try {
            IMG_BASEURL = ConfigurationManager.AppSettings["imageBaseUrl"].ToString();
            } catch(Exception ex) {
                IMG_BASEURL =  "http://www.logicab.nl/logicab/Image/";
            }
        }
        public Tuple<string, string> CAB { get; set; }
        public Tuple<string, string> VBN { get; set; }

        public Tuple<DateTime, string> RequestInfo { get; set; }
        public Tuple<DateTime, string> ApproveInfo { get; set; }

        public List<Tuple<string, string>> Properties { get; set; }

        public int PictureSequence { get; set; }
        public string PictureURL
        {
            get
            {
                return String.Format("{0}/ShowImage/{1}", IMG_BASEURL, this.PictureSequence);
            }
        }
    }
}