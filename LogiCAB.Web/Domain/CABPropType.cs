﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class CABPropType {
        public int Sequence { get; set; }
        public string Description { get; set; }
        public bool Mandatory { get; set; }
    }
}