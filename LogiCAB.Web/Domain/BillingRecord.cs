﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    [Serializable]
    public class BillingRecordL {
        Func<int, string> getSubscriptionType = ost => {
            switch (ost) {
                case 1:
                    return "Goud";
                case 2:
                    return "Zilver";
                case 3:
                    return "Bronze";
                default:
                    return "N/A";
            }
        };
        public string Organisation { get; set; }
        public string Commisionaire { get; set; }
        public double _Total { get; set; }
        public string Total {
            get {
                return _Total.ToString("C2");
            }
        }
        public int _Subscription { get; set; }
        public string Subscription {
            get {
                return getSubscriptionType(this._Subscription);
            }
        }
        public bool LogiService { get; set; }
        public double Commision {
            get {
                switch (this._Subscription) {
                    case 1: // Gold
                        return 0;
                    case 2: // Silver
                        return this._Total * 0.005;
                    case 3: // Bronze
                    case 0: // kvk (service)
                        return this._Total * 0.01;
                    default:
                        return 0;
                }
            }
        }
    }
    public class BillingRecord {
        public string Shop { get; set; }
        public string BuyerName { get; set; }
        public int BuyerContract { get; set; }
        public string BuyerContractStr {
            get {
                switch (BuyerContract) {
                    case ORAD_MemberType.BRONZE:
                        return "Bronze";
                    case ORAD_MemberType.ZILVER:
                        return "Zilver";
                    case ORAD_MemberType.GOUD:
                        return "Goud";
                    default:
                        return "N/A";
                }
            }
        }
        public bool BuyerService { get; set; }
        public decimal DetailLinePrice { get; set; }
        public int OrderHeaderSequence { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderDateShort {
            get {
                return OrderDate.ToShortDateString();
            }
        }
        public string OrderNo { get; set; }
        public string OrderDesc { get; set; }
        public DateTime OrderDeliveryDate { get; set; }
        public double OrderNettValue { get; set; }
        public string PersonName { get; set; }
        public string PersonFirstName { get; set; }
        public string PersonMiddleName { get; set; }
        public string PersonLastName { get; set; }
        public string OrderRemark { get; set; }
        public int BuyerOrgaSequence { get; set; }
        public string BuyerOrgaName { get; set; }
        public string BuyerOrgaCode { get; set; }

        public int GrowerOrgaSequence { get; set; }
        public string GrowerOrgaName { get; set; }
        public string GrowerOrgaCode { get; set; }
        public int GrowerContract { get; set; }
        public string GrowerContractStr {
            get {
                switch (GrowerContract) {
                    case ORAD_MemberType.BRONZE:
                        return "Bronze";
                    case ORAD_MemberType.ZILVER:
                        return "Zilver";
                    case ORAD_MemberType.GOUD:
                        return "Goud";
                    default:
                        return "N/A";
                }
            }
        }
        public int OrderDetailSequence { get; set; }
        public int CabcodeSequence { get; set; }
        public string MainGroup { get; set; }
        public string VBNCode { get; set; }

        public decimal NumOfItems { get; set; }
        public decimal NumOfUnits { get; set; }
        public decimal NumOfItemsPrUnit { get; set; }
        public decimal OrderDetailPrice { get; set; }
        public decimal OrderDetailNettPrice { get; set; }
        public string OrderDetailRemark { get; set; }
        public string OrderDetailCustomerCode { get; set; }
    }
}