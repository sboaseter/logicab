﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class Order {
        /*
         * OrderDetail materialization
         */ 
        public int seq { get; set; }
        public string buyer { get; set; }
        public string grower { get; set; }
        public DateTime date { get; set; }
        public string remark { get; set; }
        public string orheno { get; set; }
        public string caskdesc { get; set; }
        public string cask { get; set; }
        private string _desc { get; set; }
        public string desc {
            get {
                return cabprops;
            }
            set {
                _desc = value;
            }
        }
        public decimal items { get; set; }
        public decimal units { get; set; }
        public decimal ipu { get; set; }
        public decimal itemprice { get; set; }
        public decimal price { get; set; }
        public DateTime? deliverydate { get; set; }
        public string custcode { get; set; }
        public string cabprops { get; set; }
        public string length { get; set; }
        public string status { get; set; }

        public string transporter { get; set; }

    }
}