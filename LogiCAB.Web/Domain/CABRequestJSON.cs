﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class CABRequestJSON {
        public string AuthorizerRemark { get; set; }
        public string CABDescRequested { get; set; }
        public string CABGroupSeq { get; set; }
        public string CABGroupTypeSeq { get; set; }
        public string PDGroupSeq { get; set; }
        public string Seq { get; set; }
        public string VBNInfo { get; set; }
    }
}