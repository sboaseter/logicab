using CAB.Data.EF.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace LogiCAB.Web.Domain {
    public class Assortiment {
        public int seq { get; set; }
        public bool active { get; set; }
        public string cabcode { get; set; }
        public string prop { get; set; }

        public string picture {
            get {
                return String.Format("{0}ShowImageSmall/{1}", ConfigurationManager.AppSettings["imageBaseUrl"] as String, pictureseq);

            }
        }
        public int? cabpictureseq { get; set; }
        public int? graspictureseq { get; set; }

        public int? pictureseq {
            get {
                return graspictureseq ?? cabpictureseq;
            }
        }
        public decimal? numitems { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? end_date { get; set; }
        public string desc {
            get {
                return this.cabcode + ": " + prop;
            }
        }
        public string grower_id { get; set; }
    }
}
