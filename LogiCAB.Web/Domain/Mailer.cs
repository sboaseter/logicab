﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace LogiCAB.Web.Domain {
    public class Mailer {
        readonly SmtpClient SMTP = new SmtpClient();
        public string MailServer { get; set; }
        public MailAddress FromAddress { get; set; }
        public List<MailAddress> ToAddress;
        public string Subject { get; set; }
        public string Template { get; set; }

        public string ToAddressList {
            get {
                return string.Join(", ", this.ToAddress.Select(x => x.Address).ToArray());
            }
        }


        public Mailer(string mailserver) {
            MailServer = mailserver;
            ToAddress = new List<MailAddress>();
        }

        public void SendEmail() {
            SMTP.Host = MailServer;
            var mail = new MailMessage { From = FromAddress, IsBodyHtml = true };
            foreach (MailAddress adr in ToAddress)
                mail.To.Add(adr);
            mail.Subject = Subject;
            mail.Body = Template;

            try {
#if DEBUG
                SMTP.Credentials = new System.Net.NetworkCredential("sigurd.boaseter@logiflora.nl", "!@#lf");
#endif

                SMTP.Send(mail);
            } catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }
    }
}
