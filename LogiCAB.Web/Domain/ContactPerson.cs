﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class ContactPerson {
        public int Sequence { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string FirstName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string LoginPass { get; set; }
        public short LoginBlocked { get; set; }
    }
}