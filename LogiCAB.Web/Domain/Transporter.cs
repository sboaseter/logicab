﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogiCAB.Web.Domain {
    public class Transporter {
        public int? Seq { get; set; }
        public string Name { get; set; }
    }
}
