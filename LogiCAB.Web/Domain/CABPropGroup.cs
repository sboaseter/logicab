﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class CABPropGroup {
        public int Sequence { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
    }
}