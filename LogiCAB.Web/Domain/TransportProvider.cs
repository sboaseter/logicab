﻿using CAB.Data.EF.Models;
using CAB.Domain.Repositories;
using log4net;
using LogiCAB.Web.DAL;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects.DataClasses;
using System.IO;
using System.Linq;
using System.Web;
/*
 * TODO: Translation-lookups for labels/text
 * 
 */ 
namespace LogiCAB.Web.Domain {
    public class TransportProvider {
        private Entities ctx;
        private ILog Log;
        static Func<int, string> OrderDetailInfo;
        public static bool inProcess = false;
        private ITranslationRepository translationRepository;
        public TransportProvider(TranslationRepository _translationRepository, ILog log) {
            translationRepository = _translationRepository;
            this.Log = log;
            ctx = new Entities();
            OrderDetailInfo = o => ctx.F_CAB_CD_CAB_CODE.Single(x => x.CABC_SEQUENCE == o).CABC_CAB_DESC;
        }
        public bool InProcess() {
            return inProcess;
        }
        public void Generate() {
            inProcess = true;
            var handshakeType = ctx.F_CAB_HANDSHAKE_TYPE.Single(x => x.HATY_CODE == "Normal");
            var transporters = ctx.F_CAB_ORGANISATION.Where(x => x.ORGA_TYPE == "LOGP").Select(y => y.ORGA_SEQUENCE).ToList(); // Create Outer loop, collect
            // Foreach transporter - One Excel pr Transporter.
            foreach (var t in transporters) {
                List<List<DeliveryOrder>> Mainlist = new List<List<DeliveryOrder>>();
                var orga = ctx.F_CAB_ORGANISATION.Single(x => x.ORGA_SEQUENCE == t);
                var handshakesWithTransport = ctx.F_CAB_HANDSHAKE_OPTIONS.Where(x => x.HNDO_FK_ORGA_SEQ != null && x.HNDO_FK_ORGA_SEQ == t && x.F_CAB_HANDSHAKE.HAND_FK_HATY_SEQ == handshakeType.HATY_SEQUENCE);

                // Foreach relation where the transporter is involved
                foreach (var hndo in handshakesWithTransport) {
                    var handSeq = (int)hndo.HNDO_FK_HAND_SEQ;
                    Log.InfoFormat("\tChecking handshake: {0} ({1} - {2})", handSeq, hndo.F_CAB_HANDSHAKE.F_CAB_ORGANISATION.ORGA_NAME, hndo.F_CAB_HANDSHAKE.F_CAB_ORGANISATION1.ORGA_NAME);
                    try { 
                    List<DeliveryOrder> orderlist = getOrders(hndo.F_CAB_HANDSHAKE); // Orders between Grower X and Buyer Y, can be multiple "DeliveryOrders"
                    if (orderlist.Count() > 0)
                        Mainlist.Add(orderlist);
                    } catch (Exception ex) {
                        Log.ErrorFormat("Error retrieving orderlist from Handshake: {0}, Exception: {1}", hndo.HNDO_FK_HAND_SEQ, ex.ToString());
                    }
                }
                // Mainlist contains orders between growers/buyers who have the transporter set as the provider.
                // Pr Transporter -> Pr Relation -> Pr Order
                int language = 1;
                try {
                    language = orga.F_CAB_PERSON.FirstOrDefault().F_CAB_LOGIN.FirstOrDefault().LOGI_FK_CDLA_SEQ ?? 1;
                } catch (Exception ex) {
                    Log.ErrorFormat("Incomplete organisation, lacking Language for logins. {0} : {1}", orga.ORGA_SEQUENCE, ex.ToString());
                }
                GenerateReport(Mainlist, orga.ORGA_NAME, orga.ORGA_SEQUENCE, language);
            }
            inProcess = false;
        }
        private List<DeliveryOrder> getOrders(F_CAB_HANDSHAKE handshake) {
            List<DeliveryOrder> ret = new List<DeliveryOrder>();
            if (handshake == null) return null; // Handshake does not exist.
            var handshakeOption = ctx.F_CAB_HANDSHAKE_OPTIONS.FirstOrDefault(x => x.HNDO_FK_HAND_SEQ == handshake.HAND_SEQUENCE);
            if (handshakeOption == null) return null; // No handshake option - ERROR	
            else {
                if (handshakeOption.HNDO_FK_ORGA_SEQ == null) {
                    return null; // No transporter
                } 
            }
            var transporterOrga = ctx.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == handshakeOption.HNDO_FK_ORGA_SEQ);
            var supplierOrga = ctx.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == handshake.HAND_FK_ORGA_SEQ_GROWER);
            var customerOrga = ctx.F_CAB_ORGANISATION.FirstOrDefault(x => x.ORGA_SEQUENCE == handshake.HAND_FK_ORGA_SEQ_BUYER);
            if (transporterOrga == null || supplierOrga == null || customerOrga == null) {
                return null;
            }
            var supplierGrower = ctx.F_CAB_GROWER.FirstOrDefault(x => x.GROW_FK_ORGA == supplierOrga.ORGA_SEQUENCE);
            var customerBuyer = ctx.F_CAB_BUYER.FirstOrDefault(x => x.BUYR_FK_ORGA == customerOrga.ORGA_SEQUENCE);
            DateTime cutOff = DateTime.Now.AddDays(-4);
            // Every 5 minutes?
            var orderheaders = ctx.F_CAB_ORDER_HEA.Where(x => x.ORHE_FK_GROWER == supplierGrower.GROW_SEQUENCE && x.ORHE_FK_BUYER == customerBuyer.BUYR_SEQUENCE && x.ORHE_CREATED_ON > cutOff && x.ORHE_FK_ORDER_STATUS != 6);
            foreach (var oh in orderheaders) { // Accumulate orderheaders between Supplier & Comm
                var oforOrder = oh.F_CAB_ORDER_DET.FirstOrDefault().ORDE_SEQUENCE;
                var oforOfdt = ctx.F_CAB_OFFER_ORDER_MATRIX.Single(x => x.OFOR_FK_ORDER_DET == oforOrder);
                var ofhd = oforOfdt.F_CAB_OFFER_DETAILS.F_CAB_OFFER_HEADERS;
                ret.Add(new DeliveryOrder(supplierOrga, transporterOrga, customerOrga, oh, ofhd));
            }
            return ret;
        }
        public static string MapPath(string path) {
            if (HttpContext.Current != null)
                return HttpContext.Current.Server.MapPath(path);

            return HttpRuntime.AppDomainAppPath + path.Replace("~", string.Empty).Replace('/', '\\');
        }
        private bool GenerateReport(List<List<DeliveryOrder>> Mainlist, string orga, int orgasequence, int language) {
            var Mappingpath = MapPath("~/Content/Transport");
            FileInfo templateFile = new FileInfo(String.Format("{0}/OrderExcelOverviewTemplateRaw.xlsx", Mappingpath));
            string format = "ddMMyy"; 
            FileInfo newFile = new FileInfo(String.Format("{0}/OrderExcelOverviewOutput_{1}_{2}.xlsx", Mappingpath, orgasequence, DateTime.Now.ToString(format)));

            Func<string,string> tr = (r) => translationRepository.Get(language, r);

            Log.InfoFormat("Generating Pickuplist for: {0}: {1}", orga, newFile);
            decimal totalVolume = 0;
            using (ExcelPackage p = new ExcelPackage(templateFile, true)) { // todo, template
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                int curRow = 2;
                const int headerRow = 1;
                //ws.Cells[headerRow, 1].Value = "Transporter-list\n" + orga;
                ws.Cells[headerRow, 1].Value = String.Format("{0}\n{1}", tr("transporter"), orga);
                ws.Cells[headerRow, 2].Value = String.Format("{0}\n{1}", tr("generated"), DateTime.Now);
                

                for (int i = 1; i < 8; i++) {
                    ws.Cells[headerRow, i].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                    ws.Cells[headerRow, i].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                }
                string bulkText = string.Empty;
                foreach (List<DeliveryOrder> OrderList in Mainlist) {
                    var validFrom = OrderList.FirstOrDefault().ValidFrom;
                    var validTo = OrderList.FirstOrDefault().ValidTo;
                    var orderCount = OrderList.Count();

                    foreach (var o in OrderList) {	 // calculate volume of details before looping
                        decimal subTotalVolume = 0;
                        bulkText = o.Bulk;

                        ws.Cells[curRow, 1].Value = tr("Supplier");
                        ws.Cells[curRow, 2].Value = o.Supplier;
                        ws.Cells[curRow, 3].Value = o.SupplierAddr;
                        ws.Row(curRow).Style.Font.Bold = true;
                        curRow++;
                        ws.Cells[curRow, 1].Value = tr("Customer");;
                        ws.Cells[curRow, 2].Value = o.Buyer;
                        ws.Cells[curRow, 3].Value = o.BuyerAddr;
                        ws.Row(curRow).Style.Font.Bold = true;
                        curRow++;
                        ws.Cells[curRow, 1].Value = tr("Delivery");
                        ws.Cells[curRow, 2].Value = o.DeliveryDate.ToString();
                        ws.Cells[curRow, 2].Style.Font.Color.SetColor(System.Drawing.Color.Red);
                        curRow++;
                        int subTotalVolumeRow = curRow;
                        ws.Cells[curRow, 1].Value = tr("Volume");
                        ws.Cells[curRow, 2].Value = 3.14;
                        ws.Cells[curRow, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        curRow++;
                        ws.Cells[curRow, 1].Value = String.Format("{0}: {1}", tr("ordernr"), o.OrderNr);
                        for (int i = 1; i < 8; i++) {
                            ws.Cells[curRow, i].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            ws.Cells[curRow, i].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                        }
                        curRow++;
                        ws.Cells[curRow, 1].Value = tr("Articles");
                        ws.Cells[curRow, 3].Value = tr("Ordered");
                        ws.Cells[curRow, 4].Value = tr("Cask");
                        ws.Cells[curRow, 5].Value = tr("Volume");
                        ws.Row(curRow).Style.Font.Italic = true;
                        curRow++;
                        foreach (var d in o.Details) {
                            ws.Cells[curRow, 1].Value = OrderDetailInfo(d.ORDE_FK_CAB_CODE);
                            ws.Cells[curRow, 3].Value = String.Format("{0} x {1} {2}", d.ORDE_NUM_OF_UNITS, d.ORDE_NUM_OF_ITEMS_PER_UNIT, "APE");
                            ws.Cells[curRow, 4].Value = d.F_CAB_CD_CASK.CDCA_CASK_CODE;
                            var caca = d.F_CAB_CAB_CASK_MATRIX;
                            var units = d.ORDE_NUM_OF_UNITS;
                            var unitsPrLayer = caca.CACA_UNITS_PER_LAYER == 0 ? 1 : caca.CACA_UNITS_PER_LAYER;
                            var layersPrTrolley = caca.CACA_LAYERS_PER_TROLLEY == 0 ? 1 : caca.CACA_LAYERS_PER_TROLLEY;
                            var vol = units / unitsPrLayer / layersPrTrolley;
                            ws.Cells[curRow, 5].Value = vol.ToString("0.#0"); ;
                            totalVolume += vol;
                            subTotalVolume += vol;
                            ws.Cells[curRow, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            ws.Row(curRow).Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Row(curRow).Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);
                            curRow++;
                        }
                        ws.Cells[subTotalVolumeRow, 2].Value = subTotalVolume.ToString("0.#0");
                        for (int i = 1; i < 8; i++) {
                            ws.Cells[curRow - 1, i].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                            ws.Cells[curRow - 1, i].Style.Border.Bottom.Color.SetColor(System.Drawing.Color.Black);
                        }
                        ws.Row(curRow).Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.Row(curRow).Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.White);
                        curRow++;
                    }
                }
//                ws.Cells[headerRow, 3].Value = "Total Volume\n" + totalVolume.ToString("0.#0"); ;

//                ws.Cells[headerRow, 3].Value = "Total Volume: " + totalVolume.ToString("0.#0") + "\nBulk: " + bulkText;
                ws.Cells[headerRow, 3].Value = String.Format("{0} {1}: {2}\n{3}: {4}", tr("total"), tr("volume"), totalVolume.ToString("0.#0"), tr("Bulk"), bulkText);
                for (int i = 1; i < 100; i++) {
                    for (int u = 8; u < 25; u++) {
                        ws.Cells[i, u].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.Cells[i, u].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.White);
                    }
                }
                Byte[] bin = p.GetAsByteArray();
                System.IO.File.WriteAllBytes(newFile.FullName, bin);
            }
            return true;
        }

        public void TestImpl() {
            Log.Info("TestImpl from TransportProvider-class library.");
        }
    }
    public class Address {
        public string City { get; set; }
        public string Street { get; set; }
        public string StreetNr { get; set; }
        public string Zipcode { get; set; }
        public override string ToString() {
            return String.Format("{0} {1}, {2} {3}", this.StreetNr, this.Street, this.Zipcode, this.City);
        }
    }
    public class DeliveryOrder {
        static Func<F_CAB_ADDRESS, Address> convertToStr;
        static Func<int, string> StatusText;
        private Entities ctx;
        public DeliveryOrder(F_CAB_ORGANISATION supplierOrga, 
                             F_CAB_ORGANISATION transporterOrga, 
                             F_CAB_ORGANISATION buyerOrga, 
                             F_CAB_ORDER_HEA order, 
                             F_CAB_OFFER_HEADERS offer) {
            this.ctx = new Entities();
            this.SupplierOrga = supplierOrga;
            this.TransporterOrga = transporterOrga;
            this.BuyerOrga = buyerOrga;
            this.Order = order;
            this.Offer = offer;
            convertToStr = a => a == null ? new Address() : new Address {
                City = a.ADDR_CITY,
                Street = a.ADDR_STREET,
                StreetNr = a.ADDR_NO,
                Zipcode = a.ADDR_ZIPCODE
            };
            StatusText = s => ctx.F_CAB_CD_ORD_HEA_STATUS.Single(x => x.CDOS_SEQUENCE == s).CDOS_SYS_STATUS;
        }
        public string Supplier { get { return this.SupplierOrga.ORGA_NAME; } }
        public object SupplierAddr { get { return convertToStr(this.SupplierOrga.F_CAB_ADDRESS.FirstOrDefault()); } }
        public string Transporter { get { return this.TransporterOrga.ORGA_NAME; } }
        public object TransporterAddr { get { return convertToStr(this.TransporterOrga.F_CAB_ADDRESS.FirstOrDefault()); } }
        public string Buyer { get { return this.BuyerOrga.ORGA_NAME; } }
        public object BuyerAddr { get { return convertToStr(this.BuyerOrga.F_CAB_ADDRESS.FirstOrDefault()); } }

        public string Bulk {
            get {
                return this.Order.F_CAB_CD_CASK.CDCA_CASK_CODE;
            }
        }

        private F_CAB_ORGANISATION SupplierOrga { get; set; }
        private F_CAB_ORGANISATION TransporterOrga { get; set; }
        private F_CAB_ORGANISATION BuyerOrga { get; set; }
        private F_CAB_ORDER_HEA Order { get; set; }
        private F_CAB_OFFER_HEADERS Offer { get; set; }
        public EntityCollection<F_CAB_ORDER_DET> Details { get { return this.Order.F_CAB_ORDER_DET; } }
        private int _Status { get { return this.Order.ORHE_FK_ORDER_STATUS; } }

        public DateTime ValidFrom { get { return this.Offer.OFHD_VALID_FROM; } }
        public DateTime ValidTo { get { return this.Offer.OFHD_VALID_TO; } }

        public int NumDetails { get { return this.Details.Count(); } }
        public double NettValue { get { return this.Order.ORHE_NETT_HEADER_VALUE; } }
        public int Sequence { get { return this.Order.ORHE_SEQUENCE; } }

        public string Status { get { return StatusText(this._Status); } }
        public string OrderNr { get { return this.Order.ORHE_NO; } }
        public string OrderDesc { get { return this.Order.ORHE_DESC; } }
        public DateTime? DeliveryDate { get { return this.Order.ORHE_DELIVERY_DATE; } }
        public DateTime OrderDate { get { return this.Order.ORHE_DATE; } }
        public DateTime CreatedDate { get { return this.Order.ORHE_CREATED_ON; } }
        public DateTime ExpireDate { get { return this.Offer.OFHD_VALID_TO; } }
        public int OfferSequence { get { return this.Offer.OFHD_SEQUENCE; } }
    }
}