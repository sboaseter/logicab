﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class Heartbeat {
        public int Sequence { get; set; }
        public DateTime time { get; set; }
    }
}