﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class CABPropLookup {
        public int Sequence { get; set; }
        public string Value { get; set; }
        public int Order { get; set; }
    }
}