﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class GRASPropType {
        public int CDGP_SEQUENCE {get;set;}
        public string CDGP_DESC {get;set;}
    }

    public class GRASPropLookup {
        public int CDGL_SEQUENCE { get; set; }
        public int CDGL_FK_CDGP_SEQ { get; set; }
        public string CDGL_LOOKUP_VALUE { get; set; }
        public string CDGL_LOOKUP_DESC { get; set; }
        public int CDGL_ORDER { get; set; }
    }

    // aha! Async task!
    public class CABProperties {
        public IQueryable<CABDetail> cabdetails { get; set; }
        public IQueryable<VBNSpecifiedProperties> vbnproperties { get; set; }
        public IQueryable<GRASPropType> grasPropTypes { get; set; }
    }
}