﻿using CAB.Data.EF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//naam
//type
//telefoon
//    fax
//email
//btwnummer
//mailbox kwb
//bvh nummer
//landgard nr
//ean code
//startdatum
//einddatum
//orgacode
//orad_status
//orad_membertype
//status

namespace LogiCAB.Web.Domain {
    public  class OrganisationOld {
        public int Sequence { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string VATNumber { get; set; }
        public string EmailKWB { get; set; }
        public string VBANumber { get; set; }
        public string BVHNumber { get; set; }
        public string EANCode { get; set; }
        public string OrgaCode { get; set; }
        public int? Picture { get; set; }
                
        public F_CAB_ORGANISATION_ADMIN Orad { get; set; }
        public string OradStatus {
            get {
                switch (Orad.ORAD_STATUS) {
                    case ORAD_Status.ACTIEF:
                    return "Actief";
                    case ORAD_Status.AFGEMELD:
                    return "Afgemeld";
                    case ORAD_Status.PROSPECT:
                    return "Prospect";
                    default:
                    return "Onbekend";
                }
            }
        }
        public string OradMember {
            get {
                switch (Orad.ORAD_MEMBERTYPE) {
                    case ORAD_MemberType.BRONZE:
                    return "Bronze";
                    case ORAD_MemberType.ZILVER:
                    return "Zilver";
                    case ORAD_MemberType.GOUD:
                    return "Goud";
                    default:
                    return "Geen";
                }
            }
        }
        public F_CAB_ADDRESS Address { get; set; }
    }
}