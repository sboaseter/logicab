﻿using CAB.Data.EF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain
{
    public class PersonLogin : F_CAB_PERSON
    {
        public int LOGI_SEQUENCE { get; set; }
        public string LOGI_USER_NAME { get; set; }
        public string LOGI_PASSWORD { get; set; }
        public int LOGI_FK_PERSON { get; set; }
        public int LOGI_FK_LOGIN_GROUP { get; set; }
        public short LOGI_BLOCKED_YN { get; set; }
        public string LOGI_PASS { get; set; }
        public Nullable<int> LOGI_FK_CDLA_SEQ { get; set; }
    }
}