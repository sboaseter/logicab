using CAB.Data.EF.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LogiCAB.Web.Domain {
    public class F_CAB_CD_VBN_CODE_NUM_CAB : F_CAB_CD_VBN_CODE {
        public int numCabCodes { get; set; }
    }
}
