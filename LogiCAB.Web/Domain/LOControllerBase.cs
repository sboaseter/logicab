﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace LogiCAB.Web.Domain {
    [HandleError]
    public class LCControllerBase : Controller {

        public UserInformation CurrentUser;
        public static UserInformation CurrentUserStatic;

        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        protected override void Initialize(System.Web.Routing.RequestContext requestContext) {
            base.Initialize(requestContext);

            if (requestContext.HttpContext.User.Identity.IsAuthenticated) {
                string userName = requestContext.HttpContext.User.Identity.Name;
                CurrentUser = (UserInformation)Session["userinfo"];
                CurrentUserStatic = CurrentUser;
                ViewData["CurrentUser"] = CurrentUser;
            } else
                ViewData["CurrentUser"] = null;
        }
    }
}