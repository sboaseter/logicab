using System;
using System.Collections.Generic;
using System.Linq;

namespace LogiCAB.Web.Domain {
    public class RequestCask {
        public int seq { get; set; }
        public string caskcode { get; set; }
        public int CDCA_SEQUENCE { get; set; }
        public string caskdesc { get; set; }
        public decimal cask_prunit { get; set; }
        public decimal cask_prlayer { get; set; }
        public decimal cask_prtrolley { get; set; }

        public string desc {
            get {
                return String.Format("{0} - {1}", caskcode, caskdesc);
            }
        }
    }
}
