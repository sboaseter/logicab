﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class DeliveryMomentHeader {
        public int Sequence { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public int Grower { get; set; }
    }
}