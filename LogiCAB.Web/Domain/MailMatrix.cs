﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class MailMatrix {
        public int Sequence { get; set; }
        public string Group { get; set; }
        public string GroupType {get;set;}
        public string PersFirstName {get;set;}
        public string PersLastName {get;set;}
        public string PersEmail {get;set;}
        public string CABGroup {
            get {
                return String.Format("{0} - {1}", Group, GroupType);
            }
        }
        public string ContactPerson {
            get {
                return String.Format("{0} {1}", PersFirstName, PersLastName);
            }
        }
    }
}