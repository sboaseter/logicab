﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CAB.Data.EF.Models;
using System.IO;
using System.Globalization;
using System.Xml.Serialization;
using System.Web.Mvc;
using CAB.Domain.Repositories;
using CAB.Domain.Helpers;

namespace LogiCAB.Web.Domain {
    public static class Extensions {
        public static string Day(this int value) {
            switch (value) {
                case 1: return "Zondag";
                case 2: return "Maandag";
                case 3: return "Dinsdag";
                case 4: return "Woensdag";
                case 5: return "Donderdag";
                case 6: return "Vrijdag";
                case 0: return "Zaterdag";
                default: return "";
            }
        }
        public static string DeliveryDesc(this string value) {
            if (value == "EXW") return "Thuis gehaald";
            if (value == "DDP") return "Afl. locatie klant";
            return "Geen";
        }

        public static string UppercaseFirst(this string s) {
            // Check for empty string.
            if (string.IsNullOrEmpty(s)) {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }

        public static string AddressStr(this F_CAB_ADDRESS value) {
            return String.Format("{0}, {1}, {2} {3}",value.ADDR_ZIPCODE, value.ADDR_CITY, value.ADDR_STREET, value.ADDR_NO);
        }
        public static string SerializeToXml<T>(T value) {
            StringWriter writer = new StringWriter(CultureInfo.InvariantCulture);
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            serializer.Serialize(writer, value);
            return writer.ToString();
        }
        public static string dict(int lang, string lookup) {
            var tr = DependencyResolver.Current.GetService(typeof(ITranslationRepository)) as TranslationRepository;
            return tr.Get(lang, lookup);
        }

        public static bool hasExcelOverview() {
            //Server.MapPath("~/Content/ExcelOverview/" + Usr.GrowerSequence);

            //redo, put overviews in LogiCAB
            return true;
/*            var folderExists = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ExcelOverview/" + Usr.GrowerSequence);
            var di = new DirectoryInfo(folderExists);
            if(!di.Exists) {
                return false;

            }
            return true;*/
        }
    }
}