using CAB.Data.EF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using LogiCAB.Web.Domain;

namespace LogiCAB.Web.Domain {
    public class Handshake {
        public int seq { get; set; }
        public bool active { get; set; }
        public DateTime? request_date { get; set; }
        public DateTime? accept_date { get; set; }
        public bool Accepted {
            get {
                if (this.accept_date != null)
                    return true;
                return false;
            }
        }
        public string buyerOrga { get; set; }
        private F_CAB_ORGANISATION __buyerOrga { get; set; }
        public F_CAB_ORGANISATION _buyerOrga {
            get {
                return __buyerOrga;
            }
            set {
                __buyerOrga = value;
                buyerOrga = String.Format("{0} ({1}, {2})", __buyerOrga.ORGA_NAME, __buyerOrga.ORGA_EMAIL, __buyerOrga.ORGA_PHONE_NO);
            }
        }
        public string growerOrga { get; set; }
        private F_CAB_ORGANISATION __growerOrga { get; set; }
        public F_CAB_ORGANISATION _growerOrga {
            get {
                return __growerOrga;
            }
            set {
                __growerOrga = value;
                growerOrga = String.Format("{0} ({1}, {2})", __growerOrga.ORGA_NAME, __growerOrga.ORGA_EMAIL, __growerOrga.ORGA_PHONE_NO);
            }
        }

        /*public bool autoEkt { get; set; }
        public bool? autoKwb { get; set; }*/
        public F_CAB_HANDSHAKE_OPTIONS hndo { get; set; }

        public Transporter transporter() {
            if (this.hndo.HNDO_FK_ADDR_SEQ != null) { 
                return new Transporter {
                    Seq = 1,
                    Name = this.hndo.F_CAB_ADDRESS.F_CAB_ORGANISATION.ORGA_NAME
                };
            }
            return new Transporter();
        }
        
    }
}
