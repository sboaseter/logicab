﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class DeliveryMomentDetail {
        public int Sequence { get; set; }
        public int Header { get; set; }
        public int Day { get; set; }
        public TimeSpan MaxOrder { get; set; }
        public TimeSpan MaxDelivery { get; set; }
        public string Type { get; set; }
        
        
        //DMDT_SEQUENCE, DMDT_FK_DMHD_SEQ, DMDT_FK_OFDT_SEQ, DMDT_WEEKDAY_NUMBER, DMDT_ORDER_NUMBER, DMDT_WEEK_NUMBER, DMDT_MAX_ORDER_TIME, DMDT_MAX_DELIVERY_TIME, DMDT_DELIVERY_TYPE, DMDT_FK_ADDR_SEQ_DELIVERY, DMDT_TMP_VALID_TO, DMDT_TMP_WEEKDAY_NUMBER, DMDT_TMP_ORDER_NUMBER, DMDT_TMP_WEEK_NUMBER, DMDT_TMP_MAX_ORDER_TIME, DMDT_TMP_MAX_DELIVERY_TIME, DMDT_TMP_DELIVERY_TYPE, DMDT_TMP_FK_ADDR_SEQ_DELIVERY, DMDT_CREATED_ON, DMDT_CREATED_BY, DMDT_MODIFIED_ON, DMDT_MODIFIED_BY
    }
}