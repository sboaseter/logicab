﻿using System.Collections.Generic;
using CAB.Data.EF.Models;
using System;
using System.Linq;
using CAB.Domain.Models;

namespace LogiCAB.Web.Domain {
    public class CABCode {
        public int CABC_SEQUENCE { get; set; }
        public string CABC_CAB_CODE { get; set; }
        public string CABC_CAB_DESC { get; set; }
        public int? CABC_IS_AUTHORISED { get; set; }

        public int GroupSequence { get; set; }
        public string GroupDescription { get; set; }
        public string GroupTypeDescription { get; set; }
        public int MaingroupSequence { get; set; }
        public string MaingroupDescription { get; set; }
        public decimal MaingroupDaysOffer { get; set; }

        public string CABC_PICTURE_URL { get; set; }


        public string PROP_DESC1 { get; set; }
        public string PROP_VAL1 { get; set; }

        public string PROP_DESC2 { get; set; }
        public string PROP_VAL2 { get; set; }

        public string PROP_DESC3 { get; set; }
        public string PROP_VAL3 { get; set; }

        public string PROP_DESC4 { get; set; }
        public string PROP_VAL4 { get; set; }

        public string PROP_DESC5 { get; set; }
        public string PROP_VAL5 { get; set; }

        public string PROP_DESC6 { get; set; }
        public string PROP_VAL6 { get; set; }

        public string Details
        {
            get
            {
                return String.Format("{0}, {1}, {2}, {3}, {4}, {5}",
                    PROP_VAL1,
                    PROP_VAL2,
                    PROP_VAL3,
                    PROP_VAL4,
                    PROP_VAL5,
                    PROP_VAL6
                );
            }
        }
        public List<F_CAB_CAB_DETAIL> details { get; set; }

        private int? _CABC_FK_PICTURE;
        public int? CABC_FK_PICTURE {
            get { return _CABC_FK_PICTURE; }
            set {
                _CABC_FK_PICTURE = value;
                CABC_PICTURE_URL = String.Format("~/Image/ShowImage/{0}", this.CABC_FK_PICTURE);
            }
        }
    }

}