﻿using CAB.Domain.Helpers;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace LogiCAB.Web.Domain {
    public class AuthorizeWithSessionAttribute : AuthorizeAttribute {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        protected override bool AuthorizeCore(HttpContextBase httpContext) {
            var authorized = base.AuthorizeCore(httpContext);
            if (!authorized) {
                // the user is not authenticated or the forms authentication
                // cookie has expired
                
                if (Usr.LoginSequence != 0) {
                    Log.InfoFormat("Forms Authentication expired, Session still intact: Refreshing AuthCookie for: {0}", Usr.LoginName);
                    FormsAuthentication.SetAuthCookie(Usr.LoginName, true);
                    return true;
                }

                return false;
            }
            // Now check the session:
            if (Usr.LoginSequence == 0) {
                return false;
            }
            return true;
        }

    }
}