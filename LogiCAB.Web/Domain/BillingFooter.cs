﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.Domain {
    public class BillingFooter {
        public Double TotalSummary { get; set; }
        public double TotalCommSummary { get; set; }
        public int[] Subscriptions { get; set; }
        public string DateSelection { get; set; }
    }
}