﻿using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using CAB.Data.EF.Models;
using LogiCAB.Web.DAL;
using CAB.Domain.Helpers;

namespace LogiCAB.Web.Domain {
    public static class ImageUtils {
        static Entities db = new Entities();

        public static byte[] CreateThumbnail(byte[] PassedImage, int LargestSide) {
            byte[] ReturnedThumbnail;
            using (MemoryStream StartMemoryStream = new MemoryStream(), NewMemoryStream = new MemoryStream()) {
                StartMemoryStream.Write(PassedImage, 0, PassedImage.Length);
                Bitmap startBitmap = new Bitmap(StartMemoryStream);
                int newHeight;
                int newWidth;
                double HW_ratio;
                if (startBitmap.Height > startBitmap.Width) {
                    newHeight = LargestSide;
                    HW_ratio = (double)((double)LargestSide / (double)startBitmap.Height);
                    newWidth = (int)(HW_ratio * (double)startBitmap.Width);
                } else {
                    newWidth = LargestSide;
                    HW_ratio = (double)((double)LargestSide / (double)startBitmap.Width);
                    newHeight = (int)(HW_ratio * (double)startBitmap.Height);
                }
                Bitmap newBitmap = new Bitmap(newWidth, newHeight);
                newBitmap = ResizeImage(startBitmap, newWidth, newHeight);
                newBitmap.Save(NewMemoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                ReturnedThumbnail = NewMemoryStream.ToArray();
            }
            return ReturnedThumbnail;
        }

        // Move to Domain/Utils

        private static Bitmap ResizeImage(Bitmap image, int width, int height) {
            Bitmap resizedImage = new Bitmap(width, height);
            using (Graphics gfx = Graphics.FromImage(resizedImage)) {
                gfx.DrawImage(image, new Rectangle(0, 0, width, height),
                    new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
            }
            return resizedImage;
        }

        // Move to Domain/Utils (Duplicate)
        public static byte[] FileToByteArray(string _FileName) {
            byte[] _Buffer = null;
            try {
                FileStream _FileStream = new FileStream(_FileName, FileMode.Open, FileAccess.Read);
                BinaryReader _BinaryReader = new BinaryReader(_FileStream);
                long _TotalBytes = new FileInfo(_FileName).Length;
                _Buffer = _BinaryReader.ReadBytes((Int32)_TotalBytes);
                _FileStream.Close();
                _FileStream.Dispose();
                _BinaryReader.Close();
            } catch (Exception _Exception) {
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
            }
            return _Buffer;
        }

        public static byte[] NoPic(string fsroot) {
            return FileToByteArray(String.Format("{0}{1}", fsroot, "Content\\nopic.jpg"));
        }

        public static string getVKCImage(string id) {
            int vbncode = Int32.Parse(id);
            var vkcCode = EFHelper.ctx.F_CAB_VBN_VKC.FirstOrDefault(x => x.VBVK_VBN_CODE == vbncode);
            if (vkcCode != null) {
                return vkcCode.VBVK_VKC_CODE;
            } else {
                try {
                    String valString = String.Format("Missing VKC-Photo for VBNCode {0}", id);
                    DateTime cOn = DateTime.Now;
                    EFHelper.ctx.F_CAB_LOG.AddObject(new F_CAB_LOG {
                        CALO_FK_LOGIN = Usr.LoginSequence,
                        CALO_ACTION = "MissingVKC",
                        CALO_VALUE = valString,
                        CALO_CREATED_ON = cOn
                    });
                } catch{ }
                return "";
            }
            //return db.F_CAB_VBN_VKC.FirstOrDefault(x => x.VBVK_VBN_CODE == vbncode).VBVK_VKC_CODE;
        }
        public static Image resizeImage(Image imgToResize, Size size) {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (Image)b;
        }
    }
}