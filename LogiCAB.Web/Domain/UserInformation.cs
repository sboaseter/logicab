﻿using CAB.Data.EF.Models;
using LogiCAB.Web.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CAB.Domain.Enums;

namespace LogiCAB.Web.Domain {
    [Serializable]
    public class UserInformation {
        private int _intLoginSequence;
        private int _intPersonSequence;
        private int _intOrgaSequence;
        public UserInformation(F_CAB_LOGIN fcablogin) {
            login = fcablogin;
            _intLoginSequence = login.LOGI_SEQUENCE;
            _intPersonSequence = login.F_CAB_PERSON.PERS_SEQUENCE;
            _intOrgaSequence = login.F_CAB_PERSON.F_CAB_ORGANISATION.ORGA_SEQUENCE;
        }

        /*
         * Convinience methods
         */

        public string PersonName {
            get {
                return String.Format("{0} {1}", this.person.PERS_FIRST_NAME, this.person.PERS_LAST_NAME);
            }
        }
        public string OrgaName {
            get {
                return this.orga.ORGA_NAME;
            }        
        }
        public string LoginName {
            get {
                return this.login.LOGI_USER_NAME;
            }
        }
        public int LoginSequence {
            get {
                return this.login.LOGI_SEQUENCE;
            }
        }

        public F_CAB_LOGIN login { get; set; }

        private int _loginGroup;
        public int loginGroup {
            get {
                if (_loginGroup == 0) {
                    _loginGroup = login.LOGI_FK_LOGIN_GROUP;
                }
                return _loginGroup;
            }
        }
        private List<F_CAB_CD_MAIN_GROUP_TYPE> _orgaMaingroups = null;
        public List<F_CAB_CD_MAIN_GROUP_TYPE> orgaMaingroups {
            get {
                if (_orgaMaingroups == null) {
                    _orgaMaingroups = EFHelper.ctx.F_CAB_ORGA_MAIN_GROUP_MATRIX
                                        .Where(x => x.ORMA_FK_ORGA == _intOrgaSequence)
                                        .Select(y => y.F_CAB_CD_MAIN_GROUP_TYPE)
                                        .ToList();
                }
                return _orgaMaingroups;
            }
        }
        private int _selectedMaingroup;
        public int selectedMaingroup {
            get {
                if (_selectedMaingroup == 0) {
                    _selectedMaingroup = EFHelper.ctx.F_CAB_ORGA_MAIN_GROUP_MATRIX
                                            .Where(x => x.ORMA_FK_ORGA == _intOrgaSequence)
                                            .OrderBy(x => x.ORMA_ORDER)
                                            .FirstOrDefault()
                                            .ORMA_FK_MAIN_GROUP;
                }
                return _selectedMaingroup;
            }
            set {
                _selectedMaingroup = value;
            }
        }
        private F_CAB_PERSON _person;
        public F_CAB_PERSON person {
            get {
                if (_person == null) {
                    _person = this.login.F_CAB_PERSON;
                }
                return _person;
            }
        }
        private F_CAB_ORGANISATION _orga;
        public F_CAB_ORGANISATION orga {
            get {
                if (_orga == null) {
                    _orga = this.person.F_CAB_ORGANISATION;
                }
                return _orga;
            }
        }
        private F_CAB_ADDRESS _address;
        public F_CAB_ADDRESS address {
            get {
                if (_address == null) {
                    _address = EFHelper.ctx.F_CAB_ADDRESS
                                .FirstOrDefault(x => x.ADDR_FK_ORGANISATION == _intOrgaSequence);
                }
                return _address;
            }
        }
        private int _lang;
        public int lang {
            get {
                if (_lang == 0) {
                    _lang = this.login.LOGI_FK_CDLA_SEQ ?? 1;
                }
                return _lang;
            }
        }

        private int _orgaTypeSequence;
        public int orgaTypeSequence {
            get {
                if (_orgaTypeSequence == 0) {
                    if (orgaType == OrgaType.Grower) {
                        _orgaTypeSequence = EFHelper.ctx.F_CAB_GROWER.Single(x => x.GROW_FK_ORGA == _intOrgaSequence).GROW_SEQUENCE;
                    }
                    if (orgaType == OrgaType.Buyer) {
                        _orgaTypeSequence = EFHelper.ctx.F_CAB_BUYER.Single(x => x.BUYR_FK_ORGA == _intOrgaSequence).BUYR_SEQUENCE;
                    }
                    if (orgaType == OrgaType.Comm) {
                        _orgaTypeSequence = EFHelper.ctx.F_CAB_BUYER.Single(x => x.BUYR_FK_ORGA == _intOrgaSequence).BUYR_SEQUENCE;
                    }
                }
                return _orgaTypeSequence;
            }
        }
        private int _orgaTypeSequenceGrow;
        public int orgaTypeSequenceGrow {
            get {
                if (_orgaTypeSequenceGrow == 0) {
                    _orgaTypeSequenceGrow = EFHelper.ctx.F_CAB_GROWER.Single(x => x.GROW_FK_ORGA == _intOrgaSequence).GROW_SEQUENCE;
                }
                return _orgaTypeSequenceGrow;
            }
        }

        private bool _orgaTypeSet;
        private string _orgaType;
        public string orgaType {
            get {
                if (_orgaTypeSet) return _orgaType;
                switch (orga.ORGA_TYPE) {
                    case "GROW":
                        _orgaType = OrgaType.Grower;
                        break;
                    case "BUYR":
                        _orgaType = OrgaType.Buyer;
                        break;
                    case "COMM":
                        _orgaType = OrgaType.Comm;
                        break;
                    default:
                        break;                    
                }
                _orgaTypeSet = true;
                return _orgaType;
            }
        }
        public F_CAB_QUEUE_CODE_REQUEST currentRequest { get; set; }
        public bool RequestPictureSelected { get; set; }
        public string messages { get; set; }
    }
}