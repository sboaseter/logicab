﻿using CAB.Data.EF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.DAL {
    public static class EFHelper {
        public static Entities ctx {
            get {
                if (UOWStore.CurrentDataStore["EFHelper"] == null) {
                    UOWStore.CurrentDataStore["EFHelper"] = new Entities();
                }
                return (Entities)UOWStore.CurrentDataStore["EFHelper"];
            }
        }
    }
}