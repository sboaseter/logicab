﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LogiCAB.Web.DAL
{
    public interface IUnitOfWorkDataStore
    {
        object this[string key] { get; set; }
    }
    public static class UOWStore
    {
        public static IUnitOfWorkDataStore CurrentDataStore;
    }
}