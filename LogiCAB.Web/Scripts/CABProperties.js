﻿lastOption = null;

var CABProperties = function () {
    var self = this;
    this.name = ko.observable();

    self.CABPropGroups = ko.observableArray();
    self.selectedCABPropGroup = ko.observable();

    self.CABPropTypes = ko.observableArray();
    self.selectedCABPropType = ko.observable();

    self.CABPropLookups = ko.observableArray();
    self.selectedCABPropLookup1 = ko.observable();
    self.selectedCABPropLookup2 = ko.observable();

    self.resultText = ko.observable();

    self.newPropertyValue = ko.observable();

    self.syncButtonText = ko.observable('Run SyncProperties(Users will see blank properties for 3-4 minutes!)');
    self.addButtonText = ko.observable('Add property');

    self.addNewProperty = function () {
        var propType = self.selectedCABPropType().Sequence;
        var payload = {
            type: propType,
            description: self.selectedCABPropType().Description,
            value: self.newPropertyValue()
        };

        $.ajax({
            type: "GET",
            url: baseUrlAPI + 'CABAPI' + '/addProperty',
            data: payload,
            success: function (data) {
                console.log(data);
//                self.getCABPropGroups();
                self.selectedCABPropTypes();

            }
        });
    }


    self.setOptionColor = function (option, item) {
        if (item.Value.indexOf('  ') != -1) {
            $(option).css('background-color', 'red');
        }
    }

    self.getCABPropGroups = function () {
        $.ajax({
            type: "GET",
            url: baseUrlAPI + 'CABAPI' + '/CABPropGroups',
            success: function (data) {
                var currentCABPropGroup = self.selectedCABPropGroup();
                var currentCABPropType = self.selectedCABPropType();
                self.swapExecuted(false);
                self.CABPropGroups(data);
            }
        });
    }
    self.syncProperties = function () {
        $('#syncLoader').show();
        self.syncButtonText('Running Sync');
        $.ajax({
            type: 'GET',
            url: baseUrlAPI + 'CABAPI' + '/syncProperties',
            success: function (response) {
                $('#syncLoader').hide();
                self.syncButtonText('Syncronized!');
                setTimeout(function () {
                    self.syncButtonText('Sync properties');
                }, 10000);

            },
            error: function (response) {
                $('#syncLoader').hide();
                self.syncButtonText('An error occured!');
                setTimeout(function () {
                    self.syncButtonText('Sync properties');
                }, 3000);

            }

        });

    }

    /* Events */

    self.checkIfExist = function () {
//        console.log('checking!');
        var res = $.grep(self.CABPropLookups(), function (e, i) { return e.Value == self.newPropertyValue(); });
  //      console.log(res.length);
        if (res.length > 0) {
            self.addButtonText('Allready exists!');
            $('#addPropertyButton').css('color', 'red');
//            $('#addPropertyButton').css('color', 'red');
        } else {
            self.addButtonText('Add property');
            $('#addPropertyButton').css('color', 'blue');
        }
    }

    self.selectedCABPropGroups = function () {
//        console.log('selected a different CABPropGroup');
        console.log(self.selectedCABPropGroup());
        // Cascade further to CABPropTypes
        $.ajax({
            type: "GET",
            url: baseUrlAPI + 'CABAPI' + '/CABPropTypes',
            data: { id: self.selectedCABPropGroup().Sequence },
            success: function (data) {
                console.log(data);
                self.CABPropTypes(data);
            }
        });
    }
    self.selectedCABPropTypes = function () {
        //        console.log('selected a dfferent CABPropType');
        //self.swapExecuted = false;
        console.log(self.selectedCABPropType());
        $.ajax({
            type: "GET",
            url: baseUrlAPI + 'CABAPI' + '/CABPropLookups',
            data: { id: self.selectedCABPropType().Sequence },
            success: function (data) {
                console.log(data);
                self.CABPropLookups(data);
            }
        });
    }

    self.fromAndToSelected = ko.computed(function () {
//        console.log('computing ko');
        if (self.selectedCABPropLookup1() && self.selectedCABPropLookup2()) {
            return true;
        }
        return false;

    }, self);

    self.swapExecuted = ko.observable(false);

    self.executePropertySwap = function () {
        $.ajax({
            type: "GET",
            url: baseUrlAPI + 'CABAPI' + '/executePropertyChange',
            data: {
                idCorrect: self.selectedCABPropLookup2().Sequence,
                idNotCorrect: self.selectedCABPropLookup1().Sequence
            },
            dataType: "json",
            success: function (response) {
                self.swapExecuted(true);
                console.log(response);
                self.resultText(response);
                self.selectedCABPropTypes();                
            }
        });
    }
    //console.log('Performing constructor load!');
    self.getCABPropGroups();
};
