﻿var vbnseq = null;
var vbnseqCab = null;

function addNewCaskToRequest(s, e) {
    $.ajax({
        type: "POST",
        url: baseUrl + "/CABRequest/addNewCaskToRequest",
        data: {
            cdca_seq: cb_newCask.GetValue(),
            prcask: cask_prunit.GetValue(),
            prlayer: cask_prlayer.GetValue(),
            prtrolley: cask_prtrolley.GetValue()
        },
        success: function (msg) {
            gvSub2Editing.Refresh();
            gvSub2Editing.CancelEdit();
        }
    });
}

function saveVBNChoice() {
    var selVBN = cb_vbnCode.GetValue();
    $.ajax({
        type: "POST",
        url: baseUrl + "/CABRequest/saveVBNChoice",
        data: { vbn_seq: selVBN },
        success: function (msg) {
            $.ajax({
                type: "GET",
                url: baseUrl + "/CABRequest/getVBNPicture",
                success: function (data) {
                    $('#previewImage').attr('src', data);
                }
            });
        }
    });
}

function saveCABDesc() {
    var selDesc = txtCommDesc.GetValue();
    $.ajax({
        type: "POST",
        url: baseUrl + "/CABRequest/saveCABDesc",
        data: { cab_desc: selDesc }
    });
}

function saveCABCode(cab_seq) {
    $.ajax({
        type: "POST",
        url: baseUrl + "/CABRequest/saveCABCode",
        data: { cab_seq: cab_seq }
    });
}

function saveCABRequestType(type) {
    $.ajax({
        type: "POST",
        url: baseUrl + "/CABRequest/saveCABRequestType",
        data: { type: type },
        success: function (data) {
            $.ajax({
                type: "GET",
                url: baseUrl + "/CABRequest/getVBNPicture",
                success: function (data) {
                    $('#previewImage').attr('src', data);
                }
            });
        }
    });
}

function saveGroupType() {
    var selGt = groupType_cb.GetValue();
    $.ajax({
        type: "POST",
        url: baseUrl + "/CABRequest/saveGroupType",
        data: { cdgt_seq: selGt }
    });
}

function saveGrowerRemark() {
    var selRemark = txtGrowerRemark.GetValue();
    $.ajax({
        type: "POST",
        url: baseUrl + "/CABRequest/saveGrowerRemark",
        data: { remark: selRemark }
    });
}

function btnOkVBNClicked(s, e) {
    nbFeatures2.GetGroupByName('REQ1').SetExpanded(false);
    saveVBNChoice();
    var numCabCodes = cb_vbnCode.GetSelectedItem().GetColumnText("numCabCodes");
    if (numCabCodes > 0) {
        nbFeatures2.GetGroupByName('relatedCABGroup').SetVisible(true);
        nbFeatures2.GetGroupByName('relatedCABGroup').SetExpanded(true);
    } else {
        saveCABRequestType('New');
        nbFeatures2.GetGroupByName('relatedCABGroup').SetVisible(false);
        nbFeatures2.GetGroupByName('REQ3').SetVisible(true);
        nbFeatures2.GetGroupByName('REQ3').SetExpanded(true);
    }
}

function btnOkDescClick(s, e) {
    nbFeatures2.GetGroupByName('REQ3').SetExpanded(false);
    nbFeatures2.GetGroupByName('REQ4').SetVisible(true);
    nbFeatures2.GetGroupByName('REQ4').SetExpanded(true);
    saveCABDesc();
    saveGroupType();
}

function btnOkFotoClick(s, e) {
    nbFeatures2.GetGroupByName('REQ5').SetExpanded(false);
    nbFeatures2.GetGroupByName('REQ6').SetVisible(true);
    nbFeatures2.GetGroupByName('REQ6').SetExpanded(true);
    saveGrowerRemark();

}

function OnValueChangedVBN(s, e) {
    $('#SelectedCabCode').fadeOut();
    vbnseq = cb_vbnCode.GetValue();
    nbFeatures2.GetGroupByName('relatedCABGroup').SetVisible(false);
    nbFeatures2.GetGroupByName('REQ3').SetVisible(false);
    nbFeatures2.GetGroupByName('REQ4').SetVisible(false);
    nbFeatures2.GetGroupByName('REQ5').SetVisible(false);
    nbFeatures2.GetGroupByName('REQ6').SetVisible(false);
    if (!cbpExample.InCallback())
        cbpExample.PerformCallback();
}

function OnBeginCallback(s, e) {
    e.customArgs["vbnseq"] = vbnseq;
    vbnseqCab = vbnseq;
    vbnseq = null;
}

function OnEndCallback(s, e) {
    if (vbnseq != null)
        cbpExample.PerformCallback();
}

function showHideRelatedCAB(s, e) {
    $('#gvRelatedCab').fadeToggle();
}

function OnBeginCallbackRelated(s, e) {
    var seq = s.GetRowKey(s.GetFocusedRowIndex());
    saveCABCode(seq);
    $.ajax({
        type: "GET",
        url: baseUrl + "/CABRequest/getCABDesc",
        data: { seq: seq },
        success: function (data) {
            txtCommDesc.SetValue(data);
        }
    });
    e.customArgs['cabseq'] = seq;
    e.customArgs["vbnseq"] = vbnseqCab;
}

function gvRelatedCabFunc(s, e) {
    s.GetRowValues(s.GetFocusedRowIndex(), 'CABC_SEQUENCE;CABC_CAB_DESC', OnGetRowValues);
}

function OnGetRowValues(values) {
}

function cabRadioIndexChanged(s, e) {
    var selIndex = radioButtonList1.GetSelectedIndex();
    StoredProperties = [];
    if (selIndex == 0) { // New
        saveCABRequestType('New');
        gvReqProperties.Refresh();
        gvSub2Editing.Refresh();
        nbFeatures2.GetGroupByName('relatedCABGroup').SetExpanded(false);
        nbFeatures2.GetGroupByName('REQ3').SetVisible(true);
        nbFeatures2.GetGroupByName('REQ3').SetExpanded(true);
        btnOkOverview.SetVisible(true);
        btnOkOverview2.SetVisible(true);
        btnOkOverview3.SetVisible(false);
    }
    if (selIndex == 1) { // Take properties
        saveCABRequestType('TakeProperties');
        gvReqProperties.Refresh();
        gvSub2Editing.Refresh();
        nbFeatures2.GetGroupByName('relatedCABGroup').SetExpanded(false);
        nbFeatures2.GetGroupByName('REQ3').SetVisible(true);
        nbFeatures2.GetGroupByName('REQ3').SetExpanded(true);
        btnOkOverview.SetVisible(true);
        btnOkOverview2.SetVisible(true);
        btnOkOverview3.SetVisible(false);
    }
    if (selIndex == 2) { // Add to assortiment
        saveCABRequestType('Assortiment');
        nbFeatures2.GetGroupByName('relatedCABGroup').SetExpanded(false);
        nbFeatures2.GetGroupByName('REQ3').SetVisible(false);
        nbFeatures2.GetGroupByName('REQ4').SetVisible(false);
        nbFeatures2.GetGroupByName('REQ6').SetVisible(true);
        nbFeatures2.GetGroupByName('REQ6').SetExpanded(true);
        btnOkOverview.SetVisible(false);
        btnOkOverview2.SetVisible(false);
        btnOkOverview3.SetVisible(true);
    }
}
function btnPropsOK(s, e) {
    // make sure all required properties are filled before collapsing current tab and expanding next.
    $.ajax({
        type: "POST",
        url: baseUrl + "/CABRequest/validateCABRequestClient", // Using userinfo.currentrequest serverside.
        success: function (msg) {
            if (msg == "Valid") {
                nbFeatures2.GetGroupByName('REQ4').SetExpanded(false);
                nbFeatures2.GetGroupByName('REQ5').SetVisible(true);
                nbFeatures2.GetGroupByName('REQ5').SetExpanded(true);
                $('#PropertyWarning').html('');
            }
            if (msg == "MissingProperty") {
                nbFeatures2.GetGroupByName('REQ4').SetExpanded(false);
                nbFeatures2.GetGroupByName('REQ5').SetVisible(true);
                nbFeatures2.GetGroupByName('REQ5').SetExpanded(true);
                $('#PropertyWarning').html('');
            }
        }
    });

}
function ProcessRequest(whereTo) {
    var whereToAction = whereTo == 'new' ? 'ProcessAndNew' : 'ProcessAndClose';
    $.ajax({
        type: "POST",
        url: baseUrl + "/CABRequest/ProcessAjax",
        success: function (data) {
            if (data == "Ok") {
                window.location.href = baseUrl + "/CABRequest/" + whereToAction;
            } else {
                var messages = data.split(",");
                var toAdd = "<ul>";
                for (var i = 0; i < messages.length; i++) {
                    if (messages[i] != "")
                        toAdd += "<li>" + messages[i] + "</li>";
                }
                toAdd += "</ul>";
                $('#validationSection').html(toAdd);
            }

        }
    });
}
function SavePropertyToDetail(proptype, statusimg, combo) {
    var sendData = {
        propType: proptype,
        propValue: combo.GetText()
    };
    statusimg.SetImageUrl(baseUrl + '/Content/loader.gif');
    $.ajax({
        type: "POST",
//        url: baseUrl + "/CABRequest/saveRequestDetail", 
        url: '@Url.Action("saveRequestDetail", "CABRequest")',
        data: sendData,
        success: function (msg) {
            if (msg == "ok") {
                statusimg.SetImageUrl(baseUrl + '/Content/ok.png');
            } else {
                statusimg.SetImageUrl(baseUrl + '/Content/cancel.png');
            }
        }
    });
}
var StoredProperties = [];
function StoreProperty(imgobj, comboobj) {
    StoredProperties.push({ img: imgobj, combo: comboobj });
}