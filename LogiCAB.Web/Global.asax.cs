using CAB.Domain.Repositories;
using log4net;
using log4net.Config;
using LogiCAB.Web.DAL;
using LogiCAB.Web.Domain;
using SimpleInjector;
using StackExchange.Profiling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;
using System.Diagnostics;


namespace LogiCAB.Web {
    public class MvcApplication : HttpApplication {
        public static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public static Container Container { get; private set; }
        public static List<Heartbeat> activeUsers = new List<Heartbeat>();
        
        protected void Application_Start() {

            ViewEngines.Engines.Clear();
            IViewEngine razorEngine = new RazorViewEngine() { FileExtensions = new string[] { "cshtml" } };
            ViewEngines.Engines.Add(razorEngine);
            
            /* Log4Net Config */
            XmlConfigurator.Configure();
            ModelBinders.Binders.DefaultBinder = new DevExpress.Web.Mvc.DevExpressEditorsBinder();
            
            UOWStore.CurrentDataStore = new HttpContextDataStore();
            log4net.GlobalContext.Properties["CALO_ACTION"] = "LOGICAB";

            Log.Info("LogiCAB Starting.");
            Log.InfoFormat("MachineName: {0}", Environment.MachineName);
            Log.InfoFormat("AppPath    : {0}", HttpRuntime.AppDomainAppPath);
            Log.InfoFormat("Assembly   : {0}, {1}", Assembly.GetExecutingAssembly().FullName, Assembly.GetCallingAssembly().GetName().Version);
            Application["LiveSessionsCount"] = 0;


            // Move to delayed area, thread?
                Stopwatch sw1 = new Stopwatch();
                sw1.Start();
                var _cabcodeRepository = System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ICabcodeRepository)) as CabcodeRepository;
//                _cabcodeRepository.FillCache();
                sw1.Stop();
                Log.InfoFormat("\t _cabcodeRepository.FillCache(): {0}ms", sw1.ElapsedMilliseconds);

                sw1.Restart();
                var _organisationRepository = System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IOrganisationRepository)) as OrganisationRepository;
                _organisationRepository.FillCache();
                sw1.Stop();
                Log.InfoFormat("\t _organisationRepository.FillCache(): {0}ms", sw1.ElapsedMilliseconds);

                var _translationRepository = System.Web.Mvc.DependencyResolver.Current.GetService(typeof(ITranslationRepository)) as TranslationRepository;
                _translationRepository.Initialize();

            // TODO: initialize TranslationRepository with missing-translations cache to prevent check.
            

            
//            Timer t = new Timer(60000); // Every 1 minute
//            t.Elapsed += new ElapsedEventHandler(checkActiveUser);
//            t.Start();
//            Timer t2 = new Timer(300000); // Every 5 minutes
//            t2.Elapsed += new ElapsedEventHandler(executeTransporterProvider);
//            t2.Start();

            BundleTable.EnableOptimizations = true;
            AreaRegistration.RegisterAllAreas();
//            WebApiConfig.Register(GlobalConfiguration.Configuration);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


        }

        protected void Session_Start() {
            if(Application["LiveSessionsCount"] == null)
                Application["LiveSessionsCount"] = 0;
            Application["LiveSessionsCount"] = ((int)Application["LiveSessionsCount"]) + 1;
        }
        protected void Session_End() {
            if (Application["LiveSessionsCount"] == null) { 
                Application["LiveSessionsCount"] = 0;
                return;
            }
            Application["LiveSessionsCount"] = ((int)Application["LiveSessionsCount"]) - 1;
        }

        private static void checkActiveUser(object o, ElapsedEventArgs e) {
            try {
                for (int i = activeUsers.Count() - 1; i >= 0; i--) {
                    if (activeUsers[i].time < DateTime.Now.AddSeconds(-30)) {
                        activeUsers.RemoveAt(i);
                    }
                }
            } catch (Exception ex) {
                Log.ErrorFormat("Something went wrong while checking ActiveUsers: {0}", ex.ToString());
            }
        }

        protected void Application_BeginRequest() {
            if (Request.Url.AbsoluteUri.Contains("dev") || Request.Url.AbsoluteUri.Contains("test")) {
                MiniProfiler.Start();
            }
            if (Request.IsLocal) {
                MiniProfiler.Start();
            }
        }
        protected void Application_EndRequest() {
            MiniProfiler.Stop();
        }
        private class HttpContextDataStore : IUnitOfWorkDataStore {
            public object this[string key] {
                get { return HttpContext.Current.Items[key]; }
                set { HttpContext.Current.Items[key] = value; }
            }
        }

//        private static void executeTransporterProvider(object o, ElapsedEventArgs e) {
//            Log.InfoFormat("Initializing TransportList-Generator.");
//            var transportProvider = System.Web.Mvc.DependencyResolver.Current.GetService(typeof(TransportProvider)) as TransportProvider;
//            if(!transportProvider.InProcess())
//                transportProvider.Generate();
//        }
//#if RELEASE
/*        protected void Application_Error() {
            Exception ex = Server.GetLastError();

            bool handleError = false;
            string urlRequested = "";
            urlRequested = Request.Url.AbsoluteUri;
            int errorCode = 500;
            Log.ErrorFormat("Hit an error while requesting: {0}", urlRequested);
            if (handleError = (ex is InvalidOperationException && ex.Message.Contains("or its master was not found or no view engine supports the searched locations. The following locations were searched"))) {
                errorCode = 404;
            } else if (handleError = ex is HttpException) {
                errorCode = ((HttpException)ex).GetHttpCode();
            }

            if (handleError) {
                Server.ClearError();

                //Server.TransferRequest("/error/error/" + errorCode);
                Response.Redirect("~/error/error/" + errorCode);
            }
        }*/
//#endif
    }
}