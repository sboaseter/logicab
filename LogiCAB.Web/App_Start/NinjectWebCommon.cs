[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(LogiCAB.Web.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(LogiCAB.Web.App_Start.NinjectWebCommon), "Stop")]

namespace LogiCAB.Web.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using log4net;
    using System.Reflection;
    using CAB.Domain.Providers;
    using CAB.Domain.Repositories;
    using LogiCAB.Web.Domain;
    using WebApiContrib.IoC.Ninject;
    using System.Web.Http;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
            
            RegisterServices(kernel);
            GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ILog>().ToMethod(x => LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType)).InTransientScope();
            kernel.Bind<IDataProvider>().To<DataProvider>();
            kernel.Bind<ILoginRepository>().To<LoginRepository>();
            kernel.Bind<IShopEvidenceProvider>().To<ShopEvidenceProvider>();
            kernel.Bind<IPictureRepository>().To<PictureRepository>();
            kernel.Bind<ITranslationRepository>().To<TranslationRepository>();
            kernel.Bind<IFunctionsProvider>().To<FunctionsProvider>();
            kernel.Bind<IPersonRepository>().To<PersonRepository>();
            kernel.Bind<IOrganisationRepository>().To<OrganisationRepository>();
            kernel.Bind<IDeliveryDatesProvider>().To<DeliveryDatesProvider>();
            kernel.Bind<ISettingsProvider>().To<SettingsProvider>();
            kernel.Bind<ISpecialsProvider>().To<SpecialsProvider>();
            kernel.Bind<IOfferProvider>().To<OfferProvider>();
            kernel.Bind<IGeneralRelationsRepository>().To<GeneralRelationsRepository>();
            kernel.Bind<ICabcodeRepository>().To<CabcodeRepository>();
            kernel.Bind<IOrderProvider>().To<OrderProvider>();
            kernel.Bind<TransportProvider>().To<TransportProvider>();
            kernel.Bind<IEmailProvider>().To<EmailProvider>();
        }        
    }
}
