﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace LogiCAB.Web {
    public class WebApiConfig {
        public static void Register(HttpConfiguration configuration) {
            configuration.EnableCors();
            configuration.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            configuration.Routes.MapHttpRoute(
                            name: "ApiWithAction",
                            routeTemplate: "api/{controller}/{action}/{id}",
                            defaults: new { id = RouteParameter.Optional }
                        );
        }
    }
}