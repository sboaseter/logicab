﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogiCAB.Service.Models {
    [Serializable]
    public class ServiceOffer {
        public string Id { get; set; }
        public String ArticleDescription { get; set; }
        public String ArticleImage { get; set; }
        public String SupplierName { get; set; }
        public String SupplierEAN { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public decimal PrBunch { get; set; }
        public DeliveryMoments DeliveryMoments { get; set; }
    }
    [Serializable]
    public class Supplier {
        public String SupplierName { get; set; }
        public String SupplierEAN { get; set; }
    }
    [Serializable]
    public class DeliveryMoments {
        public List<DateTime> MaxOrderTime { get; set; }
        public List<DateTime> MaxDeliverTime { get; set; }
    }

}
