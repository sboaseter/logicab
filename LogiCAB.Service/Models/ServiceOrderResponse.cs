﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiCAB.Service.Models {
    public class ServiceOrderResponse {
        public DateTime LineDateTime { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public string Status { get; set; }
    }
}
