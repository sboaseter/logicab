﻿using LogiCAB.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LogiCAB.Service.LogiService2012;

namespace LogiCAB.Service.Domain {
    public class Service2012 {
        public ServiceOrderResponse PutOrder(string username, string password, string endpoint, string id, string delivery, string numItems) {            
            var newStr = String.Format("{0}", delivery.Substring(delivery.IndexOf("Order:") + 7, delivery.IndexOf(", Delivery: ") - 7));
            var orderDate = DateTime.Parse(newStr);
            var deliveryDateStr = String.Format("{0}", delivery.Substring(delivery.IndexOf("Delivery:") + 10));
            var deliveryDate = DateTime.Parse(deliveryDateStr);
            var num = Int32.Parse(numItems);
            var request = new PutOrderRequest();
            request.PutOrderRequest1 = new OrderRequestMessage {
                Header = new OrderRequestMessageHeader {
                    UserName = new TextType {
                        Value = username
                    },
                    Password = new TextType {
                        Value = password
                    },
                    MessageID = new IDType {
                        Value = "1"
                    },
                    MessageDateTime = DateTime.Now
                },
                Body = new OrderRequestMessageBody {
                    Order = new OrderType {
                        OrderTradeLineItem = new OrderTradeLineItemType[] {
                            new OrderTradeLineItemType {
                                ID = new IDType {
                                    schemeName = "ON",
                                    schemeDataURI = "1",
                                    schemeURI = "1",
                                    Value = id
                                },
                                DocumentType = new DocumentCodeType1 {                                    
                                    Value = DocumentNameCodeContentType1.Item210
                                },
                                //LineDateTime = DateTime.Now,
                                LineDateTime = orderDate,
                                TradingTerms = new TradingTermsType {
                                    Condition = new ConditionType[] {
                                        new ConditionType {
                                            TypeCode = new TradeConditionCodeType[] {
                                                new TradeConditionCodeType {
                                                    Value = TradeConditionCodeContentType.Item301
                                                }
                                            },
                                            ValueMeasure = new MeasureType1[] {
                                                new MeasureType1 {
                                                    Value = 301
                                                }
                                            }
                                        }
                                    }
                                },
                                ReferencedDocument = new ReferencedDocumentType[] {
                                    new ReferencedDocumentType {
                                        IssuerAssignedID = new IDType {
                                            schemeName = "AAG",
                                            schemeDataURI = "1",
                                            schemeURI= "1",
                                            Value = id

                                        }
                                    }
                                },
                                EndUserParty = new EndUserPartyType {
                                    PrimaryID = new IDType {
                                        Value = "EndUserParty here"
                                    }
                                },
                                Quantity = new QuantityType {
                                    unitCode = FEC_MeasurementUnitCommonCodeContentType.Item3,
                                    Value = num
                                },
                                Price = new PriceType[] {
                                    new PriceType {
                                        TypeCode = new PriceTypeCodeType {
                                            Value = PriceTypeCodeContentType.AE
                                        },
                                        ChargeAmount = new AmountType {
                                            currencyCode = ISO3AlphaCurrencyCodeContentType.EUR,
                                            Value = 0.33M
                                        },
                                        BasisQuantity = new QuantityType {
                                            unitCode = FEC_MeasurementUnitCommonCodeContentType.Item1,
                                            Value = 1
                                        },
                                        NetPriceIndicator = false
                                    }
                                },
                                Delivery = new DeliveryType {
                                    LatestDeliveryDateTimeSpecified = true,
                                    LatestDeliveryDateTime = deliveryDate,
                                    DeliveryTerms = new TradeDeliveryTermsType {
                                        DeliveryTypeCode = new DeliveryTermsCodeType {
                                            Value = DeliveryTermsCodeContentType.EXW
                                        }
                                    }
                                }
                            }
                        }
                    },
                }
            };


            using (var serv = new CommercialCustomerClient("CommercialCustomer1", endpoint)) {                
                try {
                    var mess = serv.PutOrder(request.PutOrderRequest1);
                    if (mess.Body == null) {
                        throw new Exception();
                    }
                    if (mess.Body.OrderResponse != null) {
                        if (mess.Body.OrderResponse.OrderResponseTradeLineItem != null) {
                            if (mess.Body.OrderResponse.OrderResponseTradeLineItem[0] != null) {
                                var or = mess.Body.OrderResponse.OrderResponseTradeLineItem[0];
                                ServiceOrderResponse item = new ServiceOrderResponse();
                                item.LineDateTime = or.LineDateTime;
                                item.Quantity = or.Quantity.Value;
                                item.Price = or.Price[0].ChargeAmount.Value;
                                item.Status = or.Status[0].DescriptionText[0].Value;
                                return item;
                            }
                        }
                    }
                } catch (Exception ex) {
                    return new ServiceOrderResponse();
                }
            }
            return new ServiceOrderResponse();
        }
        public List<ServiceOffer> GetSupply(string username, string password, string endpoint) {
            List<ServiceOffer> offerlist = new List<ServiceOffer>();
            DateTime lastSyncDateTime;
            int msgId = 1;
            var request = new SupplyRequestMessage {
                Header = new SupplyRequestMessageHeader {
                    MessageID = new IDType { Value = msgId.ToString() },
                    MessageDateTime = System.DateTime.Now,
                    UserName = new TextType { Value = username },//"vmptest" }, 
                    Password = new TextType { Value = password },//"tst!@#$" },
                    MessageSerial = 1
                },
                Body = new SupplyRequestMessageBody() {
                    SupplyRequestDetails = new SupplyRequestType {
                        SupplyRequestLine = new[] {
							new SupplyRequestLineType {
								//MutationDateTime = DateTime.Now.AddDays(-7),
								MutationDateTimeSpecified = false
							 }
						 }
                    }
                }
            };

            using (var serv = new CommercialCustomerClient("CommercialCustomer1", endpoint)) {
                try {
                    var mess = serv.GetSupply(request);

                    // Sanity check.
                    if (mess.Body == null) {
                        throw new Exception();
                    }
                    // Proces de Supply Lines.
                    if (mess.Body.SupplyResponseDetails != null) {
                        if (mess.Body.SupplyResponseDetails.SupplyTradeLineItem != null) {
                            if (mess.Body.SupplyResponseDetails.SupplyTradeLineItem.Length != 0) {
                                foreach (var sln in mess.Body.SupplyResponseDetails.SupplyTradeLineItem) {
                                    ServiceOffer item = new ServiceOffer();
                                    item.ArticleDescription = sln.Product.DescriptionText.Value;
                                    item.Id = sln.ID.Value;
                                    if (sln.Price != null) {
                                        if (sln.Price[0] != null) {
                                            item.Price = sln.Price[0].ChargeAmount.Value;
                                        }
                                    }
                                    item.Quantity = sln.Quantity.Value;
                                    try {
                                        item.PrBunch = sln.Packing[0].InnerPacking[0].InnerPacking[0].InnerPacking[0].InnerPackageQuantity.Value;
                                    }catch(Exception ex) {
                                    }

                                    if (sln.SellerParty.PrimaryID.Value != null) { //Bij CAB is leverancier ook kweker.
                                        item.SupplierEAN = sln.SellerParty.PrimaryID.Value;
                                    }
                                    if (sln.Delivery != null) {
                                        item.DeliveryMoments = new DeliveryMoments();
                                        item.DeliveryMoments.MaxOrderTime = new List<DateTime>();
                                        item.DeliveryMoments.MaxDeliverTime = new List<DateTime>();
                                        foreach (var dm in sln.Delivery) {
                                            //                                            item.DeliveryMoments.MaxOrderTime = new List<DateTime>();
                                            item.DeliveryMoments.MaxOrderTime.Add(dm.LatestOrderDateTime);
                                            item.DeliveryMoments.MaxDeliverTime.Add(dm.LatestDeliveryDateTime);
                                        }
                                    }
                                    if (sln.ReferencedDocument != null)
                                        if (sln.ReferencedDocument[0].IssuerAssignedID != null)
                                            if (sln.ReferencedDocument[0].IssuerAssignedID.schemeName != null)
                                                if (sln.ReferencedDocument[0].IssuerAssignedID.schemeName == "IRN")
                                                    if (sln.ReferencedDocument[0].URIID != null)
                                                        if (sln.ReferencedDocument[0].URIID.Value != null)
                                                            item.ArticleImage = sln.ReferencedDocument[0].URIID.Value;
                                    offerlist.Add(item);
                                }

                            }
                        }
                    }
                } catch (Exception ex) {

                }

            }
            return offerlist;
        }
    }
}
